/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/math/point2.h
 *****************************************************************************/

#pragma once

#include "vec2f.h"
#include "vec3f.h"

namespace bme
{

template < typename T >
class point2
{
public:
	
	typedef point2<T> point2T;
	typedef T valuesT;
	
	T x;
	T y;
	
public:
	
	static const point2T zero;  /// (0; 0)
	static const point2T one;   /// (1; 1)
	static const point2T one_x; /// (1; 0)
	static const point2T one_y; /// (0; 1)
	
	point2();
	explicit point2(T n);
	point2(T x, T y);
	point2(const point2T& other);
	
	explicit point2(const vec2f& v);
	explicit point2(const vec3f& v);
	
	point2T& operator += ( T v );
	point2T& operator -= ( T v );
	point2T& operator *= ( T v );
	point2T& operator /= ( T v );
	
	point2T& operator += ( const point2T& other );
	point2T& operator -= ( const point2T& other );
	point2T& operator *= ( const point2T& other );
	point2T& operator /= ( const point2T& other );
	
	point2T& operator  = ( T v );
	point2T& operator  = ( const point2T& other );
	bool     IsEquals    ( const point2T& other ) const;
};

// ----------------------------------------------------------------------------
// 
// Global operators
// 
// ----------------------------------------------------------------------------

// ------------------------------------
// point2T (== != <)
// ------------------------------------

template < typename T >
bool operator==(const point2<T>& a, const point2<T>& b)
{
	return a.IsEquals(b);
}

template < typename T >
bool operator!=(const point2<T>& a, const point2<T>& b)
{
	return !a.IsEquals(b);
}

template < typename T >
bool operator<(const point2<T>& a, const point2<T>& b)
{
	return
		(a.x < b.x) ||
		(Math::IsEquals(a.x, b.x) && a.y < b.y);
}

// ------------------------------------
// point2T (*/+-) point2T
// ------------------------------------

template < typename T >
point2<T> operator*(const point2<T>& a, const point2<T>& b)
{
	return point2<T>(a.x * b.x, a.y * b.y);
}

template < typename T >
point2<T> operator/(const point2<T>& a, const point2<T>& b)
{
	return point2<T>(a.x / b.x, a.y / b.y);
}

template < typename T >
point2<T> operator+(const point2<T>& a, const point2<T>& b)
{
	return point2<T>(a.x + b.x, a.y + b.y);
}

template < typename T >
point2<T> operator-(const point2<T>& a, const point2<T>& b)
{
	return point2<T>(a.x - b.x, a.y - b.y);
}

// ------------------------------------
// point2T (*/+-) scalar
// ------------------------------------

template < typename T >
point2<T> operator*(const point2<T>& p, T sc)
{
	return point2<T>(p.x * sc, p.y * sc);
}

template < typename T >
point2<T> operator/(const point2<T>& p, T sc)
{
	return point2<T>(p.x / sc, p.y / sc);
}

template < typename T >
point2<T> operator+(const point2<T>& p, T sc)
{
	return point2<T>(p.x + sc, p.y + sc);
}

template < typename T >
point2<T> operator-(const point2<T>& p, T sc)
{
	return point2<T>(p.x - sc, p.y - sc);
}

// ------------------------------------
// scalar (*/+-) point2T
// ------------------------------------

template < typename T >
point2<T> operator*(T sc, const point2<T>& p)
{
	return point2<T>(sc * p.x, sc * p.y);
}

template < typename T >
point2<T> operator/(T sc, const point2<T>& p)
{
	return point2<T>(sc / p.x, sc / p.y);
}

template < typename T >
point2<T> operator+(T sc, const point2<T>& p)
{
	return point2<T>(sc + p.x, sc + p.y);
}

template < typename T >
point2<T> operator-(T sc, const point2<T>& p)
{
	return point2<T>(sc - p.x, sc - p.y);
}

// ----------------------------------------------------------------------------
// 
// Impl
// 
// ----------------------------------------------------------------------------

template < typename T >
point2<T>::point2()
: x(0), y(0)
{
}

template < typename T >
point2<T>::point2(T n)
: x(n), y(n)
{
}

template < typename T >
point2<T>::point2(T x, T y)
: x(x), y(y)
{
}

template < typename T >
point2<T>::point2(const point2<T>& other)
: x(other.x), y(other.y)
{
}

template < typename T >
point2<T>::point2(const vec2f& v)
: x(Math::SmartCast<T>(v.x))
, y(Math::SmartCast<T>(v.y))
{
}

template < typename T >
point2<T>::point2(const vec3f& v)
: x(Math::SmartCast<T>(v.x))
, y(Math::SmartCast<T>(v.y))
{
}

template < typename T >
point2<T>& point2<T>::operator+=(T v)
{
	x += v;
	y += v;
	return *this;
}

template < typename T >
point2<T>& point2<T>::operator-=(T v)
{
	x -= v;
	y -= v;
	return *this;
}

template < typename T >
point2<T>& point2<T>::operator*=(T v)
{
	x *= v;
	y *= v;
	return *this;
}

template < typename T >
point2<T>& point2<T>::operator/=(T v)
{
	x /= v;
	y /= v;
	return *this;
}

template < typename T >
point2<T>& point2<T>::operator+=(const point2<T>& other)
{
	x += other.x;
	y += other.y;
	return *this;
}

template < typename T >
point2<T>& point2<T>::operator-=(const point2<T>& other)
{
	x -= other.x;
	y -= other.y;
	return *this;
}

template < typename T >
point2<T>& point2<T>::operator*=(const point2<T>& other)
{
	x *= other.x;
	y *= other.y;
	return *this;
}

template < typename T >
point2<T>& point2<T>::operator/=(const point2<T>& other)
{
	x /= other.x;
	y /= other.y;
	return *this;
}

template < typename T >
point2<T>& point2<T>::operator=(T v)
{
	x = v;
	y = v;
	return *this;
}

template < typename T >
point2<T>& point2<T>::operator=(const point2<T>& other)
{
	x = other.x;
	y = other.y;
	return *this;
}

template < typename T >
bool point2<T>::IsEquals(const point2<T>& other) const
{
	return
		Math::IsEquals(x, other.x) &&
		Math::IsEquals(y, other.y);
}

// ----------------------------------------------------------------------------
// 
// Static impl
// 
// ----------------------------------------------------------------------------

template<typename T> const point2<T> point2<T>::zero  = point2<T>(0,0);
template<typename T> const point2<T> point2<T>::one   = point2<T>(1,1);
template<typename T> const point2<T> point2<T>::one_x = point2<T>(1,0);
template<typename T> const point2<T> point2<T>::one_y = point2<T>(0,1);

} // namespace bme
