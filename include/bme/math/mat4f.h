/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/math/mat4f.h
 *****************************************************************************/

#pragma once

#include "vec2f.h"
#include "vec3f.h"

namespace bme
{

class mat4f
{
	union {
		f32 a[16];
		struct {
			f32 _11,_12,_13,_14;
			f32 _21,_22,_23,_24;
			f32 _31,_32,_33,_34;
			f32 _41,_42,_43,_44;
		} m;
	};
	
public:
	
	static const mat4f zero;
	static const mat4f identity;
	
	mat4f();
	mat4f(const mat4f& other);
	explicit mat4f(bool identity);
	
	f32&         operator ()     ( size_t row, size_t col );
	f32          operator ()     ( size_t row, size_t col ) const;
	f32&         operator []     ( size_t index );
	f32          operator []     ( size_t index ) const;
	
	f32*         getPointer      ();
	const f32*   getPointer      () const;
	
	mat4f        operator +      ( const mat4f& other ) const;
	mat4f        operator -      ( const mat4f& other ) const;
	mat4f        operator *      ( const mat4f& other ) const;
	
	mat4f&       operator +=     ( const mat4f& other );
	mat4f&       operator -=     ( const mat4f& other );
	mat4f&       operator *=     ( const mat4f& other );
	
	mat4f&       operator =      ( const mat4f& other );
	bool         operator ==     ( const mat4f& other ) const;
	bool         operator !=     ( const mat4f& other ) const;
	
	void         ApplyToVector   ( vec2f& v,       bool with_translate = true ) const;
	void         ApplyToVector   ( vec3f& v,       bool with_translate = true ) const;
	vec2f        ApplyToVector   ( const vec2f& v, bool with_translate = true ) const;
	vec3f        ApplyToVector   ( const vec3f& v, bool with_translate = true ) const;
	
	mat4f&       Zero            ();
	mat4f&       Identity        ();
	mat4f&       Scale           ( const vec2f& v );
	mat4f&       Scale           ( const vec3f& v );
	mat4f&       Translate       ( const vec2f& v );
	mat4f&       Translate       ( const vec3f& v );
	mat4f&       Rotation        ( f32 angle, const vec2f& v );
	mat4f&       Rotation        ( f32 angle, const vec3f& v );
	mat4f&       Orthogonal      ( const vec2f& size, f32 znear, f32 zfar );
	mat4f&       Perspective     ( f32 fov, f32 aspect, f32 znear, f32 zfar );
	mat4f&       Inverse         ( const mat4f& m, bool* success = NULL );
	
	static mat4f MakeZero        ();
	static mat4f MakeIdentity    ();
	static mat4f MakeScale       ( const vec2f& v );
	static mat4f MakeScale       ( const vec3f& v );
	static mat4f MakeTranslate   ( const vec2f& v );
	static mat4f MakeTranslate   ( const vec3f& v );
	static mat4f MakeRotation    ( f32 angle, const vec2f& v );
	static mat4f MakeRotation    ( f32 angle, const vec3f& v );
	static mat4f MakeOrthogonal  ( const vec2f& size, f32 znear, f32 zfar );
	static mat4f MakePerspective ( f32 fov, f32 aspect, f32 znear, f32 zfar );
	static mat4f MakeInverse     ( const mat4f& m, bool* success = NULL );
};

// ----------------------------------------------------------------------------
// 
// Impl
// 
// ----------------------------------------------------------------------------

inline mat4f::mat4f()
{
	Identity();
}

inline mat4f::mat4f(const mat4f& other)
{
	BME_memcpy(&m, &other.m, sizeof(m));
}

inline mat4f::mat4f(bool identity)
{
	if ( identity ) {
		Identity();
	}
}

inline f32& mat4f::operator()(size_t row, size_t col)
{
	BME_ASSERT(row < 4 && col < 4);
	return a[row * 4 + col];
}

inline f32 mat4f::operator()(size_t row, size_t col) const
{
	BME_ASSERT(row < 4 && col < 4);
	return a[row * 4 + col];
}

inline f32& mat4f::operator[](size_t index)
{
	BME_ASSERT(index < 16);
	return a[index];
}

inline f32 mat4f::operator[](size_t index) const
{
	BME_ASSERT(index < 16);
	return a[index];
}

inline f32* mat4f::getPointer()
{
	return a;
}

inline const f32* mat4f::getPointer() const
{
	return a;
}

inline mat4f mat4f::operator+(const mat4f& other) const
{
	mat4f r(false);
	r.a[ 0] = a[ 0] + other.a[ 0];
	r.a[ 1] = a[ 1] + other.a[ 1];
	r.a[ 2] = a[ 2] + other.a[ 2];
	r.a[ 3] = a[ 3] + other.a[ 3];
	r.a[ 4] = a[ 4] + other.a[ 4];
	r.a[ 5] = a[ 5] + other.a[ 5];
	r.a[ 6] = a[ 6] + other.a[ 6];
	r.a[ 7] = a[ 7] + other.a[ 7];
	r.a[ 8] = a[ 8] + other.a[ 8];
	r.a[ 9] = a[ 9] + other.a[ 9];
	r.a[10] = a[10] + other.a[10];
	r.a[11] = a[11] + other.a[11];
	r.a[12] = a[12] + other.a[12];
	r.a[13] = a[13] + other.a[13];
	r.a[14] = a[14] + other.a[14];
	r.a[15] = a[15] + other.a[15];
	return r;
}

inline mat4f mat4f::operator-(const mat4f& other) const
{
	mat4f r(false);
	r.a[ 0] = a[ 0] - other.a[ 0];
	r.a[ 1] = a[ 1] - other.a[ 1];
	r.a[ 2] = a[ 2] - other.a[ 2];
	r.a[ 3] = a[ 3] - other.a[ 3];
	r.a[ 4] = a[ 4] - other.a[ 4];
	r.a[ 5] = a[ 5] - other.a[ 5];
	r.a[ 6] = a[ 6] - other.a[ 6];
	r.a[ 7] = a[ 7] - other.a[ 7];
	r.a[ 8] = a[ 8] - other.a[ 8];
	r.a[ 9] = a[ 9] - other.a[ 9];
	r.a[10] = a[10] - other.a[10];
	r.a[11] = a[11] - other.a[11];
	r.a[12] = a[12] - other.a[12];
	r.a[13] = a[13] - other.a[13];
	r.a[14] = a[14] - other.a[14];
	r.a[15] = a[15] - other.a[15];
	return r;
}

inline mat4f mat4f::operator*(const mat4f& other) const
{
	mat4f r(false);
	
	r.m._11 = m._11 * other.m._11 + m._12 * other.m._21 + m._13 * other.m._31 + m._14 * other.m._41;
	r.m._12 = m._11 * other.m._12 + m._12 * other.m._22 + m._13 * other.m._32 + m._14 * other.m._42;
	r.m._13 = m._11 * other.m._13 + m._12 * other.m._23 + m._13 * other.m._33 + m._14 * other.m._43;
	r.m._14 = m._11 * other.m._14 + m._12 * other.m._24 + m._13 * other.m._34 + m._14 * other.m._44;
	
	r.m._21 = m._21 * other.m._11 + m._22 * other.m._21 + m._23 * other.m._31 + m._24 * other.m._41;
	r.m._22 = m._21 * other.m._12 + m._22 * other.m._22 + m._23 * other.m._32 + m._24 * other.m._42;
	r.m._23 = m._21 * other.m._13 + m._22 * other.m._23 + m._23 * other.m._33 + m._24 * other.m._43;
	r.m._24 = m._21 * other.m._14 + m._22 * other.m._24 + m._23 * other.m._34 + m._24 * other.m._44;
	
	r.m._31 = m._31 * other.m._11 + m._32 * other.m._21 + m._33 * other.m._31 + m._34 * other.m._41;
	r.m._32 = m._31 * other.m._12 + m._32 * other.m._22 + m._33 * other.m._32 + m._34 * other.m._42;
	r.m._33 = m._31 * other.m._13 + m._32 * other.m._23 + m._33 * other.m._33 + m._34 * other.m._43;
	r.m._34 = m._31 * other.m._14 + m._32 * other.m._24 + m._33 * other.m._34 + m._34 * other.m._44;
	
	r.m._41 = m._41 * other.m._11 + m._42 * other.m._21 + m._43 * other.m._31 + m._44 * other.m._41;
	r.m._42 = m._41 * other.m._12 + m._42 * other.m._22 + m._43 * other.m._32 + m._44 * other.m._42;
	r.m._43 = m._41 * other.m._13 + m._42 * other.m._23 + m._43 * other.m._33 + m._44 * other.m._43;
	r.m._44 = m._41 * other.m._14 + m._42 * other.m._24 + m._43 * other.m._34 + m._44 * other.m._44;
	
	return r;
}

inline mat4f& mat4f::operator+=(const mat4f& other)
{
	a[ 0] += other.a[ 0];
	a[ 1] += other.a[ 1];
	a[ 2] += other.a[ 2];
	a[ 3] += other.a[ 3];
	a[ 4] += other.a[ 4];
	a[ 5] += other.a[ 5];
	a[ 6] += other.a[ 6];
	a[ 7] += other.a[ 7];
	a[ 8] += other.a[ 8];
	a[ 9] += other.a[ 9];
	a[10] += other.a[10];
	a[11] += other.a[11];
	a[12] += other.a[12];
	a[13] += other.a[13];
	a[14] += other.a[14];
	a[15] += other.a[15];
	return *this;
}

inline mat4f& mat4f::operator-=(const mat4f& other)
{
	a[ 0] -= other.a[ 0];
	a[ 1] -= other.a[ 1];
	a[ 2] -= other.a[ 2];
	a[ 3] -= other.a[ 3];
	a[ 4] -= other.a[ 4];
	a[ 5] -= other.a[ 5];
	a[ 6] -= other.a[ 6];
	a[ 7] -= other.a[ 7];
	a[ 8] -= other.a[ 8];
	a[ 9] -= other.a[ 9];
	a[10] -= other.a[10];
	a[11] -= other.a[11];
	a[12] -= other.a[12];
	a[13] -= other.a[13];
	a[14] -= other.a[14];
	a[15] -= other.a[15];
	return *this;
}

inline mat4f& mat4f::operator*=(const mat4f& other)
{
	*this = *this * other;
	return *this;
}

inline mat4f& mat4f::operator=(const mat4f& other)
{
	if ( this != &other ) {
		BME_memcpy(&m, &other.m, sizeof(m));
	}
	return *this;
}

inline bool mat4f::operator==(const mat4f& other) const
{
	for ( size_t i = 0; i < 16; ++i ) {
		if ( !Math::IsEquals(a[i], other.a[i]) ) {
			return false;
		}
	}
	return true;
}

inline bool mat4f::operator!=(const mat4f& other) const
{
	for ( size_t i = 0; i < 16; ++i ) {
		if ( !Math::IsEquals(a[i], other.a[i]) ) {
			return true;
		}
	}
	return false;
}

inline void mat4f::ApplyToVector(vec2f& v, bool with_translate) const
{
	vec2f t = v;
	v.x = t.x * a[0] + t.y * a[4];
	v.y = t.x * a[1] + t.y * a[5];
	if ( with_translate ) {
		v.x += a[12];
		v.y += a[13];
	}
}

inline void mat4f::ApplyToVector(vec3f& v, bool with_translate) const
{
	vec3f t = v;
	v.x = t.x * a[0] + t.y * a[4] + t.z * a[ 8];
	v.y = t.x * a[1] + t.y * a[5] + t.z * a[ 9];
	v.z = t.x * a[2] + t.y * a[6] + t.z * a[10];
	if ( with_translate ) {
		v.x += a[12];
		v.y += a[13];
		v.z += a[14];
	}
}

inline vec2f mat4f::ApplyToVector(const vec2f& v, bool with_translate) const
{
	vec2f ret_value = v;
	ApplyToVector(ret_value, with_translate);
	return ret_value;
}

inline vec3f mat4f::ApplyToVector(const vec3f& v, bool with_translate) const
{
	vec3f ret_value = v;
	ApplyToVector(ret_value, with_translate);
	return ret_value;
}

// ----------------------------------------------------------------------------
// 
// Static impl
// 
// ----------------------------------------------------------------------------

inline mat4f mat4f::MakeZero()
{
	return mat4f(false).Zero();
}

inline mat4f mat4f::MakeIdentity()
{
	return mat4f(true);
}

inline mat4f mat4f::MakeScale(const vec2f& v)
{
	return mat4f(false).Scale(v);
}

inline mat4f mat4f::MakeScale(const vec3f& v)
{
	return mat4f(false).Scale(v);
}

inline mat4f mat4f::MakeTranslate(const vec2f& v)
{
	return mat4f(false).Translate(v);
}

inline mat4f mat4f::MakeTranslate(const vec3f& v)
{
	return mat4f(false).Translate(v);
}

inline mat4f mat4f::MakeRotation(f32 angle, const vec2f& v)
{
	return mat4f(false).Rotation(angle, v);
}

inline mat4f mat4f::MakeRotation(f32 angle, const vec3f& v)
{
	return mat4f(false).Rotation(angle, v);
}

inline mat4f mat4f::MakeOrthogonal(const vec2f& size, f32 znear, f32 zfar)
{
	return mat4f(false).Orthogonal(size, znear, zfar);
}

inline mat4f mat4f::MakePerspective(f32 fov, f32 aspect, f32 znear, f32 zfar)
{
	return mat4f(false).Perspective(fov, aspect, znear, zfar);
}

inline mat4f mat4f::MakeInverse(const mat4f& m, bool* success)
{
	return mat4f(false).Inverse(m, success);
}

} // namespace bme
