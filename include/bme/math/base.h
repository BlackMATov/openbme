/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/math/base.h
 *****************************************************************************/

#pragma once

#include "../config/all.h"

namespace bme
{
	// --------------------------------
	// forward declaration
	// --------------------------------
	
	class mat4f;
	class vec2f;
	class vec3f;
	
	template < typename T > class point2;
	typedef point2 <u32> pnt2u;
	typedef point2 <s32> pnt2s;
	typedef point2 <f32> pnt2f;
	
	template < typename T > class rect;
	typedef rect <u32> rectu;
	typedef rect <s32> rects;
	typedef rect <f32> rectf;
	
	// --------------------------------
	// base math namespace
	// --------------------------------
	
	namespace Math
	{
		// ----------------------------
		// Constants
		// ----------------------------
		
		const f32 ERROR_F32       = 0.00001f;
		const f64 ERROR_F64       = 0.0000001;
		
		const f32 PI              = 3.14159265359f;
		const f32 RECIPROCAL_PI   = 1.f / PI;
		const f32 TWO_PI          = PI * 2.f;
		const f32 HALF_PI         = PI / 2.f;
		
		const f64 PI64            = 3.1415926535897932384626433832795028841971693993751;
		const f64 RECIPROCAL_PI64 = 1.0 / PI64;
		const f64 TWO_PI64        = PI64 * 2.0;
		const f64 HALF_PI64       = PI64 / 2.0;
		
		const f32 DEGTORAD        = PI / 180.f;
		const f32 RADTODEG        = 180.f / PI;
		
		const f64 DEGTORAD64      = PI64 / 180.0;
		const f64 RADTODEG64      = 180.0 / PI64;
		
		// ----------------------------
		// Power 2
		// ----------------------------
		
		template < typename T >
		inline T NextPowerOfTwo(T value)
		{
			BME_STATIC_ASSERT(cpp::is_unsigned<T>::value);
			if ( 0 == value ) {
				return static_cast<T>(1);
			}
			--value;
			size_t i = 1;
			do {
				value |= value >> i;
				i <<= 1;
			} while (value & (value + 1));
			return ++value;
		}
		
		template < typename T >
		inline bool IsPowerOfTwo(T value)
		{
			BME_STATIC_ASSERT(cpp::is_unsigned<T>::value);
			return value && !(value & (value - 1));
		}
		
		// ----------------------------
		// SmartRound
		// ----------------------------
		
		namespace detail
		{
			template < typename T >
			inline T SmartRoundImpl(T v, const cpp::false_type& /*is_floating_point*/)
			{
				return v;
			}
			
			template < typename T >
			inline T SmartRoundImpl(T v, const cpp::true_type& /*is_floating_point*/)
			{
				return v >= static_cast<T>(0)
					? cpp::floor(v + static_cast<T>(0.5))
					: cpp::ceil (v - static_cast<T>(0.5));
			}
		}
		
		template < typename T >
		inline T SmartRound(T v)
		{
			BME_STATIC_ASSERT(cpp::is_arithmetic<T>::value);
			return detail::SmartRoundImpl(v, cpp::is_floating_point<T>());
		}
		
		// ----------------------------
		// SmartCast
		// ----------------------------
		
		namespace detail
		{
			template < typename TRet, typename TVal >
			inline TRet SmartCastImpl(TVal v, const cpp::false_type& /*float_to_integral*/)
			{
			#if BME_MODE == BME_MODE_DEBUG
				try {
					return cpp::numeric_cast<TRet>(v);
				} catch ( ... ) {
					BME_ASSERT_MSG(false, "bad numeric cast");
				}
			#endif
				return static_cast<TRet>(v);
			}
			
			template < typename TRet, typename TVal >
			inline TRet SmartCastImpl(TVal v, const cpp::true_type& /*float_to_integral*/)
			{
				TVal nv = SmartRound(v);
				return SmartCastImpl<TRet>(nv, cpp::false_type());
			}
		}
		
		template < typename TRet, typename TVal >
		inline TRet SmartCast(TVal v)
		{
			BME_STATIC_ASSERT(
				cpp::is_arithmetic<TRet>::value && cpp::is_arithmetic<TVal>::value);
			return detail::SmartCastImpl<TRet>(v,
				cpp::integral_constant<bool,
				cpp::is_integral<TRet>::value && cpp::is_floating_point<TVal>::value>());
		}
		
		// ----------------------------
		// Clamp && Lerp
		// ----------------------------
		
		template < typename T >
		inline T Clamp(T v, T min, T max)
		{
			BME_ASSERT(min <= max);
			BME_STATIC_ASSERT(cpp::is_arithmetic<T>::value);
			return cpp::min(cpp::max(v,min), max);
		}
		
		template < typename TV, typename TT >
		inline TV Lerp(TV a, TV b, TT t)
		{
			BME_STATIC_ASSERT(cpp::is_floating_point<TT>::value);
			return Math::SmartCast<TV>(
				a * (static_cast<TT>(1) - t) +
				b * t);
		}
		
		// ----------------------------
		// IsEquals & IsZero
		// ----------------------------
		
		namespace detail
		{
			template < typename T >
			inline bool IsEqualsImpl(T a, T b, const cpp::false_type& /*is_floating_point*/)
			{
				return a == b;
			}
			
			inline bool IsEqualsImpl(f32 a, f32 b, const cpp::true_type& /*is_floating_point*/)
			{
				f32 tolerance = ERROR_F32;
				return
					(a + tolerance >= b) &&
					(a - tolerance <= b);
			}
			
			inline bool IsEqualsImpl(f64 a, f64 b, const cpp::true_type& /*is_floating_point*/)
			{
				f64 tolerance = ERROR_F64;
				return
					(a + tolerance >= b) &&
					(a - tolerance <= b);
			}
		}
		
		template < typename T >
		inline bool IsEquals(T a, T b)
		{
			BME_STATIC_ASSERT(cpp::is_arithmetic<T>::value);
			return detail::IsEqualsImpl(a, b, cpp::is_floating_point<T>());
		}
		
		template < typename T >
		inline bool IsZero(T value)
		{
			BME_STATIC_ASSERT(cpp::is_arithmetic<T>::value);
			return IsEquals(value, static_cast<T>(0));
		}
		
		// ----------------------------
		// Angle
		// ----------------------------
		
		template < typename T >
		inline T ToDeg(T rad)
		{
			BME_STATIC_ASSERT(cpp::is_arithmetic<T>::value);
			return SmartCast<T>(rad * RADTODEG64);
		}
		
		template < typename T >
		inline T ToRad(T deg)
		{
			BME_STATIC_ASSERT(cpp::is_arithmetic<T>::value);
			return SmartCast<T>(deg * DEGTORAD64);
		}
		
		// ----------------------------
		// SignNumber & SignZeroNumber
		// ----------------------------
		
		template < typename T >
		inline ptrdiff_t SignNumber(T value)
		{
			BME_STATIC_ASSERT(cpp::is_arithmetic<T>::value);
			return value < static_cast<T>(0) ? -1 : 1;
		}
		
		template < typename T >
		inline ptrdiff_t SignZeroNumber(T value)
		{
			BME_STATIC_ASSERT(cpp::is_arithmetic<T>::value);
			return IsZero(value) ? 0 : SignNumber(value);
		}
	} // namespace Math
} // namespace bme
