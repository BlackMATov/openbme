/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/math/all.h
 *****************************************************************************/

#pragma once

#include "base.h"

#include "mat4f.h"
#include "point2.h"
#include "rect.h"
#include "vec2f.h"
#include "vec3f.h"
