/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/math/vec3f.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

class vec3f
{
public:
	
	f32 x;
	f32 y;
	f32 z;
	
public:
	
	static const vec3f zero;  /// ( 0; 0; 0)
	static const vec3f one;   /// ( 1; 1; 1)
	static const vec3f neg;   /// (-1;-1;-1)
	static const vec3f one_x; /// ( 1; 0; 0)
	static const vec3f one_y; /// ( 0; 1; 0)
	static const vec3f one_z; /// ( 0; 0; 1)
	static const vec3f neg_x; /// (-1; 0; 0)
	static const vec3f neg_y; /// ( 0;-1; 0)
	static const vec3f neg_z; /// ( 0; 0;-1)
	
	vec3f();
	explicit vec3f(f32 n);
	vec3f(f32 x, f32 y, f32 z);
	vec3f(const vec3f& other);
	vec3f(const vec2f& other, f32 z);
	
	template < typename TO >
	vec3f(const point2<TO>& other, f32 z);
	
	f32*       getPointer  ();
	const f32* getPointer  () const;
	
	f32&       operator [] ( size_t index );
	f32        operator [] ( size_t index ) const;
	
	vec3f&     operator += ( f32 v );
	vec3f&     operator -= ( f32 v );
	vec3f&     operator *= ( f32 v );
	vec3f&     operator /= ( f32 v );
	
	vec3f&     operator += ( const vec3f& other );
	vec3f&     operator -= ( const vec3f& other );
	vec3f&     operator *= ( const vec3f& other );
	vec3f&     operator /= ( const vec3f& other );
	
	vec3f&     operator =  ( f32 v );
	vec3f&     operator =  ( const vec3f& other );
	bool       IsEquals    ( const vec3f& other ) const;
	
	vec3f&     setLength   ( f32 new_len );
	f32        getLength   () const;
	f32        getLengthSQ () const;
	
	f32        Dot         ( const vec3f& other ) const;
	f32        DotAbs      ( const vec3f& other ) const;
	vec3f      Cross       ( const vec3f& other ) const;
	f32        Distance    ( const vec3f& other ) const;
	f32        DistanceSQ  ( const vec3f& other ) const;
	
	f32        getAngle    ( const vec3f& other ) const;
	
	vec3f&     Invert      ();
	vec3f&     Normalize   ();
};

// ----------------------------------------------------------------------------
// 
// Global operators
// 
// ----------------------------------------------------------------------------

// ------------------------------------
// vec3f (- == != <)
// ------------------------------------

inline vec3f operator-(const vec3f& p)
{
	return vec3f(-p.x, -p.y, -p.z);
}

inline bool operator==(const vec3f& a, const vec3f& b)
{
	return a.IsEquals(b);
}

inline bool operator!=(const vec3f& a, const vec3f& b)
{
	return !a.IsEquals(b);
}

inline bool operator<(const vec3f& a, const vec3f& b)
{
	return
		(a.x < b.x) ||
		(Math::IsEquals(a.x, b.x) && a.y < b.y) ||
		(Math::IsEquals(a.x, b.x) && Math::IsEquals(a.y, b.y) && a.z < b.z);
}

// ------------------------------------
// vec3f (*/+-) vec3f
// ------------------------------------

inline vec3f operator+(const vec3f& a, const vec3f& b)
{
	return vec3f(
		a.x + b.x,
		a.y + b.y,
		a.z + b.z);
}

inline vec3f operator-(const vec3f& a, const vec3f& b)
{
	return vec3f(
		a.x - b.x,
		a.y - b.y,
		a.z - b.z);
}

inline vec3f operator*(const vec3f& a, const vec3f& b)
{
	return vec3f(
		a.x * b.x,
		a.y * b.y,
		a.z * b.z);
}

inline vec3f operator/(const vec3f& a, const vec3f& b)
{
	return vec3f(
		a.x / b.x,
		a.y / b.y,
		a.z / b.z);
}

// ------------------------------------
// vec3f (*/+-) scalar
// ------------------------------------

inline vec3f operator+(const vec3f& p, f32 v)
{
	return vec3f(
		p.x + v,
		p.y + v,
		p.z + v);
}

inline vec3f operator-(const vec3f& p, f32 v)
{
	return vec3f(
		p.x - v,
		p.y - v,
		p.z - v);
}

inline vec3f operator*(const vec3f& p, f32 v)
{
	return vec3f(
		p.x * v,
		p.y * v,
		p.z * v);
}

inline vec3f operator/(const vec3f& p, f32 v)
{
	return vec3f(
		p.x / v,
		p.y / v,
		p.z / v);
}

// ------------------------------------
// scalar (*/+-) vec3f
// ------------------------------------

inline vec3f operator*(f32 sc, const vec3f& p)
{
	return vec3f(
		sc * p.x,
		sc * p.y,
		sc * p.z);
}

inline vec3f operator/(f32 sc, const vec3f& p)
{
	return vec3f(
		sc / p.x,
		sc / p.y,
		sc / p.z);
}

inline vec3f operator+(f32 sc, const vec3f& p)
{
	return vec3f(
		sc + p.x,
		sc + p.y,
		sc + p.z);
}

inline vec3f operator-(f32 sc, const vec3f& p)
{
	return vec3f(
		sc - p.x,
		sc - p.y,
		sc - p.z);
}

// ----------------------------------------------------------------------------
// 
// Impl
// 
// ----------------------------------------------------------------------------

template < typename TO >
vec3f::vec3f(const point2<TO>& other, f32 z)
: x(Math::SmartCast<f32>(other.x))
, y(Math::SmartCast<f32>(other.y))
, z(z)
{
}

inline f32* vec3f::getPointer()
{
	return &x;
}

inline const f32* vec3f::getPointer() const
{
	return &x;
}

inline f32& vec3f::operator[](size_t index)
{
	BME_ASSERT(index < 3);
	return *(&x + index);
}

inline f32 vec3f::operator[](size_t index) const
{
	BME_ASSERT(index < 3);
	return *(&x + index);
}

inline vec3f& vec3f::operator+=(const vec3f& other)
{
	x += other.x;
	y += other.y;
	z += other.z;
	return *this;
}

inline vec3f& vec3f::operator+=(f32 v)
{
	x += v;
	y += v;
	z += v;
	return *this;
}

inline vec3f& vec3f::operator-=(const vec3f& other)
{
	x -= other.x;
	y -= other.y;
	z -= other.z;
	return *this;
}

inline vec3f& vec3f::operator-=(f32 v)
{
	x -= v;
	y -= v;
	z -= v;
	return *this;
}

inline vec3f& vec3f::operator*=(const vec3f& other)
{
	x *= other.x;
	y *= other.y;
	z *= other.z;
	return *this;
}

inline vec3f& vec3f::operator*=(f32 v)
{
	x *= v;
	y *= v;
	z *= v;
	return *this;
}

inline vec3f& vec3f::operator/=(const vec3f& other)
{
	x /= other.x;
	y /= other.y;
	z /= other.z;
	return *this;
}

inline vec3f& vec3f::operator/=(f32 v)
{
	x /= v;
	y /= v;
	z /= v;
	return *this;
}

inline vec3f& vec3f::operator=(const f32 v)
{
	x = v;
	y = v;
	z = v;
	return *this;
}

inline vec3f& vec3f::operator=(const vec3f& other)
{
	x = other.x;
	y = other.y;
	z = other.z;
	return *this;
}

inline bool vec3f::IsEquals(const vec3f& other) const
{
	return
		Math::IsEquals(x, other.x) &&
		Math::IsEquals(y, other.y) &&
		Math::IsEquals(z, other.z);
}

inline vec3f& vec3f::setLength(f32 new_len)
{
	Normalize();
	return (*this *= new_len);
}

inline f32 vec3f::getLength() const
{
	return cpp::sqrt(
		x * x +
		y * y +
		z * z);
}

inline f32 vec3f::getLengthSQ() const
{
	return
		x * x +
		y * y +
		z * z;
}

inline f32 vec3f::Dot(const vec3f& other) const
{
	return
		x * other.x +
		y * other.y +
		z * other.z;
}

inline f32 vec3f::DotAbs(const vec3f& other) const
{
	return cpp::abs(x * other.x) + cpp::abs(y * other.y) + cpp::abs(z * other.z);
}

inline vec3f vec3f::Cross(const vec3f& other) const
{
	return vec3f(
		y * other.z - z * other.y,
		z * other.x - x * other.z,
		x * other.y - y * other.x);
}

inline f32 vec3f::Distance(const vec3f& other) const
{
	return (*this - other).getLength();
}

inline f32 vec3f::DistanceSQ(const vec3f& other) const
{
	return (*this - other).getLengthSQ();
}

inline f32 vec3f::getAngle(const vec3f& other) const
{
	f32 len_product = getLength() * other.getLength();
	if ( len_product < Math::ERROR_F32 ) {
		len_product = Math::ERROR_F32;
	}
	f32 f = Dot(other) / len_product;
	return cpp::acos(Math::Clamp(f, -1.f, 1.f));
}

inline vec3f& vec3f::Invert()
{
	x *= -1.f;
	y *= -1.f;
	z *= -1.f;
	return *this;
}

inline vec3f& vec3f::Normalize()
{
	f32 len = getLength();
	if ( len > 0.f ) {
		f32 inv_len = 1.f / len;
		x *= inv_len;
		y *= inv_len;
		z *= inv_len;
	}
	return *this;
}

} // namespace bme
