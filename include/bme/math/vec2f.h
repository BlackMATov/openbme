/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/math/vec2f.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

class vec2f
{
public:
	
	f32 x;
	f32 y;
	
public:
	
	static const vec2f zero;    /// ( 0; 0)
	static const vec2f one;     /// ( 1; 1)
	static const vec2f neg;     /// (-1;-1)
	static const vec2f one_neg; /// ( 1;-1)
	static const vec2f neg_one; /// (-1; 1)
	static const vec2f one_x;   /// ( 1; 0)
	static const vec2f one_y;   /// ( 0; 1)
	static const vec2f neg_x;   /// (-1; 0)
	static const vec2f neg_y;   /// ( 0;-1)
	
	vec2f();
	explicit vec2f(f32 n);
	vec2f(f32 x, f32 y);
	vec2f(const vec2f& other);
	vec2f(const vec3f& other);
	
	template < typename TO >
	explicit vec2f(const point2<TO>& other);
	
	f32*       getPointer  ();
	const f32* getPointer  () const;
	
	f32&       operator [] ( size_t index );
	f32        operator [] ( size_t index ) const;
	
	vec2f&     operator += ( f32 v );
	vec2f&     operator -= ( f32 v );
	vec2f&     operator *= ( f32 v );
	vec2f&     operator /= ( f32 v );
	
	vec2f&     operator += ( const vec2f& other );
	vec2f&     operator -= ( const vec2f& other );
	vec2f&     operator *= ( const vec2f& other );
	vec2f&     operator /= ( const vec2f& other );
	
	vec2f&     operator =  ( f32 v );
	vec2f&     operator =  ( const vec2f& other );
	bool       IsEquals    ( const vec2f& other ) const;
	
	vec2f&     setLength   ( f32 new_len );
	f32        getLength   () const;
	f32        getLengthSQ () const;
	
	f32        Dot         ( const vec2f& other ) const;
	f32        DotAbs      ( const vec2f& other ) const;
	f32        Cross       ( const vec2f& other ) const;
	f32        Distance    ( const vec2f& other ) const;
	f32        DistanceSQ  ( const vec2f& other ) const;
	
	f32        getAngle    ()                     const;
	f32        getAngle    ( const vec2f& other ) const;
	vec2f&     Rotate      ( f32 radians );
	vec2f&     Rotate      ( f32 radians, const vec2f& center );
	
	vec2f&     Invert      ();
	vec2f&     Normalize   ();
};

// ----------------------------------------------------------------------------
// 
// Global operators
// 
// ----------------------------------------------------------------------------

// ------------------------------------
// vec2f (- == != <)
// ------------------------------------

inline vec2f operator-(const vec2f& p)
{
	return vec2f(-p.x, -p.y);
}

inline bool operator==(const vec2f& a, const vec2f& b)
{
	return a.IsEquals(b);
}

inline bool operator!=(const vec2f& a, const vec2f& b)
{
	return !a.IsEquals(b);
}

inline bool operator<(const vec2f& a, const vec2f& b)
{
	return
		(a.x < b.x) ||
		(Math::IsEquals(a.x, b.x) && a.y < b.y);
}

// ------------------------------------
// vec2f (*/+-) vec2f
// ------------------------------------

inline vec2f operator*(const vec2f& a, const vec2f& b)
{
	return vec2f(a.x * b.x, a.y * b.y);
}

inline vec2f operator/(const vec2f& a, const vec2f& b)
{
	return vec2f(a.x / b.x, a.y / b.y);
}

inline vec2f operator+(const vec2f& a, const vec2f& b)
{
	return vec2f(a.x + b.x, a.y + b.y);
}

inline vec2f operator-(const vec2f& a, const vec2f& b)
{
	return vec2f(a.x - b.x, a.y - b.y);
}

// ------------------------------------
// vec2f (*/+-) scalar
// ------------------------------------

inline vec2f operator*(const vec2f& p, f32 sc)
{
	return vec2f(p.x * sc, p.y * sc);
}

inline vec2f operator/(const vec2f& p, f32 sc)
{
	return vec2f(p.x / sc, p.y / sc);
}

inline vec2f operator+(const vec2f& p, f32 sc)
{
	return vec2f(p.x + sc, p.y + sc);
}

inline vec2f operator-(const vec2f& p, f32 sc)
{
	return vec2f(p.x - sc, p.y - sc);
}

// ------------------------------------
// scalar (*/+-) vec2f
// ------------------------------------

inline vec2f operator*(f32 sc, const vec2f& p)
{
	return vec2f(sc * p.x, sc * p.y);
}

inline vec2f operator/(f32 sc, const vec2f& p)
{
	return vec2f(sc / p.x, sc / p.y);
}

inline vec2f operator+(f32 sc, const vec2f& p)
{
	return vec2f(sc + p.x, sc + p.y);
}

inline vec2f operator-(f32 sc, const vec2f& p)
{
	return vec2f(sc - p.x, sc - p.y);
}

// ----------------------------------------------------------------------------
// 
// Impl
// 
// ----------------------------------------------------------------------------

template < typename TO >
vec2f::vec2f(const point2<TO>& other)
: x(Math::SmartCast<f32>(other.x))
, y(Math::SmartCast<f32>(other.y))
{
}

inline f32* vec2f::getPointer()
{
	return &x;
}

inline const f32* vec2f::getPointer() const
{
	return &x;
}

inline f32& vec2f::operator[](size_t index)
{
	BME_ASSERT(index < 2);
	return *(&x + index);
}

inline f32 vec2f::operator[](size_t index) const
{
	BME_ASSERT(index < 2);
	return *(&x + index);
}

inline vec2f& vec2f::operator+=(f32 v)
{
	x += v;
	y += v;
	return *this;
}

inline vec2f& vec2f::operator-=(f32 v)
{
	x -= v;
	y -= v;
	return *this;
}

inline vec2f& vec2f::operator*=(f32 v)
{
	x *= v;
	y *= v;
	return *this;
}

inline vec2f& vec2f::operator/=(f32 v)
{
	x /= v;
	y /= v;
	return *this;
}

inline vec2f& vec2f::operator+=(const vec2f& other)
{
	x += other.x;
	y += other.y;
	return *this;
}

inline vec2f& vec2f::operator-=(const vec2f& other)
{
	x -= other.x;
	y -= other.y;
	return *this;
}

inline vec2f& vec2f::operator*=(const vec2f& other)
{
	x *= other.x;
	y *= other.y;
	return *this;
}

inline vec2f& vec2f::operator/=(const vec2f& other)
{
	x /= other.x;
	y /= other.y;
	return *this;
}

inline vec2f& vec2f::operator=(const f32 v)
{
	x = v;
	y = v;
	return *this;
}

inline vec2f& vec2f::operator=(const vec2f& other)
{
	x = other.x;
	y = other.y;
	return *this;
}

inline bool vec2f::IsEquals(const vec2f& other) const
{
	return
		Math::IsEquals(x, other.x) &&
		Math::IsEquals(y, other.y);
}

inline vec2f& vec2f::setLength(f32 new_len)
{
	Normalize();
	return (*this *= new_len);
}

inline f32 vec2f::getLength() const
{
	return cpp::sqrt(x * x + y * y);
}

inline f32 vec2f::getLengthSQ() const
{
	return x * x + y * y;
}

inline f32 vec2f::Dot(const vec2f& other) const
{
	return x * other.x + y * other.y;
}

inline f32 vec2f::DotAbs(const vec2f& other) const
{
	return cpp::abs(x * other.x) + cpp::abs(y * other.y);
}

inline f32 vec2f::Cross(const vec2f& other) const
{
	return x * other.y - y * other.x;
}

inline f32 vec2f::Distance(const vec2f& other) const
{
	return (*this - other).getLength();
}

inline f32 vec2f::DistanceSQ(const vec2f& other) const
{
	return (*this - other).getLengthSQ();
}

inline f32 vec2f::getAngle() const
{
	return cpp::atan2(y, x);
}

inline f32 vec2f::getAngle(const vec2f& other) const
{
	f32 len_product = getLength() * other.getLength();
	if ( len_product < Math::ERROR_F32 ) {
		len_product = Math::ERROR_F32;
	}
	f32 f = Dot(other) / len_product;
	return cpp::acos(Math::Clamp(f, -1.f, 1.f));
}

inline vec2f& vec2f::Rotate(f32 radians)
{
	f32 cs = cpp::cos(radians);
	f32 sn = cpp::sin(radians);
	f32 nx = x * cs - y * sn;
	f32 ny = x * sn + y * cs;
	x = nx;
	y = ny;
	return *this;
}

inline vec2f& vec2f::Rotate(f32 radians, const vec2f& center)
{
	x -= center.x;
	y -= center.y;
	Rotate(radians);
	x += center.x;
	y += center.y;
	return *this;
}

inline vec2f& vec2f::Invert()
{
	x *= -1.f;
	y *= -1.f;
	return *this;
}

inline vec2f& vec2f::Normalize()
{
	f32 len = getLength();
	if ( len > 0.f ) {
		f32 inv_len = 1.f / len;
		x *= inv_len;
		y *= inv_len;
	}
	return *this;
}

} // namespace bme
