/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/math/rect.h
 *****************************************************************************/

#pragma once

#include "point2.h"

namespace bme
{

template < typename T >
class rect
{
	T       x1;
	T       y1;
	T       x2;
	T       y2;
	void    _repair();
	
public:
	
	typedef rect<T> rectT;
	typedef point2<T> point2T;
	typedef T valuesT;
	
	static const rectT zero;
	
	rect();
	rect(T x1, T y1, T x2, T y2);
	rect(const rectT& other);
	explicit rect(const point2T& size);
	rect(const point2T& pos, const point2T& size);
	
	rectT&  operator +=     ( const point2T& pos );
	rectT&  operator -=     ( const point2T& pos );
	
	rectT&  operator  =     ( const rectT& other );
	bool    IsEquals        ( const rectT& other ) const;
	
	void    setLeft         ( T value );
	void    setTop          ( T value );
	void    setRight        ( T value );
	void    setBottom       ( T value );
	
	T       getLeft         () const;
	T       getTop          () const;
	T       getRight        () const;
	T       getBottom       () const;
	
	void    setTopBottom    ( T t, T b );
	void    setLeftRight    ( T l, T r );
	
	void    setTopLeft      ( T x, T y );
	void    setTopRight     ( T x, T y );
	void    setBottomLeft   ( T x, T y );
	void    setBottomRight  ( T x, T y );
	
	void    setTopLeft      ( const point2T& v );
	void    setTopRight     ( const point2T& v );
	void    setBottomLeft   ( const point2T& v );
	void    setBottomRight  ( const point2T& v );
	
	point2T getTopLeft      () const;
	point2T getTopRight     () const;
	point2T getBottomLeft   () const;
	point2T getBottomRight  () const;
	
	void    setWidth        ( T value );
	void    setHeight       ( T value );
	
	T       getWidth        () const;
	T       getHeight       () const;
	
	T       getArea         () const;
	point2T getSize         () const;
	point2T getCenter       () const;
	
	void    AddPoint        ( T x, T y );
	void    AddPoint        ( const point2T& p );
	
	bool    IsPointInside   ( T x, T y ) const;
	bool    IsPointInside   ( const point2T& point ) const;
	
	void    Merge           ( const rectT& other );
	rectT   Intersection    ( const rectT& other ) const;
	bool    IsRectIntersect ( const rectT& other ) const;
};

// ----------------------------------------------------------------------------
// 
// Global operators
// 
// ----------------------------------------------------------------------------

// ------------------------------------
// rectT (== != <)
// ------------------------------------

template < typename T >
bool operator==(const rect<T>& a, const rect<T>& b)
{
	return a.IsEquals(b);
}

template < typename T >
bool operator!=(const rect<T>& a, const rect<T>& b)
{
	return !a.IsEquals(b);
}

template < typename T >
bool operator<(const rect<T>& a, const rect<T>& b)
{
	return a.getArea() < b.getArea();
}

// ------------------------------------
// rectT (+-) point2T
// ------------------------------------

template < typename T >
rect<T> operator+(const rect<T>& a, const point2<T>& b)
{
	rect<T> tmp(*a);
	return tmp += b;
}

template < typename T >
rect<T> operator-(const rect<T>& a, const point2<T>& b)
{
	rect<T> tmp(*a);
	return tmp -= b;
}

// ----------------------------------------------------------------------------
// 
// Impl
// 
// ----------------------------------------------------------------------------

template < typename T >
void rect<T>::_repair()
{
	if ( x2 < x1 ) {
		cpp::swap(x1, x2);
	}
	if ( y2 < y1 ) {
		cpp::swap(y1, y2);
	}
}

template < typename T >
rect<T>::rect()
: x1(0), y1(0)
, x2(0), y2(0)
{
}

template < typename T >
rect<T>::rect(T x1, T y1, T x2, T y2)
: x1(x1), y1(y1)
, x2(x2), y2(y2)
{
	_repair();
}

template < typename T >
rect<T>::rect(const rectT& other)
: x1(other.x1), y1(other.y1)
, x2(other.x2), y2(other.y2)
{
}

template < typename T >
rect<T>::rect(const point2T& size)
: x1(0), y1(0), x2(size.x), y2(size.y)
{
	_repair();
}

template < typename T >
rect<T>::rect(const point2T& pos, const point2T& size)
: x1(pos.x), y1(pos.y)
, x2(pos.x + size.x), y2(pos.y + size.y)
{
	_repair();
}

template < typename T >
typename rect<T>::rectT& rect<T>::operator+=(const point2T& pos)
{
	x1 += pos.x; y1 += pos.y;
	x2 += pos.x; y2 += pos.y;
	return *this;
}

template < typename T >
typename rect<T>::rectT& rect<T>::operator-=(const point2T& pos)
{
	x1 -= pos.x; y1 -= pos.y;
	x2 -= pos.x; y2 -= pos.y;
	return *this;
}

template < typename T >
typename rect<T>::rectT& rect<T>::operator=(const rectT& other)
{
	x1 = other.x1; y1 = other.y1;
	x2 = other.x2; y2 = other.y2;
	return *this;
}

template < typename T >
bool rect<T>::IsEquals(const rectT& other) const
{
	return
		Math::IsEquals(x1,other.x1) && Math::IsEquals(y1,other.y1) &&
		Math::IsEquals(x2,other.x2) && Math::IsEquals(y2,other.y2);
}

template < typename T >
void rect<T>::setLeft(T value)
{
	x1 = value;
	_repair();
}

template < typename T >
void rect<T>::setTop(T value)
{
	y1 = value;
	_repair();
}

template < typename T >
void rect<T>::setRight(T value)
{
	x2 = value;
	_repair();
}

template < typename T >
void rect<T>::setBottom(T value)
{
	y2 = value;
	_repair();
}

template < typename T >
T rect<T>::getLeft() const
{
	return x1;
}

template < typename T >
T rect<T>::getTop() const
{
	return y1;
}

template < typename T >
T rect<T>::getRight() const
{
	return x2;
}

template < typename T >
T rect<T>::getBottom() const
{
	return y2;
}

template < typename T >
void rect<T>::setTopBottom(T t, T b)
{
	y1 = t;
	y2 = b;
	_repair();
}

template < typename T >
void rect<T>::setLeftRight(T l, T r)
{
	x1 = l;
	x2 = r;
	_repair();
}

template < typename T >
void rect<T>::setTopLeft(T x, T y)
{
	x1 = x;
	y1 = y;
	_repair();
}

template < typename T >
void rect<T>::setTopRight(T x, T y)
{
	x2 = x;
	y1 = y;
	_repair();
}

template < typename T >
void rect<T>::setBottomLeft(T x, T y)
{
	x1 = x;
	y2 = y;
	_repair();
}

template < typename T >
void rect<T>::setBottomRight(T x, T y)
{
	x2 = x;
	y2 = y;
	_repair();
}

template < typename T >
void rect<T>::setTopLeft(const point2T& v)
{
	x1 = v.x;
	y1 = v.y;
	_repair();
}

template < typename T >
void rect<T>::setTopRight(const point2T& v)
{
	x2 = v.x;
	y1 = v.y;
	_repair();
}

template < typename T >
void rect<T>::setBottomLeft(const point2T& v)
{
	x1 = v.x;
	y2 = v.y;
	_repair();
}

template < typename T >
void rect<T>::setBottomRight(const point2T& v)
{
	x2 = v.x;
	y2 = v.y;
	_repair();
}

template < typename T >
typename rect<T>::point2T rect<T>::getTopLeft() const
{
	return point2T(x1,y1);
}

template < typename T >
typename rect<T>::point2T rect<T>::getTopRight() const
{
	return point2T(x2,y1);
}

template < typename T >
typename rect<T>::point2T rect<T>::getBottomLeft() const
{
	return point2T(x1,y2);
}

template < typename T >
typename rect<T>::point2T rect<T>::getBottomRight() const
{
	return point2T(x2,y2);
}

template < typename T >
void rect<T>::setWidth(T value)
{
	x2 = x1 + value;
	_repair();
}

template < typename T >
void rect<T>::setHeight(T value)
{
	y2 = y1 + value;
	_repair();
}

template < typename T >
T rect<T>::getWidth() const
{
	return x2 - x1;
}

template < typename T >
T rect<T>::getHeight() const
{
	return y2 - y1;
}

template < typename T >
T rect<T>::getArea() const
{
	return getWidth() * getHeight();
}

template < typename T >
typename rect<T>::point2T rect<T>::getSize() const
{
	return point2T(getWidth(), getHeight());
}

template < typename T >
typename rect<T>::point2T rect<T>::getCenter() const
{
	return point2T((x1+x2)/T(2), (y1+y2)/T(2));
}

template < typename T >
void rect<T>::AddPoint(T x, T y)
{
	if ( x < x1 ) x1 = x;
	if ( x > x2 ) x2 = x;
	if ( y < y1 ) y1 = y;
	if ( y > y2 ) y2 = y;
}

template < typename T >
void rect<T>::AddPoint(const point2T& p)
{
	AddPoint(p.x, p.y);
}

template < typename T >
bool rect<T>::IsPointInside(T x, T y) const
{
	return
		x1 <= x && y1 <= y &&
		x2 >= x && y2 >= y;
}

template < typename T >
bool rect<T>::IsPointInside(const point2T& point) const
{
	return IsPointInside(point.x, point.y);
}

template < typename T >
void rect<T>::Merge(const rectT& other)
{
	if ( other.x1 < x1 ) x1 = other.x1;
	if ( other.y1 < y1 ) y1 = other.y1;
	if ( other.x2 > x2 ) x2 = other.x2;
	if ( other.y2 > y2 ) y2 = other.y2;
}

template < typename T >
typename rect<T>::rectT rect<T>::Intersection(const rectT& other) const
{
	return IsRectIntersect(other)
		? rectT(
			cpp::max(x1, other.x1),
			cpp::max(y1, other.y1),
			cpp::min(x2, other.x2),
			cpp::min(y2, other.y2))
		: rectT::zero;
}

template < typename T >
bool rect<T>::IsRectIntersect(const rectT& other) const
{
	return
		y2 >= other.y1 && y1 <= other.y2 &&
		x2 >= other.x1 && x1 <= other.x2;
}

// ----------------------------------------------------------------------------
// 
// Static impl
// 
// ----------------------------------------------------------------------------

template<typename T> const rect<T> rect<T>::zero = rect<T>(0,0,0,0);

} // namespace bme
