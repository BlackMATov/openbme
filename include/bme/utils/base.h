/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/utils/base.h
 *****************************************************************************/

#pragma once

#include "../math/all.h"

namespace bme
{
	template < typename T >
	class Holder;
	
	template < typename T >
	class MutexLockGuard;
	
	class Color;
	class Jobber;
	class JobberTask;
	class Path;
	class Randomizer;
	class RefCounted;
	class SmartBuffer;
	class StrId;
	class Thread;
	class Mutex;
	class RecursiveMutex;
	class ConditionVar;
	class AtomicCounter;
	class AtomicFlag;
	
	typedef cpp::intrusive_ptr<Thread>       ThreadPtr;
	typedef cpp::intrusive_ptr<Jobber>       JobberPtr;
	typedef cpp::intrusive_ptr<JobberTask>   JobberTaskPtr;
	
	typedef cpp::basic_string   <char>::type Str;
	typedef cpp::basic_string<wchar_t>::type WStr;
	typedef cpp::basic_string    <u32>::type UStr;
} // namespace bme
