/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/utils/StringsFormatImpl.h
 *****************************************************************************/

#pragma once

#include "StrId.h"
#include "Path.h"

namespace bme
{
namespace Strings
{
	// ------------------------------------------------------------------------
	// 
	// BME_snprintf
	// 
	// ------------------------------------------------------------------------
	
	#if BME_COMPILER == BME_COMPILER_MSVC
	namespace detail {
		int msvc_snprintf(char* dest, size_t size, const char* format, ...);
	} // detail
	#	define BME_snprintf ::bme::Strings::detail::msvc_snprintf
	#else
	#	define BME_snprintf ::snprintf
	#endif
	
	// ------------------------------------------------------------------------
	// 
	// Arg (arithmetic value by default)
	// 
	// ------------------------------------------------------------------------
	
	template < typename T >
	struct Arg
	{
		T   value;
		u32 width;
		u32 precision;
		
		Arg(T value, u32 width = 0, u32 precision = 6)
			: value(value), width(width), precision(precision) {}
		
		template < typename AT >
		static ptrdiff_t Write(char* dest, size_t size, const Arg<AT>& self)
		{
			BME_STATIC_ASSERT(cpp::is_arithmetic<AT>::value);
			return WriteImpl(dest, size, self);
		}
		
	private:
		
		// floating point
		
		template < typename AT >
		static typename cpp::enable_if_c<
			cpp::is_floating_point<AT>::value, ptrdiff_t>::type
			WriteImpl(char* dest, size_t size, const Arg<AT>& self)
		{
			char buf[128] = {0};
			BME_snprintf(buf, 128, "%%%u.%uf", self.width, self.precision);
			return BME_snprintf(
				dest, size, buf, Math::SmartCast<f64>(self.value));
		}
		
		// signed
		
		template < typename AT >
		static typename cpp::enable_if_c<
			cpp::is_signed<AT>::value, ptrdiff_t>::type
			WriteImpl(char* dest, size_t size, const Arg<AT>& self)
		{
			char buf[128] = {0};
			BME_snprintf(buf, 128, "%%%ui", self.width);
			return BME_snprintf(
				dest, size, buf, Math::SmartCast<s32>(self.value));
		}
		
		// unsigned
		
		template < typename AT >
		static typename cpp::enable_if_c<
			cpp::is_unsigned<AT>::value, ptrdiff_t>::type
			WriteImpl(char* dest, size_t size, const Arg<AT>& self)
		{
			char buf[128] = {0};
			BME_snprintf(buf, 128, "%%%uu", self.width);
			return BME_snprintf(
				dest, size, buf, Math::SmartCast<u32>(self.value));
		}
	};
	
	// ------------------------------------------------------------------------
	// 
	// Boolean Arg<T>
	// 
	// ------------------------------------------------------------------------
	
	template <>
	struct Arg<bool>
	{
		bool value;
		Arg(bool value) : value(value) {}
		static ptrdiff_t Write(char* dest, size_t size, const Arg<bool>& self);
	};
	
	// ------------------------------------------------------------------------
	// 
	// Char Arg<T>
	// 
	// ------------------------------------------------------------------------
	
	template <>
	struct Arg<char*>
	{
		const char* value;
		Arg(const char* value) : value(value) {}
		static ptrdiff_t Write(char* dest, size_t size, const Arg<char*>& self);
	};
	
	template <>
	struct Arg<const char*>
	{
		const char* value;
		Arg(const char* value) : value(value) {}
		static ptrdiff_t Write(char* dest, size_t size, const Arg<const char*>& self);
	};
	
	template < size_t N >
	struct Arg<char[N]>
	{
		char value[N];
		Arg(const char(&s)[N]) { BME_strcpy(value, &s[0]); }
		static ptrdiff_t Write(char* dest, size_t size, const Arg<char[N]>& self){
			return BME_snprintf(dest, size, "%s", self.value);
		}
	};
	
	template < size_t N >
	struct Arg<const char[N]>
	{
		char value[N];
		Arg(const char(&s)[N]) { BME_strcpy(value, &s[0]); }
		static ptrdiff_t Write(char* dest, size_t size, const Arg<const char[N]>& self){
			return BME_snprintf(dest, size, "%s", self.value);
		}
	};
	
	// ------------------------------------------------------------------------
	// 
	// Str and StrId Arg<T>
	// 
	// ------------------------------------------------------------------------
	
	template <>
	struct Arg<Str>
	{
		Str value;
		Arg(const Str& value) : value(value) {}
		static ptrdiff_t Write(char* dest, size_t size, const Arg<Str>& self);
	};
	
	template <>
	struct Arg<StrId>
	{
		StrId value;
		Arg(const StrId& value) : value(value) {}
		static ptrdiff_t Write(char* dest, size_t size, const Arg<StrId>& self);
	};
	
	// ------------------------------------------------------------------------
	// 
	// Path Arg<T>
	// 
	// ------------------------------------------------------------------------
	
	template <>
	struct Arg<Path>
	{
		Path value;
		Arg(const Path& value) : value(value) {}
		static ptrdiff_t Write(char* dest, size_t size, const Arg<Path>& self);
	};
	
	// ------------------------------------------------------------------------
	// 
	// vec Arg<T>
	// 
	// ------------------------------------------------------------------------
	
	template <>
	struct Arg<vec2f>
	{
		vec2f value;
		u32   width;
		u32   precision;
		
		Arg(const vec2f& value, u32 width = 0, u32 precision = 2)
			: value(value), width(width), precision(precision) {}
		static ptrdiff_t Write(char* dest, size_t size, const Arg<vec2f>& self);
	};
	
	template <>
	struct Arg<vec3f>
	{
		vec3f value;
		u32   width;
		u32   precision;
		
		Arg(const vec3f& value, u32 width = 0, u32 precision = 2)
			: value(value), width(width), precision(precision) {}
		static ptrdiff_t Write(char* dest, size_t size, const Arg<vec3f>& self);
	};
	
	// ------------------------------------------------------------------------
	// 
	// pnt Arg<T>
	// 
	// ------------------------------------------------------------------------
	
	template <>
	struct Arg<pnt2u>
	{
		pnt2u value;
		u32   width;
		
		Arg(const pnt2u& value, u32 width = 0)
			: value(value), width(width) {}
		static ptrdiff_t Write(char* dest, size_t size, const Arg<pnt2u>& self);
	};
	
	template <>
	struct Arg<pnt2s>
	{
		pnt2s value;
		u32   width;
		
		Arg(const pnt2s& value, u32 width = 0)
			: value(value), width(width) {}
		static ptrdiff_t Write(char* dest, size_t size, const Arg<pnt2s>& self);
	};
	
	template <>
	struct Arg<pnt2f>
	{
		pnt2f value;
		u32   width;
		u32   precision;
		
		Arg(const pnt2f& value, u32 width = 0, u32 precision = 2)
			: value(value), width(width), precision(precision) {}
		static ptrdiff_t Write(char* dest, size_t size, const Arg<pnt2f>& self);
	};
	
	// ------------------------------------------------------------------------
	// 
	// rect Arg<T>
	// 
	// ------------------------------------------------------------------------
	
	template <>
	struct Arg<rectu>
	{
		rectu value;
		u32   width;
		
		Arg(const rectu& value, u32 width = 0)
			: value(value), width(width) {}
		static ptrdiff_t Write(char* dest, size_t size, const Arg<rectu>& self);
	};
	
	template <>
	struct Arg<rects>
	{
		rects value;
		u32   width;
		
		Arg(const rects& value, u32 width = 0)
			: value(value), width(width) {}
		static ptrdiff_t Write(char* dest, size_t size, const Arg<rects>& self);
	};
	
	template <>
	struct Arg<rectf>
	{
		rectf value;
		u32   width;
		u32   precision;
		
		Arg(const rectf& value, u32 width = 0, u32 precision = 2)
			: value(value), width(width), precision(precision) {}
		static ptrdiff_t Write(char* dest, size_t size, const Arg<rectf>& self);
	};

// ------------------------------------------------------------------------
// 
// format_detail
// 
// ------------------------------------------------------------------------

namespace format_detail
{
	// ------------------------------------------------------------------------
	// 
	// WriteArg
	// 
	// ------------------------------------------------------------------------
	
	inline ptrdiff_t WriteArg(...)
	{
		BME_ASSERT_MSG(false, "incorrect format");
		return -1;
	}
	
	template < typename T >
	inline ptrdiff_t WriteArg(char* dest, size_t size, const Arg<T>& arg)
	{
		return Arg<T>::Write(dest, size, arg);
	}
	
	// ------------------------------------------------------------------------
	// 
	// FormatImpl
	// 
	// ------------------------------------------------------------------------
	
	template
	<
		typename T1, typename T2, typename T3, typename T4, typename T5,
		typename T6, typename T7, typename T8, typename T9
	>
	inline const char* FormatImpl(
		char* dest, size_t size, const char* format,
		T1 v1, T2 v2, T3 v3, T4 v4, T5 v5, T6 v6, T7 v7, T8 v8, T9 v9)
	{
		BME_ASSERT(dest && size && format);
		
		const char* begin_dest = dest;
		const char*   end_dest = dest + size - 1;
		
		while ( *format ) {
			if ( dest >= end_dest) {
				BME_ASSERT_MSG(false, "dest buffer too small");
				break;
			}
			
			if ( *format == '%' ) {
				const char* begin_p = format;
				const char*   end_p = format;
				
				while ( *++end_p != '%' ) {
					// "Hello%1"
					if ( !(*end_p) ) {
						goto INCORRECT_FORMAT_TAG;
					}
				}
				
				ptrdiff_t p = end_p - begin_p;
				if ( p == 2 ) {
					ptrdiff_t write_arg_ret = 0;
					format += 3;
					
					size_t lost_dest_size = end_dest - dest;
					
					switch ( *++begin_p ) {
					case '1':
						write_arg_ret = WriteArg(dest, lost_dest_size, v1);
						break;
					case '2':
						write_arg_ret = WriteArg(dest, lost_dest_size, v2);
						break;
					case '3':
						write_arg_ret = WriteArg(dest, lost_dest_size, v3);
						break;
					case '4':
						write_arg_ret = WriteArg(dest, lost_dest_size, v4);
						break;
					case '5':
						write_arg_ret = WriteArg(dest, lost_dest_size, v5);
						break;
					case '6':
						write_arg_ret = WriteArg(dest, lost_dest_size, v6);
						break;
					case '7':
						write_arg_ret = WriteArg(dest, lost_dest_size, v7);
						break;
					case '8':
						write_arg_ret = WriteArg(dest, lost_dest_size, v8);
						break;
					case '9':
						write_arg_ret = WriteArg(dest, lost_dest_size, v9);
						break;
					default:
						goto INCORRECT_FORMAT_TAG;
					}
					
					if ( write_arg_ret < 0 ) {
						BME_ASSERT_MSG(false, "write argument error");
						write_arg_ret = 0;
					}
					
					size_t write_bytes = Math::SmartCast<size_t>(write_arg_ret);
					dest += lost_dest_size < write_bytes ? lost_dest_size : write_bytes;
				} else {
					if ( p > 2 ) {
						goto INCORRECT_FORMAT_TAG;
					} else {
						if ( *end_p ) {
							// "Hel%%lo" -> "Hel%lo"
							*dest++ = *format++;
						} else {
							goto INCORRECT_FORMAT_TAG;
						}
					}
					++format;
				}
			} else {
				*dest++ = *format++;
			}
		}
		
		*dest = '\0';
		return begin_dest;
		
	// ------------------------------------
	INCORRECT_FORMAT_TAG:
	// ------------------------------------
		
		dest -= (dest-begin_dest);
		BME_snprintf(dest, size, "%s", "incorrect format");
		
		BME_ASSERT_MSG(false, "incorrect format");
		return begin_dest;
	}

} // namespace format_detail

} // namespace Strings
} // namespace bme
