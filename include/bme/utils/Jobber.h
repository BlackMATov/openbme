/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/utils/Jobber.h
 *****************************************************************************/

#pragma once

#include "Thread.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// JobberTask
// 
// ----------------------------------------------------------------------------

class JobberTask : public RefCounted
{
	AtomicFlag     _ready;
	RecursiveMutex _mutex;
	
protected:
	
	virtual void   _run         ();
	virtual void   _reset       ();
	virtual void   _done        ();
	
public:
	
	JobberTask();
	virtual ~JobberTask();
	
	void           Run          ();
	void           Reset        ();
	void           Wait         ();
	bool           isReady      () const;
};

// ----------------------------------------------------------------------------
// 
// Jobber
// 
// ----------------------------------------------------------------------------

class Jobber : public RefCounted
{
	void           _threadMain  ();
	static void    _threadMain  ( void* arg );
	void           _wakeUp      ( bool all );
	bool           _processTask ();
	JobberTaskPtr  _getNextTask ();
	bool           _isSkipAfter () const;
	bool           _isCanceled  () const;
	
private:
	
	typedef cpp::vector<ThreadPtr>::type TThreads;
	typedef cpp::circular_buffer<JobberTaskPtr>::type TTasks;
	AtomicFlag     _skipAfter;
	AtomicFlag     _canceled;
	TTasks         _tasks;
	AtomicCounter  _tasksNum;
	RecursiveMutex _tasksMutex;
	TThreads       _threads;
	ConditionVar   _sleepCond;
	RecursiveMutex _sleepMutex;
	
public:
	
	typedef void (*WaitFunc)(void* data);
	
	Jobber(size_t num_threads = 1, bool skip_after = true);
	~Jobber();
	
	void           WaitAll      ();
	void           WaitAll      ( WaitFunc func, void* data );
	void           AddTask      ( const JobberTaskPtr& task );
	bool           isEmpty      () const;
};

} // namespace bme
