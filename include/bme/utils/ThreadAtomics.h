/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/utils/ThreadAtomics.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// AtomicCounter
// 
// ----------------------------------------------------------------------------

class AtomicCounter
{
	volatile s32 _value;
	
public:
	
	AtomicCounter();
	AtomicCounter(const AtomicCounter& other);
	AtomicCounter& operator=(const AtomicCounter& other);
	~AtomicCounter();
	
	explicit AtomicCounter(s32 value);
	AtomicCounter& operator=(s32 value);
	
	s32  operator ++ ();
	s32  operator ++ (int);
	s32  operator -- ();
	s32  operator -- (int);
	
	s32  get         () const;
	void set         (s32 value);
};

// ----------------------------------------------------------------------------
// 
// AtomicFlag
// 
// ----------------------------------------------------------------------------

class AtomicFlag
{
	AtomicCounter _value;
	
public:
	
	AtomicFlag();
	AtomicFlag(const AtomicFlag& other);
	AtomicFlag& operator=(const AtomicFlag& other);
	~AtomicFlag();
	
	explicit AtomicFlag(bool value);
	AtomicFlag& operator=(bool value);
	
	bool is    () const;
	void set   ();
	void reset ();
};

} // namespace bme
