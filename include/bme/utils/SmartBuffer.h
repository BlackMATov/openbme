/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/utils/SmartBuffer.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

class SmartBuffer
{
	class Data;
	typedef cpp::intrusive_ptr<Data> DataPtr;
	DataPtr _data;
	
	struct dummy { void nonnull() {} };
	typedef void (dummy::*safe_bool)();
	
public:
	
	SmartBuffer              ( size_t size );
	SmartBuffer              ( const void* src, size_t size );
	
	SmartBuffer              ();
	SmartBuffer              ( const SmartBuffer& other );
	SmartBuffer& operator =  ( const SmartBuffer& other );
	~SmartBuffer             ();
	
	operator     safe_bool   () const;
	bool         operator!   () const;
	
	void*        Allocate    ( size_t size );
	void         Release     ();
	
	void         Clear       ( s32 value );
	bool         IsMemEquals ( const SmartBuffer& other ) const;
	bool         IsMemEquals ( const void* buffer, size_t size ) const;
	
	void*        getBuffer   ();
	const void*  getBuffer   () const;
	size_t       getSize     () const;
};

// ----------------------------------------------------------------------------
// 
// Global operators
// 
// ----------------------------------------------------------------------------

inline bool operator==(const SmartBuffer& a, const SmartBuffer& b)
{
	return a.getBuffer() == b.getBuffer();
}

inline bool operator!=(const SmartBuffer& a, const SmartBuffer& b)
{
	return a.getBuffer() != b.getBuffer();
}

inline bool operator<(const SmartBuffer& a, const SmartBuffer& b)
{
	return a.getBuffer() < b.getBuffer();
}

inline bool operator<=(const SmartBuffer& a, const SmartBuffer& b)
{
	return a.getBuffer() <= b.getBuffer();
}

inline bool operator>(const SmartBuffer& a, const SmartBuffer& b)
{
	return a.getBuffer() > b.getBuffer();
}

inline bool operator>=(const SmartBuffer& a, const SmartBuffer& b)
{
	return a.getBuffer() >= b.getBuffer();
}

} // namespace bme
