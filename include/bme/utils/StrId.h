/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/utils/StrId.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

class StrId
{
	typedef Str TData;
	TData _data;
	
public:
	
	StrId                  ( const char* str );
	StrId                  ( const char* str, size_t n );
	
	StrId                  ();
	StrId                  ( const StrId& other );
	StrId&      operator = ( const StrId& other );
	
	const char* data       () const;
	const char* c_str      () const;
	bool        empty      () const;
	size_t      size       () const;
	size_t      length     () const;
};

// ----------------------------------------------------------------------------
// 
// Global operators
// 
// ----------------------------------------------------------------------------

inline bool operator==(const StrId& a, const StrId& b)
{
	return 0 == BME_strcmp(a.c_str(), b.c_str());
}

inline bool operator!=(const StrId& a, const StrId& b)
{
	return 0 != BME_strcmp(a.c_str(), b.c_str());
}

inline bool operator<(const StrId& a, const StrId& b)
{
	return BME_strcmp(a.c_str(), b.c_str()) < 0;
}

inline bool operator<=(const StrId& a, const StrId& b)
{
	return BME_strcmp(a.c_str(), b.c_str()) <= 0;
}

inline bool operator>(const StrId& a, const StrId& b)
{
	return BME_strcmp(a.c_str(), b.c_str()) > 0;
}

inline bool operator>=(const StrId& a, const StrId& b)
{
	return BME_strcmp(a.c_str(), b.c_str()) >= 0;
}

} // namespace bme
