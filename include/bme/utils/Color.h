/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/utils/Color.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

class Color
{
	f32          _r, _g, _b, _a;
	mutable u32  _argb;
	mutable bool _invalideARGB;
	u32          _updateARGB() const;
	
public:
	
	static const Color black;
	static const Color white;
	static const Color red;
	static const Color green;
	static const Color blue;
	static const Color yellow;
	static const Color magenta;
	static const Color cyan;
	
	Color                                 ();
	Color                                 ( u32 argb );
	Color                                 ( f32 r, f32 g, f32 b, f32 a = 1.f );
	Color                                 ( const Color& other );
	Color&      operator =                ( const Color& other );
	
	void        set                       ( u32 argb );
	void        set                       ( f32 r, f32 g, f32 b, f32 a = 1.f );
	u32         getARGB                   () const;
	
	void        setR                      ( f32 v );
	void        setG                      ( f32 v );
	void        setB                      ( f32 v );
	void        setA                      ( f32 v );
	
	f32         getR                      () const;
	f32         getG                      () const;
	f32         getB                      () const;
	f32         getA                      () const;
	
	static void G8_To_X8R8G8B8            ( u32* dest, const pnt2u& dest_size, const  u8* src, const pnt2u& src_size );
	static void GA8_To_A8R8G8B8           ( u32* dest, const pnt2u& dest_size, const  u8* src, const pnt2u& src_size );
	static void RGB8_To_X8R8G8B8          ( u32* dest, const pnt2u& dest_size, const  u8* src, const pnt2u& src_size );
	static void RGBA8_To_A8R8G8B8         ( u32* dest, const pnt2u& dest_size, const  u8* src, const pnt2u& src_size );
	
	static void G8_To_RGB8                (  u8* dest, const pnt2u& dest_size, const  u8* src, const pnt2u& src_size );
	static void GA8_To_RGBA8              (  u8* dest, const pnt2u& dest_size, const  u8* src, const pnt2u& src_size );
	static void X8R8G8B8_To_RGB8          (  u8* dest, const pnt2u& dest_size, const u32* src, const pnt2u& src_size );
	static void A8R8G8B8_To_RGBA8         (  u8* dest, const pnt2u& dest_size, const u32* src, const pnt2u& src_size );
	
	static void RGB8_To_RGB8              (  u8* dest, const pnt2u& dest_size, const  u8* src, const pnt2u& src_size );
	static void RGB8_To_RGBA8             (  u8* dest, const pnt2u& dest_size, const  u8* src, const pnt2u& src_size );
	static void RGBA8_To_RGB8             (  u8* dest, const pnt2u& dest_size, const  u8* src, const pnt2u& src_size );
	static void RGBA8_To_RGBA8            (  u8* dest, const pnt2u& dest_size, const  u8* src, const pnt2u& src_size );
	static void X8R8G8B8_To_X8R8G8B8      ( u32* dest, const pnt2u& dest_size, const u32* src, const pnt2u& src_size );
	static void A8R8G8B8_To_A8R8G8B8      ( u32* dest, const pnt2u& dest_size, const u32* src, const pnt2u& src_size );
	
	static u32* InPlace_RGBA8_To_A8R8G8B8 (  u8* dest_src, const pnt2u& size );
	static u8*  InPlace_A8R8G8B8_To_RGBA8 ( u32* dest_src, const pnt2u& size );
};

// ----------------------------------------------------------------------------
// 
// Global operators
// 
// ----------------------------------------------------------------------------

// ------------------------------------
// Color (== != < <= > >=)
// ------------------------------------

inline bool operator==(const Color& a, const Color& b)
{
	return
		Math::IsEquals(a.getR(), b.getR()) &&
		Math::IsEquals(a.getG(), b.getG()) &&
		Math::IsEquals(a.getB(), b.getB()) &&
		Math::IsEquals(a.getA(), b.getA());
}

inline bool operator!=(const Color& a, const Color& b)
{
	return
		!Math::IsEquals(a.getR(), b.getR()) ||
		!Math::IsEquals(a.getG(), b.getG()) ||
		!Math::IsEquals(a.getB(), b.getB()) ||
		!Math::IsEquals(a.getA(), b.getA());
}

inline bool operator<(const Color& a, const Color& b)
{
	return a.getARGB() < b.getARGB();
}

inline bool operator<=(const Color& a, const Color& b)
{
	return a.getARGB() <= b.getARGB();
}

inline bool operator>(const Color& a, const Color& b)
{
	return a.getARGB() > b.getARGB();
}

inline bool operator>=(const Color& a, const Color& b)
{
	return a.getARGB() >= b.getARGB();
}

// ------------------------------------
// Color (+-*) scalar
// ------------------------------------

inline Color operator+(const Color& c, f32 v)
{
	return Color(
		c.getR() + v,
		c.getG() + v,
		c.getB() + v,
		c.getA() + v);
}

inline Color operator-(const Color& c, f32 v)
{
	return Color(
		c.getR() - v,
		c.getG() - v,
		c.getB() - v,
		c.getA() - v);
}

inline Color operator*(const Color& c, f32 v)
{
	return Color(
		c.getR() * v,
		c.getG() * v,
		c.getB() * v,
		c.getA() * v);
}

// ------------------------------------
// Color (+-*) Color
// ------------------------------------

inline Color operator+(const Color& a, const Color& b)
{
	return Color(
		a.getR() + b.getR(),
		a.getG() + b.getG(),
		a.getB() + b.getB(),
		a.getA() + b.getA());
}

inline Color operator-(const Color& a, const Color& b)
{
	return Color(
		a.getR() - b.getR(),
		a.getG() - b.getG(),
		a.getB() - b.getB(),
		a.getA() - b.getA());
}

inline Color operator*(const Color& a, const Color& b)
{
	return Color(
		a.getR() * b.getR(),
		a.getG() * b.getG(),
		a.getB() * b.getB(),
		a.getA() * b.getA());
}

// ----------------------------------------------------------------------------
// 
// Impl
// 
// ----------------------------------------------------------------------------

inline u32 Color::_updateARGB() const
{
	_argb =
		(u8(getA() * 255.f + 0.5f) << 24) +
		(u8(getR() * 255.f + 0.5f) << 16) +
		(u8(getG() * 255.f + 0.5f) <<  8) +
		(u8(getB() * 255.f + 0.5f));
	_invalideARGB = false;
	return _argb;
}

inline Color::Color()
{
	set(1.f, 1.f, 1.f, 1.f);
}

inline Color::Color(u32 argb)
{
	set(argb);
}

inline Color::Color(f32 r, f32 g, f32 b, f32 a)
{
	set(r, g, b, a);
}

inline Color::Color(const Color& other)
{
	set(other.getR(), other.getG(), other.getB(), other.getA());
}

inline Color& Color::operator=(const Color& other)
{
	if ( this != &other ) {
		set(other.getR(), other.getG(), other.getB(), other.getA());
	}
	return *this;
}

inline void Color::set(u32 argb)
{
	_a            = ((argb >> 24)       ) / 255.f;
	_r            = ((argb >> 16) & 0xFF) / 255.f;
	_g            = ((argb >>  8) & 0xFF) / 255.f;
	_b            = ((argb      ) & 0xFF) / 255.f;
	_argb         = argb;
	_invalideARGB = false;
}

inline void Color::set(f32 r, f32 g, f32 b, f32 a)
{
	_r = Math::Clamp(r, 0.f, 1.f);
	_g = Math::Clamp(g, 0.f, 1.f);
	_b = Math::Clamp(b, 0.f, 1.f);
	_a = Math::Clamp(a, 0.f, 1.f);
	_invalideARGB = true;
}

inline u32 Color::getARGB() const
{
	return _invalideARGB ? _updateARGB() : _argb;
}

inline void Color::setR(f32 v)
{
	_r = Math::Clamp(v, 0.f, 1.f);
	_invalideARGB = true;
}

inline void Color::setG(f32 v)
{
	_g = Math::Clamp(v, 0.f, 1.f);
	_invalideARGB = true;
}

inline void Color::setB(f32 v)
{
	_b = Math::Clamp(v, 0.f, 1.f);
	_invalideARGB = true;
}

inline void Color::setA(f32 v)
{
	_a = Math::Clamp(v, 0.f, 1.f);
	_invalideARGB = true;
}

inline f32 Color::getR() const
{
	return _r;
}

inline f32 Color::getG() const
{
	return _g;
}

inline f32 Color::getB() const
{
	return _b;
}

inline f32 Color::getA() const
{
	return _a;
}

} // namespace bme
