/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/utils/Randomizer.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

class Randomizer
{
	mutable u32 _seed;
	
	s32 _generateImpl(s32 min, s32 max) const;
	f32 _generateImpl(f32 min, f32 max) const;
	
	template < typename T >
	T _generateImpl(T min, T max, const cpp::true_type& /*is_integral*/) const;
	template < typename T >
	T _generateImpl(T min, T max, const cpp::false_type& /*is_integral*/) const;
	
public:
	
	Randomizer();
	Randomizer(const Randomizer& other);
	Randomizer& operator=(const Randomizer& other);
	~Randomizer();
	
	void setSeed  ( u32 value );
	u32  getSeed  () const;
	
	template < typename T >
	T    Generate ( T min, T max ) const;
};

// ----------------------------------------------------------------------------
// 
// Impl
// 
// ----------------------------------------------------------------------------

template < typename T >
T Randomizer::_generateImpl(T min, T max, const cpp::true_type& /*is_integral*/) const
{
	return _generateImpl(
		Math::SmartCast<s32>(min), Math::SmartCast<s32>(max));
}

template < typename T >
T Randomizer::_generateImpl(T min, T max, const cpp::false_type& /*is_integral*/) const
{
	return _generateImpl(
		Math::SmartCast<f32>(min), Math::SmartCast<f32>(max));
}

template < typename T >
T Randomizer::Generate(T min, T max) const
{
	BME_ASSERT(min <= max);
	BME_STATIC_ASSERT(cpp::is_arithmetic<T>::value);
	return Math::IsEquals(min, max) ?
		min : _generateImpl(min, max, cpp::is_integral<T>());
}

} // namespace bme
