/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/utils/Path.h
 *****************************************************************************/

#pragma once

#include "StrId.h"
#include "Thread.h"

namespace bme
{

class Path
{
	typedef cpp::map<StrId, Path>::type TAliases;
	static TAliases       _s_aliases;
	static RecursiveMutex _s_mutex;
	static void           _s_apply_aliases( Str& dest );
	
	Str  _data;
	void _normalize();
	
public:
	
	static const Str exe_dir;
	static const Str res_dir;
	static const Str wrk_dir;
	static const Str app_dir;
	
	Path                         ( const char*    path );
	Path                         ( const wchar_t* path );
	
	Path                         ( const Str&     path );
	Path                         ( const WStr&    path );
	
	Path                         ();
	Path                         ( const Path& other );
	Path&       operator =       ( const Path& other );
	Path&       operator /=      ( const Path& other );
	
	void        Clear            ();
	bool        isEmpty          () const;
	
	Path&       RemoveFilename   ();
	Path&       RemoveExtension  ();
	Path&       ReplaceFilename  ( const Str& source );
	Path&       ReplaceExtension ( const Str& source );
	
	Str         getFilename      () const;
	Str         getExtension     () const;
	Path        getParentPath    () const;
	
	bool        isHasFilename    () const;
	bool        isHasExtension   () const;
	bool        isHasParentPath  () const;
	
	bool        isAbsolute       () const;
	bool        isRelative       () const;
	
	const Str&  getNativePath    () const;
	WStr        getNativeWPath   () const;
	
	static void BindAlias        ( const StrId& id, const Path& path );
	static bool IsHasAlias       ( const StrId& id );
};

// ----------------------------------------------------------------------------
// 
// Global operators
// 
// ----------------------------------------------------------------------------

inline Path operator/(const Path& a, const Path& b)
{
	return Path(a) /= b;
}

inline bool operator==(const Path& a, const Path& b)
{
	return a.getNativePath() == b.getNativePath();
}

inline bool operator!=(const Path& a, const Path& b)
{
	return a.getNativePath() != b.getNativePath();
}

inline bool operator<(const Path& a, const Path& b)
{
	return a.getNativePath() < b.getNativePath();
}

inline bool operator<=(const Path& a, const Path& b)
{
	return a.getNativePath() <= b.getNativePath();
}

inline bool operator>(const Path& a, const Path& b)
{
	return a.getNativePath() > b.getNativePath();
}

inline bool operator>=(const Path& a, const Path& b)
{
	return a.getNativePath() >= b.getNativePath();
}

} // namespace bme
