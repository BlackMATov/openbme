/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/utils/Holder.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

template < typename T >
class Holder : private cpp::noncopyable
{
	static T* _hostage;
	
public:
	
	Holder();
	virtual ~Holder();
	static T* getHostage();
};

// ----------------------------------------------------------------------------
// 
// Impl
// 
// ----------------------------------------------------------------------------

template < typename T >
T* Holder<T>::_hostage = NULL;

template < typename T >
Holder<T>::Holder()
{
	BME_ASSERT_MSG(!_hostage, "this holder already created");
	_hostage = (T*)(this);
}

template < typename T >
Holder<T>::~Holder()
{
	_hostage = NULL;
}

template < typename T >
T* Holder<T>::getHostage()
{
	return _hostage;
}

} // namespace bme
