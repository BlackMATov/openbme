/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/utils/RefCounted.h
 *****************************************************************************/

#pragma once

#include "ThreadAtomics.h"

namespace bme
{

class RefCounted : private cpp::noncopyable
{
	mutable AtomicCounter _counter;
	
public:
	
	RefCounted();
	virtual ~RefCounted();
	
	s32  AddRef      () const;
	bool Release     () const;
	s32  getRefCount () const;
};

// ----------------------------------------------------------------------------
// 
// for boost::intrusive_ptr
// 
// ----------------------------------------------------------------------------

inline void intrusive_ptr_add_ref(const RefCounted* obj)
{
	obj->AddRef();
}

inline void intrusive_ptr_release(const RefCounted* obj)
{
	obj->Release();
}

// ----------------------------------------------------------------------------
// 
// Impl
// 
// ----------------------------------------------------------------------------

inline RefCounted::RefCounted()
: _counter(0)
{
}

inline RefCounted::~RefCounted()
{
}

inline s32 RefCounted::AddRef() const
{
	return ++_counter;
}

inline bool RefCounted::Release() const
{
	BME_ASSERT(_counter.get() > 0);
	if ( --_counter <= 0 ) {
		delete this;
		return true;
	}
	return false;
}

inline s32 RefCounted::getRefCount() const
{
	return _counter.get();
}

} // namespace bme
