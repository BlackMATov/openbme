/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/utils/StringsFormat.h
 *****************************************************************************/

#pragma once

#include "StringsFormatImpl.h"

namespace bme
{
namespace Strings
{

// ----------------------------------------------------------------------------
// 
// 'Format' code gen (tools/scripts/string_format.lua)
// 
// ----------------------------------------------------------------------------

template
<
	typename T1
>
inline const char* Format(char* dest, size_t size, const char* format,
	const Arg<T1>& v1)
{
	return format_detail::FormatImpl(dest, size, format,
	v1, 0, 0, 0, 
	0, 0, 0, 0, 
	0);
}

template
<
	typename T1, typename T2
>
inline const char* Format(char* dest, size_t size, const char* format,
	const Arg<T1>& v1, const Arg<T2>& v2)
{
	return format_detail::FormatImpl(dest, size, format,
	v1, v2, 0, 0, 
	0, 0, 0, 0, 
	0);
}

template
<
	typename T1, typename T2, typename T3
>
inline const char* Format(char* dest, size_t size, const char* format,
	const Arg<T1>& v1, const Arg<T2>& v2, const Arg<T3>& v3)
{
	return format_detail::FormatImpl(dest, size, format,
	v1, v2, v3, 0, 
	0, 0, 0, 0, 
	0);
}

template
<
	typename T1, typename T2, typename T3, typename T4
>
inline const char* Format(char* dest, size_t size, const char* format,
	const Arg<T1>& v1, const Arg<T2>& v2, const Arg<T3>& v3, const Arg<T4>& v4)
{
	return format_detail::FormatImpl(dest, size, format,
	v1, v2, v3, v4, 
	0, 0, 0, 0, 
	0);
}

template
<
	typename T1, typename T2, typename T3, typename T4, typename T5
>
inline const char* Format(char* dest, size_t size, const char* format,
	const Arg<T1>& v1, const Arg<T2>& v2, const Arg<T3>& v3, const Arg<T4>& v4, 
	const Arg<T5>& v5)
{
	return format_detail::FormatImpl(dest, size, format,
	v1, v2, v3, v4, 
	v5, 0, 0, 0, 
	0);
}

template
<
	typename T1, typename T2, typename T3, typename T4, typename T5, 
	typename T6
>
inline const char* Format(char* dest, size_t size, const char* format,
	const Arg<T1>& v1, const Arg<T2>& v2, const Arg<T3>& v3, const Arg<T4>& v4, 
	const Arg<T5>& v5, const Arg<T6>& v6)
{
	return format_detail::FormatImpl(dest, size, format,
	v1, v2, v3, v4, 
	v5, v6, 0, 0, 
	0);
}

template
<
	typename T1, typename T2, typename T3, typename T4, typename T5, 
	typename T6, typename T7
>
inline const char* Format(char* dest, size_t size, const char* format,
	const Arg<T1>& v1, const Arg<T2>& v2, const Arg<T3>& v3, const Arg<T4>& v4, 
	const Arg<T5>& v5, const Arg<T6>& v6, const Arg<T7>& v7)
{
	return format_detail::FormatImpl(dest, size, format,
	v1, v2, v3, v4, 
	v5, v6, v7, 0, 
	0);
}

template
<
	typename T1, typename T2, typename T3, typename T4, typename T5, 
	typename T6, typename T7, typename T8
>
inline const char* Format(char* dest, size_t size, const char* format,
	const Arg<T1>& v1, const Arg<T2>& v2, const Arg<T3>& v3, const Arg<T4>& v4, 
	const Arg<T5>& v5, const Arg<T6>& v6, const Arg<T7>& v7, const Arg<T8>& v8)
{
	return format_detail::FormatImpl(dest, size, format,
	v1, v2, v3, v4, 
	v5, v6, v7, v8, 
	0);
}

template
<
	typename T1, typename T2, typename T3, typename T4, typename T5, 
	typename T6, typename T7, typename T8, typename T9
>
inline const char* Format(char* dest, size_t size, const char* format,
	const Arg<T1>& v1, const Arg<T2>& v2, const Arg<T3>& v3, const Arg<T4>& v4, 
	const Arg<T5>& v5, const Arg<T6>& v6, const Arg<T7>& v7, const Arg<T8>& v8, 
	const Arg<T9>& v9)
{
	return format_detail::FormatImpl(dest, size, format,
	v1, v2, v3, v4, 
	v5, v6, v7, v8, 
	v9);
}

template
<
	typename T1
>
inline const char* Format(char* dest, size_t size, const char* format,
	const T1& v1)
{
	return Format(dest, size, format,
	Arg<T1>(v1));
}

template
<
	typename T1, typename T2
>
inline const char* Format(char* dest, size_t size, const char* format,
	const T1& v1, const T2& v2)
{
	return Format(dest, size, format,
	Arg<T1>(v1), Arg<T2>(v2));
}

template
<
	typename T1, typename T2, typename T3
>
inline const char* Format(char* dest, size_t size, const char* format,
	const T1& v1, const T2& v2, const T3& v3)
{
	return Format(dest, size, format,
	Arg<T1>(v1), Arg<T2>(v2), Arg<T3>(v3));
}

template
<
	typename T1, typename T2, typename T3, typename T4
>
inline const char* Format(char* dest, size_t size, const char* format,
	const T1& v1, const T2& v2, const T3& v3, const T4& v4)
{
	return Format(dest, size, format,
	Arg<T1>(v1), Arg<T2>(v2), Arg<T3>(v3), Arg<T4>(v4));
}

template
<
	typename T1, typename T2, typename T3, typename T4, typename T5
>
inline const char* Format(char* dest, size_t size, const char* format,
	const T1& v1, const T2& v2, const T3& v3, const T4& v4, 
	const T5& v5)
{
	return Format(dest, size, format,
	Arg<T1>(v1), Arg<T2>(v2), Arg<T3>(v3), Arg<T4>(v4), 
	Arg<T5>(v5));
}

template
<
	typename T1, typename T2, typename T3, typename T4, typename T5, 
	typename T6
>
inline const char* Format(char* dest, size_t size, const char* format,
	const T1& v1, const T2& v2, const T3& v3, const T4& v4, 
	const T5& v5, const T6& v6)
{
	return Format(dest, size, format,
	Arg<T1>(v1), Arg<T2>(v2), Arg<T3>(v3), Arg<T4>(v4), 
	Arg<T5>(v5), Arg<T6>(v6));
}

template
<
	typename T1, typename T2, typename T3, typename T4, typename T5, 
	typename T6, typename T7
>
inline const char* Format(char* dest, size_t size, const char* format,
	const T1& v1, const T2& v2, const T3& v3, const T4& v4, 
	const T5& v5, const T6& v6, const T7& v7)
{
	return Format(dest, size, format,
	Arg<T1>(v1), Arg<T2>(v2), Arg<T3>(v3), Arg<T4>(v4), 
	Arg<T5>(v5), Arg<T6>(v6), Arg<T7>(v7));
}

template
<
	typename T1, typename T2, typename T3, typename T4, typename T5, 
	typename T6, typename T7, typename T8
>
inline const char* Format(char* dest, size_t size, const char* format,
	const T1& v1, const T2& v2, const T3& v3, const T4& v4, 
	const T5& v5, const T6& v6, const T7& v7, const T8& v8)
{
	return Format(dest, size, format,
	Arg<T1>(v1), Arg<T2>(v2), Arg<T3>(v3), Arg<T4>(v4), 
	Arg<T5>(v5), Arg<T6>(v6), Arg<T7>(v7), Arg<T8>(v8));
}

template
<
	typename T1, typename T2, typename T3, typename T4, typename T5, 
	typename T6, typename T7, typename T8, typename T9
>
inline const char* Format(char* dest, size_t size, const char* format,
	const T1& v1, const T2& v2, const T3& v3, const T4& v4, 
	const T5& v5, const T6& v6, const T7& v7, const T8& v8, 
	const T9& v9)
{
	return Format(dest, size, format,
	Arg<T1>(v1), Arg<T2>(v2), Arg<T3>(v3), Arg<T4>(v4), 
	Arg<T5>(v5), Arg<T6>(v6), Arg<T7>(v7), Arg<T8>(v8), 
	Arg<T9>(v9));
}

// ----------------------------------------------------------------------------
// 
// 'RFormat' code gen (tools/scripts/string_rformat.lua)
// 
// ----------------------------------------------------------------------------

template
<
	typename TRet, size_t MaxLen,
	typename T1
>
inline TRet RFormat(const char* format,
	const T1& v1)
{
	char buffer[MaxLen];
	return Format(buffer, MaxLen, format,
	v1);
}

template
<
	typename TRet, size_t MaxLen,
	typename T1, typename T2
>
inline TRet RFormat(const char* format,
	const T1& v1, const T2& v2)
{
	char buffer[MaxLen];
	return Format(buffer, MaxLen, format,
	v1, v2);
}

template
<
	typename TRet, size_t MaxLen,
	typename T1, typename T2, typename T3
>
inline TRet RFormat(const char* format,
	const T1& v1, const T2& v2, const T3& v3)
{
	char buffer[MaxLen];
	return Format(buffer, MaxLen, format,
	v1, v2, v3);
}

template
<
	typename TRet, size_t MaxLen,
	typename T1, typename T2, typename T3, typename T4
>
inline TRet RFormat(const char* format,
	const T1& v1, const T2& v2, const T3& v3, const T4& v4)
{
	char buffer[MaxLen];
	return Format(buffer, MaxLen, format,
	v1, v2, v3, v4);
}

template
<
	typename TRet, size_t MaxLen,
	typename T1, typename T2, typename T3, typename T4, typename T5
>
inline TRet RFormat(const char* format,
	const T1& v1, const T2& v2, const T3& v3, const T4& v4, 
	const T5& v5)
{
	char buffer[MaxLen];
	return Format(buffer, MaxLen, format,
	v1, v2, v3, v4, 
	v5);
}

template
<
	typename TRet, size_t MaxLen,
	typename T1, typename T2, typename T3, typename T4, typename T5, 
	typename T6
>
inline TRet RFormat(const char* format,
	const T1& v1, const T2& v2, const T3& v3, const T4& v4, 
	const T5& v5, const T6& v6)
{
	char buffer[MaxLen];
	return Format(buffer, MaxLen, format,
	v1, v2, v3, v4, 
	v5, v6);
}

template
<
	typename TRet, size_t MaxLen,
	typename T1, typename T2, typename T3, typename T4, typename T5, 
	typename T6, typename T7
>
inline TRet RFormat(const char* format,
	const T1& v1, const T2& v2, const T3& v3, const T4& v4, 
	const T5& v5, const T6& v6, const T7& v7)
{
	char buffer[MaxLen];
	return Format(buffer, MaxLen, format,
	v1, v2, v3, v4, 
	v5, v6, v7);
}

template
<
	typename TRet, size_t MaxLen,
	typename T1, typename T2, typename T3, typename T4, typename T5, 
	typename T6, typename T7, typename T8
>
inline TRet RFormat(const char* format,
	const T1& v1, const T2& v2, const T3& v3, const T4& v4, 
	const T5& v5, const T6& v6, const T7& v7, const T8& v8)
{
	char buffer[MaxLen];
	return Format(buffer, MaxLen, format,
	v1, v2, v3, v4, 
	v5, v6, v7, v8);
}

template
<
	typename TRet, size_t MaxLen,
	typename T1, typename T2, typename T3, typename T4, typename T5, 
	typename T6, typename T7, typename T8, typename T9
>
inline TRet RFormat(const char* format,
	const T1& v1, const T2& v2, const T3& v3, const T4& v4, 
	const T5& v5, const T6& v6, const T7& v7, const T8& v8, 
	const T9& v9)
{
	char buffer[MaxLen];
	return Format(buffer, MaxLen, format,
	v1, v2, v3, v4, 
	v5, v6, v7, v8, 
	v9);
}

} // namespace Strings
} // namespace bme

#define BME_Path_Make(MaxLen, format, ...) \
	bme::Strings::RFormat<bme::Path, MaxLen>(format, __VA_ARGS__)

#define BME_StrId_Make(MaxLen, format, ...) \
	bme::Strings::RFormat<bme::StrId, MaxLen>(format, __VA_ARGS__)

#define BME_Str_Make(MaxLen, format, ...) \
	bme::Strings::RFormat<bme::Str, MaxLen>(format, __VA_ARGS__)
