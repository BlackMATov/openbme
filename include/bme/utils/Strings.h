/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/utils/Strings.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{
namespace Strings
{
	// --------------------------------
	// Unicode conversions
	// --------------------------------
	
	bool MakeUtf8  ( Str&  dest, const char*    src );
	bool MakeUtf8  ( Str&  dest, const wchar_t* src );
	bool MakeUtf8  ( Str&  dest, const u32*     src );
	
	bool MakeWide  ( WStr& dest, const char*    src );
	bool MakeWide  ( WStr& dest, const wchar_t* src );
	bool MakeWide  ( WStr& dest, const u32*     src );
	
	bool MakeUtf32 ( UStr& dest, const char*    src );
	bool MakeUtf32 ( UStr& dest, const wchar_t* src );
	bool MakeUtf32 ( UStr& dest, const u32*     src );
	
	// --------------------------------
	// Wildcards
	// --------------------------------
	
	bool WildcardCheck ( const char* wild, const char* string );
	
	// --------------------------------
	// Numeric conversions
	// --------------------------------
	
	bool ParseToType ( pnt2u& dest, const char* str );
	bool ParseToType ( pnt2s& dest, const char* str );
	bool ParseToType ( pnt2f& dest, const char* str );
	bool ParseToType ( rectu& dest, const char* str );
	bool ParseToType ( rects& dest, const char* str );
	bool ParseToType ( rectf& dest, const char* str );
	bool ParseToType ( vec2f& dest, const char* str );
	bool ParseToType ( vec3f& dest, const char* str );
	bool ParseToType ( Color& dest, const char* str );
	
	bool ParseToType ( bool&  dest, const char* str );
	bool ParseToType ( Str&   dest, const char* str );
	bool ParseToType ( StrId& dest, const char* str );
	bool ParseToType ( Path&  dest, const char* str );
	
	// f32 f64 and other floating point types
	
	template < typename T >
	typename cpp::enable_if<cpp::is_floating_point<T>, bool>::type
		ParseToType(T& dest, const char* str)
	{
		if ( !str || !str[0] ) {
			return false;
		}
		dest = Math::SmartCast<T>(BME_strtod(str, NULL));
		return true;
	}
	
	// s8, s16, s32 and other integral signed types
	
	template < typename T >
	typename cpp::enable_if<cpp::is_signed<T>, bool>::type
		ParseToType(T& dest, const char* str)
	{
		if ( !str || !str[0] ) {
			return false;
		}
		dest = Math::SmartCast<T>(BME_strtol(str, NULL, 10));
		return true;
	}
	
	// u8, u16, u32 and other integral unsigned types
	
	template < typename T >
	typename cpp::enable_if<cpp::is_unsigned<T>, bool>::type
		ParseToType(T& dest, const char* str)
	{
		if ( !str || !str[0] ) {
			return false;
		}
		ptrdiff_t tmp = 0;
		if ( !ParseToType(tmp, str) || tmp < 0 ) {
			return false;
		}
		dest = Math::SmartCast<T>(tmp);
		return true;
	}
	
	// common ReturnParseType function with default value
	
	template < typename T >
	T ReturnParseType(const T& def, const char* str)
	{
		T ret_value = def;
		ParseToType(ret_value, str);
		return ret_value;
	}
	
	// --------------------------------
	// Base64 conversions
	// --------------------------------
	
	bool Base64Encode ( Str&         dest, const void* src, size_t size );
	bool Base64Decode ( SmartBuffer& dest, const char* src, size_t size );
} // namespace Strings
} // namespace bme
