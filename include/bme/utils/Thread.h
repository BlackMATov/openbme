/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/utils/Thread.h
 *****************************************************************************/

#pragma once

#include "RefCounted.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// Thread
// 
// ----------------------------------------------------------------------------

class Thread : public RefCounted
{
	class Impl;
	typedef cpp::intrusive_ptr<Impl> TImpl;
	TImpl _impl;
	
public:
	
	typedef void (*TFunction)(void*);
	typedef void* TFunctionArg;
	
	Thread                      ();
	Thread                      ( TFunction func, TFunctionArg arg );
	~Thread                     ();
	
	void          Join          ();
	bool          isJoinable    () const;
	
	static void   YieldThis     ();
	static void   SleepThis     ( size_t ms );
	static size_t GetProcessors ();
};

// ----------------------------------------------------------------------------
// 
// Mutex
// 
// ----------------------------------------------------------------------------

class Mutex : private cpp::noncopyable
{
	class Impl;
	typedef cpp::intrusive_ptr<Impl> TImpl;
	TImpl _impl;
	friend class ConditionVar;
	
public:
	
	Mutex();
	~Mutex();
	void Lock   () const;
	void Unlock () const;
};

// ----------------------------------------------------------------------------
// 
// RecursiveMutex
// 
// ----------------------------------------------------------------------------

class RecursiveMutex : private cpp::noncopyable
{
	class Impl;
	typedef cpp::intrusive_ptr<Impl> TImpl;
	TImpl _impl;
	friend class ConditionVar;
	
public:
	
	RecursiveMutex();
	~RecursiveMutex();
	void Lock   () const;
	void Unlock () const;
};

// ----------------------------------------------------------------------------
// 
// ConditionVar
// 
// ----------------------------------------------------------------------------

class ConditionVar : private cpp::noncopyable
{
	class Impl;
	typedef cpp::intrusive_ptr<Impl> TImpl;
	TImpl _impl;
	
public:
	
	ConditionVar();
	~ConditionVar();
	
	void Wait      ( Mutex& mutex );
	void Wait      ( RecursiveMutex& mutex );
	void NotifyOne ();
	void NotifyAll ();
};

// ----------------------------------------------------------------------------
// 
// MutexLockGuard
// 
// ----------------------------------------------------------------------------

template < typename T >
class MutexLockGuard : private cpp::noncopyable
{
	const T* _mutex;
	
public:
	
	MutexLockGuard()
	: _mutex(NULL)
	{
	}
	
	MutexLockGuard(const T& mutex)
		: _mutex(&mutex)
	{
		if ( _mutex ) {
			_mutex->Lock();
		}
	}
	
	~MutexLockGuard()
	{
		if ( _mutex ) {
			_mutex->Unlock();
		}
	}
};

} // namespace bme
