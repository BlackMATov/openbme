/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/bme_main.h
 *****************************************************************************/

#pragma once

#include "core/Application.h"

// ----------------------------------------------------------------------------
//
// FOR USER IMPL
//
// ----------------------------------------------------------------------------

void BMEMain();

// ----------------------------------------------------------------------------
//
// BME START IMPL
//
// ----------------------------------------------------------------------------

void BMEStart(const bme::Application::Desc& desc);
void BMEShutdown();

template < typename T >
void BMEStart(const bme::Application::Desc& desc)
{
	new T();
	BMEStart(desc);
}
