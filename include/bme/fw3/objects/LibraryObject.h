/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/LibraryObject.h
 *****************************************************************************/

#pragma once

#include "../base.h"

namespace bme
{
namespace fw3
{

class LibraryObject : public RefCounted
{
	StrId           _name;
	LibraryAssetPtr _asset;
	
protected:
	
	LibraryObject();
	LibraryObject(const LibraryAssetPtr& asset);
	
public:
	
	virtual ~LibraryObject();
	
	void                   setName  ( const StrId& value );
	const StrId&           getName  () const;
	const LibraryAssetPtr& getAsset () const;
};

} // namespace fw3
} // namespace bme
