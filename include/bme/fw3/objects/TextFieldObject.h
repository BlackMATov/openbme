/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/TextFieldObject.h
 *****************************************************************************/

#pragma once

#include "DisplayObject.h"
#include "../assets/BMFontAsset.h"
#include "../common/QuadHelper.h"

namespace bme
{
namespace fw3
{

class TextFieldObject : public DisplayObject
{
public:
	
	enum E_HALIGN
	{
		HALIGN_LEFT,
		HALIGN_CENTER,
		HALIGN_RIGHT,
		HALIGN_JUSTIFY_LEFT,
		HALIGN_JUSTIFY_CENTER,
		HALIGN_JUSTIFY_RIGHT,
		HALIGN_JUSTIFY_FULL
	};
	
	enum E_VALIGN
	{
		VALIGN_TOP,
		VALIGN_CENTER,
		VALIGN_BOTTOM,
		VALIGN_JUSTIFY
	};
	
private:
	
	Str      _text;
	UStr     _utext;
	Color    _color;
	bool     _smoothing;
	E_HALIGN _halign;
	E_VALIGN _valign;
	vec2f    _fixedSize;
	f32      _lineSpacing;
	
	struct GlyphDesc
	{
		const FontAsset::Glyph* glyph;
		f32 kern;
		GlyphDesc()
		: glyph(NULL), kern(0.f) {}
	};
	
	struct StringDesc
	{
		f32    width;
		size_t start;
		size_t length;
		size_t space_count;
		bool   justifiable;
		StringDesc( size_t start )
		: width(0.f), start(start), length(0)
		, space_count(0), justifiable(false) {}
	};
	
	typedef cpp::vector <GlyphDesc>::type TGlyphs;
	typedef cpp::vector<StringDesc>::type TStrings;
	typedef cpp::vector<QuadHelper>::type TQuads;
	
	FontAsset::Info      _info;
	mutable rectf        _selfBounds;
	mutable f32          _internalScale;
	
	mutable TQuads       _quads;
	mutable TGlyphs      _glyphs;
	mutable TStrings     _strings;
	mutable f32          _maxStringWidth;
	mutable bool         _invalidQuads;
	mutable bool         _invalidGlyphs;
	mutable bool         _invalidStrings;
	void                 _updateQuads() const;
	void                 _updateGlyphs() const;
	void                 _updateStrings() const;
	void                 _invalidateText(bool quads, bool glyphs, bool strings);
	void                 _checkInvalidateText() const;
	
	f32                  _calcHAlignmentOffset(f32 str_width) const;
	f32                  _calcVAlignmentOffset(f32* out_text_height) const;
	f32                  _getStringHeight() const;
	f32                  _getMaxStringWidth() const;
	f32                  _getMaxStringHeight() const;
	
protected:
	
	virtual void         _invalidateLocalTransform();
	virtual void         _invalidateGlobalTransform();
	
	virtual void         _updateSelf(f32 dt);
	virtual void         _renderSelf();
	virtual const rectf& _getSelfBounds() const;
	
	TextFieldObject(const LibraryAssetPtr& asset);
	
public:
	
	static TextFieldObjectPtr Create(const LibraryAssetPtr& asset);
	static TextFieldObjectPtr Create(const BMFontAsset::Desc& desc);
	virtual ~TextFieldObject();
	
	void                 setText        ( const Str& value );
	const Str&           getText        () const;
	
	void                 setColor       ( const Color& value );
	const Color&         getColor       () const;
	
	void                 setSmoothing   ( bool yesno );
	bool                 isSmoothing    () const;
	
	void                 setHAlignment  ( E_HALIGN value );
	void                 setVAlignment  ( E_VALIGN value );
	void                 setAlignment   ( E_HALIGN halign, E_VALIGN valign );
	
	E_HALIGN             getHAlignment  () const;
	E_VALIGN             getVAlignment  () const;
	
	void                 setFixedWidth  ( f32 value );
	void                 setFixedHeight ( f32 value );
	void                 setFixedSize   ( f32 w, f32 h );
	void                 setFixedSize   ( const vec2f& value );
	
	f32                  getFixedWidth  () const;
	f32                  getFixedHeight () const;
	const vec2f&         getFixedSize   () const;
	
	void                 setLineSpacing ( f32 value );
	f32                  getLineSpacing () const;
};

} // namespace fw3
} // namespace bme
