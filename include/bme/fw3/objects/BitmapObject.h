/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/BitmapObject.h
 *****************************************************************************/

#pragma once

#include "DisplayObject.h"
#include "../assets/BitmapAsset.h"
#include "../common/QuadHelper.h"

namespace bme
{
namespace fw3
{

class BitmapObject : public DisplayObject
{
	Color                _color;
	bool                 _smoothing;
	TexturePtr           _texture;
	rectf                _texcoords;
	
	mutable QuadHelper   _quad;
	mutable bool         _invalidQuad;
	bool                 _updateQuad() const;
	
	rectf                _selfBounds;
	void                 _updateBounds();
	
protected:
	
	virtual void         _invalidateLocalTransform();
	virtual void         _invalidateGlobalTransform();
	
	virtual void         _updateSelf(f32 dt);
	virtual void         _renderSelf();
	virtual const rectf& _getSelfBounds() const;
	
	BitmapObject(const LibraryAssetPtr& asset);
	
public:
	
	static BitmapObjectPtr Create(const LibraryAssetPtr& asset);
	static BitmapObjectPtr Create(const BitmapAsset::Desc& desc);
	virtual ~BitmapObject();
	
	void                 setColor     ( const Color& value );
	const Color&         getColor     () const;
	
	void                 setSmoothing ( bool yesno );
	bool                 isSmoothing  () const;
	
	void                 setTexture   ( const TexturePtr& texture,
	                                    const rectf& texcoords = rectf::zero);
	const TexturePtr&    getTexture   () const;
	const rectf&         getTexCoords () const;
};

} // namespace fw3
} // namespace bme
