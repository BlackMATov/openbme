/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/StageObject.h
 *****************************************************************************/

#pragma once

#include "DisplayObject.h"

namespace bme
{
namespace fw3
{

class StageObject
	: public DisplayObject
	, public Receiver
{
	// Receiver override ------------------------------------------------------
	bool OnEvent(const Receiver::Event& event);
	// ------------------------------------------------------------------------
	
protected:
	
	virtual void _postRenderSelf();
	virtual void _postUpdateSelf(f32 dt);
	StageObject();
	
public:
	
	static StageObjectPtr Create();
	virtual ~StageObject();
};

} // namespace fw3
} // namespace bme
