/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/DisplayObject.h
 *****************************************************************************/

#pragma once

#include "LibraryObject.h"

namespace bme
{
namespace fw3
{

class DisplayObject : public LibraryObject
{
public:
	
	struct Transform
	{
		mat4f                 matrix;
		f32                   alpha;
		bool                  visible;
		RenderStates::E_BLEND blend;
		
		Transform();
		Transform operator* (const Transform& other) const;
		bool      operator==(const Transform& other) const;
		bool      operator!=(const Transform& other) const;
	};
	
	struct ChildDef
	{
		DisplayObjectPtr child;
		ChildDef(const DisplayObjectPtr& child);
		bool operator==(const ChildDef& rhs) const;
		bool operator!=(const ChildDef& rhs) const;
	};
	typedef cpp::list<ChildDef>::type TChildren;
	
private:
	
	vec2f                 _position;
	vec2f                 _scale;
	f32                   _rotation;
	
	mutable rectf         _localBounds;
	mutable rectf         _globalBounds;
	mutable bool          _invalidLocalBounds;
	mutable bool          _invalidGlobalBounds;
	void                  _updateLocalBounds() const;
	void                  _updateGlobalBounds() const;
	
	mutable Transform     _localTransform;
	mutable Transform     _globalTransform;
	mutable bool          _invalidLocalTransform;
	mutable bool          _invalidGlobalTransform;
	void                  _updateLocalTransform() const;
	void                  _updateGlobalTransform() const;
	
	DisplayObject*        _parent;
	TChildren             _children;
	size_t                _childrenNum;
	void                  _setParent(DisplayObject* parent);
	
protected:
	
	virtual void          _invalidateLocalBounds();
	virtual void          _invalidateGlobalBounds();
	virtual void          _invalidateLocalTransform();
	virtual void          _invalidateGlobalTransform();
	
	virtual void          _localBoundsUpdated() const;
	virtual void          _globalBoundsUpdated() const;
	virtual void          _localTransformUpdated() const;
	virtual void          _globalTransformUpdated() const;
	
	virtual void          _renderSelf();
	virtual void          _updateSelf(f32 dt);
	virtual void          _postRenderSelf();
	virtual void          _postUpdateSelf(f32 dt);
	virtual const rectf&  _getSelfBounds() const;
	
	DisplayObject();
	DisplayObject(const LibraryAssetPtr& asset);
	
public:
	
	static const size_t bad_index;
	
	static DisplayObjectPtr Create();
	virtual ~DisplayObject();
	
	// Events
	
	void                  Render             ();
	void                  Update             ( f32 dt );
	
	// transformation
	
	const Transform&      getLocalTransform  () const;
	const Transform&      getGlobalTransform () const;
	
	// bounds
	
	const rectf&          getSelfBounds      () const;
	const rectf&          getLocalBounds     () const;
	const rectf&          getGlobalBounds    () const;
	
	// position
	
	f32                   getX               () const;
	f32                   getY               () const;
	const vec2f&          getPosition        () const;
	
	void                  setX               ( f32 value );
	void                  setY               ( f32 value );
	void                  setPosition        ( f32 x, f32 y );
	void                  setPosition        ( const vec2f& value );
	
	void                  MoveX              ( f32 value );
	void                  MoveY              ( f32 value );
	void                  Move               ( f32 x, f32 y );
	void                  Move               ( const vec2f& value );
	
	// scale
	
	f32                   getScaleX          () const;
	f32                   getScaleY          () const;
	const vec2f&          getScale           () const;
	
	void                  setScaleX          ( f32 value );
	void                  setScaleY          ( f32 value );
	void                  setScale           ( f32 scale );
	void                  setScale           ( f32 x, f32 y );
	void                  setScale           ( const vec2f& value );
	
	void                  ScaleX             ( f32 value );
	void                  ScaleY             ( f32 value );
	void                  Scale              ( f32 scale );
	void                  Scale              ( f32 x, f32 y );
	void                  Scale              ( const vec2f& value );
	
	// rotation
	
	f32                   getRotation        () const;
	void                  setRotation        ( f32 value );
	void                  Rotate             ( f32 value );
	
	// size
	
	f32                   getWidth           () const;
	f32                   getHeight          () const;
	vec2f                 getSize            () const;
	
	void                  setWidth           ( f32 value );
	void                  setHeight          ( f32 value );
	void                  setSize            ( f32 w, f32 h );
	void                  setSize            ( const vec2f& value );
	
	void                  ResizeWidth        ( f32 value );
	void                  ResizeHeight       ( f32 value );
	void                  Resize             ( f32 x, f32 y );
	void                  Resize             ( const vec2f& value );
	
	// visible
	
	f32                   getAlpha           () const;
	bool                  isVisible          () const;
	RenderStates::E_BLEND getBlend           () const;
	
	void                  setAlpha           ( f32 value );
	void                  setVisible         ( bool yesno );
	void                  setBlend           ( RenderStates::E_BLEND blend );
	
	// mouse
	
	f32                   getMouseX          () const;
	f32                   getMouseY          () const;
	vec2f                 getMousePosition   () const;
	
	// methods
	
	vec2f                 GlobalToLocal      ( const vec2f& point ) const;
	vec2f                 LocalToGlobal      ( const vec2f& point ) const;
	
	// container
	
	DisplayObjectPtr      getRoot            () const;
	DisplayObjectPtr      getParent          () const;
	
	void                  AddChild           ( const DisplayObjectPtr& child );
	bool                  RemoveChild        ( const DisplayObjectPtr& child );
	void                  RemoveChildren     ();
	void                  RemoveFromParent   ();
	
	DisplayObjectPtr      GetChildByName     ( const StrId& name, bool recursive ) const;
	DisplayObjectPtr      GetChildByIndex    ( size_t index ) const;
	size_t                GetChildIndex      ( const DisplayObjectPtr& child ) const;
	bool                  IsHasChild         ( const DisplayObjectPtr& child, bool recursive ) const;
	
	const TChildren&      getChildren        () const;
	size_t                getNumChildren     ( bool recursive = false ) const;
};

} // namespace fw3
} // namespace bme
