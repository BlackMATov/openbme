/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/XmlParser.h
 *****************************************************************************/

#pragma once

#include "../base.h"

// library from depends
#include <pugixml/pugixml.hpp>

namespace bme
{
namespace fw3
{
	// --------------------------------
	// LoadXmlDocument
	// --------------------------------
	
	bool LoadXmlDocument ( const Path& path,
	                       pugi::xml_document& out_doc);
	
	bool LoadXmlDocument ( const SmartBuffer& mem,
	                       pugi::xml_document& out_doc);
	
	bool LoadXmlDocument ( const void* data, size_t size,
	                       pugi::xml_document& out_doc);
	
	// --------------------------------
	// GetXmlAttribute
	// --------------------------------
	
	void GetXmlAttribute ( const pugi::xml_node& node,
	                       const char* name,
	                       bool& value, bool def);
	
	void GetXmlAttribute ( const pugi::xml_node& node,
	                       const char* name,
	                       Str& value, const Str& def);
	
	void GetXmlAttribute ( const pugi::xml_node& node,
	                       const char* name,
	                       WStr& value, const WStr& def);
	
	void GetXmlAttribute ( const pugi::xml_node& node,
	                       const char* name,
	                       Path& value, const Path& def);
	
	void GetXmlAttribute ( const pugi::xml_node& node,
	                       const char* name,
	                       StrId& value, const StrId& def);
	
	void GetXmlAttribute ( const pugi::xml_node& node,
	                       const char* name,
	                       Material::E_ADDRESS& value,
	                       Material::E_ADDRESS def);
	
	void GetXmlAttribute ( const pugi::xml_node& node,
	                       const char* name,
	                       RenderStates::E_BLEND& value,
	                       RenderStates::E_BLEND def);
	
	template < typename T >
	void GetXmlAttribute ( const pugi::xml_node& node,
	                       const char* name,
	                       T& value, T def)
	{
		pugi::xml_attribute attr = node.attribute(name);
		value = attr.empty()
			? def
			: Strings::ReturnParseType(def, attr.value());
	}
} // namespace fw3
} // namespace bme

#define BME_GET_XML_ATTRIBUTE(node, name, value) \
	bme::fw3::GetXmlAttribute((node), (name), (value), (value));
