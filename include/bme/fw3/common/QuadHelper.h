/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/QuadHelper.h
 *****************************************************************************/

#pragma once

#include "../base.h"

namespace bme
{
namespace fw3
{

class QuadHelper
{
public:
	
	Quad quad;
	
	// texture coords
	
	void CalculateTexCoords(
		const rectf& coords);
	
	void CalculateTexCoords(
		const vec2f& coords, const vec2f& size);
	
	void CalculateTexCoords(
		f32 x0, f32 y0, f32 x1, f32 y1, f32 x2, f32 y2, f32 x3, f32 y3);
	
	// vertices
	
	void CalculateVertices(
		f32 x0, f32 y0, f32 x1, f32 y1);
	
	void CalculateVertices(
		f32 x0, f32 y0, f32 x1, f32 y1, f32 x2, f32 y2, f32 x3, f32 y3);
	
	void CalculateVertices(
		const vec2f& pos, const vec2f& size);
	
	void CalculateVertices(
		const vec2f& pos, const vec2f& size,
		f32 rot, const vec2f& hotSpot,
		const vec2f& scale, bool flip_x, bool flip_y);
	
	void CalculateVertices(
		const mat4f& matrix, const vec2f& size);
	
	void CalculateVertices(
		const mat4f& matrix, const vec2f& size,
		const vec2f& hotSpot, bool flip_x, bool flip_y);
	
	// colors
	
	void CalculateColors(
		const Color& color);
	
	void CalculateColors(
		const Color& c1, const Color& c2,
		const Color& c3, const Color& c4);
	
	// render
	
	void Render() const;
};

} // namespace fw3
} // namespace bme
