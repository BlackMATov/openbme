/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/ExcelXmlTable.h
 *****************************************************************************/

#pragma once

#include "../base.h"

namespace bme
{
namespace fw3
{

// ----------------------------------------------------------------------------
// 
// ExcelXmlCell
// 
// ----------------------------------------------------------------------------

template < typename T >
class ExcelXmlCell
{
	typedef cpp::weak_ptr<ExcelXmlTable> TOwner;
	TOwner         _owner;
	StrId          _worksheet;
	T              _default;
	pnt2u          _position;
	mutable T      _value;
	mutable size_t _lastReload;
	void           _updateValue () const;
	
public:
	
	ExcelXmlCell();
	ExcelXmlCell(const ExcelXmlCell& other);
	ExcelXmlCell& operator=(const ExcelXmlCell& other);
	~ExcelXmlCell();
	
	ExcelXmlCell(const ExcelXmlTablePtr& owner, const StrId& worksheet,
	             const T& def, const pnt2u& position);
	ExcelXmlCell(const ExcelXmlTablePtr& owner, const StrId& worksheet,
	             const T& def, size_t row, size_t column);
	
	size_t         getRow       () const;
	size_t         getColumn    () const;
	const pnt2u&   getPosition  () const;
	const StrId&   getWorksheet () const;
	
	const T&       getValue     () const;
	const T&       getDefault   () const;
};

// ----------------------------------------------------------------------------
// 
// ExcelXmlTable
// 
// ----------------------------------------------------------------------------

class ExcelXmlTable : private cpp::noncopyable
{
public:
	
	typedef cpp::vector<StrId>::type TWorksheets;
	
private:
	
	struct CellDesc
	{
		pnt2u cell;
		StrId data;
		CellDesc(const pnt2u& cell, const StrId& data = StrId());
		CellDesc(u32 row, u32 column, const StrId& data = StrId());
		bool operator<(const CellDesc& other) const;
	};
	typedef cpp::vector<CellDesc>::type TCells;
	
	struct WorksheetDesc
	{
		TCells cells;
		pnt2u  maxcell;
	};
	typedef cpp::map<StrId, WorksheetDesc>::type TWorksheetsDescs;
	
	TWorksheets        _worksheetsNames;
	TWorksheetsDescs   _worksheetsDescs;
	Path               _path;
	size_t             _reloadCount;
	
private:
	
	ExcelXmlTable( const Path& path );
	
public:
	
	virtual ~ExcelXmlTable();
	static ExcelXmlTablePtr Create(const Path& path);
	
	bool               Reload         ( const StrId& worksheet = StrId() );
	
	const Path&        getPath        () const;
	size_t             getReloadCount () const;
	const TWorksheets& getWorksheets  () const;
	
	StrId              GetCellData    ( const StrId& worksheet,
	                                    const pnt2u& position ) const;
	StrId              GetCellData    ( const StrId& worksheet,
	                                    size_t row, size_t column ) const;
	
	size_t             getMaxRow      ( const StrId& worksheet ) const;
	size_t             getMaxColumn   ( const StrId& worksheet ) const;
	const pnt2u&       getMaxCell     ( const StrId& worksheet ) const;
};

// ----------------------------------------------------------------------------
// 
// ExcelXmlCell impl
// 
// ----------------------------------------------------------------------------

template < typename T >
void ExcelXmlCell<T>::_updateValue() const
{
	ExcelXmlTablePtr owner = _owner.lock();
	if ( owner ) {
		size_t owner_reload_count = owner->getReloadCount();
		if ( owner_reload_count != _lastReload ) {
			StrId str_value = owner->GetCellData(getWorksheet(), getPosition());
			_value = Strings::ReturnParseType(getDefault(), str_value.c_str());
			_lastReload = owner_reload_count;
		}
	} else {
		_value = getDefault();
		_lastReload = size_t(-1);
	}
}

template < typename T >
ExcelXmlCell<T>::ExcelXmlCell()
: _lastReload(size_t(-1))
{
}

template < typename T >
ExcelXmlCell<T>::ExcelXmlCell(const ExcelXmlCell& other)
{
	_owner      = other._owner;
	_worksheet  = other._worksheet;
	_default    = other._default;
	_position   = other._position;
	_value      = other._value;
	_lastReload = other._lastReload;
}

template < typename T >
ExcelXmlCell<T>& ExcelXmlCell<T>::operator=(const ExcelXmlCell& other)
{
	if ( this != &other ) {
		_owner      = other._owner;
		_worksheet  = other._worksheet;
		_default    = other._default;
		_position   = other._position;
		_value      = other._value;
		_lastReload = other._lastReload;
	}
	return *this;
}

template < typename T >
ExcelXmlCell<T>::~ExcelXmlCell()
{
}

template < typename T >
ExcelXmlCell<T>::ExcelXmlCell(
	const ExcelXmlTablePtr& owner, const StrId& worksheet,
	const T& def, const pnt2u& position)
: _owner(owner), _worksheet(worksheet)
, _default(def), _position(position)
, _value(def), _lastReload(size_t(-1))
{
}

template < typename T >
ExcelXmlCell<T>::ExcelXmlCell(
	const ExcelXmlTablePtr& owner, const StrId& worksheet,
	const T& def, size_t row, size_t column)
: _owner(owner), _worksheet(worksheet)
, _default(def), _position(column, row)
, _value(def), _lastReload(size_t(-1))
{
}

template < typename T >
size_t ExcelXmlCell<T>::getRow() const
{
	return _position.y;
}

template < typename T >
size_t ExcelXmlCell<T>::getColumn() const
{
	return _position.x;
}

template < typename T >
const pnt2u& ExcelXmlCell<T>::getPosition() const
{
	return _position;
}

template < typename T >
const StrId& ExcelXmlCell<T>::getWorksheet() const
{
	return _worksheet;
}

template < typename T >
const T& ExcelXmlCell<T>::getValue() const
{
	_updateValue();
	return _value;
}

template < typename T >
const T& ExcelXmlCell<T>::getDefault() const
{
	return _default;
}

} // namespace fw3
} // namespace bme
