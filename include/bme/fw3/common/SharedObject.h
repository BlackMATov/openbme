/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/SharedObject.h
 *****************************************************************************/

#pragma once

#include "../base.h"

namespace bme
{
namespace fw3
{

class SharedObject : public RefCounted
{
public:
	
	enum E_VALUE
	{
		VALUE_INT,
		VALUE_FLOAT,
		VALUE_VEC2,
		VALUE_VEC3,
		VALUE_COLOR,
		VALUE_TEXT,
		VALUE_BIN
	};
	
	virtual ~SharedObject() {}
	static SharedObjectPtr Get();
	
	virtual void         Clear       () = 0;
	virtual void         Flush       () const = 0;
	
	virtual void         setSection  ( const StrId& section ) = 0;
	virtual const StrId& getSection  () const = 0;
	
	virtual bool         RemoveValue ( const StrId& name, E_VALUE type ) = 0;
	virtual bool         IsHasValue  ( const StrId& name, E_VALUE type ) const = 0;
	
	virtual s32          GetInt      ( const StrId& name ) const = 0;
	virtual f32          GetFloat    ( const StrId& name ) const = 0;
	virtual vec2f        GetVec2     ( const StrId& name ) const = 0;
	virtual vec3f        GetVec3     ( const StrId& name ) const = 0;
	virtual Color        GetColor    ( const StrId& name ) const = 0;
	virtual Str          GetString   ( const StrId& name ) const = 0;
	virtual SmartBuffer  GetBin      ( const StrId& name ) const = 0;
	
	virtual void         SetInt      ( const StrId& name, s32                value ) = 0;
	virtual void         SetFloat    ( const StrId& name, f32                value ) = 0;
	virtual void         SetVec2     ( const StrId& name, const vec2f&       value ) = 0;
	virtual void         SetVec3     ( const StrId& name, const vec3f&       value ) = 0;
	virtual void         SetColor    ( const StrId& name, const Color&       value ) = 0;
	virtual void         SetString   ( const StrId& name, const Str&         value ) = 0;
	virtual void         SetBin      ( const StrId& name, const SmartBuffer& value ) = 0;
};

} // namespace fw3
} // namespace bme
