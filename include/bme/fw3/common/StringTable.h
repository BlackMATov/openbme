/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/StringTable.h
 *****************************************************************************/

#pragma once

#include "../base.h"

namespace bme
{
namespace fw3
{

class StringTable : public RefCounted
{
private:
	
	typedef cpp::map   <StrId,StrId>::type TStrings;  // id | value
	typedef cpp::map<StrId,TStrings>::type TSections; // idSection | strings
	
	mutable TSections _sections;
	void              _load(const ExcelXmlTablePtr& table);
	
private:
	
	StringTable();
	
public:
	
	virtual ~StringTable();
	static StringTablePtr Create(const Path& path);
	
	const StrId& GetString(const StrId& id, const StrId& section) const;
};

} // namespace fw3
} // namespace bme
