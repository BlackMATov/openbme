/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/all.h
 *****************************************************************************/

#pragma once

#include "base.h"

#include "assets/BitmapAsset.h"
#include "assets/BMFontAsset.h"
#include "assets/FontAsset.h"
#include "assets/Library.h"
#include "assets/LibraryAsset.h"

#include "common/ExcelXmlTable.h"
#include "common/QuadHelper.h"
#include "common/SharedObject.h"
#include "common/StringTable.h"
#include "common/XmlParser.h"

#include "objects/BitmapObject.h"
#include "objects/DisplayObject.h"
#include "objects/LibraryObject.h"
#include "objects/StageObject.h"
#include "objects/TextFieldObject.h"
