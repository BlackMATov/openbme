/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/FontAsset.h
 *****************************************************************************/

#pragma once

#include "LibraryAsset.h"

namespace bme
{
namespace fw3
{

class FontAsset : public LibraryAsset
{
public:
	
	struct Desc : public LibraryAsset::Desc
	{
		Path path;
		bool smoothing;
		Desc() : smoothing(true) {}
	};
	
	typedef u32 TChar;
	
	struct Info
	{
		f32          base;
		f32          lineHeight;
		pnt2u        pageSize;
		size_t       pages;
		
		Info()
		: base       (0.f)
		, lineHeight (0.f)
		, pageSize   (pnt2u::zero)
		, pages      (0) {}
	};
	
	struct Glyph
	{
		TChar        id;
		rectf        pageCoords;
		vec2f        offset;
		f32          advance;
		size_t       page;
		
		Glyph()
		: id         (0)
		, pageCoords (rectf::zero)
		, offset     (vec2f::zero)
		, advance    (0.f)
		, page       (0) {}
	};
	
private:
	
	// preload
	Path                 _path;
	bool                 _smoothing;
	bool                 _checkPreLoad () const;
	
protected:
	
	virtual bool         _loadSelf     ();
	virtual void         _freeSelf     ();
	virtual bool         _preLoadSelf  ( const Desc& desc );
	virtual bool         _preLoadSelf  ( const pugi::xml_node& node,
	                                     const LibraryPtr& library );
	
	FontAsset();
	
public:
	
	virtual ~FontAsset();
	
	const Path&          getPath       () const;
	bool                 isSmoothing   () const;
	
	virtual const Info&  getInfo       () const = 0;
	virtual const Glyph* FindGlyph     ( TChar symbol ) const = 0;
	virtual TexturePtr   FindPage      ( size_t page ) const = 0;
	virtual ptrdiff_t    FindKerning   ( const Glyph* first,
	                                     const Glyph* second ) const = 0;
};

} // namespace fw3
} // namespace bme
