/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/BMFontAsset.h
 *****************************************************************************/

#pragma once

#include "FontAsset.h"

namespace bme
{
namespace fw3
{

class BMFontAsset : public FontAsset
{
public:
	
	struct Desc : public FontAsset::Desc
	{
		Desc() {}
	};
	
private:
	
	struct BMGlyph : public Glyph
	{
		size_t firstKerning;
		BMGlyph(TChar glyph_id = 0);
		bool operator<(const BMGlyph& other) const;
	};
	
	struct KerningPair
	{
		TChar first;
		TChar second;
		ptrdiff_t amount;
		KerningPair(TChar first = 0, TChar second = 0, ptrdiff_t amount = 0);
		bool operator<(const KerningPair& other) const;
	};
	
	typedef cpp::vector <TexturePtr>::type TPages;
	typedef cpp::vector    <BMGlyph>::type TGlyphs;
	typedef cpp::vector<KerningPair>::type TKerningPairs;
	
	// load
	Info          _info;
	TPages        _pages;
	TGlyphs       _glyphs;
	TKerningPairs _kerningPairs;
	bool          _parse       ();
	void          _clear       ();
	
protected:
	
	virtual bool  _loadSelf    ();
	virtual void  _freeSelf    ();
	virtual bool  _preLoadSelf ( const Desc& desc );
	virtual bool  _preLoadSelf ( const pugi::xml_node& node,
	                             const LibraryPtr& library );
	
	BMFontAsset();
	
public:
	
	static BMFontAssetPtr Create( const Desc& desc, bool with_load );
	static BMFontAssetPtr Create( const pugi::xml_node& node,
	                              const LibraryPtr& library );
	virtual ~BMFontAsset();
	
	// FontAsset override -----------------------------------------------------
	const Info&   getInfo      () const;
	const Glyph*  FindGlyph    ( TChar symbol ) const;
	TexturePtr    FindPage     ( size_t page ) const;
	ptrdiff_t     FindKerning  ( const Glyph* first, const Glyph* second ) const;
	// ------------------------------------------------------------------------
};

} // namespace fw3
} // namespace bme
