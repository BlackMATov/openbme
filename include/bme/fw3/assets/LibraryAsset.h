/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/LibraryAsset.h
 *****************************************************************************/

#pragma once

#include "../base.h"

namespace bme
{
namespace fw3
{

class LibraryAsset : public RefCounted
{
public:
	
	struct Desc
	{
		StrId name;
		Desc() {}
	};
	
private:
	
	bool         _isLoaded;
	
	// preload
	StrId        _name;
	
protected:
	
	virtual bool _loadSelf    ();
	virtual void _freeSelf    ();
	virtual bool _preLoadSelf ( const Desc& desc );
	virtual bool _preLoadSelf ( const pugi::xml_node& node,
	                            const LibraryPtr& library );
	LibraryAsset();
	
public:
	
	virtual ~LibraryAsset();
	
	bool         Load         ();
	void         Free         ();
	bool         isLoaded     () const;
	
	const StrId& getName      () const;
};

} // namespace fw3
} // namespace bme
