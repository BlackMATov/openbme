/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/TextureAsset.h
 *****************************************************************************/

#pragma once

#include "LibraryAsset.h"

namespace bme
{
namespace fw3
{

class TextureAsset : public LibraryAsset
{
public:
	
	struct Desc : public LibraryAsset::Desc
	{
		Path path;
		Desc() {}
	};
	
private:
	
	// preload
	Path              _path;
	bool              _checkPreLoad () const;
	
	// load
	TexturePtr        _texture;
	
protected:
	
	virtual bool      _loadSelf     ();
	virtual void      _freeSelf     ();
	virtual bool      _preLoadSelf  ( const Desc& desc );
	virtual bool      _preLoadSelf  ( const pugi::xml_node& node,
	                                  const LibraryPtr& library );
	TextureAsset();
	
public:
	
	static TextureAssetPtr Create( const Desc& desc, bool with_load );
	static TextureAssetPtr Create( const pugi::xml_node& node,
	                               const LibraryPtr& library );
	virtual ~TextureAsset();
	
	const Path&       getPath       () const;
	const TexturePtr& getTexture    () const;
};

} // namespace fw3
} // namespace bme
