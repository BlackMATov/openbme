/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/BitmapAsset.h
 *****************************************************************************/

#pragma once

#include "LibraryAsset.h"

namespace bme
{
namespace fw3
{

class BitmapAsset : public LibraryAsset
{
public:
	
	struct Desc : public LibraryAsset::Desc
	{
		Path  path;
		Color color;
		bool  smoothing;
		rectf texcoords;
		Desc() : smoothing(true) {}
	};
	
private:
	
	// preload
	Color             _color;
	bool              _smoothing;
	rectf             _texcoords;
	TextureAssetPtr   _textureAsset;
	bool              _checkPreLoad () const;
	
protected:
	
	virtual bool      _loadSelf     ();
	virtual void      _freeSelf     ();
	virtual bool      _preLoadSelf  ( const Desc& desc );
	virtual bool      _preLoadSelf  ( const pugi::xml_node& node,
	                                  const LibraryPtr& library );
	BitmapAsset();
	
public:
	
	static BitmapAssetPtr Create( const Desc& desc, bool with_load );
	static BitmapAssetPtr Create( const pugi::xml_node& node,
	                              const LibraryPtr& library );
	virtual ~BitmapAsset();
	
	const Color&      getColor      () const;
	bool              isSmoothing   () const;
	const rectf&      getTexCoords  () const;
	const TexturePtr& getTexture    () const;
};

} // namespace fw3
} // namespace bme
