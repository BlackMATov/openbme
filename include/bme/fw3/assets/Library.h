/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/Library.h
 *****************************************************************************/

#pragma once

#include "LibraryAsset.h"

namespace bme
{
namespace fw3
{

class Library : public RefCounted
{
public:
	
	typedef LibraryAssetPtr (*TLoader)(const pugi::xml_node&, const LibraryPtr&);
	typedef cpp::map<StrId, TLoader        >::type TLoaders;
	typedef cpp::map<StrId, LibraryAssetPtr>::type TAssets;
	
private:
	
	Library();
	
	TAssets         _assets;
	TLoaders        _loaders;
	
	bool            _load        ( const Path& path );
	LibraryAssetPtr _findAsset   ( const StrId& name ) const;
	
	static void     _register    ( const StrId& type, TLoader loader );
	static bool     _unregister  ( const StrId& type );
	
public:
	
	static LibraryPtr Create(const Path& path);
	virtual ~Library();
	
	template < typename T >
	typename LibraryAssetTypeTraits<T>::PtrType
	                FindAsset    ( const StrId& name ) const;
	template < typename T >
	typename LibraryObjectTypeTraits<T>::PtrType
	                CreateObject ( const StrId& asset_name ) const;
	
	template < typename T >
	static void     Register     ();
	template < typename T >
	static bool     Unregister   ();
};

// ----------------------------------------------------------------------------
// 
// library_detail
// 
// ----------------------------------------------------------------------------

namespace library_detail
{
	template < typename T >
	LibraryAssetPtr CommonAssetLoader(
		const pugi::xml_node& node, const LibraryPtr& library)
	{
		return T::Create(node, library);
	}
} // namespace library_detail

// ----------------------------------------------------------------------------
// 
// library impl
// 
// ----------------------------------------------------------------------------

template < typename T >
typename LibraryAssetTypeTraits<T>::PtrType
	Library::FindAsset(const StrId& name) const
{
	LibraryAssetPtr asset = _findAsset(name);
	return cpp::dynamic_pointer_cast<T>(asset);
}

template < typename T >
typename LibraryObjectTypeTraits<T>::PtrType
	Library::CreateObject(const StrId& asset_name) const
{
	LibraryAssetPtr asset = _findAsset(asset_name);
	return asset && asset->Load()
		? T::Create(asset)
		: typename LibraryObjectTypeTraits<T>::PtrType();
}

template < typename T >
void Library::Register()
{
	_register(
		LibraryAssetTypeTraits<T>::StrType(),
		&library_detail::CommonAssetLoader<T>);
}

template < typename T >
bool Library::Unregister()
{
	return _unregister(
		LibraryAssetTypeTraits<T>::StrType());
}

} // namespace fw3
} // namespace bme
