/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/fw3/base.h
 *****************************************************************************/

#pragma once

#include "../core/all.h"

namespace pugi
{
	class xml_document;
	class xml_node;
}

namespace bme
{
namespace fw3
{
	class BitmapAsset;
	class BMFontAsset;
	class FontAsset;
	class Library;
	class LibraryAsset;
	class TextureAsset;
	
	template < typename T >
	class ExcelXmlCell;
	class ExcelXmlTable;
	class QuadHelper;
	class SharedObject;
	class StringTable;
	
	class BitmapObject;
	class DisplayObject;
	class LibraryObject;
	class StageObject;
	class TextFieldObject;
	
	typedef cpp::intrusive_ptr<BitmapAsset>     BitmapAssetPtr;
	typedef cpp::intrusive_ptr<BMFontAsset>     BMFontAssetPtr;
	typedef cpp::intrusive_ptr<FontAsset>       FontAssetPtr;
	typedef cpp::intrusive_ptr<Library>         LibraryPtr;
	typedef cpp::intrusive_ptr<LibraryAsset>    LibraryAssetPtr;
	typedef cpp::intrusive_ptr<TextureAsset>    TextureAssetPtr;
	
	typedef cpp::shared_ptr<ExcelXmlTable>      ExcelXmlTablePtr;
	typedef cpp::intrusive_ptr<SharedObject>    SharedObjectPtr;
	typedef cpp::intrusive_ptr<StringTable>     StringTablePtr;
	
	typedef cpp::intrusive_ptr<BitmapObject>    BitmapObjectPtr;
	typedef cpp::intrusive_ptr<DisplayObject>   DisplayObjectPtr;
	typedef cpp::intrusive_ptr<LibraryObject>   LibraryObjectPtr;
	typedef cpp::intrusive_ptr<StageObject>     StageObjectPtr;
	typedef cpp::intrusive_ptr<TextFieldObject> TextFieldObjectPtr;
	
	// ------------------------------------------------------------------------
	// 
	// LibraryAssetTypeTraits
	// 
	// ------------------------------------------------------------------------
	
	template < typename T >
	struct LibraryAssetTypeTraits;
	
	template <>
	struct LibraryAssetTypeTraits<TextureAsset>
	{
		typedef TextureAssetPtr PtrType;
		static const char* StrType() { return "texture"; }
	};
	
	template <>
	struct LibraryAssetTypeTraits<BitmapAsset>
	{
		typedef BitmapAssetPtr PtrType;
		static const char* StrType() { return "bitmap"; }
	};
	
	template <>
	struct LibraryAssetTypeTraits<BMFontAsset>
	{
		typedef BMFontAssetPtr PtrType;
		static const char* StrType() { return "bmfont"; }
	};
	
	// ------------------------------------------------------------------------
	// 
	// LibraryObjectTypeTraits
	// 
	// ------------------------------------------------------------------------
	
	template < typename T >
	struct LibraryObjectTypeTraits;
	
	template <>
	struct LibraryObjectTypeTraits<BitmapObject>
	{
		typedef BitmapObjectPtr PtrType;
	};
	
	template <>
	struct LibraryObjectTypeTraits<TextFieldObject>
	{
		typedef TextFieldObjectPtr PtrType;
	};
} // namespace fw3
} // namespace bme
