/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/bme.h
 *****************************************************************************/

#pragma once

// ----------------------------------------------------------------------------
//
// ALL BME INCLUDES
//
// ----------------------------------------------------------------------------

#include "bme_main.h"

#include "config/all.h"
#include "math/all.h"
#include "utils/all.h"
#include "core/all.h"
#include "fw3/all.h"

// ----------------------------------------------------------------------------
//
// BME LIBS
//
// ----------------------------------------------------------------------------

#if BME_COMPILER == BME_COMPILER_MSVC && defined(BME_LINK_3RD_PARTY_FROM_SRC)
#	if BME_RENDERER == BME_RENDERER_DX8
#		pragma comment(lib, "d3d8.lib")
#		pragma comment(lib, "dxerr8.lib")
#	endif
#	if BME_RENDERER == BME_RENDERER_OGL
#		pragma comment(lib, "opengl32.lib")
#	endif
#	if BME_SOUNDER == BME_SOUNDER_BASS
#		pragma comment(lib, "bass.lib")
#	endif
#	if BME_SYSTEM == BME_SYSTEM_WINDOWS
#		pragma comment(lib, "Winmm.lib")
#	endif
#	if BME_MODE == BME_MODE_DEBUG
#		pragma comment(lib, "BMEngine_d.lib")
#	else
#		pragma comment(lib, "BMEngine.lib")
#	endif
#endif
