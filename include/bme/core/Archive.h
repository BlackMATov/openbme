/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/core/Archive.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// Archive
// 
// ----------------------------------------------------------------------------

class Archive : public RefCounted
{
	Path                _path;
	StrId               _pass;
	
public:
	
	Archive(const Path& path, const StrId& pass);
	virtual ~Archive();
	
	const Path&         getPath        () const;
	const StrId&        getPass        () const;
	
	// for override -----------------------------------------------------------
	virtual bool        isExists       () const                   = 0;
	virtual ReadFilePtr LoadFile       ( const Path& path ) const = 0;
	virtual bool        IsFileExists   ( const Path& path ) const = 0;
	virtual bool        IsFolderExists ( const Path& path ) const = 0;
	// ------------------------------------------------------------------------
};

// ----------------------------------------------------------------------------
// 
// ZipArchive
// 
// ----------------------------------------------------------------------------

class ZipArchive : public Archive
{
	typedef cpp::map<Path, size_t>::type TPaths;
	TPaths              _files;
	TPaths              _folders;
	void                _fillPaths();
	
public:
	
	ZipArchive(const Path& path, const StrId& pass);
	virtual ~ZipArchive();
	
	// Archive override -------------------------------------------------------
	virtual bool        isExists       () const;
	virtual ReadFilePtr LoadFile       ( const Path& path ) const;
	virtual bool        IsFileExists   ( const Path& path ) const;
	virtual bool        IsFolderExists ( const Path& path ) const;
	// ------------------------------------------------------------------------
};

} // namespace bme
