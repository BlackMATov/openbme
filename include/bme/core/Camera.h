/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/core/Camera.h
 *****************************************************************************/

#pragma once

#include "Texture.h"

namespace bme
{

class Camera
{
	mat4f             _view;
	mat4f             _world;
	mat4f             _projection;
	
	rectu             _viewport;
	TexturePtr        _renderTarget;
	
public:
	
	Camera();
	Camera(const Camera& other);
	Camera& operator=(const Camera& other);
	Camera(const TexturePtr& renderTarget);
	
	void              To2D               ( const vec2f& size );
	
	void              setWorld           ( const mat4f& value );
	void              setViewport        ( const rectu& value );
	
	const mat4f&      getWorld           () const;
	const mat4f&      getProjection      () const;
	const rectu&      getViewport        () const;
	const TexturePtr& getRenderTarget    () const;
	
	const mat4f&      getCorrectView     () const;
	rectu             getCorrectViewport () const;
};

} // namespace bme
