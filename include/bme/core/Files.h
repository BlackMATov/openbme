/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/core/Files.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// MMapFile
// 
// ----------------------------------------------------------------------------

class MMapFile : public RefCounted
{
public:
	virtual ~MMapFile() {}
	virtual size_t      getSize  () const = 0;
	virtual const void* getData  () const = 0;
	virtual const Path& getPath  () const = 0;
};

// ----------------------------------------------------------------------------
// 
// ReadFile
// 
// ----------------------------------------------------------------------------

class ReadFile : public RefCounted
{
public:
	
	typedef void (*TBufferDeleter)(void*);
	
	virtual ~ReadFile() {}
	static ReadFilePtr  Create   ( const Path& path );
	static ReadFilePtr  Create   ( const void* buffer, size_t size,
	                               const Path& path, TBufferDeleter deleter );
	
	virtual size_t      Read     ( void* dest, size_t size ) = 0;
	virtual bool        Seek     ( ptrdiff_t offset, bool relative ) = 0;
	
	virtual size_t      getPos   () const = 0;
	virtual size_t      getSize  () const = 0;
	virtual const Path& getPath  () const = 0;
	
	virtual MMapFilePtr MakeMMap () const = 0;
	
	template < typename T >
	size_t              ReadV    ( T* v, size_t count );
	template < typename T >
	size_t              ReadV    ( T& v );
	
	size_t              ReadS    ( Str&   v );
	size_t              ReadS    ( StrId& v );
};

// ----------------------------------------------------------------------------
// 
// WriteFile
// 
// ----------------------------------------------------------------------------

class WriteFile : public RefCounted
{
public:
	
	virtual ~WriteFile() {}
	static WriteFilePtr Create   ( const Path& path, bool append );
	
	virtual size_t      Write    ( const void* src, size_t size ) = 0;
	virtual bool        Seek     ( ptrdiff_t offset, bool relative ) = 0;
	
	virtual size_t      getPos   () const = 0;
	virtual const Path& getPath  () const = 0;
	
	template < typename T >
	size_t              WriteV   ( const T* v, size_t count );
	template < typename T >
	size_t              WriteV   ( const T& v );
	
	size_t              WriteS   ( const Str&   v );
	size_t              WriteS   ( const StrId& v );
};

// ----------------------------------------------------------------------------
// 
// Impl
// 
// ----------------------------------------------------------------------------

template < typename T >
size_t ReadFile::ReadV(T* v, size_t count)
{
	BME_ASSERT(v && count);
	BME_STATIC_ASSERT(cpp::is_arithmetic<T>::value);
	return Read(v, sizeof(T) * count);
}

template < typename T >
size_t ReadFile::ReadV(T& v)
{
	return ReadV(&v, 1);
}

template < typename T >
size_t WriteFile::WriteV(const T* v, size_t count)
{
	BME_ASSERT(v && count);
	BME_STATIC_ASSERT(cpp::is_arithmetic<T>::value);
	return Write(v, sizeof(T) * count);
}

template < typename T >
size_t WriteFile::WriteV(const T& v)
{
	return WriteV(&v, 1);
}

} // namespace bme
