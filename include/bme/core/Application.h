/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/core/Application.h
 *****************************************************************************/

#pragma once

#include "System.h"

namespace bme
{

class Application : public Holder<Application>
{
public:
	
	struct Desc {
		pnt2u                     resolution;
		bool                      isVsync;
		bool                      isFullscreen;
		bool                      isWidescreen;
		bool                      isAllowScreensaver;
		bool                      isAllowWindowAutoSize;
		bool                      isOneInstanceOnly;      /// Windows only
		bool                      isFullscreenCursorClip; /// Windows only
		bool                      isNoFocusSuspend;
		System::E_ORIENTATION     allowedOrientation;
		ptrdiff_t                 parentHandler;
		StrId                     gameName;
		StrId                     compName;
		
		Desc()
		: resolution              (800,600)
		, isVsync                 (false)
		, isFullscreen            (false)
		, isWidescreen            (false)
		, isAllowScreensaver      (false)
		, isAllowWindowAutoSize   (true)
		, isOneInstanceOnly       (true)
		, isFullscreenCursorClip  (true)
		, isNoFocusSuspend        (true)
		, allowedOrientation      (System::ORIENTATION_ALL)
		, parentHandler           (0)
		, gameName                ("NoNameGame")
		, compName                ("NoNameCompany") {}
	};
	
private:
	
	Desc _desc;
	
public:
	
	Application();
	virtual ~Application();
	
	bool         Start            ( const Desc& desc );
	bool         Step             ();
	void         Shutdown         ();
	
	bool         ToggleFullscreen ( bool yesno );
	bool         ToggleWidescreen ( bool yesno );
	
	const Desc&  getDesc          () const;
	f32          getTime          () const;
	f32          getDeltaTime     () const;
	size_t       getTicks         () const;
	size_t       getFPS           () const;
	
	// -----------------------------------
	// for user override
	// -----------------------------------
	
	virtual bool OnInit           () { return true; }
	virtual bool OnUpdate         () { return true; }
	virtual void OnRender         () { }
	virtual void OnShutdown       () { }
	virtual bool OnQuit           () { return true; }
};

inline Application* theApplication() { return Holder<Application>::getHostage(); }

} // namespace bme
