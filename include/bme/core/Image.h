/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/core/Image.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// ImageDesc
// 
// ----------------------------------------------------------------------------

struct ImageDesc
{
	pnt2u         size;
	size_t        pitch;
	E_FORMAT      format;
	SmartBuffer   pixels;
	
	ImageDesc();
	bool          isEmpty       () const;
	bool          isLoaded      () const;
	bool          isPreLoaded   () const;
	
	static size_t BPPFromFormat ( E_FORMAT format );
};

// ----------------------------------------------------------------------------
// 
// ImageLoader
// 
// ----------------------------------------------------------------------------

class ImageLoader : private cpp::noncopyable
{
public:
	
	typedef ImageDesc (*TLoader)(const ReadFilePtr& file, bool preload);
	typedef cpp::vector<TLoader>::type TLoaders;
	
private:
	
	TLoaders    _loaders;
	ImageDesc   _load(const ReadFilePtr& file, bool preload) const;
	
public:
	
	ImageLoader();
	ImageDesc   Load       ( const Path& path, bool preload );
	ImageDesc   Load       ( const SmartBuffer& mem, bool preload );
	ImageDesc   Load       ( const void* data, size_t size, bool preload );
	static void Register   ( TLoader loader );
	static bool Unregister ( TLoader loader );
};

} // namespace bme
