/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/core/Texture.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

class Texture : public RefCounted
{
public:
	
	enum E_LOCK_MODE {
		LOCK_READONLY,
		LOCK_WRITEONLY,
		LOCK_READWRITE
	};
	
	static TexturePtr    Get             ( const Path& path );
	
	static TexturePtr    Create          ( const ImageDesc& desc );
	static TexturePtr    Create          ( const SmartBuffer& mem );
	static TexturePtr    Create          ( const void* data, size_t size );
	
	static TexturePtr    Create          ( const TexturePtr& source );
	static TexturePtr    Create          ( const pnt2u& size, E_FORMAT format,
	                                       bool render_target, bool z_buffer );
	virtual ~Texture() {}
	
	virtual bool         Lock            ( E_LOCK_MODE mode,
	                                       void** outData, size_t* outPitch ) = 0;
	virtual bool         Unlock          () = 0;
	virtual bool         isLocked        () const = 0;
	
	virtual const pnt2u& getSize         () const = 0;
	virtual const pnt2u& getOriginalSize () const = 0;
	virtual E_FORMAT     getFormat       () const = 0;
	virtual size_t       getMemoryUsage  () const = 0;
	virtual bool         isRenderTarget  () const = 0;
	virtual bool         isZBuffer       () const = 0;
};

} // namespace bme
