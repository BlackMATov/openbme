/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/core/System.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

class System : public Holder<System>
{
public:
	
	enum E_ORIENTATION {
		ORIENTATION_LANDSCAPE,
		ORIENTATION_PORTRAIT,
		ORIENTATION_ALL
	};
	
	virtual ~System() {}
	
	virtual void          setWindowCaption      ( const Str&   value ) = 0;
	virtual void          setWindowIcon         ( const Path&  value ) = 0;
	
	virtual void          setCursor             ( const Path&  value, const pnt2u& hotSpot ) = 0;
	virtual void          setCursorPosition     ( const pnt2u& value ) = 0;
	virtual void          setCursorVisible      ( bool yesno ) = 0;
	virtual bool          isCursorVisible       () const = 0;
	
	virtual pnt2u         getWindowResolution   () const = 0;
	virtual pnt2u         getDesktopResolution  () const = 0;
	
	virtual void          ShowWindow            () = 0;
	virtual void          HideWindow            () = 0;
	virtual void          RestoreWindow         () = 0;
	virtual void          MinimizeWindow        () = 0;
	
	virtual bool          isWindowVisible       () const = 0;
	virtual bool          isWindowFocused       () const = 0;
	virtual bool          isWindowActive        () const = 0;
	virtual bool          isWindowMinimized     () const = 0;
	
	virtual void          ShowConsole           () const = 0;
	virtual void          HideConsole           () const = 0;
	
	virtual bool          ShowSoftKeyboard      () const = 0;
	virtual void          HideSoftKeyboard      () const = 0;
	virtual bool          isSoftKeyboardVisible () const = 0;
	virtual E_ORIENTATION getDeviceOrientation  () const = 0;
};

inline System* theSystem() { return Holder<System>::getHostage(); }

} // namespace bme
