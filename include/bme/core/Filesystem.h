/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/core/Filesystem.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

class Filesystem : public Holder<Filesystem>
{
public:
	
	enum E_ORDER_MODE {
		ORDER_DISK_FIRST,
		ORDER_ARCHIVE_FIRST,
		ORDER_DISK_ONLY,
		ORDER_ARCHIVE_ONLY
	};
	
	class FileTask : public JobberTask {
		Path               _path;
		E_ORDER_MODE       _mode;
		ReadFilePtr        _file;
	protected:
		virtual void       _run    ();
		virtual void       _reset  ();
	public:
		FileTask(const Path& path, E_ORDER_MODE mode);
		virtual ~FileTask();
		const Path&        getPath () const;
		E_ORDER_MODE       getMode () const;
		const ReadFilePtr& getFile ();
	};
	
	typedef cpp::intrusive_ptr<FileTask> FileTaskPtr;
	typedef bool (*TraceFunc)(const Path& path, bool folder, void* data);
	
	virtual ~Filesystem() {}
	
	virtual bool        RemoveFile        ( const Path& path ) const = 0;
	virtual bool        RemoveFolder      ( const Path& path ) const = 0;
	virtual bool        CreateDirectories ( const Path& path ) const = 0;
	
	virtual void        TraceDirectory    ( const Path& path,
	                                        TraceFunc func, void* data = NULL ) const = 0;
	virtual void        TraceDirectories  ( const Path& path,
	                                        TraceFunc func, void* data = NULL ) const = 0;
	
	virtual ReadFilePtr OpenFile          ( const Path& path,
	                                        E_ORDER_MODE mode = ORDER_DISK_FIRST ) const = 0;
	virtual ReadFilePtr ReadFile          ( const Path& path,
	                                        E_ORDER_MODE mode = ORDER_DISK_FIRST ) const = 0;
	virtual FileTaskPtr ReadFileAsync     ( const Path& path,
	                                        E_ORDER_MODE mode = ORDER_DISK_FIRST ) const = 0;
	
	virtual bool        IsFileExists      ( const Path& path,
	                                        E_ORDER_MODE mode = ORDER_DISK_FIRST ) const = 0;
	virtual bool        IsFolderExists    ( const Path& path,
	                                        E_ORDER_MODE mode = ORDER_DISK_FIRST ) const = 0;
	
	virtual Path        GetExecuteDir     () const = 0;
	virtual Path        GetResourceDir    () const = 0;
	virtual Path        GetWorkingDir     () const = 0;
	virtual Path        GetAppDataDir     () const = 0;
	
	virtual bool        AttachArchive     ( const ArchivePtr& archive ) = 0;
	virtual bool        IsArchiveAttached ( const Path& path ) const = 0;
	virtual bool        DetachArchive     ( const Path& path ) = 0;
	virtual void        DetachAllArchives () = 0;
};

inline Filesystem* theFilesystem() { return Holder<Filesystem>::getHostage(); }

} // namespace bme
