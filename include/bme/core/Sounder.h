/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/core/Sounder.h
 *****************************************************************************/

#pragma once

#include "Sound.h"

namespace bme
{

class Sounder : public Holder<Sounder>
{
public:
	
	virtual ~Sounder() {}
	
	virtual bool   isValid         () const = 0;
	
	virtual void   setMasterVolume ( f32 value ) = 0;
	virtual f32    getMasterVolume () const = 0;
	
	virtual void   setGroupVolume  ( f32 value, const StrId& group ) = 0;
	virtual f32    getGroupVolume  ( const StrId& group ) const = 0;
	
	virtual size_t StopChannels    ( const StrId& group ) = 0;
	virtual size_t PauseChannels   ( const StrId& group ) = 0;
	virtual size_t ResumeChannels  ( const StrId& group ) = 0;
	
	virtual size_t StopChannels    ( const SoundPtr& sound ) = 0;
	virtual size_t PauseChannels   ( const SoundPtr& sound ) = 0;
	virtual size_t ResumeChannels  ( const SoundPtr& sound ) = 0;
};

inline Sounder* theSounder() { return Holder<Sounder>::getHostage(); }

} // namespace bme
