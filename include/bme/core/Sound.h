/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/core/Sound.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// Sound
// 
// ----------------------------------------------------------------------------

class Sound : public RefCounted
{
public:
	
	static SoundPtr    Get        ( const Path& path, bool stream );
	
	static SoundPtr    Create     ( const SmartBuffer& mem );
	static SoundPtr    Create     ( const void* data, size_t size );
	
	virtual ~Sound() {}
	
	virtual ChannelPtr Play       ( f32 volume = 1.f, bool loop = false,
	                                const StrId& group = "default" ) = 0;
	virtual size_t     Stop       () = 0;
	virtual size_t     Pause      () = 0;
	virtual size_t     Resume     () = 0;
	
	virtual f32        getLength  () const = 0;
	virtual bool       isStreamed () const = 0;
};

// ----------------------------------------------------------------------------
// 
// Channel
// 
// ----------------------------------------------------------------------------

class Channel : public RefCounted
{
public:
	
	enum E_PROPERTY
	{
		PROPERTY_PANNING, ///< [-1.0; 1.0] (0.0 - default)
		PROPERTY_PITCH,   ///< [ 0.0; ...] (1.0 - default)
		PROPERTY_VOLUME,  ///< [ 0.0; 1.0] (1.0 - default)
		PROPERTY_POSITION ///< [ 0.0; ...]
	};
	
	virtual ~Channel() {}
	
	virtual bool            Pause       () = 0;
	virtual bool            Resume      () = 0;
	virtual bool            Stop        () = 0;
	virtual bool            isStopped   () const = 0;
	
	virtual void            setLooped   ( bool yesno ) = 0;
	virtual bool            isLooped    () const = 0;
	
	virtual void            setProperty ( E_PROPERTY prop, f32 value ) = 0;
	virtual f32             getProperty ( E_PROPERTY prop ) const = 0;
	
	virtual const StrId&    getGroup    () const = 0;
	virtual const SoundPtr& getSound    () const = 0;
	virtual f32             getLength   () const = 0;
	virtual bool            isStreamed  () const = 0;
};

} // namespace bme
