/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/core/Receiver.h
 *****************************************************************************/

#pragma once

#include "System.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// ReceiverList
// 
// ----------------------------------------------------------------------------

class Receiver;

typedef cpp::intrusive::list_base_hook<
	cpp::intrusive::link_mode<cpp::intrusive::auto_unlink>
> ReceiverListHook;

typedef cpp::intrusive::list<
	Receiver,
	cpp::intrusive::constant_time_size<false>
> TReceiverList;

// ----------------------------------------------------------------------------
// 
// Receiver
// 
// ----------------------------------------------------------------------------

class Receiver
	: public ReceiverListHook
	, private cpp::noncopyable
{
public:
	
	struct Event {
		enum E_TYPE {
			// Inputer events
			TYPE_MOUSE_DOWN,
			TYPE_MOUSE_UP,
			TYPE_MOUSE_MOVE,
			TYPE_KEYBOARD_DOWN,
			TYPE_KEYBOARD_UP,
			TYPE_JOYSTICK_DOWN,
			TYPE_JOYSTICK_UP,
			
			// System events
			TYPE_FOCUS,
			TYPE_ACTIVE,
			TYPE_RESET_DEVICE,
			TYPE_ROTATE_DEVICE,
			TYPE_SOFTKEYBOARD_CHANGE,
			TYPE_SOFTKEYBOARD_DONE,
			TYPE_MEMORY_WARNING,
			TYPE_QUIT,
			
			TYPE_UNKNOWN
		};
		
		struct MouseEvent {
			E_MOUSE key;
			f32 x, y;
			size_t which;
		};
		
		struct KeyboardEvent {
			E_KEY key;
			u32 charKey;
			size_t which;
		};
		
		struct JoystickEvent {
			E_JOYSTICK_BUTTON button;
			size_t which;
		};
		
		struct FocusEvent {
			bool gain;
		};
		
		struct ActiveEvent {
			bool gain;
		};
		
		struct RotateEvent {
			System::E_ORIENTATION orientation;
		};
		
		struct SoftKeyboardEvent {
			const char* text;
		};
		
		E_TYPE type;
		union {
			// Inputer events
			struct MouseEvent        mouse;
			struct KeyboardEvent     keyboard;
			struct JoystickEvent     joystick;
			// System events
			struct FocusEvent        focus;
			struct ActiveEvent       active;
			struct RotateEvent       rotate;
			struct SoftKeyboardEvent softkeyboard;
		};
		
		Event();
		void Push(bool immediately = false);
	};
	
public:
	
	Receiver();
	virtual ~Receiver();
	
	virtual bool OnEvent(const Event& event) = 0;
};

} // namespace bme
