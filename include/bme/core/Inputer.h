/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/core/Inputer.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// Mouse
// 
// ----------------------------------------------------------------------------

class Mouse : private cpp::noncopyable
{
public:
	
	virtual ~Mouse() {}
	
	virtual bool        IsKeyDown      ( E_MOUSE key ) const = 0;
	virtual bool        IsKeyUp        ( E_MOUSE key ) const = 0;
	virtual bool        IsKeyDowned    ( E_MOUSE key ) const = 0;
	virtual vec2f       getPosition    ()              const = 0;
};

// ----------------------------------------------------------------------------
// 
// Keyboard
// 
// ----------------------------------------------------------------------------

class Keyboard : private cpp::noncopyable
{
public:
	
	virtual ~Keyboard() {}
	
	virtual bool        IsKeyDown      ( E_KEY key ) const = 0;
	virtual bool        IsKeyUp        ( E_KEY key ) const = 0;
	virtual bool        IsKeyDowned    ( E_KEY key ) const = 0;
};

// ----------------------------------------------------------------------------
// 
// Joystick
// 
// ----------------------------------------------------------------------------

class Joystick : private cpp::noncopyable
{
public:
	
	struct Desc
	{
		size_t axes;
		size_t balls;
		size_t hats;
		size_t buttons;
		StrId  name;
		Desc()
		: axes(0)
		, balls(0)
		, hats(0)
		, buttons(0)
		, name("NoName") {}
	};
	
	virtual ~Joystick() {}
	
	virtual const Desc& getDesc        ()                                     const = 0;
	virtual f32         getAxisState   ( size_t index )                       const = 0;
	virtual vec2f       getBallState   ( size_t index )                       const = 0;
	
	virtual bool        IsButtonDown   ( E_JOYSTICK_BUTTON button )           const = 0;
	virtual bool        IsButtonUp     ( E_JOYSTICK_BUTTON button )           const = 0;
	virtual bool        IsButtonDowned ( E_JOYSTICK_BUTTON button )           const = 0;
	virtual bool        IsHatDowned    ( size_t index, E_JOYSTICK_HAT state ) const = 0;
};

// ----------------------------------------------------------------------------
// 
// Inputer
// 
// ----------------------------------------------------------------------------

class Inputer : public Holder<Inputer>
{
public:
	
	struct Desc
	{
		size_t mouses;
		size_t keyboards;
		size_t joysticks;
		
		Desc()
		: mouses(0)
		, keyboards(0)
		, joysticks(0) {}
	};
	
	virtual ~Inputer() {}
	
	virtual const Desc& getDesc        ()               const = 0;
	virtual Mouse*      getMouse       ( size_t index ) const = 0;
	virtual Keyboard*   getKeyboard    ( size_t index ) const = 0;
	virtual Joystick*   getJoystick    ( size_t index ) const = 0;
};

inline Inputer*  theInputer  ()               { return Holder<Inputer>::getHostage(); }
inline Keyboard* theKeyboard (size_t num = 0) { return (theInputer() ? theInputer()->getKeyboard (num) : NULL); }
inline Mouse*    theMouse    (size_t num = 0) { return (theInputer() ? theInputer()->getMouse    (num) : NULL); }
inline Joystick* theJoystick (size_t num = 0) { return (theInputer() ? theInputer()->getJoystick (num) : NULL); }

} // namespace bme
