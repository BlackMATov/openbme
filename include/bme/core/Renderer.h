/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/core/Renderer.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

class Renderer : public Holder<Renderer>
{
public:
	
	enum E_PRIMITIVE {
		PRIMITIVE_LINES     = 2,
		PRIMITIVE_TRIANGLES = 3
	};
	
	struct Caps {
		bool  npot_textures;
		bool  render_to_texture;
		pnt2u max_texture_size;
		Caps()
		: npot_textures(false)
		, render_to_texture(false)
		, max_texture_size(1024, 1024) {}
	};
	
	struct SceneInfo {
		size_t nTriangles;
		size_t nLines;
		size_t nDIP;
		SceneInfo() { reset(); }
		void reset() { nTriangles = 0; nLines = 0; nDIP = 0; }
	};
	
	virtual ~Renderer() {}
	
	virtual Caps&            getCaps       ()       = 0;
	virtual const Caps&      getCaps       () const = 0;
	
	virtual bool             BeginScene    () = 0;
	virtual void             ClearScene    ( const Color& color = Color::black,
	                                         bool back_buffer = true,
	                                         bool z_buffer = true ) = 0;
	virtual const SceneInfo& EndScene      () = 0;
	
	virtual void             setCamera     ( const Camera& camera ) = 0;
	virtual const Camera&    getCamera     () const = 0;
	
	virtual void             RenderObject  ( const Quad& prim ) = 0;
	virtual void             RenderObject  ( const Triangle& prim ) = 0;
	virtual void             RenderObject  ( const Line& prim ) = 0;
	virtual void             RenderObject  ( const Material& material,
	                                         const RenderStates& states,
	                                         const Vertex* vertices, size_t vertices_count,
	                                         const u16* indices, size_t indices_count,
	                                         E_PRIMITIVE prim_type ) = 0;
	virtual void             RenderBatches () = 0;
};

inline Renderer* theRenderer() { return Holder<Renderer>::getHostage(); }

} // namespace bme
