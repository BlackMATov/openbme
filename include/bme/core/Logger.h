/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/core/Logger.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

class Logger : public Holder<Logger>
{
public:
	
	enum E_LEVEL {
		LEVEL_DEBUG,
		LEVEL_INFO,
		LEVEL_COMPLETE,
		LEVEL_WARNING,
		LEVEL_ERROR
	};
	
	enum E_OUTPUT {
		OUTPUT_OFF,
		OUTPUT_FILE,
		OUTPUT_NATIVE,
		OUTPUT_ALL
	};
	
	typedef bool (*TWriteHook)(const char* text, E_LEVEL level);
	
	virtual ~Logger() {}
	
	virtual bool        setLogfile    ( const Path& path ) = 0;
	virtual const Path& getLogfile    () const = 0;
	
	virtual void        setMinLevel   ( E_LEVEL level ) = 0;
	virtual E_LEVEL     getMinLevel   () const = 0;
	
	virtual void        setOutputType ( E_OUTPUT value ) = 0;
	virtual E_OUTPUT    getOutputType () const = 0;
	
	virtual void        setWriteHook  ( TWriteHook hook ) = 0;
	virtual TWriteHook  getWriteHook  () const = 0;
	
	virtual void        Write         ( const char* text, E_LEVEL level ) = 0;
};

inline Logger* theLogger() { return Holder<Logger>::getHostage(); }

} // namespace bme

// ------------------------------------
// generate
// BME_LOG_#LEVEL#
//   (const char* text)
// ------------------------------------

#define BME_LOG_LEVEL_IMPL(name_func, log_level)\
	inline void name_func(const char* text)\
	{\
		using namespace bme;\
		if ( theLogger() )\
			theLogger()->Write(text, Logger::log_level);\
	}
	BME_LOG_LEVEL_IMPL(BME_LOG_DEBUG,   LEVEL_DEBUG)
	BME_LOG_LEVEL_IMPL(BME_LOG_INFO,    LEVEL_INFO)
	BME_LOG_LEVEL_IMPL(BME_LOG_COMPLETE,LEVEL_COMPLETE)
	BME_LOG_LEVEL_IMPL(BME_LOG_WARNING, LEVEL_WARNING)
	BME_LOG_LEVEL_IMPL(BME_LOG_ERROR,   LEVEL_ERROR)
#undef BME_LOG_LEVEL_IMPL

// ------------------------------------
// generate
// BME_LOG_#LEVEL#_OR_ERROR
//   (bool yesno, const char* text)
// ------------------------------------

#define BME_LOG_LOE_IMPL(name_func, log_level)\
	inline void name_func(bool yesno, const char* text)\
	{\
		using namespace bme;\
		if ( theLogger() )\
			theLogger()->Write(text, yesno ? Logger::log_level : Logger::LEVEL_ERROR);\
	}
	BME_LOG_LOE_IMPL(BME_LOG_DEBUG_OR_ERROR,   LEVEL_DEBUG)
	BME_LOG_LOE_IMPL(BME_LOG_INFO_OR_ERROR,    LEVEL_INFO)
	BME_LOG_LOE_IMPL(BME_LOG_COMPLETE_OR_ERROR,LEVEL_COMPLETE)
	BME_LOG_LOE_IMPL(BME_LOG_WARNING_OR_ERROR, LEVEL_WARNING)
#undef BME_LOG_LOE_IMPL

// ------------------------------------
// generate
// BME_LOG_#LEVEL#_FMT
//   (const char* format, #vars#)
// ------------------------------------

#define BME_LOG_LEVEL_FMT_IMPL(z, n, log_level)\
template<BOOST_PP_ENUM_PARAMS_Z(z, BOOST_PP_INC(n), class T)>\
inline void BME_TMP_FN(const char* format,\
	BOOST_PP_ENUM_BINARY_PARAMS_Z(z, BOOST_PP_INC(n), const T, &v))\
{\
	using namespace bme;\
	char buffer[BME_MAX_LOGGER_MESSAGE] = {0};\
	Strings::Format(buffer, BME_MAX_LOGGER_MESSAGE, format,\
		BOOST_PP_ENUM_PARAMS_Z(z, BOOST_PP_INC(n), v));\
	if ( theLogger() )\
		theLogger()->Write(buffer, Logger::log_level);\
}
	#define BME_TMP_FN BME_LOG_DEBUG_FMT
	BOOST_PP_REPEAT(9, BME_LOG_LEVEL_FMT_IMPL, LEVEL_DEBUG)
	#undef BME_TMP_FN
	#define BME_TMP_FN BME_LOG_INFO_FMT
	BOOST_PP_REPEAT(9, BME_LOG_LEVEL_FMT_IMPL, LEVEL_INFO)
	#undef BME_TMP_FN
	#define BME_TMP_FN BME_LOG_COMPLETE_FMT
	BOOST_PP_REPEAT(9, BME_LOG_LEVEL_FMT_IMPL, LEVEL_COMPLETE)
	#undef BME_TMP_FN
	#define BME_TMP_FN BME_LOG_WARNING_FMT
	BOOST_PP_REPEAT(9, BME_LOG_LEVEL_FMT_IMPL, LEVEL_WARNING)
	#undef BME_TMP_FN
	#define BME_TMP_FN BME_LOG_ERROR_FMT
	BOOST_PP_REPEAT(9, BME_LOG_LEVEL_FMT_IMPL, LEVEL_ERROR)
	#undef BME_TMP_FN
#undef BME_LOG_LEVEL_FMT_IMPL

// ------------------------------------
// generate
// BME_LOG_#LEVEL#_OR_ERROR_FMT
//   (bool yesno, const char* format, #vars#)
// ------------------------------------

#define BME_LOG_LEVEL_OR_ERROR_FMT_IMPL(z, n, log_level)\
template<BOOST_PP_ENUM_PARAMS_Z(z, BOOST_PP_INC(n), class T)>\
inline void BME_TMP_FN(bool yesno, const char* format,\
	BOOST_PP_ENUM_BINARY_PARAMS_Z(z, BOOST_PP_INC(n), const T, &v))\
{\
	using namespace bme;\
	char buffer[BME_MAX_LOGGER_MESSAGE] = {0};\
	Strings::Format(buffer, BME_MAX_LOGGER_MESSAGE, format,\
		BOOST_PP_ENUM_PARAMS_Z(z, BOOST_PP_INC(n), v));\
	if ( theLogger() )\
		theLogger()->Write(buffer, yesno ? Logger::log_level : Logger::LEVEL_ERROR);\
}
	#define BME_TMP_FN BME_LOG_DEBUG_OR_ERROR_FMT
	BOOST_PP_REPEAT(9, BME_LOG_LEVEL_OR_ERROR_FMT_IMPL, LEVEL_DEBUG)
	#undef BME_TMP_FN
	#define BME_TMP_FN BME_LOG_INFO_OR_ERROR_FMT
	BOOST_PP_REPEAT(9, BME_LOG_LEVEL_OR_ERROR_FMT_IMPL, LEVEL_INFO)
	#undef BME_TMP_FN
	#define BME_TMP_FN BME_LOG_COMPLETE_OR_ERROR_FMT
	BOOST_PP_REPEAT(9, BME_LOG_LEVEL_OR_ERROR_FMT_IMPL, LEVEL_COMPLETE)
	#undef BME_TMP_FN
	#define BME_TMP_FN BME_LOG_WARNING_OR_ERROR_FMT
	BOOST_PP_REPEAT(9, BME_LOG_LEVEL_OR_ERROR_FMT_IMPL, LEVEL_WARNING)
	#undef BME_TMP_FN
#undef BME_LOG_LEVEL_FMT_IMPL
