/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/core/Primitives.h
 *****************************************************************************/

#pragma once

#include "base.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// RenderStates
// 
// ----------------------------------------------------------------------------

struct RenderStates
{
	enum E_DEFAULT {
		DEFAULT_2D,
		DEFAULT_3D
	};
	
	enum E_BLEND {
		BLEND_ALPHA,
		BLEND_ADD,
		BLEND_MULTIPLY,
		BLEND_NONE
	};
	
	//
	
	E_BLEND blend;
	bool    zEnable;
	
	//
	
	RenderStates(E_DEFAULT def = DEFAULT_2D)
	: blend(BLEND_ALPHA), zEnable(false)
	{
		switch(def) {
		case DEFAULT_2D:
			blend   = BLEND_ALPHA;
			zEnable = false;
			break;
		case DEFAULT_3D:
			blend   = BLEND_NONE;
			zEnable = true;
			break;
		default:
			BME_ASSERT(false);
		}
	}
	
	bool operator==(const RenderStates& other) const
	{
		return
			blend   == other.blend &&
			zEnable == other.zEnable;
	}
};

// ----------------------------------------------------------------------------
// 
// Material
// 
// ----------------------------------------------------------------------------

struct Material
{
	enum E_ADDRESS {
		ADDRESS_WRAP,
		ADDRESS_CLAMP
	};
	
	bool       filter;
	E_ADDRESS  address;
	TexturePtr texture;
	
	Material()
	: filter(true), address(ADDRESS_WRAP) {}
	
	bool operator==(const Material& other) const
	{
		return
			filter  == other.filter &&
			address == other.address &&
			texture == other.texture;
	}
};

// ----------------------------------------------------------------------------
// 
// Vertex
// 
// ----------------------------------------------------------------------------

struct Vertex
{
	f32 x, y, z;
	u32 color;
	f32 tx, ty;
	
	Vertex()
	: x(0.f), y(0.f), z(0.5f)
	, color(0xFFFFFFFF)
	, tx(0.f), ty(0.f) {}
};

// ----------------------------------------------------------------------------
// 
// Primitive
// 
// ----------------------------------------------------------------------------

template < size_t NumVertices >
struct Primitive
{
	enum { numVertices = NumVertices };
	Vertex       vertices[NumVertices];
	Material     material;
	RenderStates renderStates;
};

} // namespace bme
