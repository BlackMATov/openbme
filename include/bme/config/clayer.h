/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/config/clayer.h
 *****************************************************************************/

#pragma once

#include "base.h"

// ----------------------------------------------------------------------------
// 
// Asserts
// 
// ----------------------------------------------------------------------------

// ------------------------------------
// BME_STATIC_ASSERT
// ------------------------------------

#define BME_STATIC_ASSERT(expr)\
	BOOST_STATIC_ASSERT((expr))

// ------------------------------------
// BME_ASSERT
// ------------------------------------

#if BME_MODE == BME_MODE_DEBUG
#	if BME_COMPILER == BME_COMPILER_MSVC
#		include <crtdbg.h>
#		define BME_ASSERT(expr)          _ASSERT_EXPR((expr), NULL)
#		define BME_ASSERT_MSG(expr, msg) _ASSERT_EXPR((expr), _CRT_WIDE(msg))
#	else
#		define BME_ASSERT(expr)          assert((expr))
#		define BME_ASSERT_MSG(expr, msg) assert((expr) && (msg))
#	endif
#else
#	define BME_ASSERT(expr)              ((void)0)
#	define BME_ASSERT_MSG(expr, msg)     ((void)0)
#endif

// ----------------------------------------------------------------------------
// 
// POD types
// 
// ----------------------------------------------------------------------------

namespace bme
{
	typedef float  f32;
	typedef double f64;
	BME_STATIC_ASSERT(4 == sizeof(f32));
	BME_STATIC_ASSERT(8 == sizeof(f64));
	
	typedef signed char  s8;
	typedef signed short s16;
	typedef signed int   s32;
	BME_STATIC_ASSERT(1 == sizeof(s8));
	BME_STATIC_ASSERT(2 == sizeof(s16));
	BME_STATIC_ASSERT(4 == sizeof(s32));
	
	typedef unsigned char  u8;
	typedef unsigned short u16;
	typedef unsigned int   u32;
	BME_STATIC_ASSERT(1 == sizeof(u8));
	BME_STATIC_ASSERT(2 == sizeof(u16));
	BME_STATIC_ASSERT(4 == sizeof(u32));
}

// ----------------------------------------------------------------------------
// 
// C mem functions
// 
// ----------------------------------------------------------------------------

#define BME_malloc  ::malloc
#define BME_free    ::free

#define BME_memcpy  ::memcpy
#define BME_memmove ::memmove
#define BME_memset  ::memset
#define BME_memcmp  ::memcmp

// ----------------------------------------------------------------------------
// 
// C str functions
// 
// ----------------------------------------------------------------------------

#define BME_strtod  ::strtod
#define BME_strtol  ::strtol
#define BME_strtoul ::strtoul
#define BME_strtok  ::strtok
#define BME_strcmp  ::strcmp
#define BME_strlen  ::strlen
#define BME_strstr  ::strstr
#define BME_strcpy  ::strcpy

#define BME_wcstod  ::wcstod
#define BME_wcstol  ::wcstol
#define BME_wcstoul ::wcstoul
#define BME_wcstok  ::wcstok
#define BME_wcscmp  ::wcscmp
#define BME_wcslen  ::wcslen
#define BME_wcsstr  ::wcsstr
#define BME_wcscpy  ::wcscpy
