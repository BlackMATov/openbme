/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/config/cpplayer.h
 *****************************************************************************/

#pragma once

#include "base.h"

// ----------------------------------------------------------------------------
// 
// bme::cpp namespace
// 
// ----------------------------------------------------------------------------

namespace bme { namespace cpp
{
	// std
	
	using std::pair;
	using std::auto_ptr;
	
	using std::iterator;
	using std::back_inserter;
	using std::reverse_iterator;
	
	using std::exception;
	using std::bad_alloc;
	using std::out_of_range;
	
	using std::less;
	using std::less_equal;
	using std::greater;
	using std::greater_equal;
	
	using std::swap;
	using std::copy;
	using std::fill;
	using std::find;
	using std::sort;
	using std::for_each;
	using std::make_pair;
	using std::stable_sort;
	using std::lower_bound;
	using std::upper_bound;
	
	using std::cos;
	using std::sin;
	using std::tan;
	using std::acos;
	using std::sqrt;
	using std::atan2;
	
	using std::min;
	using std::max;
	using std::abs;
	using std::ceil;
	using std::floor;
	
	// boost
	
	using boost::weak_ptr;
	using boost::scoped_ptr;
	using boost::shared_ptr;
	using boost::intrusive_ptr;
	using boost::make_shared;
	using boost::allocate_shared;
	using boost::enable_shared_from_this;
	
	using boost::static_pointer_cast;
	using boost::const_pointer_cast;
	using boost::dynamic_pointer_cast;
	using boost::reinterpret_pointer_cast;
	
	using boost::enable_if;
	using boost::enable_if_c;
	using boost::integral_constant;
	
	using boost::noncopyable;
	using boost::numeric::bounds;
	using boost::numeric_cast;
	using boost::numeric::bad_numeric_cast;
	using boost::numeric::positive_overflow;
	using boost::numeric::negative_overflow;
	
	using boost::algorithm::replace_all;
	namespace intrusive = boost::intrusive;
	
	using boost::true_type;
	using boost::false_type;
	using boost::is_signed;
	using boost::is_unsigned;
	using boost::is_integral;
	using boost::is_arithmetic;
	using boost::is_floating_point;
}} //namespace bme::cpp

namespace bme { namespace cpp
{
	template
	<
		typename T,
		typename A = std::allocator<T>
	>
	struct basic_string
	{
		typedef typename std::basic_string<T, std::char_traits<T>, A> type;
		typedef typename type::iterator                               iterator;
		typedef typename type::const_iterator                         const_iterator;
	};
	
	template
	<
		typename T,
		typename A = std::allocator<T>
	>
	struct list
	{
		typedef typename std::list<T, A>      type;
		typedef typename type::iterator       iterator;
		typedef typename type::const_iterator const_iterator;
	};
	
	template
	<
		typename T,
		typename A = std::allocator<T>
	>
	struct deque
	{
		typedef typename std::deque<T, A>     type;
		typedef typename type::iterator       iterator;
		typedef typename type::const_iterator const_iterator;
	};
	
	template
	<
		typename T,
		typename A = std::allocator<T>
	>
	struct vector
	{
		typedef typename std::vector<T, A>    type;
		typedef typename type::iterator       iterator;
		typedef typename type::const_iterator const_iterator;
	};
	
	template
	<
		typename T,
		typename P = cpp::less<T>,
		typename A = std::allocator<T>
	>
	struct set
	{
		typedef typename std::set<T, P, A>    type;
		typedef typename type::iterator       iterator;
		typedef typename type::const_iterator const_iterator;
	};
	
	template
	<
		typename K,
		typename T,
		typename P = cpp::less<K>,
		typename A = std::allocator<cpp::pair<const K,T> >
	>
	struct map
	{
		typedef typename std::map<K, T, P, A> type;
		typedef typename type::iterator       iterator;
		typedef typename type::const_iterator const_iterator;
	};
	
	template
	<
		typename T,
		typename A = std::allocator<T>
	>
	struct circular_buffer
	{
		typedef typename boost::circular_buffer<T, A> type;
		typedef typename type::iterator               iterator;
		typedef typename type::const_iterator         const_iterator;
	};
}} //namespace bme::cpp
