/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/config/base.h
 *****************************************************************************/

#pragma once

// ----------------------------------------------------------------------------
// 
// C standart includes
// 
// ----------------------------------------------------------------------------

#include <ctime>
#include <cmath>
#include <cfloat>
#include <cwchar>
#include <cstdio>
#include <cstdarg>
#include <cassert>
#include <cstring>
#include <cstdlib>
#include <climits>

// ----------------------------------------------------------------------------
// 
// C++ standart includes
// 
// ----------------------------------------------------------------------------

#include <set>
#include <map>
#include <list>
#include <deque>
#include <vector>
#include <string>
#include <iterator>
#include <exception>
#include <algorithm>
#include <stdexcept>
#include <functional>

// ----------------------------------------------------------------------------
// 
// Boost includes
// 
// ----------------------------------------------------------------------------

#define BOOST_ENABLE_ASSERT_HANDLER

#include <boost/assert.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <boost/type_traits.hpp>
#include <boost/static_assert.hpp>
#include <boost/intrusive/list.hpp>
#include <boost/circular_buffer.hpp>
#include <boost/numeric/conversion/cast.hpp>
#include <boost/numeric/conversion/bounds.hpp>
#include <boost/algorithm/string/replace.hpp>

#include <boost/preprocessor/arithmetic.hpp>
#include <boost/preprocessor/repetition/enum_params.hpp>
#include <boost/preprocessor/repetition/enum_binary_params.hpp>

// ----------------------------------------------------------------------------
// 
// Defines
// 
// ----------------------------------------------------------------------------

// BME_OS (auto detect)
#define BME_OS_ANDROID            1
#define BME_OS_WINDOWS            2
#define BME_OS_IOS                3
#define BME_OS_MACOSX             4

// BME_COMPILER (auto detect)
#define BME_COMPILER_MSVC         1
#define BME_COMPILER_GCC          2

// BME_MODE (auto detect)
#define BME_MODE_DEBUG            1
#define BME_MODE_RELEASE          2

// BME_SYSTEM (manual)
#define BME_SYSTEM_ANDROID        1
#define BME_SYSTEM_WINDOWS        2
#define BME_SYSTEM_IOS            3
#define BME_SYSTEM_MACOSX         4

// BME_RENDERER (manual)
#define BME_RENDERER_DX8          1
#define BME_RENDERER_OGL          2
#define BME_RENDERER_OGLES        3

// BME_SOUNDER (manual)
#define BME_SOUNDER_NONE          1
#define BME_SOUNDER_BASS          2

// BME_FILESYSTEM (manual)
#define BME_FILESYSTEM_WINDOWS    1
#define BME_FILESYSTEM_POSIX      2

// ----------------------------------------------------------------------------
// 
// Manual config
// 
// ----------------------------------------------------------------------------

#define BME_VERSION_MAJOR         0
#define BME_VERSION_MINOR         0
#define BME_VERSION_REVISION      9
#define BME_VERSION_STR           "0.0.9"

#define BME_SYSTEM                BME_SYSTEM_WINDOWS
#define BME_RENDERER              BME_RENDERER_DX8
#define BME_SOUNDER               BME_SOUNDER_BASS
#define BME_FILESYSTEM            BME_FILESYSTEM_WINDOWS

#define BME_WITH_MAIN
//#define BME_LINK_3RD_PARTY_FROM_SRC

#define BME_MAX_BATCHES           512
#define BME_MAX_BATCH_VERTICES    65535
#define BME_MAX_LOGGER_MESSAGE    1024
#define BME_MAX_JOBBER_TASKS      1000
#define BME_MAX_SOUND_CHANNELS    16
#define BME_ARCHIVES_RESERVE      10
#define BME_SYSTEM_EVENTS_RESERVE 100
#define BME_IMAGE_LOADERS_RESERVE 10

// ----------------------------------------------------------------------------
// 
// Auto detect config
// 
// ----------------------------------------------------------------------------

// ------------------------------------
// BME_OS
// ------------------------------------

#ifndef BME_OS
	#if defined(__ANDROID__) || defined(ANDROID)
	#	define BME_OS BME_OS_ANDROID
	#elif defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
	#	define BME_OS BME_OS_WINDOWS
	#elif defined(macintosh) || defined(__APPLE__) || defined(__APPLE_CC__)
	#	include <TargetConditionals.h>
	#	if ( TARGET_IPHONE_SIMULATOR == 1 ) || ( TARGET_OS_IPHONE == 1 )
	#		define BME_OS BME_OS_IOS
	#	elif ( TARGET_OS_MAC == 1 )
	#		define BME_OS BME_OS_MACOSX
	#	endif
	#endif
#endif

// ------------------------------------
// BME_COMPILER
// ------------------------------------

#ifndef BME_COMPILER
	#if defined(_MSC_VER)
	#	define BME_COMPILER BME_COMPILER_MSVC
	#elif defined(__GNUC__)
	#	define BME_COMPILER BME_COMPILER_GCC
	#endif
#endif

// ------------------------------------
// BME_MODE
// ------------------------------------

#ifndef BME_MODE
	#if defined (DEBUG) || defined(_DEBUG) || defined(NRELEASE)
	#	define BME_MODE BME_MODE_DEBUG
	#elif defined(RELEASE) || defined(_RELEASE) || defined(NDEBUG)
	#	define BME_MODE BME_MODE_RELEASE
	#endif
#endif

// ----------------------------------------------------------------------------
// 
// Check configs
// 
// ----------------------------------------------------------------------------

#ifndef BME_OS
#	error BME_OS not detected
#endif

#ifndef BME_COMPILER
#	error BME_COMPILER not detected
#endif

#ifndef BME_MODE
#	error BME_MODE not detected
#endif

#ifndef BME_SYSTEM
#	error BME_SYSTEM not detected
#endif

#ifndef BME_RENDERER
#	error BME_RENDERER not detected
#endif

#ifndef BME_SOUNDER
#	error BME_SOUNDER not detected
#endif

#ifndef BME_FILESYSTEM
#	error BME_FILESYSTEM not detected
#endif
