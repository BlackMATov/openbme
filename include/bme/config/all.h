/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: include/bme/config/all.h
 *****************************************************************************/

#pragma once

#include "base.h"

#include "clayer.h"
#include "cpplayer.h"
