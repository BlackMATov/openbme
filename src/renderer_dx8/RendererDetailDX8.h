/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/renderer_dx8/RendererDetailDX8.h
 *****************************************************************************/

#pragma once

#include <bme/core/Image.h>
#include <bme/core/Logger.h>
#include <bme/core/Receiver.h>
#include <bme/core/Application.h>

#include <bme/core/SystemImpl.h>
#include <bme/core/TextureImpl.h>
#include <bme/core/RendererImpl.h>

#if BME_RENDERER == BME_RENDERER_DX8

#include <dx8/d3d8.h>
#include <dx8/dxerr8.h>

#define D3DFVF_BMEVERTEX (D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1)

namespace bme
{
namespace renderer_dx8_detail
{
	bool             CheckD3DError      ( const char* place, HRESULT result );
	D3DFORMAT        SelectD3DFormat    ( E_FORMAT format );
	D3DPRIMITIVETYPE SelectD3DPrimitive ( Renderer::E_PRIMITIVE primitive );
	
	bool CopyPixels( IDirect3DSurface8* dest, IDirect3DSurface8* source );
	bool CopyPixels( IDirect3DSurface8* dest, IDirect3DTexture8* source );
	bool CopyPixels( IDirect3DTexture8* dest, IDirect3DSurface8* source );
	bool CopyPixels( IDirect3DTexture8* dest, IDirect3DTexture8* source );
	
	bool CopyPixels( IDirect3DTexture8* dest, const ImageDesc& bitmap_desc );
} // namespace renderer_dx8_detail
} // namespace bme

#endif // BME_RENDERER_DX8
