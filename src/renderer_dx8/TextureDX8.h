/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/renderer_dx8/TextureDX8.h
 *****************************************************************************/

#pragma once

#include "RendererDetailDX8.h"

#if BME_RENDERER == BME_RENDERER_DX8

namespace bme
{

// ----------------------------------------------------------------------------
// 
// TextureDX8
// 
// ----------------------------------------------------------------------------

class TextureDX8 : public TextureImpl
{
	IDirect3DTexture8* _d3dInterface;
	IDirect3DSurface8* _d3dDepth;
	IDirect3DSurface8* _d3dRTTSurface;
	bool               _create();
	bool               _createRTTSurf();
	void               _clear();
	
	E_LOCK_MODE        _lockMode;
	void*              _lockData;
	
public:
	
	TextureDX8();
	virtual ~TextureDX8();
	
	IDirect3DTexture8* getInterface  ();
	IDirect3DSurface8* getDepth      ();
	IDirect3DSurface8* getRTTSurface ();
	
	// Texture override -------------------------------------------------------
	bool               Lock          ( E_LOCK_MODE mode,
	                                   void** outData, size_t* outPitch );
	bool               Unlock        ();
	bool               isLocked      () const;
	// TextureImpl override ---------------------------------------------------
	bool               CreateImpl    ();
	bool               CreateImpl    ( const ImageDesc& desc );
	bool               CreateImpl    ( const TexturePtr& source );
	void               Release       ();
	bool               Recovery      ();
	// ------------------------------------------------------------------------
};

} // namespace bme

#endif // BME_RENDERER_DX8
