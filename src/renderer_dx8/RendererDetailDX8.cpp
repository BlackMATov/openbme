/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/renderer_dx8/RendererDetailDX8.cpp
 *****************************************************************************/

#include "RendererDetailDX8.h"
#include "RendererDX8.h"

#if BME_RENDERER == BME_RENDERER_DX8

namespace bme
{
namespace renderer_dx8_detail
{
	bool CheckD3DError(const char* place, HRESULT result)
	{
		if ( FAILED(result) ) {
			BME_LOG_ERROR_FMT(
				"%1%\nD3DError: %2% - %3%",
				place ? place : "",
				DXGetErrorString8A(result),
				DXGetErrorDescription8A(result));
			return true;
		}
		return false;
	}
	
	D3DFORMAT SelectD3DFormat(E_FORMAT format)
	{
		switch ( format ) {
		case FORMAT_X8R8G8B8:
			return D3DFMT_X8R8G8B8;
		case FORMAT_A8R8G8B8:
			return D3DFMT_A8R8G8B8;
		case FORMAT_DXT1:
			return D3DFMT_DXT1;
		case FORMAT_DXT3:
			return D3DFMT_DXT3;
		case FORMAT_DXT5:
			return D3DFMT_DXT5;
		default:
			BME_ASSERT(false);
			return D3DFMT_UNKNOWN;
		}
	}
	
	D3DPRIMITIVETYPE SelectD3DPrimitive(Renderer::E_PRIMITIVE primitive)
	{
		switch( primitive ) {
		case Renderer::PRIMITIVE_LINES:
			return D3DPT_LINELIST;
		case Renderer::PRIMITIVE_TRIANGLES:
			return D3DPT_TRIANGLELIST;
		default:
			BME_ASSERT(false);
			return D3DPT_FORCE_DWORD;
		}
	}
	
	bool CopyPixels(IDirect3DSurface8* dest, IDirect3DSurface8* source)
	{
		RendererDX8* device = RendererDX8::deviceImpl();
		if ( !device || !dest || !source ) {
			return false;
		}
		
		if ( CheckD3DError("renderer_dx8_detail::CopyPixels() CopyRects.",
			 device->getD3DDevice()->CopyRects(source, 0, 0, dest, 0)) )
		{
			return false;
		}
		
		return true;
	}
	
	bool CopyPixels(IDirect3DSurface8* dest, IDirect3DTexture8* source)
	{
		RendererDX8* device = RendererDX8::deviceImpl();
		if ( !device || !dest || !source ) {
			return false;
		}
		
		IDirect3DSurface8* src_surf = NULL;
		if ( CheckD3DError("renderer_dx8_detail::CopyPixels() GetSurfaceLevel.",
			 source->GetSurfaceLevel(0, &src_surf)) )
		{
			return false;
		}
		
		bool success = CopyPixels(dest, src_surf);
		src_surf->Release();
		return success;
	}
	
	bool CopyPixels(IDirect3DTexture8* dest, IDirect3DSurface8* source)
	{
		RendererDX8* device = RendererDX8::deviceImpl();
		if ( !device || !dest || !source ) {
			return false;
		}
		
		IDirect3DSurface8* dest_surf = NULL;
		if ( CheckD3DError("renderer_dx8_detail::CopyPixels() GetSurfaceLevel.",
			 dest->GetSurfaceLevel(0, &dest_surf)) )
		{
			return false;
		}
		
		bool success = CopyPixels(dest_surf, source);
		dest_surf->Release();
		return success;
	}
	
	bool CopyPixels(IDirect3DTexture8* dest, IDirect3DTexture8* source)
	{
		if ( !dest || !source ) {
			return false;
		}
		
		IDirect3DSurface8* dest_surf = NULL;
		if ( CheckD3DError("renderer_dx8_detail::CopyPixels() GetSurfaceLevel.",
			 dest->GetSurfaceLevel(0, &dest_surf)) )
		{
			return false;
		}
		
		bool success = CopyPixels(dest_surf, source);
		dest_surf->Release();
		return success;
	}
	
	bool CopyPixels(IDirect3DTexture8* dest, const ImageDesc& bitmap_desc)
	{
		if ( !dest || !bitmap_desc.pixels ) {
			return false;
		}
		
		D3DLOCKED_RECT TRect;
		if ( CheckD3DError("renderer_dx8_detail::CopyPixels() LockRect.",
			 dest->LockRect(0, &TRect, NULL, 0)) )
		{
			return false;
		}
		
		u8*       dest_bits  = reinterpret_cast<u8*>(TRect.pBits);
		size_t    dest_pitch = Math::SmartCast<size_t>(TRect.Pitch);
		
		const u8* src_bits   = reinterpret_cast<const u8*>(bitmap_desc.pixels.getBuffer());
		size_t    src_pitch  = bitmap_desc.pitch;
		size_t    src_height = bitmap_desc.size.y;
		
		if ( src_pitch == dest_pitch ) {
			BME_memcpy(dest_bits, src_bits, bitmap_desc.pixels.getSize());
		} else {
			for ( size_t i = 0; i < src_height; ++i ) {
				BME_memcpy(dest_bits, src_bits, src_pitch);
				src_bits  +=  src_pitch;
				dest_bits += dest_pitch;
			}
		}
		
		if ( CheckD3DError("renderer_dx8_detail::CopyPixels() UnlockRect.",
			 dest->UnlockRect(0)) )
		{
			return false;
		}
		
		return true;
	}
} // namespace renderer_dx8_detail
} // namespace bme

#endif // BME_RENDERER_DX8
