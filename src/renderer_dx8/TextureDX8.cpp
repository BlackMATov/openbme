/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/renderer_dx8/TextureDX8.cpp
 *****************************************************************************/

#include "TextureDX8.h"
#include "RendererDX8.h"

#if BME_RENDERER == BME_RENDERER_DX8

namespace bme
{

// ----------------------------------------------------------------------------
// 
// TextureImpl
// 
// ----------------------------------------------------------------------------

TextureImpl* TextureImpl::Create()
{
	return new TextureDX8();
}

// ----------------------------------------------------------------------------
// 
// TextureDX8
// 
// ----------------------------------------------------------------------------

TextureDX8::TextureDX8()
: _d3dInterface(NULL)
, _d3dDepth(NULL)
, _d3dRTTSurface(NULL)
, _lockMode(LOCK_READONLY)
, _lockData(NULL)
{
}

TextureDX8::~TextureDX8()
{
	_clear();
}

bool TextureDX8::_create()
{
	using namespace renderer_dx8_detail;
	RendererDX8* device = RendererDX8::deviceImpl();
	if ( !device ) {
		return false;
	}
	
	D3DFORMAT d3d_fmt = SelectD3DFormat(getFormat());
	if ( D3DFMT_UNKNOWN == d3d_fmt ) {
		BME_LOG_ERROR("TextureDX8::_create() can't select d3d format!");
		return false;
	}
	
	// create main surface
	if ( CheckD3DError("TextureDX8::_create() CreateTexture.",
		 device->getD3DDevice()->CreateTexture(
			Math::SmartCast<UINT>(getSize().x),
			Math::SmartCast<UINT>(getSize().y),
			1,
			isRenderTarget() ? D3DUSAGE_RENDERTARGET : 0,
			d3d_fmt,
			isRenderTarget() ? D3DPOOL_DEFAULT : D3DPOOL_MANAGED,
			&_d3dInterface)) )
	{
		return false;
	}
	
	// RT ZBuffer
	if ( isRenderTarget() && isZBuffer() ) {
		if ( CheckD3DError("TextureDX8::_create() CreateDepthStencilSurface.",
			 device->getD3DDevice()->CreateDepthStencilSurface(
				Math::SmartCast<UINT>(getSize().x),
				Math::SmartCast<UINT>(getSize().y),
				D3DFMT_D16,
				D3DMULTISAMPLE_NONE,
				&_d3dDepth)) )
		{
			return false;
		}
	}
	
	return true;
}

bool TextureDX8::_createRTTSurf()
{
	using namespace renderer_dx8_detail;
	RendererDX8* device = RendererDX8::deviceImpl();
	if ( !device || !getInterface() ) {
		return false;
	}
	
	D3DSURFACE_DESC desc;
	if ( CheckD3DError("TextureDX8::_createRTTSurf() GetLevelDesc.",
		 getInterface()->GetLevelDesc(0, &desc)) )
	{
		return false;
	}
	
	if ( CheckD3DError("TextureDX8::_createRTTSurf() CreateImageSurface.",
		 device->getD3DDevice()->CreateImageSurface(
			desc.Width, desc.Height, desc.Format, &_d3dRTTSurface)) )
	{
		return false;
	}
	
	return true;
}

void TextureDX8::_clear()
{
	if ( _d3dInterface ) {
		_d3dInterface->Release();
		_d3dInterface = NULL;
	}
	
	if ( _d3dDepth ) {
		_d3dDepth->Release();
		_d3dDepth = NULL;
	}
	
	if ( _d3dRTTSurface ) {
		_d3dRTTSurface->Release();
		_d3dRTTSurface = NULL;
	}
}

IDirect3DTexture8* TextureDX8::getInterface()
{
	return _d3dInterface;
}

IDirect3DSurface8* TextureDX8::getDepth()
{
	return _d3dDepth;
}

IDirect3DSurface8* TextureDX8::getRTTSurface()
{
	return _d3dRTTSurface;
}

bool TextureDX8::Lock(E_LOCK_MODE mode, void** outData, size_t* outPitch)
{
	using namespace renderer_dx8_detail;
	RendererDX8* device = RendererDX8::deviceImpl();
	if ( !device ) {
		return false;
	}
	
	if ( isLocked() ) {
		BME_LOG_ERROR("TextureDX8::Lock() texture already locked!");
		return false;
	}
	
	if ( !getInterface() ) {
		BME_LOG_ERROR("TextureDX8::Lock() texture not valid!");
		return false;
	}
	
	D3DLOCKED_RECT TRect;
	
	if ( isRenderTarget() ) {
		// create RTTSurface, if needed
		if ( !getRTTSurface() && !_createRTTSurf() ) {
			BME_LOG_ERROR("TextureDX8::Lock() can't create RTTSurface!");
			return false;
		}
		
		// copy texture data to RTTSurface, if needed
		if ( mode != LOCK_WRITEONLY &&
			!renderer_dx8_detail::CopyPixels(getRTTSurface(), getInterface()) )
		{
			BME_LOG_ERROR("TextureDX8::Lock() can't copy pixels to RTTSurface!");
			return false;
		}
		
		// lock RTTSurface
		if ( CheckD3DError("TextureDX8::Lock() LockRect.",
			 getRTTSurface()->LockRect(
				&TRect, 0, (mode == LOCK_READONLY) ? D3DLOCK_READONLY : 0)) )
		{
			return false;
		}
	} else {
		// lock texture surface
		if ( CheckD3DError("TextureDX8::Lock() LockRect.",
			 getInterface()->LockRect(
				0, &TRect, 0, (mode == LOCK_READONLY) ? D3DLOCK_READONLY : 0)) )
		{
			return false;
		}
	}
	
	if ( TRect.pBits ) {
		_lockMode = mode;
		_lockData = TRect.pBits;
		if ( outData )  *outData  = TRect.pBits;
		if ( outPitch ) *outPitch = TRect.Pitch;
		return true;
	}
	
	BME_LOG_ERROR("TextureDX8::Lock() unknown error");
	return false;
}

bool TextureDX8::Unlock()
{
	using namespace renderer_dx8_detail;
	RendererDX8* device = RendererDX8::deviceImpl();
	if ( !device ) {
		return false;
	}
	
	if ( !isLocked() ) {
		BME_LOG_ERROR("TextureDX8::Unlock() texture not locked!");
		return false;
	}
	
	if ( !getInterface() ) {
		BME_LOG_ERROR("TextureDX8::Unlock() texture not valid!");
		return false;
	}
	
	if ( isRenderTarget() && !getRTTSurface() ) {
		BME_LOG_ERROR("TextureDX8::Unlock() render target texture not valid!");
		return false;
	}
	
	if ( isRenderTarget() ) {
		//  unlock RTT surface
		if ( CheckD3DError("TextureDX8::Unlock() UnlockRect.",
			 getRTTSurface()->UnlockRect()) )
		{
			return false;
		}
		
		// apply result, if needed
		if ( _lockMode != LOCK_READONLY &&
			 !renderer_dx8_detail::CopyPixels(getInterface(), getRTTSurface()) )
		{
			BME_LOG_ERROR("TextureDX8::Unlock() can't copy pixels from RTTSurface!");
			return false;
		}
	} else {
		// unlock texture surfaces
		if ( CheckD3DError("TextureDX8::Unlock() UnlockRect.",
			 getInterface()->UnlockRect(0)) )
		{
			return false;
		}
	}
	
	_lockData = NULL;
	return true;
}

bool TextureDX8::isLocked() const
{
	return _lockData ? true : false;
}

bool TextureDX8::CreateImpl()
{
	return _create();
}

bool TextureDX8::CreateImpl(const ImageDesc& desc)
{
	using namespace renderer_dx8_detail;
	return
		_create() && CopyPixels(getInterface(), desc);
}

bool TextureDX8::CreateImpl(const TexturePtr& source)
{
	TextureDX8* other_tex = static_cast<TextureDX8*>(source.get());
	if ( !other_tex ) {
		return false;
	}
	using namespace renderer_dx8_detail;
	return
		_create() && CopyPixels(getInterface(), other_tex->getInterface());
}

void TextureDX8::Release()
{
	_clear();
}

bool TextureDX8::Recovery()
{
	// to dx8 needed recreate render target only
	if ( isRenderTarget() ) {
		return Create(getOriginalSize(), getFormat(), isRenderTarget(), isZBuffer());
	}
	return true;
}

} // namespace bme

#endif // BME_RENDERER_DX8
