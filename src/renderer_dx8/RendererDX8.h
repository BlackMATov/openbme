/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/renderer_dx8/RendererDX8.h
 *****************************************************************************/

#pragma once

#include "RendererDetailDX8.h"

#if BME_RENDERER == BME_RENDERER_DX8

namespace bme
{

class RendererDX8 : public RendererDevice
{
	// --------------------------------
	// renderer specific
	// --------------------------------
	
	D3DPRESENT_PARAMETERS _d3dPP;
	D3DCAPS8              _d3dCaps;
	
	IDirect3D8*           _d3d;
	IDirect3DDevice8*     _d3dDevice;
	bool                  _deviceLost;
	bool                  _createDevice      ();
	void                  _releaseDevice     ();
	bool                  _resetDevice       ();
	
	IDirect3DSurface8*    _d3dScreenSurf;
	IDirect3DSurface8*    _d3dScreenDepth;
	bool                  _prepareSurfaces   ();
	void                  _releaseSurfaces   ();
	
	// --------------------------------
	// common
	// --------------------------------
	
	void                  _checkFeatures     () const;
	void                  _setDefaultStates  ();
	bool                  _setTexture        ( const TexturePtr& texture );
	bool                  _setTextureFilter  ( bool yesno );
	bool                  _setTextureAddress ( Material::E_ADDRESS addres );
	bool                  _setBlend          ( RenderStates::E_BLEND blend );
	bool                  _setZEnable        ( bool yesno );
	
public:
	
	RendererDX8();
	virtual ~RendererDX8();
	static RendererDX8*   deviceImpl         ();
	
	IDirect3DDevice8*     getD3DDevice       () const;
	const D3DCAPS8&       getD3DCaps         () const;
	
	// Override ---------------------------------------------------------------
	bool                  Initialize         ();
	void                  Shutdown           ();
	void                  EndFrame           ();
	void                  Resize             ();
	void                  DeviceLost         ();
	
	bool                  BeginScene         ();
	void                  ClearScene         ( const Color& color, bool back_buffer, bool z_buffer );
	bool                  EndScene           ();
	
	void                  setTransform       ( E_TRANSFORM transform, const mat4f& mat );
	void                  setViewport        ( const rectu& vp );
	
	bool                  setRenderTarget    ( const TexturePtr& texture );
	void                  setStates          ( const Material& material, const RenderStates& states, bool lazy );
	void                  DrawPrimitives     ( const Vertex* vertices, size_t vertices_count,
	                                           const u16* indices, size_t indices_count,
	                                           Renderer::E_PRIMITIVE type );
	// ------------------------------------------------------------------------
};

} // namespace bme

#endif // BME_RENDERER_DX8
