/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/renderer_dx8/RendererDX8.cpp
 *****************************************************************************/

#include "RendererDX8.h"
#include "TextureDX8.h"

#if BME_RENDERER == BME_RENDERER_DX8

namespace bme
{

// ----------------------------------------------------------------------------
// 
// RendererDevice
// 
// ----------------------------------------------------------------------------

const vec2f& RendererDevice::getTexelOffset()
{
	static vec2f ret_value(-0.5f, -0.5f);
	return ret_value;
}

RendererDevice* RendererDevice::Create()
{
	return new RendererDX8();
}

// ----------------------------------------------------------------------------
// 
// RendererDX8
// 
// ----------------------------------------------------------------------------

RendererDX8::RendererDX8()
: _d3d(NULL)
, _d3dDevice(NULL)
, _deviceLost(false)
, _d3dScreenSurf(NULL)
, _d3dScreenDepth(NULL)
{
	::ZeroMemory(&_d3dPP,   sizeof(_d3dPP));
	::ZeroMemory(&_d3dCaps, sizeof(_d3dCaps));
}

RendererDX8::~RendererDX8()
{
}

RendererDX8* RendererDX8::deviceImpl()
{
	RendererImpl* renderer = getOwner();
	return
		renderer ?
		static_cast<RendererDX8*>(renderer->getDevice().get()) :
		NULL;
}

IDirect3DDevice8* RendererDX8::getD3DDevice() const
{
	return _d3dDevice;
}

const D3DCAPS8& RendererDX8::getD3DCaps() const
{
	return _d3dCaps;
}

bool RendererDX8::_createDevice()
{
	if ( getD3DDevice() ) {
		BME_LOG_ERROR("RendererDX8::_createDevice() device already created!");
		return false;
	}
	
	if ( !theApplication() ) {
		BME_LOG_ERROR("RendererDX8::_createDevice() application not found!");
		return false;
	}
	
	SystemImpl* system_impl = Holder<SystemImpl>::getHostage();
	if ( !system_impl ) {
		BME_LOG_ERROR("RendererDX8::_createDevice() system not found!");
		return false;
	}
	
	// --------------------------------
	// Create the IDirect3D8 object
	// --------------------------------
	
	_d3d = Direct3DCreate8(D3D_SDK_VERSION);
	if ( NULL == _d3d ) {
		BME_LOG_ERROR("RendererDX8::_createDevice() Direct3DCreate8 failed");
		return false;
	}
	
	// --------------------------------
	// device caps
	// --------------------------------
	
	if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_createDevice() GetDeviceCaps.",
		_d3d->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &_d3dCaps)) )
	{
		return false;
	}
	
	// --------------------------------
	// hardware vertex processing
	// --------------------------------
	
	DWORD vertexProcessing = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
	if( _d3dCaps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT ) {
		vertexProcessing = D3DCREATE_HARDWARE_VERTEXPROCESSING;
	}
	
	// --------------------------------
	// d3d present parameters
	// --------------------------------
	
	D3DDISPLAYMODE displayMode;
	if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_createDevice() GetAdapterDisplayMode.",
		_d3d->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode)) )
	{
		return false;
	}
	
	_d3dPP.BackBufferWidth  = Math::SmartCast<UINT>(system_impl->getWindowResolution().x);
	_d3dPP.BackBufferHeight = Math::SmartCast<UINT>(system_impl->getWindowResolution().y);
	_d3dPP.BackBufferFormat = displayMode.Format;
	_d3dPP.BackBufferCount  = 1;
	_d3dPP.MultiSampleType  = D3DMULTISAMPLE_NONE;
	_d3dPP.hDeviceWindow    = reinterpret_cast<HWND>(system_impl->getHWND());
	
	if ( bool this_is_for_true_fullscreen = false ) {
		_d3dPP.Windowed = false;
		_d3dPP.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	} else {
		_d3dPP.Windowed = true;
		_d3dPP.FullScreen_RefreshRateInHz = 0;
	}
	
	// --------------------------------
	// vsync
	// --------------------------------
	
	if ( bool this_is_for_true_fullscreen = false ) {
		_d3dPP.SwapEffect = D3DSWAPEFFECT_FLIP;
		if ( theApplication()->getDesc().isVsync ) {
			_d3dPP.FullScreen_PresentationInterval = D3DPRESENT_INTERVAL_ONE;
		} else {
			_d3dPP.FullScreen_PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
		}
	} else {
		if ( theApplication()->getDesc().isVsync ) {
			_d3dPP.SwapEffect = D3DSWAPEFFECT_COPY_VSYNC;
		} else {
			_d3dPP.SwapEffect = D3DSWAPEFFECT_COPY;
		}
	}
	
	// --------------------------------
	// zbuffer
	// --------------------------------
	
	_d3dPP.EnableAutoDepthStencil = TRUE;
	_d3dPP.AutoDepthStencilFormat = D3DFMT_D16;
	
	// --------------------------------
	// create d3d device
	// --------------------------------
	
	if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_createDevice() CreateDevice.",
		_d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, _d3dPP.hDeviceWindow,
		vertexProcessing, &_d3dPP, &_d3dDevice)) )
	{
		return false;
	}
	
	return true;
}

void RendererDX8::_releaseDevice()
{
	if ( _d3dDevice ) {
		_d3dDevice->Release();
		_d3dDevice = NULL;
	}
	
	if ( _d3d ) {
		_d3d->Release();
		_d3d = NULL;
	}
}

bool RendererDX8::_resetDevice()
{
	const char* log_str = "Resetting device.";
	
	// --------------------------------
	_releaseSurfaces();
	getOwner()->ReleaseTextures(true);
	// --------------------------------
	
	HRESULT hr = _d3dDevice->Reset(&_d3dPP);
	
	// --------------------------------
	_prepareSurfaces();
	if ( !getOwner()->RecoveryTextures() ) {
		BME_LOG_ERROR(log_str);
		return false;
	}
	// --------------------------------
	
	if ( FAILED(hr) ) {
		BME_LOG_ERROR(log_str);
		return false;
	}
	
	_deviceLost = false;
	_setDefaultStates();
	
	Receiver::Event e;
	e.type = Receiver::Event::TYPE_RESET_DEVICE;
	e.Push();
	
	BME_LOG_COMPLETE(log_str);
	return true;
}

bool RendererDX8::_prepareSurfaces()
{
	if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_prepareSurfaces() GetRenderTarget.",
		getD3DDevice()->GetRenderTarget(&_d3dScreenSurf)) )
	{
		return false;
	}
	
	if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_prepareSurfaces() GetDepthStencilSurface.",
		getD3DDevice()->GetDepthStencilSurface(&_d3dScreenDepth)) )
	{
		return false;
	}
	
	return true;
}

void RendererDX8::_releaseSurfaces()
{
	if ( _d3dScreenSurf ) {
		_d3dScreenSurf->Release();
		_d3dScreenSurf = NULL;
	}
	
	if ( _d3dScreenDepth ) {
		_d3dScreenDepth->Release();
		_d3dScreenDepth = NULL;
	}
}

void RendererDX8::_checkFeatures() const
{
	if ( !theRenderer() || !getD3DDevice() ) {
		return;
	}
	
	// NPOT textures
	if ( !(_d3dCaps.TextureCaps & D3DPTEXTURECAPS_POW2) ) {
		theRenderer()->getCaps().npot_textures = true;
	}
	
	// render to texture (supported always)
	theRenderer()->getCaps().render_to_texture = true;
	
	// max texture size
	theRenderer()->getCaps().max_texture_size = pnt2u(_d3dCaps.MaxTextureWidth, _d3dCaps.MaxTextureHeight);
}

void RendererDX8::_setDefaultStates()
{
	if ( _d3dCaps.AlphaCmpCaps & D3DPCMPCAPS_GREATEREQUAL ) {
		getD3DDevice()->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
		getD3DDevice()->SetRenderState(D3DRS_ALPHAREF,        0x00000001);
		getD3DDevice()->SetRenderState(D3DRS_ALPHAFUNC,       D3DCMP_GREATEREQUAL);
	}
	
	getD3DDevice()->SetRenderState(D3DRS_LIGHTING, FALSE);
	getD3DDevice()->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	
	getD3DDevice()->SetTextureStageState(0, D3DTSS_COLOROP,   D3DTOP_MODULATE);
	getD3DDevice()->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	getD3DDevice()->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
	getD3DDevice()->SetTextureStageState(0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE);
	getD3DDevice()->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	getD3DDevice()->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
	
	RetryCurrentStates();
}

bool RendererDX8::_setTexture(
	const TexturePtr& texture)
{
	TextureDX8* impl = static_cast<TextureDX8*>(texture.get());
	if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setTexture()",
		getD3DDevice()->SetTexture(0, impl ? impl->getInterface() : 0)))
	{
		return false;
	}
	
	_currentMaterial.texture = texture;
	return true;
}

bool RendererDX8::_setTextureFilter(
	bool yesno)
{
	if ( yesno ) {
		if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setTextureFilter()",
			getD3DDevice()->SetTextureStageState(0, D3DTSS_MAGFILTER, D3DTEXF_LINEAR)))
		{
			return false;
		}
		
		if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setTextureFilter()",
			getD3DDevice()->SetTextureStageState(0, D3DTSS_MINFILTER, D3DTEXF_LINEAR)))
		{
			return false;
		}
	} else {
		if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setTextureFilter()",
			getD3DDevice()->SetTextureStageState(0, D3DTSS_MAGFILTER, D3DTEXF_POINT)))
		{
			return false;
		}
		
		if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setTextureFilter()",
			getD3DDevice()->SetTextureStageState(0, D3DTSS_MINFILTER, D3DTEXF_POINT)))
		{
			return false;
		}
	}
	
	if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setTextureFilter()",
		getD3DDevice()->SetTextureStageState(0, D3DTSS_MIPFILTER, D3DTEXF_POINT)))
	{
		return false;
	}
	
	_currentMaterial.filter = yesno;
	return true;
}

bool RendererDX8::_setTextureAddress(
	Material::E_ADDRESS address)
{
	switch(address) {
	case Material::ADDRESS_WRAP:
		{
			if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setTextureAddress()",
				getD3DDevice()->SetTextureStageState(0, D3DTSS_ADDRESSU, D3DTADDRESS_WRAP)))
			{
				return false;
			}
			
			if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setTextureAddress()",
				getD3DDevice()->SetTextureStageState(0, D3DTSS_ADDRESSV, D3DTADDRESS_WRAP)))
			{
				return false;
			}
		}
		break;
	case Material::ADDRESS_CLAMP:
		{
			if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setTextureAddress()",
				getD3DDevice()->SetTextureStageState(0, D3DTSS_ADDRESSU, D3DTADDRESS_CLAMP)))
			{
				return false;
			}
			
			if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setTextureAddress()",
				getD3DDevice()->SetTextureStageState(0, D3DTSS_ADDRESSV, D3DTADDRESS_CLAMP)))
			{
				return false;
			}
		}
		break;
	default:
		BME_ASSERT(false);
		return false;
	}
	
	_currentMaterial.address = address;
	return true;
}

bool RendererDX8::_setBlend(
	RenderStates::E_BLEND blend)
{
	switch(blend) {
	case RenderStates::BLEND_ALPHA:
		{
			if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setBlend()",
				getD3DDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE)))
			{
				return false;
			}
			
			if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setBlend()",
				getD3DDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA)))
			{
				return false;
			}
			
			if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setBlend()",
				getD3DDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA)))
			{
				return false;
			}
		}
		break;
	case RenderStates::BLEND_ADD:
		{
			if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setBlend()",
				getD3DDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE)))
			{
				return false;
			}
			
			if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setBlend()",
				getD3DDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA)))
			{
				return false;
			}
			
			if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setBlend()",
				getD3DDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE)))
			{
				return false;
			}
		}
		break;
	case RenderStates::BLEND_MULTIPLY:
		{
			if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setBlend()",
				getD3DDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE)))
			{
				return false;
			}
			
			if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setBlend()",
				getD3DDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_DESTCOLOR)))
			{
				return false;
			}
			
			if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setBlend()",
				getD3DDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO)))
			{
				return false;
			}
		}
		break;
	case RenderStates::BLEND_NONE:
		{
			if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setBlend()",
				getD3DDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE)))
			{
				return false;
			}
		}
		break;
	default:
		BME_ASSERT(false);
		return false;
	}
	
	_currentRenderStates.blend = blend;
	return true;
}

bool RendererDX8::_setZEnable(
	bool yesno)
{
	bool success = true;
	
	if ( renderer_dx8_detail::CheckD3DError("RendererDX8::_setZEnable()",
		getD3DDevice()->SetRenderState(D3DRS_ZENABLE, yesno ? TRUE : FALSE)) )
	{
		success = false;
	}
	
	if ( yesno && renderer_dx8_detail::CheckD3DError("RendererDX8::_setZEnable()",
		getD3DDevice()->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL)) )
	{
		success = false;
	}
	
	_currentRenderStates.zEnable = yesno;
	return success;
}

// ----------------------------------------------------------------------------
// 
// Override
// 
// ----------------------------------------------------------------------------

bool RendererDX8::Initialize()
{
	if ( !_createDevice() ) {
		return false;
	}
	
	if ( !_prepareSurfaces() ) {
		return false;
	}
	
	_checkFeatures();
	_setDefaultStates();
	return true;
}

void RendererDX8::Shutdown()
{
	ResetCurrentState();
	_releaseSurfaces();
	_releaseDevice();
}

void RendererDX8::EndFrame()
{
	HRESULT hr = getD3DDevice()->Present(NULL, NULL, NULL, NULL);
	if ( SUCCEEDED(hr) ) {
		return;
	}
	
	if ( hr == D3DERR_DEVICELOST ) {
		DeviceLost();
	} else {
		BME_LOG_WARNING("RendererDX8::EndFrame() failed");
	}
}

void RendererDX8::Resize()
{
	if ( !theSystem() ) {
		BME_LOG_ERROR("RendererDX8::Resize() system not found!");
		return;
	}
	
	_d3dPP.BackBufferWidth  = Math::SmartCast<UINT>(theSystem()->getWindowResolution().x);
	_d3dPP.BackBufferHeight = Math::SmartCast<UINT>(theSystem()->getWindowResolution().y);
	
	DeviceLost();
	_resetDevice();
}

void RendererDX8::DeviceLost()
{
	_deviceLost = true;
	BME_LOG_WARNING("Renderer device lost.");
}

bool RendererDX8::BeginScene()
{
	// check device lost
	
	if ( _deviceLost ) {
		HRESULT hr = getD3DDevice()->TestCooperativeLevel();
		if ( FAILED(hr) ) {
			if ( hr == D3DERR_DEVICELOST ) {
				::Sleep(100);
				hr = getD3DDevice()->TestCooperativeLevel();
				if ( hr == D3DERR_DEVICELOST ) {
					return false;
				}
			}
			if ( hr == D3DERR_DEVICENOTRESET ) {
				if ( !_resetDevice() ) {
					return false;
				}
			}
		}
	}
	
	// begin scene
	
	if ( renderer_dx8_detail::CheckD3DError("RendererDX8::BeginScene() SetVertexShader",
		getD3DDevice()->SetVertexShader(D3DFVF_BMEVERTEX)) )
	{
		return false;
	}
	
	if ( renderer_dx8_detail::CheckD3DError("RendererDX8::BeginScene() BeginScene",
		getD3DDevice()->BeginScene()) )
	{
		return false;
	}
	
	return true;
}

void RendererDX8::ClearScene(
	const Color& color, bool backBuffer, bool zBuffer)
{
	DWORD flags = 0;
	
	if ( backBuffer ) {
		flags |= D3DCLEAR_TARGET;
	}
	
	if ( zBuffer ) {
		if (!_currentRenderTarget || _currentRenderTarget->isZBuffer()) {
			flags |= D3DCLEAR_ZBUFFER;
		}
	}
	
	renderer_dx8_detail::CheckD3DError("RendererDX8::ClearScene() Clear",
		getD3DDevice()->Clear(0, NULL, flags, color.getARGB(), 1.f, 0));
}

bool RendererDX8::EndScene()
{
	HRESULT hr = getD3DDevice()->EndScene();
	if ( SUCCEEDED(hr) ) {
		return true;
	}
	
	if ( hr == D3DERR_DEVICELOST ) {
		BME_LOG_WARNING("RendererDX8::EndScene() direct3d8 device lost");
	} else {
		BME_LOG_WARNING("RendererDX8::EndScene() EndScene failed");
	}
	
	return false;
}

void RendererDX8::setTransform(
	E_TRANSFORM transform, const mat4f& mat)
{
	switch(transform) {
	case TRANSFORM_VIEW:
		_viewMat = mat;
		renderer_dx8_detail::CheckD3DError("RendererDX8::setTransform()",
			getD3DDevice()->SetTransform(D3DTS_VIEW, (D3DMATRIX*)&mat));
		break;
	case TRANSFORM_WORLD:
		_worldMat = mat;
		renderer_dx8_detail::CheckD3DError("RendererDX8::setTransform()",
			getD3DDevice()->SetTransform(D3DTS_WORLD, (D3DMATRIX*)&mat));
		break;
	case TRANSFORM_PROJECTION:
		_projMat = mat;
		renderer_dx8_detail::CheckD3DError("RendererDX8::setTransform()",
			getD3DDevice()->SetTransform(D3DTS_PROJECTION, (D3DMATRIX*)&mat));
		break;
	default:
		BME_ASSERT(false);
	}
}

void RendererDX8::setViewport(
	const rectu& vp)
{
	D3DVIEWPORT8 d3dVP;
	d3dVP.X      = Math::SmartCast<DWORD>(vp.getTopLeft().x);
	d3dVP.Y      = Math::SmartCast<DWORD>(vp.getTopLeft().y);
	d3dVP.Width  = Math::SmartCast<DWORD>(vp.getWidth  ());
	d3dVP.Height = Math::SmartCast<DWORD>(vp.getHeight ());
	d3dVP.MinZ   = 0.f;
	d3dVP.MaxZ   = 1.f;
	
	if ( !renderer_dx8_detail::CheckD3DError("RendererDX8::setViewport()",
		getD3DDevice()->SetViewport(&d3dVP)) )
	{
		_viewport = vp;
	}
}

bool RendererDX8::setRenderTarget(
	const TexturePtr& texture)
{
	TextureDX8* textureDX8 = NULL;
	
	// check render target texture
	
	if ( texture ) {
		textureDX8 = static_cast<TextureDX8*>(texture.get());
		
		if ( !textureDX8->isRenderTarget() ) {
			BME_LOG_ERROR("RendererDX8::setRenderTarget() non render target texture");
			return false;
		}
		
		if ( !textureDX8->getInterface() ) {
			BME_LOG_ERROR("RendererDX8::setRenderTarget() texture not valid!");
			return false;
		}
	}
	
	// set render target
	
	IDirect3DSurface8* pSurf  = _d3dScreenSurf;
	IDirect3DSurface8* pDepth = _d3dScreenDepth;
	
	if ( texture ) {
		textureDX8->getInterface()->GetSurfaceLevel(0, &pSurf);
		pDepth = textureDX8->getDepth();
	}
	
	HRESULT hr = getD3DDevice()->SetRenderTarget(pSurf, pDepth);
	
	if ( texture ) {
		pSurf->Release();
	}
	
	if ( FAILED(hr) ) {
		BME_LOG_ERROR("RendererDX8::setRenderTarget() Can't set render target");
		return false;
	}
	
	_currentRenderTarget = texture;
	return true;
}

void RendererDX8::setStates(
	const Material& material, const RenderStates& states, bool lazy)
{
	// Material
	
	bool textureChanged = false;
	
	if ( !lazy || _currentMaterial.texture != material.texture ) {
		textureChanged = true;
		if (!_setTexture(material.texture)) {
			BME_LOG_ERROR("RendererDX8::setStates() setTexture failed");
		}
	}
	
	if ( !lazy || textureChanged || _currentMaterial.filter != material.filter ) {
		if (!_setTextureFilter(material.filter)) {
			BME_LOG_ERROR("RendererDX8::setStates() setTextureFilter failed");
		}
	}
	
	if ( !lazy || textureChanged || _currentMaterial.address != material.address ) {
		if (!_setTextureAddress(material.address)) {
			BME_LOG_ERROR("RendererDX8::setStates() setTextureAddress failed");
		}
	}
	
	// Render states
	
	if ( !lazy || _currentRenderStates.blend != states.blend ) {
		if (!_setBlend(states.blend)) {
			BME_LOG_ERROR("RendererDX8::setStates() setBlend failed");
		}
	}
	
	if ( !lazy || _currentRenderStates.zEnable != states.zEnable ) {
		if (!_setZEnable(states.zEnable)) {
			BME_LOG_ERROR("RendererDX8::setStates() setZEnable failed");
		}
	}
}

void RendererDX8::DrawPrimitives(
	const Vertex* vertices, size_t vertices_count,
	const u16* indices, size_t indices_count,
	Renderer::E_PRIMITIVE type)
{
	D3DPRIMITIVETYPE d3dprim = renderer_dx8_detail::SelectD3DPrimitive(type);
	if ( D3DPT_FORCE_DWORD == d3dprim || !vertices || vertices_count <= 0 ) {
		return;
	}
	
	if ( indices && indices_count > 0 && indices_count > vertices_count ) {
		renderer_dx8_detail::CheckD3DError("RendererDX8::DrawIPrimitives() DrawIndexedPrimitiveUP.",
			getD3DDevice()->DrawIndexedPrimitiveUP(
			d3dprim, 0, Math::SmartCast<UINT>(indices_count), Math::SmartCast<UINT>(indices_count/type),
			indices, D3DFMT_INDEX16, vertices, sizeof(Vertex)));
	} else {
		renderer_dx8_detail::CheckD3DError("RendererDX8::DrawVPrimitives() DrawPrimitiveUP.",
			getD3DDevice()->DrawPrimitiveUP(
			d3dprim, Math::SmartCast<UINT>(vertices_count/type),
			vertices, sizeof(Vertex)));
	}
}

} // namespace bme

#endif // BME_RENDERER_DX8
