/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/sounder_bass/SoundBASS.cpp
 *****************************************************************************/

#include "SoundBASS.h"
#include "SounderBASS.h"

#if BME_SOUNDER == BME_SOUNDER_BASS

namespace bme
{

// ----------------------------------------------------------------------------
// 
// SoundImpl && ChannelImpl
// 
// ----------------------------------------------------------------------------

SoundImpl* SoundImpl::Create()
{
	return new SoundBASS();
}

ChannelImpl* ChannelImpl::Create(const StrId& group, const SoundPtr &sound)
{
	return new ChannelBASS(group, sound);
}

// ----------------------------------------------------------------------------
// 
// SoundBASS
// 
// ----------------------------------------------------------------------------

void CALLBACK SoundBASS::_file_close_proc(
	void *user)
{
	ReadFilePtr& file = static_cast<SoundBASS*>(user)->_streamFile;
	if ( file ) {
		file.reset();
	}
}

QWORD CALLBACK SoundBASS::_file_len_proc(
	void *user)
{
	const ReadFilePtr& file = static_cast<SoundBASS*>(user)->_streamFile;
	return
		file ? Math::SmartCast<QWORD>(file->getSize()) : 0;
}

DWORD CALLBACK SoundBASS::_file_read_proc(
	void *buffer, DWORD length, void *user)
{
	const ReadFilePtr& file = static_cast<SoundBASS*>(user)->_streamFile;
	return
		file ? Math::SmartCast<DWORD>(file->Read(buffer, length)) : 0;
}

BOOL CALLBACK SoundBASS::_file_seek_proc(
	QWORD offset, void *user)
{
	const ReadFilePtr& file = static_cast<SoundBASS*>(user)->_streamFile;
	return
		file ? file->Seek(Math::SmartCast<ptrdiff_t>(offset), false) : FALSE;
}

bool SoundBASS::CreateImpl(const Path& path, f32& outLength)
{
	if ( isStreamed() ) {
		_streamFile = theFilesystem() ?
			theFilesystem()->OpenFile(path) : ReadFilePtr();
		if ( _streamFile ) {
			BASS_FILEPROCS file_proc =
				{_file_close_proc, _file_len_proc, _file_read_proc, _file_seek_proc};
			_interface = BASS_StreamCreateFileUser(
				STREAMFILE_NOBUFFER, 0, &file_proc, this);
			if ( _interface ) {
				QWORD pos_byte = BASS_ChannelGetLength(_interface, BASS_POS_BYTE);
				outLength = Math::SmartCast<f32>(
					BASS_ChannelBytes2Seconds(_interface, pos_byte));
			}
		}
		return 0 != _interface;
	} else {
		ReadFilePtr file = theFilesystem() ?
			theFilesystem()->OpenFile(path) : ReadFilePtr();
		MMapFilePtr mmap_file = file ? file->MakeMMap() : MMapFilePtr();
		return CreateImpl(mmap_file->getData(), mmap_file->getSize(), outLength);
	}
}

bool SoundBASS::CreateImpl(const void* data, size_t size, f32& outLength)
{
	if ( !isStreamed() && data && size ) {
		_interface = BASS_SampleLoad(
			TRUE, data, 0, Math::SmartCast<DWORD>(size),
			BME_MAX_SOUND_CHANNELS, BASS_SAMPLE_OVER_VOL);
		if ( _interface ) {
			QWORD pos_byte = BASS_ChannelGetLength(_interface, BASS_POS_BYTE);
			outLength = Math::SmartCast<f32>(
				BASS_ChannelBytes2Seconds(_interface, pos_byte));
		}
	}
	return 0 != _interface;
}

SoundBASS::SoundBASS()
: _interface(0)
{
}

SoundBASS::~SoundBASS()
{
	if ( getInterface() ) {
		if ( isStreamed() ) {
			BASS_StreamFree(getInterface());
		} else {
			BASS_SampleFree(getInterface());
		}
		_interface = 0;
	}
}

DWORD SoundBASS::getInterface() const
{
	return _interface;
}

// ----------------------------------------------------------------------------
// 
// ChannelBASS
// 
// ----------------------------------------------------------------------------

bool ChannelBASS::PlayImpl()
{
	return getInterface() ?
		TRUE == BASS_ChannelPlay(getInterface(), TRUE) : false;
}

bool ChannelBASS::CreateImpl()
{
	const SoundBASS* sound_bass = static_cast<const SoundBASS*>(getSound().get());
	if ( sound_bass && sound_bass->getInterface() ) {
		_interface = isStreamed()
			? sound_bass->getInterface()
			: BASS_SampleGetChannel(sound_bass->getInterface(), FALSE);
	}
	return 0 != _interface;
}

void ChannelBASS::setPropertyImpl(E_PROPERTY prop, f32 value)
{
	switch ( prop )
	{
	case PROPERTY_PANNING:
		{
			if ( getInterface() ) {
				BASS_ChannelSetAttribute(
					getInterface(), BASS_ATTRIB_PAN, value);
			}
		}
		break;
	case PROPERTY_PITCH:
		{
			if ( getInterface() ) {
				BASS_CHANNELINFO info;
				BASS_ChannelGetInfo(getInterface(), &info );
				BASS_ChannelSetAttribute(
					getInterface(), BASS_ATTRIB_FREQ, (value*info.freq));
			}
		}
		break;
	case PROPERTY_VOLUME:
		{
			if ( getInterface() ) {
				BASS_ChannelSetAttribute(
					getInterface(), BASS_ATTRIB_VOL, value);
			}
		}
		break;
	case PROPERTY_POSITION:
		{
			if ( getInterface() ) {
				QWORD time = BASS_ChannelSeconds2Bytes(
					getInterface(), Math::SmartCast<double>(value));
				BASS_ChannelSetPosition(
					getInterface(), time, BASS_POS_BYTE);
			}
		}
		break;
	default:
		BME_ASSERT(false);
		break;
	}
}

f32 ChannelBASS::getPropertyImpl(E_PROPERTY prop) const
{
	f32 ret_value = 0.f;
	switch ( prop )
	{
	case PROPERTY_PANNING:
		{
			if ( getInterface() ) {
				BASS_ChannelGetAttribute(
					getInterface(), BASS_ATTRIB_PAN, &ret_value);
			}
		}
		break;
	case PROPERTY_PITCH:
		{
			if ( getInterface() ) {
				f32 freq = 0.f;
				BASS_CHANNELINFO info;
				BASS_ChannelGetInfo(getInterface(), &info);
				BASS_ChannelGetAttribute(
					getInterface(), BASS_ATTRIB_FREQ, &freq);
				ret_value = freq / info.freq;
			}
		}
		break;
	case PROPERTY_VOLUME:
		{
			if ( getInterface() ) {
				BASS_ChannelGetAttribute(
					getInterface(), BASS_ATTRIB_VOL, &ret_value);
			}
		}
		break;
	case PROPERTY_POSITION:
		{
			if ( getInterface() ) {
				QWORD pos_byte = BASS_ChannelGetPosition(getInterface(), BASS_POS_BYTE);
				ret_value = Math::SmartCast<f32>(
					BASS_ChannelBytes2Seconds(getInterface(), pos_byte));
			}
		}
		break;
	default:
		BME_ASSERT(false);
	}
	return ret_value;
}

ChannelBASS::ChannelBASS(const StrId& group, const SoundPtr &sound)
: ChannelImpl(group, sound)
, _interface(0)
, _looped(false)
{
}

ChannelBASS::~ChannelBASS()
{
}

DWORD ChannelBASS::getInterface() const
{
	return _interface;
}

bool ChannelBASS::Pause()
{
	return getInterface() ?
		TRUE == BASS_ChannelPause(getInterface()) : false;
}

bool ChannelBASS::Resume()
{
	return getInterface() ?
		TRUE == BASS_ChannelPlay(getInterface(), FALSE) : false;
}

bool ChannelBASS::Stop()
{
	return getInterface() ?
		TRUE == BASS_ChannelStop(getInterface()) : false;
}

bool ChannelBASS::isStopped() const
{
	return
		!getInterface() ||
		BASS_ACTIVE_STOPPED == BASS_ChannelIsActive(getInterface());
}

void ChannelBASS::setLooped(bool yesno)
{
	_looped = yesno;
	if ( getInterface() ) {
		BASS_ChannelFlags(
			getInterface(),
			yesno ? BASS_SAMPLE_LOOP : 0,
			BASS_SAMPLE_LOOP);
	}
}

bool ChannelBASS::isLooped() const
{
	return _looped;
}

} // namespace bme

#endif // BME_SOUNDER_BASS
