/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/sounder_bass/SoundBASS.h
 *****************************************************************************/

#pragma once

#include "SounderDetailBASS.h"

#if BME_SOUNDER == BME_SOUNDER_BASS

namespace bme
{

// ----------------------------------------------------------------------------
// 
// SoundBASS
// 
// ----------------------------------------------------------------------------

class SoundBASS : public SoundImpl
{
	DWORD       _interface;
	ReadFilePtr _streamFile;
	
	static void  CALLBACK _file_close_proc ( void *user );
	static QWORD CALLBACK _file_len_proc   ( void *user );
	static DWORD CALLBACK _file_read_proc  ( void *buffer, DWORD length, void *user );
	static BOOL  CALLBACK _file_seek_proc  ( QWORD offset, void *user );
	
protected:
	
	// SoundImpl override -----------------------------------------------------
	bool  CreateImpl      ( const Path& path, f32& outLength );
	bool  CreateImpl      ( const void* data, size_t size, f32& outLength );
	// ------------------------------------------------------------------------
	
public:
	
	SoundBASS();
	virtual ~SoundBASS();
	
	DWORD getInterface    () const;
};

// ----------------------------------------------------------------------------
// 
// ChannelBASS
// 
// ----------------------------------------------------------------------------

class ChannelBASS : public ChannelImpl
{
	DWORD _interface;
	bool  _looped;
	
protected:
	
	// ChannelImpl override ---------------------------------------------------
	bool  PlayImpl        ();
	bool  CreateImpl      ();
	void  setPropertyImpl ( E_PROPERTY prop, f32 value );
	f32   getPropertyImpl ( E_PROPERTY prop ) const;
	// ------------------------------------------------------------------------
	
public:
	
	ChannelBASS(const StrId& group, const SoundPtr &sound);
	virtual ~ChannelBASS();
	
	DWORD getInterface    () const;
	
	// Sound override ---------------------------------------------------------
	bool  Pause           ();
	bool  Resume          ();
	bool  Stop            ();
	bool  isStopped       () const;
	
	void  setLooped       ( bool yesno );
	bool  isLooped        () const;
	// ------------------------------------------------------------------------
};

} // namespace bme

#endif // BME_SOUNDER_BASS
