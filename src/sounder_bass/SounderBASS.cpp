/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/sounder_bass/SounderBASS.cpp
 *****************************************************************************/

#include "SounderBASS.h"
#include "SoundBASS.h"

#if BME_SOUNDER == BME_SOUNDER_BASS

namespace bme
{

// ----------------------------------------------------------------------------
// 
// SounderImpl
// 
// ----------------------------------------------------------------------------

bool SounderImpl::Initialize()
{
	new SounderBASS();
	return true;
}

void SounderImpl::Shutdown()
{
	delete theSounder();
}

// ----------------------------------------------------------------------------
// 
// SounderBASS
// 
// ----------------------------------------------------------------------------

SounderBASS::SounderBASS()
{
	_valid = (TRUE == BASS_Init(-1, 44100, 0, 0, NULL));
	if ( !isValid() ) {
		BME_LOG_WARNING("BASS sounder initialize error!");
	}
}

SounderBASS::~SounderBASS()
{
	if ( isValid() ) {
		BASS_Free();
	}
}

bool SounderBASS::isValid() const
{
	return _valid;
}

} // namespace bme

#endif // BME_SOUNDER_BASS
