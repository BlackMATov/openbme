/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/sounder_bass/SounderBASS.h
 *****************************************************************************/

#pragma once

#include "SounderDetailBASS.h"

#if BME_SOUNDER == BME_SOUNDER_BASS

namespace bme
{

class SounderBASS : public SounderImpl
{
	bool _valid;
	
public:

	SounderBASS();
	virtual ~SounderBASS();
	
	// Sounder override -------------------------------------------------------
	bool isValid() const;
	// ------------------------------------------------------------------------
};

} // namespace bme

#endif // BME_SOUNDER_BASS
