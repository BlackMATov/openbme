/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/sounder_bass/SounderDetailBASS.h
 *****************************************************************************/

#pragma once

#include <bme/core/Files.h>
#include <bme/core/Logger.h>
#include <bme/core/Filesystem.h>

#include <bme/core/SoundImpl.h>
#include <bme/core/SounderImpl.h>

#if BME_SOUNDER == BME_SOUNDER_BASS

#include <bass/bass.h>

#endif // BME_SOUNDER_BASS
