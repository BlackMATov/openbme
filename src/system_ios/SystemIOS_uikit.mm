/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_macosx/SystemIOS_uikit.mm
 *****************************************************************************/

#import "SystemIOS_uikit.h"

#if BME_SYSTEM == BME_SYSTEM_IOS

#include "SystemIOS.h"

// ----------------------------------------------------------------------------
// 
// BMEUIKitMain
// 
// ----------------------------------------------------------------------------

int main(int argc, char *argv[])
{
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	int ret = UIApplicationMain(argc, argv, nil, NSStringFromClass([BMEUIKitDelegate class]));
	[pool release];
	return ret;
}

// -----------------------------------------------------------------------------
//
// bme::renderer_gl_detail
//
// -----------------------------------------------------------------------------

namespace bme { namespace renderer_gl_detail
{
	GLuint GetDefaultFrameBuffer()
	{
		BMEUIKitView* view = uikit_detail::GetView();
		return view ? [view GetFrameBuffer] : 0;
	}
}} // bme::renderer_gl_detail

// -----------------------------------------------------------------------------
// 
// bme::uikit_detail
// 
// -----------------------------------------------------------------------------

namespace bme { namespace uikit_detail
{
	BMEUIKitView* GetView()
	{
		return [GetDelegate() getView];
	}
	
	BMEUIKitWindow* GetWindow()
	{
		return [GetDelegate() getWindow];
	}
	
	BMEUIKitViewController*	GetController()
	{
		return [GetDelegate() getController];
	}
	
	BMEUIKitDelegate* GetDelegate()
	{
		return (BMEUIKitDelegate*)[GetApplication() delegate];
	}
	
	UIApplication* GetApplication()
	{
		return [UIApplication sharedApplication];
	}
	
	void DoMouseEvent(UITouchPhase phase, const vec2f& pos, size_t which)
	{
		Receiver::Event e;
		e.mouse.key   = MOUSE_LEFT;
		e.mouse.x     = pos.x;
		e.mouse.y     = pos.y;
		e.mouse.which = which;
		switch (phase) {
			case UITouchPhaseBegan:
				e.type = Receiver::Event::TYPE_MOUSE_DOWN;
				e.Push();
				break;
			case UITouchPhaseMoved:
				e.type = Receiver::Event::TYPE_MOUSE_MOVE;
				e.Push();
				break;
			case UITouchPhaseEnded:
			case UITouchPhaseCancelled:
				e.type = Receiver::Event::TYPE_MOUSE_UP;
				e.Push();
				break;
			case UITouchPhaseStationary:
				// nothing
				break;
		}
	}
	
	void OnAccelEvent(const vec3f& acceleration)
	{
		SystemIOS* system = Holder<SystemIOS>::getHostage();
		if ( system ) {
			system->setJoyAxis(0, acceleration.x);
			system->setJoyAxis(1, acceleration.y);
			system->setJoyAxis(2, acceleration.z);
		}
	}
}} // namespace bme::uikit_detail

#endif // BME_SYSTEM_IOS
