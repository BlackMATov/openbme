/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_ios/SystemIOS_UIKitWindow.mm
 *****************************************************************************/

#import "SystemIOS_uikit.h"

#if BME_SYSTEM == BME_SYSTEM_IOS

@implementation BMEUIKitWindow

- (void)StartWithView:(BMEUIKitView*)view controller:(BMEUIKitViewController*)controller
{
	self.rootViewController = controller;
	[self addSubview:view];
	[self makeKeyAndVisible];
}

@end

#endif // BME_SYSTEM_IOS
