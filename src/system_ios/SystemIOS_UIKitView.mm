/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_ios/SystemIOS_UIKitView.mm
 *****************************************************************************/

#import "SystemIOS_uikit.h"

#if BME_SYSTEM == BME_SYSTEM_IOS

@implementation BMEUIKitView

// ----------------------------------------------------------------------------
// 
// Override
// 
// ----------------------------------------------------------------------------

+ (Class)layerClass
{
	return [CAEAGLLayer class];
}

- (id)initWithFrame:(CGRect)frame
{
	if ((self = [super initWithFrame:frame])) {
		CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
		eaglLayer.opaque = TRUE;
		eaglLayer.drawableProperties =
			[NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:FALSE],
			 kEAGLDrawablePropertyRetainedBacking,
			 kEAGLColorFormatRGBA8,
			 kEAGLDrawablePropertyColorFormat, nil];
	}
	return self;
}

- (void)dealloc
{
	[self DestroyFrameBuffer];
	[self DestroyContext];
	[super dealloc];
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	if ( _context ) {
		[self DestroyFrameBuffer];
		[self CreateFrameBuffer];
	}
}

// ----------------------------------------------------------------------------
// 
// Impl
// 
// ----------------------------------------------------------------------------

- (void)StartAnimation
{
	if (!_displayLink) {
		_displayLink = [NSClassFromString(@"CADisplayLink")
					   displayLinkWithTarget : self
									selector : @selector(Step:)];
		
		[_displayLink setFrameInterval:1];
		[_displayLink addToRunLoop : [NSRunLoop currentRunLoop]
						   forMode : NSDefaultRunLoopMode];
	}
}

- (void)StopAnimation
{
	if (_displayLink) {
		[_displayLink invalidate];
		_displayLink = nil;
	}
}

- (BOOL)CreateContext
{
	[self DestroyContext];
	_context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
	if ( _context && [EAGLContext setCurrentContext:_context] ) {
		if ( [self CreateFrameBuffer] ) {
			[self BoundsCorrection:UIInterfaceOrientationPortrait];
			return YES;
		}
	}
	[self DestroyFrameBuffer];
	[self DestroyContext];
	return NO;
}

- (void)DestroyContext
{
	if ( _context ) {
		if ( [EAGLContext currentContext] == _context ) {
			[EAGLContext setCurrentContext:nil];
		}
		[_context release];
		_context = nil;
	}
}

- (BOOL)CreateFrameBuffer
{
	[self DestroyFrameBuffer];
	
	CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
	
	glGenFramebuffersOES (1, &_frameBuffer);
	glGenRenderbuffersOES(1, &_renderBuffer);
	glGenRenderbuffersOES(1, &_depthBuffer);
	
	glBindFramebufferOES(GL_FRAMEBUFFER_OES, _frameBuffer);
	glBindRenderbufferOES(GL_RENDERBUFFER_OES, _renderBuffer);
	if ( ![_context renderbufferStorage:GL_RENDERBUFFER_OES fromDrawable:eaglLayer] ) {
		return FALSE;
	}
	
	GLint width, height;
	glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, _renderBuffer);
	glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES,  &width);
	glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &height);
	
	glBindRenderbufferOES           (GL_RENDERBUFFER_OES, _depthBuffer);
	glRenderbufferStorageOES        (GL_RENDERBUFFER_OES, GL_DEPTH_COMPONENT16_OES, width, height);
	glFramebufferRenderbufferOES    (GL_FRAMEBUFFER_OES,  GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, _depthBuffer);
	if ( glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES) != GL_FRAMEBUFFER_COMPLETE_OES ) {
		return FALSE;
	}
	return TRUE;
}

- (void)DestroyFrameBuffer
{
	if ( _frameBuffer ) {
		glDeleteFramebuffersOES(1, &_frameBuffer);
		_frameBuffer = 0;
	}
	
	if ( _renderBuffer ) {
		glDeleteRenderbuffersOES(1, &_renderBuffer);
		_renderBuffer = 0;
	}
	
	if ( _depthBuffer ) {
		glDeleteRenderbuffersOES(1, &_depthBuffer);
		_depthBuffer = 0;
	}
}

- (void)Step:(id)sender
{
	if ( bme::theApplication() ) {
		bme::theApplication()->Step();
	}
}

- (void)SwapBuffers
{
	if ( _context && _renderBuffer ) {
		EAGLContext* lastContext = [EAGLContext currentContext];
		if ( lastContext != _context ) {
			[EAGLContext setCurrentContext:_context];
		}
		
		GLint lastRenderBuffer;
		glGetIntegerv(GL_RENDERBUFFER_BINDING_OES, &lastRenderBuffer);
		glBindRenderbufferOES(GL_RENDERBUFFER_OES, _renderBuffer);
		
		if ( ![_context presentRenderbuffer:GL_RENDERBUFFER_OES] ) {
			NSLog(@"context present render buffer failed");
		}
		
		glBindRenderbufferOES(GL_RENDERBUFFER_OES, lastRenderBuffer);
		
		if ( lastContext != _context ) {
			[EAGLContext setCurrentContext:lastContext];
		}
	}
}

- (void)BoundsCorrection: (UIInterfaceOrientation)orientation
{
	using namespace bme;
	CGRect view_rect = [[[UIScreen screens] objectAtIndex:0] bounds];
	switch ( orientation ) {
		case UIInterfaceOrientationLandscapeRight:
			cpp::swap(view_rect.size.width, view_rect.size.height);
			self.transform = CGAffineTransformMakeRotation(Math::HALF_PI);
			break;
		case UIInterfaceOrientationLandscapeLeft:
			cpp::swap(view_rect.size.width, view_rect.size.height);
			self.transform = CGAffineTransformMakeRotation(-Math::HALF_PI);
			break;
		case UIInterfaceOrientationPortrait:
			self.transform = CGAffineTransformMakeRotation(0.f);
			break;
		case UIInterfaceOrientationPortraitUpsideDown:
			self.transform = CGAffineTransformMakeRotation(Math::PI);
			break;
		default:
			self.transform = CGAffineTransformMakeRotation(0.f);
	}
	[self setBounds:view_rect];
}

- (GLuint)GetFrameBuffer
{
	return _frameBuffer;
}

@end

#endif // BME_SYSTEM_IOS
