/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_ios/SystemIOS.mm
 *****************************************************************************/

#include "SystemIOS.h"

#if BME_SYSTEM == BME_SYSTEM_IOS

#include "SystemIOS_uikit.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// SystemImpl
// 
// ----------------------------------------------------------------------------

bool SystemImpl::Initialize()
{
	SystemIOS* self = new SystemIOS();
	return self->Init();
}

void SystemImpl::Shutdown()
{
	SystemIOS* self = Holder<SystemIOS>::getHostage();
	if ( self ) {
		self->DeInit();
		delete self;
	}
}

// ----------------------------------------------------------------------------
// 
// SystemIOS
// 
// ----------------------------------------------------------------------------

SystemIOS::SystemIOS()
{
}

SystemIOS::~SystemIOS()
{
}

bool SystemIOS::_initTimers()
{
	gettimeofday(&_startTimer, NULL);
	return true;
}

bool SystemIOS::_initWindow()
{
	Application* app = theApplication();
	if ( !app ) {
		BME_LOG_ERROR("SystemIOS::_initWindow() application not found");
		return false;
	}
	
	// -----------------------------------
	// Options
	// -----------------------------------
	
	[UIApplication sharedApplication].idleTimerDisabled =
		app->getDesc().isAllowScreensaver ? NO : YES;
	
	// -----------------------------------
	// calculate screen resolutions
	// -----------------------------------
	
	CalculateResolutions(
		getWindowResolution(), getDesktopResolution());
	
	// -----------------------------------
	// create window
	// -----------------------------------
	
	BMEUIKitDelegate* delegate = uikit_detail::GetDelegate();
	return YES == [delegate CreateWindow];
}

void SystemIOS::_deinitWindow()
{
	BMEUIKitDelegate* delegate = uikit_detail::GetDelegate();
	[delegate DestroyWindow];
}

bool SystemIOS::Init()
{
	bool success = (_initWindow() && _initTimers());
	if ( !success ) {
		BME_LOG_ERROR("SystemIOS::Init() Window init failed!");
		return false;
	}
	
	if ( !RendererImpl::Initialize() ) {
		BME_LOG_ERROR("SystemIOS::Init() Renderer init failed!");
		return false;
	}
	
	return true;
}

void SystemIOS::DeInit()
{
	RendererImpl::Shutdown();
	_deinitWindow();
}

void SystemIOS::setJoyAxis(size_t axis, f32 value)
{
	_axisValues[axis] = value;
}

// ----------------------------------------------------------------------------
// 
// System override
// 
// ----------------------------------------------------------------------------

void SystemIOS::setWindowCaption(const Str& /*value*/)
{
	// nothing
}

void SystemIOS::setWindowIcon(const Path& /*value*/)
{
	// nothing
}

void SystemIOS::setCursor(const Path& /*value*/, const pnt2u& /*hotSpot*/)
{
	// nothing
}

void SystemIOS::setCursorPosition(const pnt2u& /*value*/)
{
	// nothing
}

void SystemIOS::setCursorVisible(bool yesno)
{
	// nothing
}

bool SystemIOS::isCursorVisible() const
{
	return false;
}

void SystemIOS::ShowWindow()
{
	// nothing
}

void SystemIOS::HideWindow()
{
	// nothing
}

void SystemIOS::RestoreWindow()
{
	// nothing
}

void SystemIOS::MinimizeWindow()
{
	// nothing
}

bool SystemIOS::isWindowVisible() const
{
	return isWindowFocused();
}

bool SystemIOS::isWindowFocused() const
{
	BMEUIKitDelegate* delegate = uikit_detail::GetDelegate();
	return (delegate && [delegate isFocus]);
}

bool SystemIOS::isWindowActive() const
{
	BMEUIKitDelegate* delegate = uikit_detail::GetDelegate();
	return (delegate && [delegate isActive]);
}

bool SystemIOS::isWindowMinimized() const
{
	return !isWindowFocused();
}

void SystemIOS::ShowConsole() const
{
	// nothing
}

void SystemIOS::HideConsole() const
{
	// nothing
}

bool SystemIOS::ShowSoftKeyboard() const
{
	BMEUIKitViewController* controller = uikit_detail::GetController();
	return (controller && YES == [controller ShowKeyboard]);
}

void SystemIOS::HideSoftKeyboard() const
{
	BMEUIKitViewController* controller = uikit_detail::GetController();
	if ( controller )
		[controller HideKeyboard];
}

bool SystemIOS::isSoftKeyboardVisible() const
{
	BMEUIKitViewController* controller = uikit_detail::GetController();
	return (controller && YES == [controller isKeyboardVisible]);
}

System::E_ORIENTATION SystemIOS::getDeviceOrientation() const
{
	UIDevice* device = [UIDevice currentDevice];
	if ( device && UIDeviceOrientationIsPortrait([device orientation]) ) {
		return ORIENTATION_PORTRAIT;
	}
	return ORIENTATION_LANDSCAPE;
}

// ----------------------------------------------------------------------------
// 
// SystemImpl override
// 
// ----------------------------------------------------------------------------

pnt2u SystemIOS::getWindowResolution() const
{
	BMEUIKitView* view = uikit_detail::GetView();
	CGRect rect = view ? [view bounds] : [[[UIScreen screens] objectAtIndex:0] bounds];
	return pnt2u(rect.size.width, rect.size.height);
}

pnt2u SystemIOS::getDesktopResolution() const
{
	BMEUIKitWindow* window = uikit_detail::GetWindow();
	CGRect rect = window ? [window bounds] : [[[UIScreen screens] objectAtIndex:0] bounds];
	return pnt2u(rect.size.width, rect.size.height);
}

void SystemIOS::Update()
{
	// nothing
}

void SystemIOS::EndFrame()
{
	BMEUIKitView* view = uikit_detail::GetView();
	if ( view ) {
		[view SwapBuffers];
	}
	
	RendererImpl* renderer = Holder<RendererImpl>::getHostage();
	if ( renderer ) {
		renderer->EndFrame();
	}
}

bool SystemIOS::ToggleFullscreen(bool yesno)
{
	return true;
}

bool SystemIOS::ToggleWidescreen(bool yesno)
{
	return true;
}

void SystemIOS::NativeLog(const char* text, Logger::E_LEVEL /*level*/) const
{
	if ( !text || !text[0] ) {
		return;
	}
	NSLog(@"%s", text);
}

size_t SystemIOS::getTicks() const
{
	timeval now;
	gettimeofday(&now, NULL);
	size_t ticks =
		(now.tv_sec  - _startTimer.tv_sec ) * 1000 +
		(now.tv_usec - _startTimer.tv_usec) / 1000;
	return ticks;
}

Inputer::Desc SystemIOS::getInputerDesc() const
{
	Inputer::Desc desc;
	desc.mouses    = 10;
	desc.keyboards =  1;
	desc.joysticks =  1;
	return desc;
}

Joystick::Desc SystemIOS::getJoystickDesc(size_t index) const
{
	Joystick::Desc desc;
	desc.axes = 3;
	return desc;
}

f32 SystemIOS::getJoyAxisState(size_t index, size_t nAxis) const
{
	if ( 0 == index ) {
		TAxisValues::const_iterator iter = _axisValues.find(nAxis);
		if ( iter != _axisValues.end() ) {
			return (*iter).second;
		}
	}
	return 0.f;
}

vec2f SystemIOS::getJoyBallState(size_t index, size_t nBall) const
{
	return vec2f::zero;
}

bool SystemIOS::IsJoyHatDowned(size_t index, size_t nHat, E_JOYSTICK_HAT state) const
{
	return false;
}

} // namespace bme

#endif // BME_SYSTEM_IOS
