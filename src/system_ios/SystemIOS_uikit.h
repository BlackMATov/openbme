/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_macosx/SystemIOS_uikit.h
 *****************************************************************************/

#include <bme/core/base.h>

#if BME_SYSTEM == BME_SYSTEM_IOS

#include <sys/time.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

#include <bme/bme_main.h>
#include <bme/core/System.h>
#include <bme/core/Inputer.h>
#include <bme/core/Receiver.h>
#include <bme/core/EngineImpl.h>
#include <bme/core/RendererImpl.h>

// ----------------------------------------------------------------------------
// 
// BMEUIKitView
// 
// ----------------------------------------------------------------------------

@interface BMEUIKitView : UIView
{
@private
	id           _displayLink;
	EAGLContext* _context;
	GLuint       _frameBuffer;
	GLuint       _renderBuffer;
	GLuint       _depthBuffer;
}
- (void)StartAnimation;
- (void)StopAnimation;
- (BOOL)CreateContext;
- (void)DestroyContext;
- (BOOL)CreateFrameBuffer;
- (void)DestroyFrameBuffer;

- (void)Step:(id)sender;
- (void)SwapBuffers;
- (void)BoundsCorrection: (UIInterfaceOrientation)orientation;
- (GLuint)GetFrameBuffer;
@end

// ----------------------------------------------------------------------------
// 
// BMEUIKitViewController
// 
// ----------------------------------------------------------------------------

@interface BMEUIKitViewController : UIViewController
{
	BMEUIKitView*          _myView;
	UITextField*           _textField;
	CFMutableDictionaryRef _touchMap;
}
- (void)StartWithView:(BMEUIKitView*)view;
- (BOOL)ShowKeyboard;
- (void)HideKeyboard;
- (BOOL)isKeyboardVisible;
@end

// ----------------------------------------------------------------------------
// 
// BMEUIKitWindow
// 
// ----------------------------------------------------------------------------

@interface BMEUIKitWindow : UIWindow
{
@private
}
- (void)StartWithView:(BMEUIKitView*)view controller:(BMEUIKitViewController*)controller;
@end

// ----------------------------------------------------------------------------
// 
// BMEUIKitDelegate
// 
// ----------------------------------------------------------------------------

@interface BMEUIKitDelegate : NSObject <UIApplicationDelegate, UIAccelerometerDelegate> {
	BMEUIKitView*           _view;
	BMEUIKitWindow*         _window;
	BMEUIKitViewController* _controller;
	UIAccelerometer*        _accelerometer;
	BOOL                    _focus;
	BOOL                    _active;
}
- (BMEUIKitView*)           getView;
- (BMEUIKitWindow*)         getWindow;
- (BMEUIKitViewController*) getController;
- (BOOL)                    CreateWindow;
- (void)                    DestroyWindow;
- (BOOL)                    isFocus;
- (BOOL)                    isActive;
@end

// ----------------------------------------------------------------------------
//
// bme::renderer_gl_detail
//
// ----------------------------------------------------------------------------

namespace bme { namespace renderer_gl_detail
{
	GLuint GetDefaultFrameBuffer();
}} // bme::renderer_gl_detail

// ----------------------------------------------------------------------------
// 
// bme::uikit_detail
// 
// ----------------------------------------------------------------------------

namespace bme { namespace uikit_detail
{
	BMEUIKitView*           GetView         ();
	BMEUIKitWindow*         GetWindow       ();
	BMEUIKitViewController* GetController   ();
	BMEUIKitDelegate*       GetDelegate     ();
	UIApplication*          GetApplication  ();
	
	void                    DoMouseEvent    ( UITouchPhase phase, const vec2f& pos, size_t which );
	void                    OnAccelEvent    ( const vec3f& acceleration );
}} // namespace bme::uikit_detail

#endif // BME_SYSTEM_IOS
