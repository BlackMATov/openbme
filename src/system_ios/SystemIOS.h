/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_ios/SystemIOS.h
 *****************************************************************************/

#pragma once

#include <bme/core/SystemImpl.h>

#if BME_SYSTEM == BME_SYSTEM_IOS

namespace bme
{

class SystemIOS
	: public SystemImpl
	, public Holder<SystemIOS>
{
	typedef cpp::map<size_t,f32>::type TAxisValues;
	
	timeval        _startTimer;
	TAxisValues    _axisValues;
	
	bool           _initTimers();
	bool           _initWindow();
	void           _deinitWindow();
	
public:
	
	SystemIOS();
	virtual ~SystemIOS();
	
	bool           Init                  ();
	void           DeInit                ();
	
	void           setJoyAxis            ( size_t axis, f32 value );
	
	// System override --------------------------------------------------------
	void           setWindowCaption      ( const Str&   value );
	void           setWindowIcon         ( const Path&  value );
	
	void           setCursor             ( const Path&  value, const pnt2u& hotSpot );
	void           setCursorPosition     ( const pnt2u& value );
	void           setCursorVisible      ( bool yesno );
	bool           isCursorVisible       () const;
	
	void           ShowWindow            ();
	void           HideWindow            ();
	void           RestoreWindow         ();
	void           MinimizeWindow        ();
	
	bool           isWindowVisible       () const;
	bool           isWindowFocused       () const;
	bool           isWindowActive        () const;
	bool           isWindowMinimized     () const;
	
	void           ShowConsole           () const;
	void           HideConsole           () const;
	
	bool           ShowSoftKeyboard      () const;
	void           HideSoftKeyboard      () const;
	bool           isSoftKeyboardVisible () const;
	E_ORIENTATION  getDeviceOrientation  () const;
	// SystemImpl override ----------------------------------------------------
	pnt2u          getWindowResolution   () const;
	pnt2u          getDesktopResolution  () const;
	
	void           Update                ();
	void           EndFrame              ();
	bool           ToggleFullscreen      ( bool yesno );
	bool           ToggleWidescreen      ( bool yesno );
	void           NativeLog             ( const char* text, Logger::E_LEVEL level ) const;
	size_t         getTicks              () const;
	
	Inputer::Desc  getInputerDesc        ()                                                  const;
	Joystick::Desc getJoystickDesc       ( size_t index )                                    const;
	f32            getJoyAxisState       ( size_t index, size_t nAxis )                      const;
	vec2f          getJoyBallState       ( size_t index, size_t nBall )                      const;
	bool           IsJoyHatDowned        ( size_t index, size_t nHat, E_JOYSTICK_HAT state ) const;
	// ------------------------------------------------------------------------
};

} // namespace bme

#endif // BME_SYSTEM_IOS
