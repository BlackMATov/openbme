/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_ios/SystemIOS_UIKitDelegate.mm
 *****************************************************************************/

#import "SystemIOS_uikit.h"

#if BME_SYSTEM == BME_SYSTEM_IOS

@implementation BMEUIKitDelegate

// ----------------------------------------------------------------------------
// 
// Override
// 
// ----------------------------------------------------------------------------

- (void)applicationDidFinishLaunching:(UIApplication*)application
{
	// Fix working directory
	NSString* filename = [[NSBundle mainBundle] resourcePath];
	[[NSFileManager defaultManager] changeCurrentDirectoryPath:filename];
	
	// get accelerometer
	_accelerometer = [UIAccelerometer sharedAccelerometer];
	[_accelerometer setUpdateInterval:0.1f];
	[_accelerometer setDelegate:self];
}

- (void)applicationDidBecomeActive:(UIApplication*)application
{
	_focus = YES;
	
	if ( _view )
	{
		[_view StartAnimation];
		
		using namespace bme;
		Receiver::Event e;
		e.type = Receiver::Event::TYPE_FOCUS;
		e.active.gain = [self isFocus];
		e.Push(true);
	}
	else
	{
		BMEMain();
	}
}

- (void)applicationWillResignActive:(UIApplication*)application
{
	_focus = NO;
	
	if ( _view )
	{
		[_view StopAnimation];
		
		using namespace bme;
		Receiver::Event e;
		e.type = Receiver::Event::TYPE_FOCUS;
		e.active.gain = [self isFocus];
		e.Push(true);
	}
}

- (void)applicationWillEnterForeground:(UIApplication*)application
{
	/// \todo load all disk resources
	/// (because it free after applicationDidEnterBackground)
	_active = YES;
	
	using namespace bme;
	Receiver::Event e;
	e.type = Receiver::Event::TYPE_ACTIVE;
	e.active.gain = [self isActive];
	e.Push(true);
}

- (void)applicationDidEnterBackground:(UIApplication*)application
{
	/// \todo free all disk resources (textures, sounds)
	_active = NO;
	
	using namespace bme;
	Receiver::Event e;
	e.type = Receiver::Event::TYPE_ACTIVE;
	e.active.gain = [self isActive];
	e.Push(true);
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication*)application
{	
	using namespace bme;
	Receiver::Event e;
	e.type = Receiver::Event::TYPE_MEMORY_WARNING;
	e.Push(true);
}

- (void)applicationWillTerminate:(UIApplication*)application
{	
	BMEShutdown();
}

- (void)accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration
{
	bme::uikit_detail::OnAccelEvent(
		bme::vec3f(acceleration.x,
				   acceleration.y,
				   acceleration.z));
}

// ----------------------------------------------------------------------------
//
// Impl
//
// ----------------------------------------------------------------------------

- (BMEUIKitView*)           getView       { return _view;       }
- (BMEUIKitWindow*)         getWindow     { return _window;     }
- (BMEUIKitViewController*) getController { return _controller; }

- (BOOL) CreateWindow
{
	CGRect rect = [[[UIScreen screens] objectAtIndex:0] bounds];
	_view       = [[BMEUIKitView           alloc] initWithFrame:rect];
	_window     = [[BMEUIKitWindow         alloc] initWithFrame:rect];
	_controller = [[BMEUIKitViewController alloc] init];
	
	if ( _view && _window && _controller ) {
		[_window     StartWithView:_view controller:_controller];
		[_controller StartWithView:_view];
		if ( [_view CreateContext] )
			return YES;
	}
	
	[self DestroyWindow];
	return NO;
}

- (void) DestroyWindow
{
	if ( _controller ) {
		[_controller release];
		_controller = nil;
	}
	
	if ( _view ) {
		[_view release];
		_view = nil;
	}
	
	if ( _window ) {
		[_window release];
		_window = nil;
	}
}

- (BOOL) isFocus
{
	return _focus;
}

- (BOOL) isActive
{
	return _active;
}

@end

#endif // BME_SYSTEM_IOS
