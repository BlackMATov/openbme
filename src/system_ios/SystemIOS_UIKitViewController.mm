/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_ios/SystemIOS_UIKitViewController.mm
 *****************************************************************************/

#import "SystemIOS_uikit.h"

#if BME_SYSTEM == BME_SYSTEM_IOS

@implementation BMEUIKitViewController

// ----------------------------------------------------------------------------
// 
// Override
// 
// ----------------------------------------------------------------------------

- (void)dealloc
{	
	if ( _textField ) {
		[_textField release];
	}
	[super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	using namespace bme;
	if ( theApplication() ) {
		System::E_ORIENTATION allowedOrientation =
			theApplication()->getDesc().allowedOrientation;
		
		switch ( interfaceOrientation ) {
			case UIInterfaceOrientationLandscapeLeft:
			case UIInterfaceOrientationLandscapeRight: {
				return
				allowedOrientation == System::ORIENTATION_LANDSCAPE ||
				allowedOrientation == System::ORIENTATION_ALL;
			}
			case UIInterfaceOrientationPortrait:
			case UIInterfaceOrientationPortraitUpsideDown: {
				return
				allowedOrientation == System::ORIENTATION_PORTRAIT ||
				allowedOrientation == System::ORIENTATION_ALL;
			}
		}
	}
	return NO;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	if ( _myView ) {
		[_myView BoundsCorrection: toInterfaceOrientation];
	}
	
	using namespace bme;
	Receiver::Event e;
	e.type = Receiver::Event::TYPE_ROTATE_DEVICE;
	e.rotate.orientation = UIInterfaceOrientationIsPortrait(toInterfaceOrientation) ?
		System::ORIENTATION_PORTRAIT : System::ORIENTATION_LANDSCAPE;
	e.Push();
}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{	
	size_t new_touch_index = 1;
	for ( UITouch* touch in touches ) {
		while ( CFDictionaryContainsValue(_touchMap, (const void*)new_touch_index) ) {
			++new_touch_index;
		}
		
		// IOS 4.2.1 on 2g bug
		// http://stackoverflow.com/questions/8497795
		static CGPoint location;
		location = [touch locationInView:_myView];
		bme::uikit_detail::DoMouseEvent([touch phase], bme::vec2f(location.x, location.y), new_touch_index - 1);
		
		CFDictionarySetValue(_touchMap, touch, (const void*)new_touch_index);
		++new_touch_index;
	}
}

- (void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event
{
	for ( UITouch* touch in touches ) {
		size_t index = (size_t)CFDictionaryGetValue(_touchMap, touch);
		if ( index ) {
			// IOS 4.2.1 on 2g bug
			// http://stackoverflow.com/questions/8497795
			static CGPoint location;
			location = [touch locationInView:_myView];
			bme::uikit_detail::DoMouseEvent([touch phase], bme::vec2f(location.x, location.y), index - 1);
			
			CFDictionaryRemoveValue(_touchMap, touch);
		}
	}
}

- (void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{
	for ( UITouch* touch in touches ) {
		size_t index = (size_t)CFDictionaryGetValue(_touchMap, touch);
		if ( index ) {
			// IOS 4.2.1 on 2g bug
			// http://stackoverflow.com/questions/8497795
			static CGPoint location;
			location = [touch locationInView:_myView];
			bme::uikit_detail::DoMouseEvent([touch phase], bme::vec2f(location.x, location.y), index - 1);
		}
	}
}

- (void)touchesCancelled:(NSSet*)touches withEvent:(UIEvent*)event
{
	[self touchesEnded:touches withEvent:event];
}

- (IBAction)keyboardFieldDone:(id)sender
{
	using namespace bme;
	static Str utf8_str;
	utf8_str = [[_textField text] UTF8String];
	
	Receiver::Event e;
	e.type = Receiver::Event::TYPE_SOFTKEYBOARD_DONE;
	e.softkeyboard.text = utf8_str.c_str();
	e.Push();
	
	[_textField resignFirstResponder];
}

- (void)keyboardFieldChange:(NSNotification*)notification
{
	using namespace bme;
	static Str utf8_str;
	utf8_str = [[_textField text] UTF8String];
	
	Receiver::Event e;
	e.type = Receiver::Event::TYPE_SOFTKEYBOARD_CHANGE;
	e.softkeyboard.text = utf8_str.c_str();
	e.Push();
}

// ----------------------------------------------------------------------------
// 
// Impl
// 
// ----------------------------------------------------------------------------

- (void)StartWithView:(BMEUIKitView*)view;
{
	_myView = view;
	[view StartAnimation];
	
	_touchMap = CFDictionaryCreateMutable(NULL, 0, NULL, NULL);
	[[self view] setMultipleTouchEnabled:YES];
	
	_textField = [[UITextField alloc] initWithFrame:CGRectMake(-1000.f, -1000.f, 0.f, 0.f)];
	_textField.autocorrectionType = UITextAutocorrectionTypeNo;
	_textField.returnKeyType = UIReturnKeyDone;
	_textField.hidden = YES;
	[_textField addTarget:self action:@selector(keyboardFieldChange:) forControlEvents:UIControlEventEditingChanged];
	[_textField addTarget:self action:@selector(keyboardFieldDone:)   forControlEvents:UIControlEventEditingDidEndOnExit];
	[[self view] addSubview:_textField];
}

- (BOOL)ShowKeyboard
{
	if ( _textField ) {
		[_textField setText:@""];
		[_textField becomeFirstResponder];
		return YES;
	}
	return NO;
}

- (void)HideKeyboard
{
	if ( _textField ) {
		[_textField resignFirstResponder];
	}
}

- (BOOL)isKeyboardVisible
{
	return (_textField && [_textField isFirstResponder]);
}

@end

#endif // BME_SYSTEM_IOS
