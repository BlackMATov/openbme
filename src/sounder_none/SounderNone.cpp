/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/sounder_none/SounderNone.cpp
 *****************************************************************************/

#include "SounderNone.h"
#include "SoundNone.h"

#if BME_SOUNDER == BME_SOUNDER_NONE

namespace bme
{

// ----------------------------------------------------------------------------
//
// SounderImpl
//
// ----------------------------------------------------------------------------

bool SounderImpl::Initialize()
{
	return true;
}

void SounderImpl::Shutdown()
{
}

} // namespace bme

#endif // BME_SOUNDER_NONE
