/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/sounder_none/SounderNone.h
 *****************************************************************************/

#pragma once

#include "SounderDetailNone.h"
