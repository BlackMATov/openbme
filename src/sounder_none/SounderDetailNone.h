/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/sounder_none/SounderDetailNone.h
 *****************************************************************************/

#pragma once

#include <bme/core/Files.h>
#include <bme/core/Logger.h>
#include <bme/core/Filesystem.h>

#include <bme/core/SoundImpl.h>
#include <bme/core/SounderImpl.h>
