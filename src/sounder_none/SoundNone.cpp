/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/sounder_none/SoundNone.cpp
 *****************************************************************************/

#include "SoundNone.h"
#include "SounderNone.h"

#if BME_SOUNDER == BME_SOUNDER_NONE

namespace bme
{

// ----------------------------------------------------------------------------
//
// SoundImpl && ChannelImpl
//
// ----------------------------------------------------------------------------

SoundImpl* SoundImpl::Create()
{
	return NULL;
}

ChannelImpl* ChannelImpl::Create(
    const SoundPtr &sound)
{
	return NULL;
}

} // namespace bme

#endif // BME_SOUNDER_NONE
