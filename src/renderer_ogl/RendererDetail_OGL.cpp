/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/renderer_ogl/RendererDetail_OGL.cpp
 *****************************************************************************/

#include "RendererDetailOGL.h"

#if BME_RENDERER == BME_RENDERER_OGL

#include "TextureOGL.h"
#include "RendererOGL.h"

namespace bme
{
namespace renderer_gl_detail
{
	bool CreateFBO(
		GLuint colorBuffer, const pnt2u& size, bool withZBuffer,
		GLuint* outFB, GLuint* outDB)
	{
		// ret values
		
		GLuint gl_frame_buffer = 0;
		GLuint gl_depth_buffer = 0;
		
		// gen FB
		
		glGenFramebuffersEXT            (1, &gl_frame_buffer);
		glBindFramebufferEXT            (GL_FRAMEBUFFER_EXT, gl_frame_buffer);
		CheckOGLError("renderer_gl_detail::CreateFBO() glBindFramebufferEXT.");
		
		// gen ZBuffer, if needed
		
		if ( withZBuffer ) {
			glGenRenderbuffersEXT       (1, &gl_depth_buffer);
			glBindRenderbufferEXT       (GL_RENDERBUFFER_EXT, gl_depth_buffer );
			glRenderbufferStorageEXT    (GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT16, size.x, size.y);
			glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,  GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, gl_depth_buffer);
			CheckOGLError("renderer_gl_detail::CreateFBO() glFramebufferRenderbufferEXT.");
		}
		
		// attach colot buffer
		
		glBindTexture                   (GL_TEXTURE_2D, colorBuffer);
		glFramebufferTexture2DEXT       (GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, colorBuffer, 0);
		CheckOGLError("renderer_gl_detail::CreateFBO() glFramebufferTexture2DEXT.");
		
		// check status and return renderstates
		
		GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
		CheckOGLError("renderer_gl_detail::CreateFBO() glCheckFramebufferStatusEXT.");
		// RendererOGL::deviceImpl()->RetryCurrentStates(); /// \todo fixit? (recursive loop)
		
		// return result
		
		if ( GL_FRAMEBUFFER_COMPLETE_EXT == status ) {
			if ( outFB ) *outFB = gl_frame_buffer;
			if ( outDB ) *outDB = gl_depth_buffer;
			return true;
		} else {
			ClearBuffers(NULL, &gl_frame_buffer, &gl_depth_buffer);
			return false;
		}
	}
	
	bool BindFBO(GLuint frameBuffer)
	{
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frameBuffer);
		CheckOGLError("renderer_gl_detail::BindFBO() glBindFramebufferEXT.");
		return true;
	}
	
	void ClearBuffers(
		GLuint* colorBuffer, GLuint* frameBuffer, GLuint* depthBuffer)
	{
		if ( colorBuffer && *colorBuffer ) {
			glDeleteTextures(1, static_cast<const GLuint*>(colorBuffer));
			*colorBuffer = 0;
			CheckOGLError("renderer_gl_detail::ClearBuffers() glDeleteTextures.");
		}
		
		if ( frameBuffer && *frameBuffer ) {
			glDeleteFramebuffersEXT(1, static_cast<const GLuint*>(frameBuffer));
			*frameBuffer = 0;
			CheckOGLError("renderer_gl_detail::ClearBuffers() glDeleteFramebuffersEXT.");
		}
		
		if ( depthBuffer && *depthBuffer ) {
			glDeleteFramebuffersEXT(1, static_cast<const GLuint*>(depthBuffer));
			*depthBuffer = 0;
			CheckOGLError("renderer_gl_detail::ClearBuffers() glDeleteFramebuffersEXT.");
		}
	}
	
	SmartBuffer	GrabPixelsFromCB(
		GLuint colorBuffer, const pnt2u& size, E_FORMAT format)
	{
		glBindTexture(GL_TEXTURE_2D, colorBuffer);
		CheckOGLError("renderer_gl_detail::GrabPixelsFromCB() glBindTexture.");
		
		SmartBuffer ret_value(size.x * size.y * GetBitsPerPixelFormat(format) / 8);
		if ( !ret_value ) {
			return SmartBuffer();
		}
		
		if ( IsCompressedFormat(format) ) {
			glGetCompressedTexImage(GL_TEXTURE_2D, 0, ret_value.getBuffer());
			CheckOGLError("renderer_gl_detail::GrabPixelsFromCB() glGetCompressedTexImage.");
		} else {
			glGetTexImage(GL_TEXTURE_2D, 0,
				GetGLOriginFormat(format), GetGLOriginDataType(format),
				ret_value.getBuffer());
			CheckOGLError("renderer_gl_detail::GrabPixelsFromCB() glGetTexImage.");
		}
		
		RendererOGL::deviceImpl()->RetryCurrentStates();
		return ret_value;
	}
	
	SmartBuffer	PixelsToOGLPixels(
		const SmartBuffer& pixels, E_FORMAT src_format,
		const pnt2u& src_size, const pnt2u& dest_size)
	{
		switch ( src_format ) {
		case FORMAT_X8R8G8B8:
			{
				if ( src_size == dest_size ) {
					return pixels;
				} else {
					SmartBuffer ret_value(dest_size.x * dest_size.y * sizeof(u32));
					const u32* src = static_cast<const u32*>(pixels.getBuffer());
					u32* dest = static_cast<u32*>(ret_value.getBuffer());
					if ( src && dest ) {
						Color::X8R8G8B8_To_X8R8G8B8(dest, dest_size, src, src_size);
						return ret_value;
					}
				}
			}
			break;
		case FORMAT_A8R8G8B8:
			{
				if ( src_size == dest_size ) {
					return pixels;
				} else {
					SmartBuffer ret_value(dest_size.x * dest_size.y * sizeof(u32));
					const u32* src = static_cast<const u32*>(pixels.getBuffer());
					u32* dest = static_cast<u32*>(ret_value.getBuffer());
					if ( src && dest ) {
						Color::A8R8G8B8_To_A8R8G8B8(dest, dest_size, src, src_size);
						return ret_value;
					}
				}
			}
			break;
		case FORMAT_DXT1:
		case FORMAT_DXT3:
		case FORMAT_DXT5:
			return pixels;
		default:
			BME_ASSERT(false);
		}
		return SmartBuffer();
	}
} // namespace renderer_gl_detail
} // namespace bme

#endif // BME_RENDERER_OGL
