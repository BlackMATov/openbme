/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/renderer_ogl/TextureOGL.h
 *****************************************************************************/

#pragma once

#include "RendererDetailOGL.h"

#if BME_RENDERER == BME_RENDERER_OGL || BME_RENDERER == BME_RENDERER_OGLES

namespace bme
{

class TextureOGL : public TextureImpl
{
	GLuint         _colorBuffer;
	mutable GLuint _frameBuffer;
	mutable GLuint _depthBuffer;
	bool           _create(const SmartBuffer& pixels);
	void           _clear();
	
	E_LOCK_MODE    _lockMode;
	SmartBuffer    _lockData;
	
public:
	
	TextureOGL();
	virtual ~TextureOGL();
	
	GLuint         getColorBuffer () const;
	GLuint         getFrameBuffer () const;
	GLuint         getDepthBuffer () const;
	
	// Texture override -------------------------------------------------------
	bool           Lock           ( E_LOCK_MODE mode,
	                                void** outData, size_t* outPitch );
	bool           Unlock         ();
	bool           isLocked       () const;
	// TextureImpl override ---------------------------------------------------
	bool           CreateImpl     ();
	bool           CreateImpl     ( const ImageDesc& desc );
	bool           CreateImpl     ( const TexturePtr& source );
	void           Release        ();
	bool           Recovery       ();
	// ------------------------------------------------------------------------
};

} // namespace bme

#endif // BME_RENDERER_OGL || BME_RENDERER_OGLES
