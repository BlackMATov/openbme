/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/renderer_ogl/RendererOGL.h
 *****************************************************************************/

#pragma once

#include "RendererDetailOGL.h"

#if BME_RENDERER == BME_RENDERER_OGL || BME_RENDERER == BME_RENDERER_OGLES

namespace bme
{

class RendererOGL : public RendererDevice
{
	// --------------------------------
	// renderer specific
	// --------------------------------
	
	typedef cpp::vector<u8>::type TColorBuffer;
	
	bool         _deviceLost;
	bool         _resetDevice       ();
	TColorBuffer _colorBuffer;
	void         _createColorBuffer ( const Vertex* vertices,
	                                  size_t vertices_count );
	
	// --------------------------------
	// common
	// --------------------------------
	
	void         _checkFeatures     () const;
	void         _setDefaultStates  ();
	bool         _setTexture        ( const TexturePtr& texture );
	bool         _setTextureFilter  ( bool yesno );
	bool         _setTextureAddress ( Material::E_ADDRESS addres );
	bool         _setBlend          ( RenderStates::E_BLEND blend );
	bool         _setZEnable        ( bool yesno );
	
public:
	
	RendererOGL();
	virtual ~RendererOGL();
	
	static RendererOGL* deviceImpl  ();
	
	// Override ---------------------------------------------------------------
	bool         Initialize         ();
	void         Shutdown           ();
	void         EndFrame           ();
	void         Resize             ();
	void         DeviceLost         ();
	
	bool         BeginScene         ();
	void         ClearScene         ( const Color& color, bool backBuffer, bool zBuffer );
	bool         EndScene           ();
	
	void         setTransform       ( E_TRANSFORM transform, const mat4f& mat );
	void         setViewport        ( const rectu& vp );
	
	bool         setRenderTarget    ( const TexturePtr& texture );
	void         setStates          ( const Material& material, const RenderStates& states, bool lazy );
	void         DrawPrimitives     ( const Vertex* vertices, size_t vertices_count,
	                                  const u16* indices, size_t indices_count,
	                                  Renderer::E_PRIMITIVE type );
	// ------------------------------------------------------------------------
};

} // namespace bme

#endif // BME_RENDERER_OGL || BME_RENDERER_OGLES
