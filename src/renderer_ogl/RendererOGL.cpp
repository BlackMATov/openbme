/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/renderer_ogl/RendererOGL.cpp
 *****************************************************************************/

#include "RendererOGL.h"
#include "TextureOGL.h"

#if BME_RENDERER == BME_RENDERER_OGL || BME_RENDERER == BME_RENDERER_OGLES

namespace bme
{

// ----------------------------------------------------------------------------
// 
// renderer_gl_detail
// 
// ----------------------------------------------------------------------------

namespace renderer_gl_detail
{
#if BME_OS == BME_OS_IOS
	// system impl
	GLuint GetDefaultFrameBuffer();
#else
	GLuint GetDefaultFrameBuffer()
	{
		return 0;
	}
#endif
}

// ----------------------------------------------------------------------------
// 
// RendererDevice
// 
// ----------------------------------------------------------------------------

const vec2f& RendererDevice::getTexelOffset()
{
	static vec2f ret_value;
	return ret_value;
}

RendererDevice* RendererDevice::Create()
{
	return new RendererOGL();
}

// ----------------------------------------------------------------------------
// 
// RendererOGL
// 
// ----------------------------------------------------------------------------

RendererOGL::RendererOGL()
: _deviceLost(false)
{
	_colorBuffer.reserve(4*BME_MAX_BATCH_VERTICES);
}

RendererOGL::~RendererOGL()
{
}

RendererOGL* RendererOGL::deviceImpl()
{
	RendererImpl* renderer = getOwner();
	return
		renderer ?
		static_cast<RendererOGL*>(renderer->getDevice().get()) :
		NULL;
}

bool RendererOGL::_resetDevice()
{
	const char* log_str = "Resetting device.";
	
	if ( !getOwner()->RecoveryTextures() ) {
		BME_LOG_ERROR(log_str);
		return false;
	}
	
	_deviceLost = false;
	_setDefaultStates();
	
	Receiver::Event e;
	e.type = Receiver::Event::TYPE_RESET_DEVICE;
	e.Push();
	
	BME_LOG_COMPLETE(log_str);
	return true;
}

void RendererOGL::_createColorBuffer(
	const Vertex* vertices, size_t vertices_count)
{
	_colorBuffer.resize(vertices_count * 4);
	u8* dest = &_colorBuffer[0];
	for ( size_t i = 0; i < vertices_count; ++i ) {
		// a8r8g8b8 to rgba8
		u32 color = (vertices++)->color;
		*dest++	  = (color >> 16) & 0xFF;
		*dest++   = (color >>  8) & 0xFF;
		*dest++   = (color      ) & 0xFF;
		*dest++   = (color >> 24);
	}
}

void RendererOGL::_checkFeatures() const
{
	using namespace renderer_gl_detail;
	if ( !theRenderer() ) {
		return;
	}
#if BME_RENDERER == BME_RENDERER_OGLES
	// GL_OES_texture_npot not working...
	theRenderer()->getCaps().npot_textures = false;
	
	if ( IsExtensionSupported("GL_OES_framebuffer_object") ) {
		theRenderer()->getCaps().render_to_texture = true;
	}
#else
	if ( IsExtensionSupported("GL_ARB_texture_non_power_of_two") ) {
		theRenderer()->getCaps().npot_textures = true;
	}
	
	if ( IsExtensionSupported("EXT_framebuffer_object") ) {
		theRenderer()->getCaps().render_to_texture = true;
	}
#endif
	GLint max_size = 0;
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max_size);
	CheckOGLError("RendererOGL::_checkFeatures() glGetIntegerv.");
	theRenderer()->getCaps().max_texture_size = pnt2u(max_size, max_size);
}

void RendererOGL::_setDefaultStates()
{
	glDisable   (GL_ALPHA_TEST);
	glDisable   (GL_LIGHTING);
	glDisable   (GL_CULL_FACE);
	
	glEnable    (GL_TEXTURE_2D);
	glEnable    (GL_SCISSOR_TEST);
	
	glLineWidth (1.f);
	glShadeModel(GL_SMOOTH);
	glHint      (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glHint      (GL_LINE_SMOOTH_HINT,            GL_NICEST);
	glHint      (GL_POINT_SMOOTH_HINT,           GL_FASTEST);
	
	renderer_gl_detail::CheckOGLError("RendererOGL::_setDefaultStates()");
	RetryCurrentStates();
}

bool RendererOGL::_setTexture(
	const TexturePtr& texture)
{
	TextureOGL* impl = static_cast<TextureOGL*>(texture.get());
	glBindTexture(GL_TEXTURE_2D, impl ? impl->getColorBuffer() : 0);
	renderer_gl_detail::CheckOGLError("RendererOGL::_setTexture()");
	
	// flip opengl rtt
	if ( texture && texture->isRenderTarget() ) {
		glMatrixMode(GL_TEXTURE);
		glLoadIdentity();
		glScalef(1.f, -1.f, 1.f);
		glTranslatef(0.f, 1.f, 0.f);
	} else {
		glMatrixMode(GL_TEXTURE);
		glLoadIdentity();
	}
	
	renderer_gl_detail::CheckOGLError("RendererOGL::_setTexture()");
	_currentMaterial.texture = texture;
	return true;
}

bool RendererOGL::_setTextureFilter(
	bool yesno)
{
	if ( yesno ) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	} else {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	}
	
	renderer_gl_detail::CheckOGLError("RendererOGL::_setTextureFilter()");
	_currentMaterial.filter = yesno;
	return true;
}

bool RendererOGL::_setTextureAddress(
	Material::E_ADDRESS address)
{
	switch(address) {
	case Material::ADDRESS_WRAP:
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		break;
	case Material::ADDRESS_CLAMP:
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		break;
	default:
		BME_ASSERT(false);
		return false;
	}
	
	renderer_gl_detail::CheckOGLError("RendererOGL::_setTextureAddress()");
	_currentMaterial.address = address;
	return true;
}

bool RendererOGL::_setBlend(
	RenderStates::E_BLEND blend)
{
	switch(blend) {
	case RenderStates::BLEND_ALPHA:
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		break;
	case RenderStates::BLEND_ADD:
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		break;
	case RenderStates::BLEND_MULTIPLY:
		glEnable(GL_BLEND);
		glBlendFunc(GL_DST_COLOR, GL_ZERO);
		break;
	case RenderStates::BLEND_NONE:
		glDisable(GL_BLEND);
		break;
	default:
		BME_ASSERT(false);
		return false;
	}
	
	renderer_gl_detail::CheckOGLError("RendererOGL::_setBlend()");
	_currentRenderStates.blend = blend;
	return true;
}

bool RendererOGL::_setZEnable(
	bool yesno)
{
	if ( yesno ) {
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
	} else {
		glDisable(GL_DEPTH_TEST);
	}
	
	renderer_gl_detail::CheckOGLError("RendererOGL::_setZEnable()");
	_currentRenderStates.zEnable = yesno;
	return true;
}

// ----------------------------------------------------------------------------
// 
// Override
// 
// ----------------------------------------------------------------------------

bool RendererOGL::Initialize()
{
	renderer_gl_detail::InitExtensions();
	_checkFeatures();
	_setDefaultStates();
	return true;
}

void RendererOGL::Shutdown()
{
	ResetCurrentState();
}

void RendererOGL::EndFrame()
{
}

void RendererOGL::Resize()
{
	DeviceLost();
	_resetDevice();
}

void RendererOGL::DeviceLost()
{
	_deviceLost = true;
	BME_LOG_WARNING("Renderer device lost.");
}

bool RendererOGL::BeginScene()
{
	// check device lost
	
	if ( _deviceLost ) {
		if ( !_resetDevice() ) {
			return false;
		}
	}
	
	return true;
}

void RendererOGL::ClearScene(
	const Color& color, bool backBuffer, bool zBuffer)
{
	GLbitfield flags = 0;
	
	if ( backBuffer ) {
		flags |= GL_COLOR_BUFFER_BIT;
	}
	
	if ( zBuffer ) {
		if ( !_currentRenderTarget || _currentRenderTarget->isZBuffer() ) {
			flags |= GL_DEPTH_BUFFER_BIT;
		}
	}
	
#if BME_RENDERER == BME_RENDERER_OGLES
	glClearDepthf(1.f);
#else
	glClearDepth(1.f);
#endif
	
	glClearColor(color.getR(), color.getG(), color.getB(), color.getA());
	glClear(flags);
	renderer_gl_detail::CheckOGLError("RendererOGL::ClearScene()");
}

bool RendererOGL::EndScene()
{
	return true;
}

void RendererOGL::setTransform(
	E_TRANSFORM transform, const mat4f& mat)
{
	switch(transform) {
	case TRANSFORM_VIEW:
		_viewMat = mat;
		glMatrixMode(GL_MODELVIEW);
		glLoadMatrixf((_viewMat * _worldMat).getPointer());
		break;
	case TRANSFORM_WORLD:
		_worldMat = mat;
		glMatrixMode(GL_MODELVIEW);
		glLoadMatrixf((_viewMat * _worldMat).getPointer());
		break;
	case TRANSFORM_PROJECTION:
		_projMat = mat;
		glMatrixMode(GL_PROJECTION);
		glLoadMatrixf(mat.getPointer());
		break;
	default:
		BME_ASSERT(false);
	}
	
	renderer_gl_detail::CheckOGLError("RendererOGL::setTransform()");
}

void RendererOGL::setViewport(
	const rectu& vp)
{
	glViewport(
		Math::SmartCast<GLint>(vp.getTopLeft().x),
		Math::SmartCast<GLint>(_getMaxCurrentRTSize().y - vp.getBottom()),
		Math::SmartCast<GLsizei>(vp.getWidth()),
		Math::SmartCast<GLsizei>(vp.getHeight()));
	
	glScissor(
		Math::SmartCast<GLint>(vp.getTopLeft().x),
		Math::SmartCast<GLint>(_getMaxCurrentRTSize().y - vp.getBottom()),
		Math::SmartCast<GLsizei>(vp.getWidth()),
		Math::SmartCast<GLsizei>(vp.getHeight()));
	
	renderer_gl_detail::CheckOGLError("RendererOGL::setViewport()");
	_viewport = vp;
}

bool RendererOGL::setRenderTarget(
	const TexturePtr& texture)
{
	TextureOGL* textureOGL = NULL;
	
	if ( texture ) {
		textureOGL = static_cast<TextureOGL*>(texture.get());
		
		if ( !textureOGL->isRenderTarget() ) {
			BME_LOG_ERROR("RendererOGL::setRenderTarget() non render target texture");
			return false;
		}
		
		if ( !textureOGL->getFrameBuffer() ) {
			BME_LOG_ERROR("RendererOGL::setRenderTarget() texture not valid!");
			return false;
		}
	}
	
	// set render target
	
	using namespace renderer_gl_detail;
	GLuint fbo = (textureOGL && textureOGL->getFrameBuffer()) ?
		textureOGL->getFrameBuffer() : renderer_gl_detail::GetDefaultFrameBuffer();
	if ( !BindFBO(fbo) ) {
		BME_LOG_ERROR("RendererOGL::setRenderTarget() Can't set render target");
		return false;
	}
	
	_currentRenderTarget = texture;
	return true;
}

void RendererOGL::setStates(
	const Material& material, const RenderStates& states, bool lazy)
{
	// Material
	
	bool textureChanged = false;
	
	if ( !lazy || _currentMaterial.texture != material.texture ) {
		textureChanged = true;
		if (!_setTexture(material.texture)) {
			BME_LOG_ERROR("RendererOGL::setStates() setTexture failed");
		}
	}
	
	if ( !lazy || textureChanged || _currentMaterial.filter != material.filter ) {
		if (!_setTextureFilter(material.filter)) {
			BME_LOG_ERROR("RendererOGL::setStates() setTextureFilter failed");
		}
	}
	
	if ( !lazy || textureChanged || _currentMaterial.address != material.address ) {
		if (!_setTextureAddress(material.address)) {
			BME_LOG_ERROR("RendererOGL::setStates() setTextureAddress failed");
		}
	}
	
	// Render states
	
	if ( !lazy || _currentRenderStates.blend != states.blend ) {
		if (!_setBlend(states.blend)) {
			BME_LOG_ERROR("RendererOGL::setStates() setBlend failed");
		}
	}
	
	if ( !lazy || _currentRenderStates.zEnable != states.zEnable ) {
		if (!_setZEnable(states.zEnable)) {
			BME_LOG_ERROR("RendererOGL::setStates() setZEnable failed");
		}
	}
}

void RendererOGL::DrawPrimitives(
	const Vertex* vertices, size_t vertices_count,
	const u16* indices, size_t indices_count,
	Renderer::E_PRIMITIVE type)
{
	GLenum oglprim = renderer_gl_detail::GetOGLPrimitiveType(type);
	if ( 0 == oglprim || !vertices || vertices_count <= 0 ) {
		return;
	}
	
	_createColorBuffer(vertices, vertices_count);
	
	if ( indices && indices_count > 0 && indices_count > vertices_count ) {
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);
		renderer_gl_detail::CheckOGLError("RendererOGL::DrawPrimitives() glEnableClientState.");
		
		glVertexPointer  (3, GL_FLOAT,         sizeof(Vertex), &vertices[0].x);
		glColorPointer   (4, GL_UNSIGNED_BYTE, 0,              &_colorBuffer[0]);
		glTexCoordPointer(2, GL_FLOAT,         sizeof(Vertex), &vertices[0].tx);
		renderer_gl_detail::CheckOGLError("RendererOGL::DrawPrimitives() glXXXPointer.");
		
		glDrawElements(
			oglprim, static_cast<GLsizei>(indices_count), GL_UNSIGNED_SHORT, indices);
		renderer_gl_detail::CheckOGLError("RendererOGL::DrawPrimitives() glDrawElements.");
		
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		renderer_gl_detail::CheckOGLError("RendererOGL::DrawPrimitives() glDisableClientState.");
	} else {
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);
		renderer_gl_detail::CheckOGLError("RendererOGL::DrawPrimitives() glEnableClientState.");
		
		glVertexPointer  (3, GL_FLOAT,         sizeof(Vertex), &vertices[0].x);
		glColorPointer   (4, GL_UNSIGNED_BYTE, 0,              &_colorBuffer[0]);
		glTexCoordPointer(2, GL_FLOAT,         sizeof(Vertex), &vertices[0].tx);
		renderer_gl_detail::CheckOGLError("RendererOGL::DrawPrimitives() glXXXPointer.");
		
		glDrawArrays(
			oglprim, 0, static_cast<GLsizei>(vertices_count));
		renderer_gl_detail::CheckOGLError("RendererOGL::DrawPrimitives() glDrawArrays.");
		
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		renderer_gl_detail::CheckOGLError("RendererOGL::DrawPrimitives() glDisableClientState.");
	}
}

} // namespace bme

#endif // BME_RENDERER_OGL || BME_RENDERER_OGLES
