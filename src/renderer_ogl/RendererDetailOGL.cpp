/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/renderer_ogl/RendererDetailOGL.cpp
 *****************************************************************************/

#include "RendererDetailOGL.h"
#include "RendererOGL.h"

#if BME_RENDERER == BME_RENDERER_OGL || BME_RENDERER == BME_RENDERER_OGLES

// ----------------------------------------------------------------------------
//
// GL EXTENSION
//
// ----------------------------------------------------------------------------

#if BME_OS == BME_OS_WINDOWS
PFNGLGENFRAMEBUFFERSEXTPROC         glGenFramebuffersEXT         = NULL;
PFNGLDELETEFRAMEBUFFERSEXTPROC      glDeleteFramebuffersEXT      = NULL;
PFNGLBINDFRAMEBUFFEREXTPROC         glBindFramebufferEXT         = NULL;
PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC  glCheckFramebufferStatusEXT  = NULL;

PFNGLGENRENDERBUFFERSEXTPROC        glGenRenderbuffersEXT        = NULL;
PFNGLBINDRENDERBUFFEREXTPROC        glBindRenderbufferEXT        = NULL;
PFNGLRENDERBUFFERSTORAGEEXTPROC     glRenderbufferStorageEXT     = NULL;

PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC glFramebufferRenderbufferEXT = NULL;
PFNGLFRAMEBUFFERTEXTURE2DEXTPROC    glFramebufferTexture2DEXT    = NULL;

PFNGLCOMPRESSEDTEXIMAGE2DPROC       glCompressedTexImage2D       = NULL;
PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC    glCompressedTexSubImage2D    = NULL;
PFNGLGETCOMPRESSEDTEXIMAGEPROC      glGetCompressedTexImage      = NULL;

PFNGLUNIFORM1FARBPROC               glUniform1fARB               = NULL;
PFNGLUNIFORM2FARBPROC               glUniform2fARB               = NULL;
PFNGLUNIFORM3FARBPROC               glUniform3fARB               = NULL;
PFNGLUNIFORM4FARBPROC               glUniform4fARB               = NULL;
PFNGLUNIFORMMATRIX2FVARBPROC        glUniformMatrix2fvARB        = NULL;
PFNGLUNIFORMMATRIX3FVARBPROC        glUniformMatrix3fvARB        = NULL;
PFNGLUNIFORMMATRIX4FVARBPROC        glUniformMatrix4fvARB        = NULL;

PFNGLGETUNIFORMLOCATIONARBPROC      glGetUniformLocationARB      = NULL;
PFNGLGETUNIFORMFVARBPROC            glGetUniformfvARB            = NULL;

PFNGLVERTEXATTRIB1FARBPROC          glVertexAttrib1fARB          = NULL;
PFNGLVERTEXATTRIB2FARBPROC          glVertexAttrib2fARB          = NULL;
PFNGLVERTEXATTRIB3FARBPROC          glVertexAttrib3fARB          = NULL;
PFNGLVERTEXATTRIB4FARBPROC          glVertexAttrib4fARB          = NULL;

PFNGLGETATTRIBLOCATIONARBPROC       glGetAttribLocationARB       = NULL;
PFNGLGETVERTEXATTRIBFVARBPROC       glGetVertexAttribfvARB       = NULL;
#endif

// ----------------------------------------------------------------------------
// 
// renderer_gl_detail
// 
// ----------------------------------------------------------------------------

namespace bme
{
namespace renderer_gl_detail
{
	static void* getProcAddress(const char* name)
	{
		#if BME_OS == BME_OS_WINDOWS
			return (void*)wglGetProcAddress(name);
		#else
			BME_ASSERT(false);
			return NULL;
		#endif
	}
	
	#define BME_DEF_GL_EXTENSION(funcName, funcType) \
		funcName = (funcType)getProcAddress(#funcName)
	
	void InitExtensions()
	{
	#if BME_OS == BME_OS_WINDOWS
		BME_DEF_GL_EXTENSION(glGenFramebuffersEXT,         PFNGLGENFRAMEBUFFERSEXTPROC);
		BME_DEF_GL_EXTENSION(glDeleteFramebuffersEXT,      PFNGLDELETEFRAMEBUFFERSEXTPROC);
		BME_DEF_GL_EXTENSION(glBindFramebufferEXT,         PFNGLBINDFRAMEBUFFEREXTPROC);
		BME_DEF_GL_EXTENSION(glCheckFramebufferStatusEXT,  PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC);
		
		BME_DEF_GL_EXTENSION(glGenRenderbuffersEXT,        PFNGLGENRENDERBUFFERSEXTPROC);
		BME_DEF_GL_EXTENSION(glBindRenderbufferEXT,        PFNGLBINDRENDERBUFFEREXTPROC);
		BME_DEF_GL_EXTENSION(glRenderbufferStorageEXT,     PFNGLRENDERBUFFERSTORAGEEXTPROC);
		
		BME_DEF_GL_EXTENSION(glFramebufferRenderbufferEXT, PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC);
		BME_DEF_GL_EXTENSION(glFramebufferTexture2DEXT,    PFNGLFRAMEBUFFERTEXTURE2DEXTPROC);
		
		BME_DEF_GL_EXTENSION(glCompressedTexImage2D,       PFNGLCOMPRESSEDTEXIMAGE2DPROC);
		BME_DEF_GL_EXTENSION(glCompressedTexSubImage2D,    PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC);
		BME_DEF_GL_EXTENSION(glGetCompressedTexImage,      PFNGLGETCOMPRESSEDTEXIMAGEPROC);
		
		BME_DEF_GL_EXTENSION(glUniform1fARB,               PFNGLUNIFORM1FARBPROC);
		BME_DEF_GL_EXTENSION(glUniform2fARB,               PFNGLUNIFORM2FARBPROC);
		BME_DEF_GL_EXTENSION(glUniform3fARB,               PFNGLUNIFORM3FARBPROC);
		BME_DEF_GL_EXTENSION(glUniform4fARB,               PFNGLUNIFORM4FARBPROC);
		BME_DEF_GL_EXTENSION(glUniformMatrix2fvARB,        PFNGLUNIFORMMATRIX2FVARBPROC);
		BME_DEF_GL_EXTENSION(glUniformMatrix3fvARB,        PFNGLUNIFORMMATRIX3FVARBPROC);
		BME_DEF_GL_EXTENSION(glUniformMatrix4fvARB,        PFNGLUNIFORMMATRIX4FVARBPROC);
		
		BME_DEF_GL_EXTENSION(glGetUniformLocationARB,      PFNGLGETUNIFORMLOCATIONARBPROC);
		BME_DEF_GL_EXTENSION(glGetUniformfvARB,            PFNGLGETUNIFORMFVARBPROC);
		
		BME_DEF_GL_EXTENSION(glVertexAttrib1fARB,          PFNGLVERTEXATTRIB1FARBPROC);
		BME_DEF_GL_EXTENSION(glVertexAttrib2fARB,          PFNGLVERTEXATTRIB2FARBPROC);
		BME_DEF_GL_EXTENSION(glVertexAttrib3fARB,          PFNGLVERTEXATTRIB3FARBPROC );
		BME_DEF_GL_EXTENSION(glVertexAttrib4fARB,          PFNGLVERTEXATTRIB4FARBPROC);
		
		BME_DEF_GL_EXTENSION(glGetAttribLocationARB,       PFNGLGETATTRIBLOCATIONARBPROC);
		BME_DEF_GL_EXTENSION(glGetVertexAttribfvARB,       PFNGLGETVERTEXATTRIBFVARBPROC);
	#endif
	}
	
	bool IsExtensionSupported(const char* ext)
	{
		const size_t ext_length = BME_strlen(ext);
		const char* ptr = NULL;
		const char* start = reinterpret_cast<const char*>(glGetString(GL_EXTENSIONS));
		while ( NULL != (ptr = BME_strstr(start, ext)) ) {
			const char* end = ptr + ext_length;
			if ( *end == ' ' || *end == '\0' ) {
				return true;
			}
			start = end;
		}
		return false;
	}
	
	const char* OGLErrorToString(GLenum error)
	{
		switch ( error ) {
		case GL_INVALID_ENUM      : return "GL_INVALID_ENUM";
		case GL_INVALID_VALUE     : return "GL_INVALID_VALUE";
		case GL_INVALID_OPERATION : return "GL_INVALID_OPERATION";
		case GL_STACK_OVERFLOW    : return "GL_STACK_OVERFLOW";
		case GL_STACK_UNDERFLOW   : return "GL_STACK_UNDERFLOW";
		case GL_OUT_OF_MEMORY     : return "GL_OUT_OF_MEMORY";
		default:
			{
				static char buf[64];
				return Strings::Format(buf, 64, "ErrorCode: %1%", error);
			}
		}
	}
	
	bool CheckOGLError(const char* place)
	{
		bool ret_value = false;
		for ( ; ; ) {
			GLenum error = glGetError();
			if ( GL_NO_ERROR == error ) {
				break;
			}
			ret_value = true;
			BME_LOG_ERROR_FMT(
				"%1%\nOGLError: %2%",
				place ? place : "",
				OGLErrorToString(error));
		}
		return ret_value;
	}
	
	SmartBuffer BitmapToOGLPixels(
		const ImageDesc& desc, const pnt2u& dest_size)
	{
		return PixelsToOGLPixels(
			desc.pixels, desc.format, desc.size, dest_size);
	}
	
	void FlipPixelsByY(
		void* pixels, E_FORMAT format, const pnt2u& size)
	{
		u8*    dest   = static_cast<u8*>(pixels);
		size_t pitch  = size.x * GetBitsPerPixelFormat(format) / 8;
		size_t height = size.y;
		
		/// \todo make it without allocs
		u8* p2 = dest + (height - 1) * pitch;
		u8* tmpBuf = static_cast<u8*>(BME_malloc(pitch * sizeof(u8)));
		for ( size_t i = 0; i < height; i += 2 )
		{
			BME_memcpy(tmpBuf, dest, pitch);
			BME_memcpy(dest, p2, pitch);
			BME_memcpy(p2, tmpBuf, pitch);
			dest += pitch;
			p2 -= pitch;
		}
		BME_free(tmpBuf);
	}
	
	bool CreateCB(
		const pnt2u& size, E_FORMAT format, const SmartBuffer& pixels, GLuint* outCB)
	{
		GLuint colorBuffer;
		glGenTextures(1, &colorBuffer);
		glBindTexture(GL_TEXTURE_2D, colorBuffer);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		CheckOGLError("renderer_gl_detail::CreateCB() glPixelStorei.");
		
		GLenum gl_format          = GetGLOriginFormat(format);
		GLenum gl_data_type       = GetGLOriginDataType(format);
		GLenum gl_internal_format = GetGLInternalFormat(format);
		
		bool success = false;
		
		if ( IsCompressedFormat(format) ) {
			if ( gl_internal_format ) {
				glCompressedTexImage2D(
					GL_TEXTURE_2D, 0,
					gl_internal_format,
					Math::SmartCast<GLsizei>(size.x), Math::SmartCast<GLsizei>(size.y), 0,
					Math::SmartCast<GLsizei>(pixels.getSize()), pixels.getBuffer());
				CheckOGLError("renderer_gl_detail::CreateCB() glCompressedTexImage2D.");
				success = true;
			}
		} else {
			if ( gl_internal_format && gl_format && gl_data_type ) {
				glTexImage2D(
					GL_TEXTURE_2D, 0,
					gl_internal_format,
					Math::SmartCast<GLsizei>(size.x), Math::SmartCast<GLsizei>(size.y), 0,
					gl_format, gl_data_type, pixels.getBuffer());
				CheckOGLError("renderer_gl_detail::CreateCB() glTexImage2D.");
				success = true;
			}
		}
		
		// RendererOGL::deviceImpl()->RetryCurrentStates(); /// \todo fixit? (recursive loop)
		
		if ( success ) {
			if ( outCB ) *outCB = colorBuffer;
			return true;
		} else {
			glDeleteTextures(1, &colorBuffer);
			CheckOGLError("renderer_gl_detail::CreateCB() glDeleteTextures.");
			return false;
		}
	}
	
	bool CopyPixelsTo(
		GLuint colorBuffer, const pnt2u& size, E_FORMAT format, const SmartBuffer& pixels)
	{
		glBindTexture(GL_TEXTURE_2D, colorBuffer);
		CheckOGLError("renderer_gl_detail::CreateCB() glBindTexture.");
		
		GLenum gl_format    = GetGLOriginFormat(format);
		GLenum gl_data_type = GetGLOriginDataType(format);
		
		bool success = false;
		
		if ( IsCompressedFormat(format) ) {
			if ( gl_format ) {
				glCompressedTexSubImage2D(
					GL_TEXTURE_2D, 0,
					0, 0, size.x, size.y,
					gl_format,
					static_cast<GLsizei>(pixels.getSize()), pixels.getBuffer());
				CheckOGLError("renderer_gl_detail::CopyPixelsTo() glCompressedTexSubImage2D.");
				success = true;
			}
		} else {
			if ( gl_format && gl_data_type ) {
				glTexSubImage2D(
					GL_TEXTURE_2D, 0,
					0, 0, size.x, size.y,
					gl_format, gl_data_type, pixels.getBuffer());
				CheckOGLError("renderer_gl_detail::CopyPixelsTo() glTexSubImage2D.");
				success = true;
			}
		}
		RendererOGL::deviceImpl()->RetryCurrentStates();
		return success;
	}
	
	GLenum GetOGLPrimitiveType(Renderer::E_PRIMITIVE type)
	{
		switch( type ) {
		case Renderer::PRIMITIVE_LINES:
			return GL_LINES;
		case Renderer::PRIMITIVE_TRIANGLES:
			return GL_TRIANGLES;
		default:
			BME_ASSERT(false);
			return 0;
		}
	}

	bool IsCompressedFormat(E_FORMAT format)
	{
		switch ( format ) {
#if BME_RENDERER == BME_RENDERER_OGLES
		case FORMAT_RGB8:
		case FORMAT_RGBA8:
			return false;
		case FORMAT_RGB_PVRTC2:
		case FORMAT_RGB_PVRTC4:
		case FORMAT_RGBA_PVRTC2:
		case FORMAT_RGBA_PVRTC4:
			return true;
#else
		case FORMAT_X8R8G8B8:
		case FORMAT_A8R8G8B8:
			return false;
		case FORMAT_DXT1:
		case FORMAT_DXT3:
		case FORMAT_DXT5:
			return true;
#endif
		default:
			BME_ASSERT(false);
			return false;
		}
	}
	
	size_t GetBitsPerPixelFormat(E_FORMAT format )
	{
		switch ( format ) {
#if BME_RENDERER == BME_RENDERER_OGLES
		case FORMAT_RGB8:
			return 24;
		case FORMAT_RGBA8:
			return 32;
		case FORMAT_RGB_PVRTC2:
			return 2;
		case FORMAT_RGB_PVRTC4:
			return 4;
		case FORMAT_RGBA_PVRTC2:
			return 2;
		case FORMAT_RGBA_PVRTC4:
			return 4;
#else
		case FORMAT_X8R8G8B8:
		case FORMAT_A8R8G8B8:
			return 32;
		case FORMAT_DXT1:
			return 4;
		case FORMAT_DXT3:
			return 8;
		case FORMAT_DXT5:
			return 8;
#endif
		default:
			BME_ASSERT(false);
			return false;
		}
	}
	
	GLenum GetGLOriginFormat(E_FORMAT format)
	{
		switch ( format ) {
#if BME_RENDERER == BME_RENDERER_OGLES
		case FORMAT_RGB8:
			return GL_RGB;
		case FORMAT_RGBA8:
			return GL_RGBA;
		case FORMAT_RGB_PVRTC2:
			return GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG;
		case FORMAT_RGB_PVRTC4:
			return GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG;
		case FORMAT_RGBA_PVRTC2:
			return GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG;
		case FORMAT_RGBA_PVRTC4:
			return GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG;
#else
		case FORMAT_X8R8G8B8:
		case FORMAT_A8R8G8B8:
			return GL_BGRA_EXT;
		case FORMAT_DXT1:
			return GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
		case FORMAT_DXT3:
			return GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
		case FORMAT_DXT5:
			return GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
#endif
		default:
			BME_ASSERT(false);
			return 0;
		}
	}
	
	GLenum GetGLOriginDataType(E_FORMAT format)
	{
		switch ( format ) {
#if BME_RENDERER == BME_RENDERER_OGLES
		case FORMAT_RGB8:
		case FORMAT_RGBA8:
			return GL_UNSIGNED_BYTE;
		case FORMAT_RGB_PVRTC2:
		case FORMAT_RGB_PVRTC4:
		case FORMAT_RGBA_PVRTC2:
		case FORMAT_RGBA_PVRTC4:
			return 0;
#else
		case FORMAT_X8R8G8B8:
		case FORMAT_A8R8G8B8:
			return GL_UNSIGNED_BYTE;
		case FORMAT_DXT1:
		case FORMAT_DXT3:
		case FORMAT_DXT5:
			return 0;
#endif
		default:
			BME_ASSERT(false);
			return 0;
		}
	}
	
	GLenum GetGLInternalFormat(E_FORMAT format)
	{
		switch ( format ) {
#if BME_RENDERER == BME_RENDERER_OGLES
		case FORMAT_RGB8:
			return GL_RGB;
		case FORMAT_RGBA8:
			return GL_RGBA;
		case FORMAT_RGB_PVRTC2:
			return GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG;
		case FORMAT_RGB_PVRTC4:
			return GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG;
		case FORMAT_RGBA_PVRTC2:
			return GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG;
		case FORMAT_RGBA_PVRTC4:
			return GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG;
#else
		case FORMAT_X8R8G8B8:
			return GL_RGB8;
		case FORMAT_A8R8G8B8:
			return GL_RGBA8;
		case FORMAT_DXT1:
			return GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
		case FORMAT_DXT3:
			return GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
		case FORMAT_DXT5:
			return GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
#endif
		default:
			BME_ASSERT(false);
			return 0;
		}
	}
	
	E_FORMAT GetColorFormatFromGLInternal(GLenum format)
	{
		switch ( format ) {
#if BME_RENDERER == BME_RENDERER_OGLES
		case GL_RGB:
		case GL_RGB8_OES:
			return FORMAT_RGB8;
		case GL_RGBA:
		case GL_RGBA8_OES:
			return FORMAT_RGBA8;
		case GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG:
			return FORMAT_RGB_PVRTC2;
		case GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG:
			return FORMAT_RGB_PVRTC4;
		case GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG:
			return FORMAT_RGBA_PVRTC2;
		case GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG:
			return FORMAT_RGBA_PVRTC4;
#else
		case GL_RGB:
		case GL_RGB8:
			return FORMAT_X8R8G8B8;
		case GL_RGBA:
		case GL_RGBA8:
			return FORMAT_A8R8G8B8;
		case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT:
			return FORMAT_DXT1;
		case GL_COMPRESSED_RGBA_S3TC_DXT3_EXT:
			return FORMAT_DXT3;
		case GL_COMPRESSED_RGBA_S3TC_DXT5_EXT:
			return FORMAT_DXT5;
#endif
		default:
			return FORMAT_UNKNOWN;
		}
	}
} // namespace renderer_gl_detail
} // namespace bme

#endif // BME_RENDERER_OGL || BME_RENDERER_OGLES
