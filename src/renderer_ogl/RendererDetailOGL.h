/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/renderer_ogl/RendererDetailOGL.h
 *****************************************************************************/

#pragma once

#include <bme/core/Logger.h>
#include <bme/core/Receiver.h>
#include <bme/core/Application.h>

#include <bme/core/SystemImpl.h>
#include <bme/core/TextureImpl.h>
#include <bme/core/RendererImpl.h>

#if BME_RENDERER == BME_RENDERER_OGL || BME_RENDERER == BME_RENDERER_OGLES

#if BME_OS == BME_OS_ANDROID
#	define GL_GLEXT_LEGACY
#	define GL_GLEXT_PROTOTYPES
#	include <GLES/gl.h>
#	include <GLES/glext.h>
#endif

#if BME_OS == BME_OS_IOS
#	include <OpenGLES/ES1/gl.h>
#	include <OpenGLES/ES1/glext.h>
#endif

#if BME_OS == BME_OS_WINDOWS
#	include <Windows.h>
#	include <gl/GL.h>
#	include <glext/glext.h>
#	include <glext/wglext.h>
#endif

#if BME_OS == BME_OS_MACOSX
#	define GL_GLEXT_LEGACY
#	define GL_GLEXT_PROTOTYPES
#	include <OpenGL/gl.h>
#	include <glext/glext.h>
#endif

// ----------------------------------------------------------------------------
// 
// GL EXTENSION
// 
// ----------------------------------------------------------------------------

#if BME_OS == BME_OS_WINDOWS
extern PFNGLGENFRAMEBUFFERSEXTPROC         glGenFramebuffersEXT;
extern PFNGLDELETEFRAMEBUFFERSEXTPROC      glDeleteFramebuffersEXT;
extern PFNGLBINDFRAMEBUFFEREXTPROC         glBindFramebufferEXT;
extern PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC  glCheckFramebufferStatusEXT;

extern PFNGLGENRENDERBUFFERSEXTPROC        glGenRenderbuffersEXT;
extern PFNGLBINDRENDERBUFFEREXTPROC        glBindRenderbufferEXT;
extern PFNGLRENDERBUFFERSTORAGEEXTPROC     glRenderbufferStorageEXT;

extern PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC glFramebufferRenderbufferEXT;
extern PFNGLFRAMEBUFFERTEXTURE2DEXTPROC    glFramebufferTexture2DEXT;

extern PFNGLCOMPRESSEDTEXIMAGE2DPROC       glCompressedTexImage2D;
extern PFNGLCOMPRESSEDTEXSUBIMAGE2DPROC    glCompressedTexSubImage2D;
extern PFNGLGETCOMPRESSEDTEXIMAGEPROC      glGetCompressedTexImage;

extern PFNGLUNIFORM1FARBPROC               glUniform1fARB;
extern PFNGLUNIFORM2FARBPROC               glUniform2fARB;
extern PFNGLUNIFORM3FARBPROC               glUniform3fARB;
extern PFNGLUNIFORM4FARBPROC               glUniform4fARB;
extern PFNGLUNIFORMMATRIX2FVARBPROC        glUniformMatrix2fvARB;
extern PFNGLUNIFORMMATRIX3FVARBPROC        glUniformMatrix3fvARB;
extern PFNGLUNIFORMMATRIX4FVARBPROC        glUniformMatrix4fvARB;

extern PFNGLGETUNIFORMLOCATIONARBPROC      glGetUniformLocationARB;
extern PFNGLGETUNIFORMFVARBPROC            glGetUniformfvARB;

extern PFNGLVERTEXATTRIB1FARBPROC          glVertexAttrib1fARB;
extern PFNGLVERTEXATTRIB2FARBPROC          glVertexAttrib2fARB;
extern PFNGLVERTEXATTRIB3FARBPROC          glVertexAttrib3fARB;
extern PFNGLVERTEXATTRIB4FARBPROC          glVertexAttrib4fARB;

extern PFNGLGETATTRIBLOCATIONARBPROC       glGetAttribLocationARB;
extern PFNGLGETVERTEXATTRIBFVARBPROC       glGetVertexAttribfvARB;
#endif

// ----------------------------------------------------------------------------
// 
// renderer_gl_detail
// 
// ----------------------------------------------------------------------------

namespace bme
{
namespace renderer_gl_detail
{
	// --------------------------------
	// Common OGL functions
	// --------------------------------
	
	void        InitExtensions               ();
	bool        IsExtensionSupported         ( const char* ext );
	
	const char* OGLErrorToString             ( GLenum error );
	bool        CheckOGLError                ( const char* place );
	
	SmartBuffer BitmapToOGLPixels            ( const ImageDesc& desc, const pnt2u& dest_size );
	void        FlipPixelsByY                ( void* pixels, E_FORMAT format, const pnt2u& size );
	
	bool        CreateCB                     ( const pnt2u& size, E_FORMAT format, const SmartBuffer& pixels, GLuint* outCB );
	bool        CopyPixelsTo                 ( GLuint colorBuffer, const pnt2u& size, E_FORMAT format, const SmartBuffer& pixels );
	GLenum      GetOGLPrimitiveType          ( Renderer::E_PRIMITIVE type );
	
	bool        IsCompressedFormat           ( E_FORMAT format );
	size_t      GetBitsPerPixelFormat        ( E_FORMAT format );
	
	GLenum      GetGLOriginFormat            ( E_FORMAT format );
	GLenum      GetGLOriginDataType          ( E_FORMAT format );
	GLenum      GetGLInternalFormat          ( E_FORMAT format );
	E_FORMAT    GetColorFormatFromGLInternal ( GLenum format );
	
	// --------------------------------
	// OGL/OGLES functions
	// --------------------------------
	
	bool        CreateFBO                    ( GLuint colorBuffer, const pnt2u& size, bool z_buffer, GLuint* outFB, GLuint* outDB );
	bool        BindFBO                      ( GLuint frameBuffer );
	void        ClearBuffers                 ( GLuint* colorBuffer, GLuint* frameBuffer, GLuint* depthBuffer );
	SmartBuffer GrabPixelsFromCB             ( GLuint colorBuffer, const pnt2u& size, E_FORMAT format );
	SmartBuffer PixelsToOGLPixels            ( const SmartBuffer& pixels, E_FORMAT src_format, const pnt2u& src_size, const pnt2u& dest_size );
} // namespace renderer_gl_detail
} // namespace bme

#endif // BME_RENDERER_OGL || BME_RENDERER_OGLES
