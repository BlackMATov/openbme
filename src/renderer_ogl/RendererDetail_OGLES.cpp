/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/renderer_ogl/RendererDetail_OGLES.cpp
 *****************************************************************************/

#include "RendererDetailOGL.h"

#if BME_RENDERER == BME_RENDERER_OGLES

#include "TextureOGL.h"
#include "RendererOGL.h"

namespace bme
{
namespace renderer_gl_detail
{
	bool CreateFBO(
		GLuint colorBuffer, const pnt2u& size, bool withZBuffer,
		GLuint* outFB, GLuint* outDB)
	{
		// ret values
		
		GLuint gl_frame_buffer = 0;
		GLuint gl_depth_buffer = 0;
		
		// gen FB
		
		glGenFramebuffersOES            (1, &gl_frame_buffer);
		glBindFramebufferOES            (GL_FRAMEBUFFER_OES, gl_frame_buffer);
		CheckOGLError("renderer_gl_detail::CreateFBO() glBindFramebufferOES.");
		
		// gen ZBuffer, if needed
		
		if ( withZBuffer ) {
			glGenRenderbuffersOES       (1, &gl_depth_buffer);
			glBindRenderbufferOES       (GL_RENDERBUFFER_OES, gl_depth_buffer);
			glRenderbufferStorageOES    (GL_RENDERBUFFER_OES, GL_DEPTH_COMPONENT16_OES, size.x, size.y);
			glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES,  GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, gl_depth_buffer);
			CheckOGLError("renderer_gl_detail::CreateFBO() glFramebufferRenderbufferOES.");
		}
		
		// attach color buffer
		
		glBindTexture                   (GL_TEXTURE_2D, colorBuffer);
		glFramebufferTexture2DOES       (GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_TEXTURE_2D, colorBuffer, 0);
		CheckOGLError("renderer_gl_detail::CreateFBO() glFramebufferTexture2DOES.");
		
		// check status and return renderstates
		
		GLenum status = glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES);
		CheckOGLError("renderer_gl_detail::CreateFBO() glCheckFramebufferStatusOES.");
		// RendererOGL::deviceImpl()->RetryCurrentStates(); /// \todo fixit? (recursive loop)
		
		// return result
		
		if ( GL_FRAMEBUFFER_COMPLETE_OES == status ) {
			if ( outFB ) *outFB = gl_frame_buffer;
			if ( outDB ) *outDB = gl_depth_buffer;
			return true;
		} else {
			ClearBuffers(NULL, &gl_frame_buffer, &gl_depth_buffer);
			return false;
		}
	}
	
	bool BindFBO(GLuint frameBuffer)
	{
		glBindFramebufferOES(GL_FRAMEBUFFER_OES, frameBuffer);
		CheckOGLError("renderer_gl_detail::BindFBO() glBindFramebufferOES.");
		return true;
	}
	
	void ClearBuffers(
		GLuint* colorBuffer, GLuint* frameBuffer, GLuint* depthBuffer)
	{
		if ( colorBuffer && *colorBuffer ) {
			glDeleteTextures(1, static_cast<const GLuint*>(colorBuffer));
			*colorBuffer = 0;
			CheckOGLError("renderer_gl_detail::ClearBuffers() glDeleteTextures.");
		}
		
		if ( frameBuffer && *frameBuffer ) {
			glDeleteFramebuffersOES(1, static_cast<const GLuint*>(frameBuffer));
			*frameBuffer = 0;
			CheckOGLError("renderer_gl_detail::ClearBuffers() glDeleteFramebuffersOES.");
		}
		
		if ( depthBuffer && *depthBuffer ) {
			glDeleteFramebuffersOES(1, static_cast<const GLuint*>(depthBuffer));
			*depthBuffer = 0;
			CheckOGLError("renderer_gl_detail::ClearBuffers() glDeleteFramebuffersOES.");
		}
	}
	
	SmartBuffer	GrabPixelsFromCB(
		GLuint colorBuffer, const pnt2u& size, E_FORMAT format)
	{
		// check supported formats
		if ( format != FORMAT_RGB8 && format != FORMAT_RGBA8 ) {
			return SmartBuffer();
		}
		
		// allocate buffer for pixels
		SmartBuffer read_pixels(size.x * size.y * 4 * sizeof(u8));
		if ( !read_pixels ) {
			return SmartBuffer();
		}
		
		// create fbo for grab pixels hack
		GLuint frameBuffer = 0;
		if ( !CreateFBO(colorBuffer, size, false, &frameBuffer, NULL) ) {
			return SmartBuffer();
		}
		
		// read pixels
		glBindFramebufferOES(GL_FRAMEBUFFER_OES, frameBuffer);
		glReadPixels(0, 0, size.x, size.y, GL_RGBA, GL_UNSIGNED_BYTE, read_pixels.getBuffer());
		CheckOGLError("renderer_gl_detail::GrabPixelsFromCB() glReadPixels.");
		
		// delete temp fbo
		ClearBuffers(NULL, &frameBuffer, NULL);
		
		// retry renderstates and return result
		RendererOGL::deviceImpl()->RetryCurrentStates();
		
		// convert for return
		SmartBuffer ret_value;
		switch ( format ) {
		case FORMAT_RGB8:
			{
				void* dest = ret_value.Allocate(size.x * size.y * 3 * sizeof(u8));
				if ( dest ) {
					Color::RGBA8_To_RGB8(
						static_cast<u8*>(dest), size,
						static_cast<u8*>(read_pixels.getBuffer()), size);
				}
			}
			break;
		case FORMAT_RGBA8:
			ret_value = read_pixels;
			break;
		default:
			BME_ASSERT(false);
			break;
		}
		return ret_value;
	}
	
	SmartBuffer	PixelsToOGLPixels(
		const SmartBuffer& pixels, E_FORMAT src_format,
		const pnt2u& src_size, const pnt2u& dest_size)
	{
		switch ( src_format ) {
		case FORMAT_RGB8:
			{
				if ( src_size == dest_size ) {
					return pixels;
				} else {
					SmartBuffer ret_value(dest_size.x * dest_size.y * 3 * sizeof(u8));
					const u8* src = static_cast<const u8*>(pixels.getBuffer());
					u8* dest = static_cast<u8*>(ret_value.getBuffer());
					if ( src && dest ) {
						Color::RGB8_To_RGB8(dest, dest_size, src, src_size);
						return ret_value;
					}
				}
			}
			break;
		case FORMAT_RGBA8:
			{
				if ( src_size == dest_size ) {
					return pixels;
				} else {
					SmartBuffer ret_value(dest_size.x * dest_size.y * 4 * sizeof(u8));
					const u8* src = static_cast<const u8*>(pixels.getBuffer());
					u8* dest = static_cast<u8*>(ret_value.getBuffer());
					if ( src && dest ) {
						Color::RGBA8_To_RGBA8(dest, dest_size, src, src_size);
						return ret_value;
					}
				}
			}
			break;
		case FORMAT_RGB_PVRTC2:
		case FORMAT_RGB_PVRTC4:
		case FORMAT_RGBA_PVRTC2:
		case FORMAT_RGBA_PVRTC4:
			return pixels;
		default:
			BME_ASSERT(false);
		}
		return SmartBuffer();
	}
} // namespace renderer_gl_detail
} // namespace bme

#endif // BME_RENDERER_OGLES
