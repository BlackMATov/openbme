/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/renderer_ogl/TextureOGL.cpp
 *****************************************************************************/

#include "TextureOGL.h"
#include "RendererOGL.h"

#if BME_RENDERER == BME_RENDERER_OGL || BME_RENDERER == BME_RENDERER_OGLES

namespace bme
{

// ----------------------------------------------------------------------------
// 
// TextureImpl
// 
// ----------------------------------------------------------------------------

TextureImpl* TextureImpl::Create()
{
	return new TextureOGL();
}

// ----------------------------------------------------------------------------
// 
// TextureOGL
// 
// ----------------------------------------------------------------------------

TextureOGL::TextureOGL()
: _colorBuffer(0)
, _frameBuffer(0)
, _depthBuffer(0)
, _lockMode(LOCK_READONLY)
{

}

TextureOGL::~TextureOGL()
{
	_clear();
}

bool TextureOGL::_create(const SmartBuffer& pixels)
{
	using namespace renderer_gl_detail;
	
	// create color buffer
	if ( !CreateCB(getSize(), getFormat(), pixels, &_colorBuffer) ) {
		BME_LOG_ERROR("TextureOGL::_create() create color buffer");
		return false;
	}
	
	// create frame buffer, if needed
	if ( isRenderTarget() && !CreateFBO(
		getColorBuffer(), getSize(), isZBuffer(), &_frameBuffer, &_depthBuffer) )
	{
		BME_LOG_ERROR("TextureOGL::_create() create frame buffer");
		return false;
	}
	
	return true;
}

void TextureOGL::_clear()
{
	using namespace renderer_gl_detail;
	ClearBuffers(&_colorBuffer, &_frameBuffer, &_depthBuffer);
	_lockData.Release();
}

GLuint TextureOGL::getColorBuffer() const
{
	return _colorBuffer;
}

GLuint TextureOGL::getFrameBuffer() const
{
	return _frameBuffer;
}

GLuint TextureOGL::getDepthBuffer() const
{
	return _depthBuffer;
}

bool TextureOGL::Lock(E_LOCK_MODE mode, void** outData, size_t* outPitch)
{
	using namespace renderer_gl_detail;
	
	if ( isLocked() ) {
		BME_LOG_ERROR("TextureOGL::Lock() texture already locked!");
		return false;
	}
	
	if ( !getColorBuffer() ) {
		BME_LOG_ERROR("TextureOGL::Lock() texture not valid!");
		return false;
	}
	
#if BME_RENDERER == BME_RENDERER_OGLES
	if ( isRenderTarget() ) {
		BME_LOG_ERROR("TextureOGL::Lock() render target lock not supported!");
		return false;
	}
	
	if ( IsCompressedFormat(getFormat()) ) {
		BME_LOG_ERROR("TextureOGL::Lock() compressed texture lock not supported!");
		return false;
	}
#endif
	
	_lockMode = mode;
	
	if ( mode == LOCK_WRITEONLY ) {
		_lockData.Allocate(getSize().x * getSize().y * GetBitsPerPixelFormat(getFormat()));
		if ( !_lockData ) {
			BME_LOG_ERROR("TextureOGL::Lock() memory allocation for lock buffer");
			return false;
		}
	} else {
		_lockData = GrabPixelsFromCB(getColorBuffer(), getSize(), getFormat());
		if ( !_lockData ) {
			BME_LOG_ERROR("TextureOGL::Lock() grab texture pixels for lock buffer");
			return false;
		}
		
		// invert grab pixels by Y from render targets
		if ( isRenderTarget() ) {
			FlipPixelsByY(_lockData.getBuffer(), getFormat(), getSize());
		}
	}
	
	if ( outData )  *outData  = _lockData.getBuffer();
	if ( outPitch ) *outPitch = getSize().x * GetBitsPerPixelFormat(getFormat()) / 8;
	return true;
}

bool TextureOGL::Unlock()
{
	using namespace renderer_gl_detail;
	
	if ( !isLocked() ) {
		BME_LOG_ERROR("TextureOGL::Unlock() texture not locked!");
		return false;
	}
	
	if ( !getColorBuffer() ) {
		BME_LOG_ERROR("TextureOGL::Unlock() texture not valid!");
		return false;
	}
	
	// if read only then not apply result
	if ( _lockMode == LOCK_READONLY ) {
		_lockData.Release();
		return true;
	}
	
	// invert pixels by Y for render targets
	if ( isRenderTarget() ) {
		FlipPixelsByY(_lockData.getBuffer(), getFormat(), getSize());
	}
	
	// apply result
	if ( !CopyPixelsTo(getColorBuffer(), getSize(), getFormat(), _lockData) ) {
		BME_LOG_ERROR("TextureOGL::Unlock() can't copy pixels to texture!");
		return false;
	}
	
	_lockData.Release();
	return true;
}

bool TextureOGL::isLocked() const
{
	return _lockData;
}

bool TextureOGL::CreateImpl()
{
	_clear();
	return _create(SmartBuffer());
}

bool TextureOGL::CreateImpl(const ImageDesc& desc)
{
	_clear();
	
	// pixels conversions
	using namespace renderer_gl_detail;
	SmartBuffer gl_pixels = BitmapToOGLPixels(desc, getSize());
	if ( !gl_pixels ) {
		BME_LOG_ERROR("TextureOGL::CreateImpl() pixel conversions");
		return false;
	}
	
	// create
	return _create(gl_pixels);
}

bool TextureOGL::CreateImpl(const TexturePtr& source)
{
	_clear();
	
	TextureOGL* other_tex = static_cast<TextureOGL*>(source.get());
	if ( !other_tex ) {
		return false;
	}
	
	using namespace renderer_gl_detail;
	
#if BME_RENDERER == BME_RENDERER_OGLES
	if ( other_tex->isRenderTarget() ) {
		BME_LOG_ERROR("TextureOGL::CreateImpl() render target copy not supported!");
		return false;
	}
	
	if ( IsCompressedFormat(other_tex->getFormat()) ) {
		BME_LOG_ERROR("TextureOGL::CreateImpl() compressed texture copy not supported!");
		return false;
	}
#endif
	
	SmartBuffer src_pixels = GrabPixelsFromCB(
		other_tex->getColorBuffer(), other_tex->getSize(), other_tex->getFormat());
	if ( !src_pixels ) {
		return false;
	}
	
	// invert pixels by Y for render targets
	if ( other_tex->isRenderTarget() ) {
		FlipPixelsByY(src_pixels.getBuffer(), getFormat(), getSize());
	}
	
	return _create(src_pixels);
}

void TextureOGL::Release()
{
	_clear();
}

bool TextureOGL::Recovery()
{
	return true;
}

} // namespace bme

#endif // BME_RENDERER_OGL || BME_RENDERER_OGLES
