/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_win/SystemWIN.h
 *****************************************************************************/

#pragma once

#include <bme/core/SystemImpl.h>

#if BME_SYSTEM == BME_SYSTEM_WINDOWS

#ifndef NOMINMAX
#	define NOMINMAX
#endif

#ifndef _WIN32_WINNT
#	define _WIN32_WINNT _WIN32_WINNT_NT4
#endif

#include <windows.h>

#ifndef WM_XBUTTONDOWN
#	define WM_XBUTTONDOWN 0x020B
#endif
#ifndef WM_XBUTTONUP
#	define WM_XBUTTONUP 0x020C
#endif

#ifndef MK_XBUTTON1
#	define MK_XBUTTON1 0x0001
#endif
#ifndef MK_XBUTTON2
#	define MK_XBUTTON2 0x0002
#endif

#ifndef WHEEL_DELTA
#	define WHEEL_DELTA 120
#endif
#ifndef GET_WHEEL_DELTA_WPARAM
#	define GET_WHEEL_DELTA_WPARAM(wParam) ((short)HIWORD(wParam))
#endif

namespace bme
{

class SystemWIN
	: public SystemImpl
	, public Holder<SystemWIN>
{
	// cursor and icon
	HICON         _defaultIcon;
	HICON         _currentIcon;
	HICON         _defaultCursor;
	HICON         _currentCursor;
	bool          _cursorVisible;
	void          _updateCursor();
	void          _updateClipCursor();
	
	// window
	HWND          _hWnd;
	HINSTANCE     _hInstance;
	bool          _initWindow();
	void          _deinitWindow();
	void          _changeWindowStyle(bool isFullscreen, bool isChild, LONG* outStyle, rects* outRect) const;
	
	// instance mutex and restore message
	HANDLE        _hInstanceMutex;
	UINT          _restoreMessage;
	bool          _initInstanceMutex();
	void          _deinitInstanceMutex();
	
	// gl context
	HDC           _hDC;
	HGLRC         _hRC;
	HMODULE       _oglLib;
	HGLRC         (WINAPI *_wglCreateContext) (HDC hdc);
	BOOL          (WINAPI *_wglMakeCurrent)   (HDC hdc, HGLRC hglrc);
	BOOL          (WINAPI *_wglDeleteContext) (HGLRC hglrc);
	PROC          (WINAPI *_wglGetProcAddress)(LPCSTR);
	bool          _initGLContext();
	void          _deinitGLContext();
	
	// timers
	DWORD         _startTimer;
	bool          _initTimers();
	void          _deinitTimers();
	
	static void KeyMsg(UINT msg, WPARAM wparam, LPARAM lparam);
	static void MouseMsg(UINT msg, WPARAM wparam, LPARAM lparam);
	static LRESULT CALLBACK WindowProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);
	
public:
	
	SystemWIN();
	virtual ~SystemWIN();
	
	bool          Init                  ();
	void          DeInit                ();
	
	// System override --------------------------------------------------------
	void          setWindowCaption      ( const Str&   value );
	void          setWindowIcon         ( const Path&  value );
	
	void          setCursor             ( const Path&  value, const pnt2u& hotSpot );
	void          setCursorPosition     ( const pnt2u& value );
	void          setCursorVisible      ( bool yesno );
	bool          isCursorVisible       () const;
	
	void          ShowWindow            ();
	void          HideWindow            ();
	void          RestoreWindow         ();
	void          MinimizeWindow        ();
	
	bool          isWindowVisible       () const;
	bool          isWindowFocused       () const;
	bool          isWindowActive        () const;
	bool          isWindowMinimized     () const;
	
	void          ShowConsole           () const;
	void          HideConsole           () const;
	
	bool          ShowSoftKeyboard      () const;
	void          HideSoftKeyboard      () const;
	bool          isSoftKeyboardVisible () const;
	E_ORIENTATION getDeviceOrientation  () const;
	// SystemImpl override ----------------------------------------------------
	void          Update                ();
	void          EndFrame              ();
	bool          ToggleFullscreen      ( bool yesno );
	bool          ToggleWidescreen      ( bool yesno );
	void          NativeLog             ( const char* text, Logger::E_LEVEL level ) const;
	size_t        getTicks              () const;
	
	Inputer::Desc getInputerDesc        () const;
	
	ptrdiff_t     getHWND               () const;
	// ------------------------------------------------------------------------
};

} // namespace bme

#endif // BME_SYSTEM_WINDOWS
