/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_win/SystemWIN.cpp
 *****************************************************************************/

#include "SystemWIN.h"

#if BME_SYSTEM == BME_SYSTEM_WINDOWS

#include <bme/bme_main.h>
#include <bme/core/Receiver.h>
#include <bme/core/RendererImpl.h>

// ----------------------------------------------------------------------------
// 
// BMEWindowsMain
// 
// ----------------------------------------------------------------------------

#ifdef BME_WITH_MAIN

int main()
{
	BMEMain();
	return 0;
}

#endif // BME_WITH_MAIN

namespace bme
{

// ----------------------------------------------------------------------------
// 
// SystemImpl
// 
// ----------------------------------------------------------------------------

bool SystemImpl::Initialize()
{
	SystemWIN* self = new SystemWIN();
	return self->Init();
}

void SystemImpl::Shutdown()
{
	SystemWIN* self = Holder<SystemWIN>::getHostage();
	if ( self ) {
		self->DeInit();
		delete self;
	}
}

// ----------------------------------------------------------------------------
// 
// detail
// 
// ----------------------------------------------------------------------------

static LPCTSTR BME_WINDOW_CLASS_NAME           = __TEXT("BME__WNDCLASS");
static LPCTSTR BME_INSTANCE_MUTEX_NAME         = __TEXT("BME__WNDCLASS__MUTEX");
static LPCTSTR BME_WINDOW_MESSAGE_RESTORE_NAME = __TEXT("BME__WNDCLASS__RESTORE");

#define WINX_GET_X_LPARAM(lp) ((int)(short)LOWORD(lp))
#define WINX_GET_Y_LPARAM(lp) ((int)(short)HIWORD(lp))

namespace system_win_detail
{
	static HICON LoadIconByPath(const Path& path)
	{
		if ( !path.isEmpty() ) {
			HICON ret_value = (HICON)::LoadImageW(
				NULL,
				path.getNativeWPath().c_str(),
				IMAGE_ICON,
				0,
				0,
				LR_DEFAULTSIZE | LR_LOADFROMFILE);
			if ( !ret_value ) {
				BME_LOG_ERROR("system_win_detail::LoadIconByPath() path is wrong!");
			}
			return ret_value;
		}
		return NULL;
	}
	
	static HICON LoadCursorByPath(const Path& path, const pnt2u& hotSpot)
	{
		if ( !path.isEmpty() ) {
			HICON cursor_image = (HICON)::LoadImageW(
				NULL,
				path.getNativeWPath().c_str(),
				IMAGE_CURSOR,
				0,
				0,
				LR_DEFAULTSIZE | LR_LOADFROMFILE);
			if ( cursor_image ) {
				ICONINFO info;
				if ( ::GetIconInfo(cursor_image, &info) ) {
					info.xHotspot = Math::SmartCast<int>(hotSpot.x);
					info.yHotspot = Math::SmartCast<int>(hotSpot.y);
					info.fIcon = FALSE;
					HICON ret_value = ::CreateIconIndirect(&info);
					if ( ret_value ) {
						::DestroyCursor(cursor_image);
						return ret_value;
					}
				}
				BME_LOG_WARNING(
					"system_win_detail::LoadCursorByPath() can't set cursor hotspot");
				return cursor_image;
			} else {
				BME_LOG_ERROR(
					"system_win_detail::LoadCursorByPath() path is wrong!");
			}
		}
		return NULL;
	}
	
	enum CONSOLE_COLOR
	{
		COLOR_BLACK        = 0,
		COLOR_BLUE         = 1,
		COLOR_GREEN        = 2,
		COLOR_AQUA         = 3,
		COLOR_RED          = 4,
		COLOR_PURPLE       = 5,
		COLOR_YELLOW       = 6,
		COLOR_WHITE        = 7,
		
		COLOR_GRAY         = 8,
		COLOR_LIGHT_BLUE   = 9,
		COLOR_LIGHT_GREEN  = 10,
		COLOR_LIGHT_AQUA   = 11,
		COLOR_LIGHT_RED    = 12,
		COLOR_LIGHT_PURPLE = 13,
		COLOR_LIGHT_YELLOW = 14,
		COLOR_BRIGHT_WHITE = 15
	};
	
	static void ChangeConsoleColor(CONSOLE_COLOR color)
	{
		HANDLE handle = ::GetStdHandle(STD_OUTPUT_HANDLE);
		if ( handle && INVALID_HANDLE_VALUE != handle ) {
			::SetConsoleTextAttribute(handle, static_cast<WORD>(color));
		}
	}
	
	static void WriteToConsole(const WStr& wtext, Logger::E_LEVEL level)
	{
		switch ( level ) {
		case Logger::LEVEL_DEBUG:
			ChangeConsoleColor(COLOR_WHITE);
			break;
		case Logger::LEVEL_INFO:
			ChangeConsoleColor(COLOR_BRIGHT_WHITE);
			break;
		case Logger::LEVEL_COMPLETE:
			ChangeConsoleColor(COLOR_LIGHT_GREEN);
			break;
		case Logger::LEVEL_WARNING:
			ChangeConsoleColor(COLOR_LIGHT_YELLOW);
			break;
		case Logger::LEVEL_ERROR:
			ChangeConsoleColor(COLOR_LIGHT_RED);
			break;
		default:
			BME_ASSERT(false);
			ChangeConsoleColor(COLOR_WHITE);
		}
		
		HANDLE handle = ::GetStdHandle(STD_OUTPUT_HANDLE);
		if ( handle && INVALID_HANDLE_VALUE != handle ) {
			::WriteConsoleW(handle, wtext.c_str(), wtext.length(), NULL, NULL);
		}
		
		ChangeConsoleColor(COLOR_WHITE);
	}
}

// ----------------------------------------------------------------------------
// 
// SystemWIN
// 
// ----------------------------------------------------------------------------

SystemWIN::SystemWIN()
: _defaultIcon(NULL), _currentIcon(NULL)
, _defaultCursor(NULL), _currentCursor(NULL), _cursorVisible(true)
, _hWnd(0), _hInstance(GetModuleHandle(0))
, _hInstanceMutex(0), _restoreMessage(0)
, _hDC(0), _hRC(0), _oglLib(0)
, _wglCreateContext(NULL), _wglMakeCurrent(NULL)
, _wglDeleteContext(NULL), _wglGetProcAddress(NULL)
, _startTimer(0)
{
}

SystemWIN::~SystemWIN()
{
}

void SystemWIN::_updateCursor()
{
	::SetCursor(_cursorVisible ? _currentCursor : NULL);
}

void SystemWIN::_updateClipCursor()
{
	Application* app = theApplication();
	if ( !app ) {
		BME_LOG_ERROR("SystemWIN::_updateClipCursor() application not found");
		return;
	}
	if ( app->getDesc().isFullscreenCursorClip && app->getDesc().isFullscreen ) {
		rectu correct_vp = Camera().getCorrectViewport();
		RECT rect;
		rect.left   = Math::SmartCast<LONG>(correct_vp.getLeft  ());
		rect.top    = Math::SmartCast<LONG>(correct_vp.getTop   ());
		rect.right  = Math::SmartCast<LONG>(correct_vp.getRight ());
		rect.bottom = Math::SmartCast<LONG>(correct_vp.getBottom());
		::ClipCursor(&rect);
	} else {
		::ClipCursor(NULL);
	}
}

bool SystemWIN::_initWindow()
{
	Application* app = theApplication();
	if ( !app ) {
		BME_LOG_ERROR("SystemWIN::_initWindow() application not found");
		return false;
	}
	
	// -----------------------------------
	// default cursors and icon options
	// -----------------------------------
	
	_defaultIcon   = ::LoadIcon(NULL, IDI_APPLICATION);
	_currentIcon   = _defaultIcon;
	_defaultCursor = ::LoadCursor(NULL, IDC_ARROW);
	_currentCursor = _defaultCursor;
	_cursorVisible = true;
	
	// -----------------------------------
	// register window
	// -----------------------------------
	
	WNDCLASS winclass;
	winclass.style         = CS_HREDRAW | CS_VREDRAW;
	winclass.lpfnWndProc   = &WindowProc;
	winclass.cbClsExtra    = 0;
	winclass.cbWndExtra    = 0;
	winclass.hInstance     = _hInstance;
	winclass.hIcon         = _currentIcon;
	winclass.hCursor       = _currentCursor;
	winclass.hbrBackground = (HBRUSH)::GetStockObject(BLACK_BRUSH);
	winclass.lpszMenuName  = 0;
	winclass.lpszClassName = BME_WINDOW_CLASS_NAME;
	
	if ( !::RegisterClass(&winclass) ) {
		return false;
	}
	
	// -----------------------------------
	// calculate screen resolutions
	// -----------------------------------
	
	CalculateResolutions(
		pnt2u(::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN)));
	
	// -----------------------------------
	// create window
	// -----------------------------------
	
	LONG  styleW;
	rects clientSize;
	_changeWindowStyle(
		app->getDesc().isFullscreen,
		app->getDesc().parentHandler != 0,
		&styleW, &clientSize);
	
	_hWnd = CreateWindowEx(
		app->getDesc().isFullscreen ? WS_EX_TOPMOST : 0,
		BME_WINDOW_CLASS_NAME, __TEXT(""), styleW,
		Math::SmartCast<int>(clientSize.getLeft  ()),
		Math::SmartCast<int>(clientSize.getTop   ()),
		Math::SmartCast<int>(clientSize.getWidth ()),
		Math::SmartCast<int>(clientSize.getHeight()),
		(HWND)app->getDesc().parentHandler, NULL, _hInstance, NULL);
	
	if ( !_hWnd ) {
		return false;
	}
	
	// -----------------------------------
	// create gl context
	// -----------------------------------
	
#if BME_RENDERER == BME_RENDERER_OGL
	if ( !_initGLContext() ) {
		return false;
	}
#endif
	
	return true;
}

void SystemWIN::_deinitWindow()
{
	_deinitGLContext();
	if ( _hWnd ) {
		::DestroyWindow(_hWnd);
		_hWnd = 0;
	}
	if ( _hInstance ) {
		::UnregisterClass(BME_WINDOW_CLASS_NAME, _hInstance);
		_hInstance = 0;
	}
}

void SystemWIN::_changeWindowStyle(
	bool isFullscreen, bool isChild, LONG* outStyle, rects* outRect) const
{
	LONG  styleW;
	rects clientSize(0, 0, getWindowResolution().x, getWindowResolution().y);
	if ( isFullscreen || isChild ) {
		if ( isChild ) {
			styleW = WS_CHILD;
		} else {
			styleW = WS_POPUP | WS_EX_TOPMOST;
		}
	} else {
		styleW = WS_POPUP|WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX|WS_CLIPCHILDREN|WS_CLIPSIBLINGS;
		
		RECT clien_rect;
		clien_rect.left   = 0;
		clien_rect.top    = 0;
		clien_rect.right  = Math::SmartCast<LONG>(getWindowResolution().x);
		clien_rect.bottom = Math::SmartCast<LONG>(getWindowResolution().y);
		::AdjustWindowRect(&clien_rect, styleW, FALSE);
		
		LONG real_width  = clien_rect.right  - clien_rect.left;
		LONG real_height = clien_rect.bottom - clien_rect.top;
		LONG real_left   = (Math::SmartCast<LONG>(getDesktopResolution().x) - real_width)  / 2;
		LONG real_top    = (Math::SmartCast<LONG>(getDesktopResolution().y) - real_height) / 2;
		
		real_left = cpp::max(real_left, 0L);
		real_top  = cpp::max(real_top,  0L);
		
		clientSize = rects(
			pnt2s(real_left,  real_top),
			pnt2s(real_width, real_height));
	}
	
	if ( outStyle ) {
		*outStyle = styleW;
	}
	if ( outRect ) {
		*outRect = clientSize;
	}
}

bool SystemWIN::_initInstanceMutex()
{
	if ( theApplication() && theApplication()->getDesc().isOneInstanceOnly ) {
		_restoreMessage = ::RegisterWindowMessage(BME_WINDOW_MESSAGE_RESTORE_NAME);
		_hInstanceMutex = ::CreateMutex(NULL, FALSE, BME_INSTANCE_MUTEX_NAME);
		if ( ::GetLastError() == ERROR_ALREADY_EXISTS ) {
			if ( _restoreMessage ) {
				::PostMessage(HWND_BROADCAST, _restoreMessage, 0, 0);
			}
			return false;
		}
	}
	return true;
}

void SystemWIN::_deinitInstanceMutex()
{
	if ( _hInstanceMutex ) {
		::ReleaseMutex(_hInstanceMutex);
		_hInstanceMutex = 0;
	}
}

bool SystemWIN::_initGLContext()
{
	_oglLib = ::LoadLibrary(__TEXT("OPENGL32.DLL"));
	if ( !_oglLib ) {
		BME_LOG_ERROR("SystemWIN::_initGLContext() can't load OGL library");
		return false;
	}
	
	_wglCreateContext  = (HGLRC(WINAPI *)(HDC))        ::GetProcAddress(_oglLib, "wglCreateContext");
	_wglMakeCurrent    = (BOOL (WINAPI *)(HDC, HGLRC)) ::GetProcAddress(_oglLib, "wglMakeCurrent");
	_wglDeleteContext  = (BOOL (WINAPI *)(HGLRC))      ::GetProcAddress(_oglLib, "wglDeleteContext");
	_wglGetProcAddress = (PROC (WINAPI *)(LPCSTR))     ::GetProcAddress(_oglLib, "wglGetProcAddress");
	
	if ( !(_wglCreateContext && _wglMakeCurrent && _wglDeleteContext && _wglGetProcAddress) ) {
		BME_LOG_ERROR("SystemWIN::_initGLContext() can't get any wgl functions");
		return false;
	}
	
	PIXELFORMATDESCRIPTOR pfd;
	BME_memset(&pfd, 0, sizeof(pfd));
	pfd.nSize      = sizeof(pfd);
	pfd.nVersion   = 1;
	pfd.dwFlags    = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 16;
	pfd.iLayerType = PFD_MAIN_PLANE;
	
	_hDC = ::GetDC(_hWnd);
	int pixelFormat = ::ChoosePixelFormat(_hDC, &pfd);
	if ( 0 == pixelFormat ) {
		return false;
	}
	
	if ( !::SetPixelFormat(_hDC, pixelFormat, &pfd) ) {
		return false;
	}
	
	_hRC = wglCreateContext(_hDC);
	if ( !_hRC ) {
		return false;
	}
	
	if ( !wglMakeCurrent(_hDC, _hRC) ) {
		return false;
	}
	
	// vsync
	
	typedef BOOL (WINAPI *wglSwapIntervalEXT_Func)(int);
	wglSwapIntervalEXT_Func wglSwapIntervalEXT = (wglSwapIntervalEXT_Func)wglGetProcAddress("wglSwapIntervalEXT");
	if ( wglSwapIntervalEXT && theApplication() ) {
		wglSwapIntervalEXT(theApplication()->getDesc().isVsync ? 1 : 0);
	}
	
	return true;
}

void SystemWIN::_deinitGLContext()
{
	if ( _hRC ) {
		wglMakeCurrent(0, 0);
		wglDeleteContext(_hRC);
		_hRC = 0;
	}
	
	if ( _hDC ) {
		::ReleaseDC(_hWnd, _hDC);
		_hDC = 0;
	}
	
	if ( _oglLib ) {
		_wglCreateContext  = NULL;
		_wglMakeCurrent    = NULL;
		_wglDeleteContext  = NULL;
		_wglGetProcAddress = NULL;
		::FreeLibrary(_oglLib);
		_oglLib = 0;
	}
}

bool SystemWIN::_initTimers()
{
	::timeBeginPeriod(1);
	_startTimer = ::timeGetTime();
	return true;
}

void SystemWIN::_deinitTimers()
{
	::timeEndPeriod(1);
}

void SystemWIN::KeyMsg(UINT msg, WPARAM wparam, LPARAM lparam)
{
	E_KEY event_keyboard_key_1   = KEY_UNKNOWN;
	E_KEY event_keyboard_key_2   = KEY_UNKNOWN;
	bool  event_keyboard_down    = false;
	u32   event_keyboard_charKey = 0;
	
	// type event
	
	switch( msg ) {
	case WM_SYSKEYDOWN:
	case WM_KEYDOWN:
		event_keyboard_down = true;
		break;
	case WM_SYSKEYUP:
	case WM_KEYUP:
		event_keyboard_down = false;
		break;
	default:
		return;
	}
	
	UINT  scanCode = HIWORD(lparam) & 0xFF;
	event_keyboard_key_1 = static_cast<E_KEY>(wparam);
	event_keyboard_key_2 = KEY_UNKNOWN;
	
	WCHAR unibuffer[8];
	BYTE lpKeyState[256];
	::GetKeyboardState(lpKeyState);
	::ToUnicode(static_cast<UINT>(wparam), scanCode, lpKeyState, unibuffer, 8, 0);
	
	// fix left and right key
	
	const UINT MYPVK_VSC_TO_VK_EX = 3;
	if ( wparam == VK_SHIFT ) {
		event_keyboard_key_1 = static_cast<E_KEY>(::MapVirtualKey(scanCode, MYPVK_VSC_TO_VK_EX));
		if (lparam & 0x1000000) { // fix from irrlicht
			event_keyboard_key_1 = KEY_RSHIFT;
		}
		
		// check and fix down both shift
		if ( event_keyboard_down ) {
			if ( event_keyboard_key_1 == KEY_RSHIFT && theKeyboard()->IsKeyDowned(KEY_LSHIFT) ) {
				return;
			}
			if ( event_keyboard_key_1 == KEY_LSHIFT && theKeyboard()->IsKeyDowned(KEY_RSHIFT) ) {
				return;
			}
		} else {
			event_keyboard_key_1 = KEY_LSHIFT;
			event_keyboard_key_2 = KEY_RSHIFT;
		}
	}
	else if ( event_keyboard_key_1 == VK_CONTROL ) {
		event_keyboard_key_1 = static_cast<E_KEY>(::MapVirtualKey(scanCode, MYPVK_VSC_TO_VK_EX));
		if (lparam & 0x1000000) {
			event_keyboard_key_1 = KEY_RCONTROL;
		}
	}
	else if ( event_keyboard_key_1 == VK_MENU ) {
		event_keyboard_key_1 = static_cast<E_KEY>(::MapVirtualKey(scanCode, MYPVK_VSC_TO_VK_EX));
		if (lparam & 0x1000000) {
			event_keyboard_key_1 = KEY_RMENU;
		}
	}
	
	UStr tmp_utf32;
	Strings::MakeUtf32(tmp_utf32, unibuffer);
	event_keyboard_charKey = tmp_utf32.empty() ? 0 : tmp_utf32[0];
	
	// Build event
	
	if ( KEY_UNKNOWN != event_keyboard_key_1 ) {
		Receiver::Event e;
		e.type             = event_keyboard_down ? Receiver::Event::TYPE_KEYBOARD_DOWN : Receiver::Event::TYPE_KEYBOARD_UP;
		e.keyboard.key     = event_keyboard_key_1;
		e.keyboard.charKey = event_keyboard_charKey;
		e.keyboard.which   = 0;
		e.Push();
	}
	
	if ( KEY_UNKNOWN != event_keyboard_key_2 ) {
		Receiver::Event e;
		e.type             = event_keyboard_down ? Receiver::Event::TYPE_KEYBOARD_DOWN : Receiver::Event::TYPE_KEYBOARD_UP;
		e.keyboard.key     = event_keyboard_key_2;
		e.keyboard.charKey = event_keyboard_charKey;
		e.keyboard.which   = 0;
		e.Push();
	}
}

void SystemWIN::MouseMsg(UINT msg, WPARAM wparam, LPARAM lparam)
{
	E_MOUSE event_mouse_key  = MOUSE_UNKNOWN;
	bool    event_mouse_down = false;
	bool    event_mouse_move = false;
	vec2f   event_mouse_pos  = vec2f::zero;
	
	// --------------------------------
	// type event
	// --------------------------------
	
	switch ( msg ) {
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_MBUTTONDOWN:
	case WM_XBUTTONDOWN:
	case WM_MOUSEWHEEL:
		event_mouse_down = true;
		break;
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MBUTTONUP:
	case WM_XBUTTONUP:
		event_mouse_down = false;
		break;
	case WM_MOUSEMOVE:
		event_mouse_move = true;
		break;
	default:
		return;
	}
	
	// --------------------------------
	// type button
	// --------------------------------
	
	switch ( msg ) {
	case WM_LBUTTONUP:
	case WM_LBUTTONDOWN:
		event_mouse_key = MOUSE_LEFT;
		break;
	case WM_RBUTTONUP:
	case WM_RBUTTONDOWN:
		event_mouse_key = MOUSE_RIGHT;
		break;
	case WM_MBUTTONUP:
	case WM_MBUTTONDOWN:
		event_mouse_key = MOUSE_MIDLE;
		break;
	case WM_XBUTTONUP:
	case WM_XBUTTONDOWN:
		{
			if ( LOWORD(wparam) & MK_XBUTTON1 ) {
				event_mouse_key = MOUSE_X1MOUSE;
			} else if ( LOWORD(wparam) & MK_XBUTTON2 ) {
				event_mouse_key = MOUSE_X2MOUSE;
			} else {
				return;
			}
		}
		break;
	case WM_MOUSEWHEEL:
		{
			f32 wheel = GET_WHEEL_DELTA_WPARAM(wparam)/Math::SmartCast<f32>(WHEEL_DELTA);
			event_mouse_key = wheel <= 0.f ? MOUSE_WHEELDOWN : MOUSE_WHEELUP;
		}
		break;
	case WM_MOUSEMOVE:
		break;
	default:
		return;
	}
	
	// --------------------------------
	// mouse pos
	// --------------------------------
	
	event_mouse_pos.x = Math::SmartCast<f32>(WINX_GET_X_LPARAM(lparam));
	event_mouse_pos.y = Math::SmartCast<f32>(WINX_GET_Y_LPARAM(lparam));
	
	// --------------------------------
	// Build event
	// --------------------------------
	
	Receiver::Event e;
	e.type        = event_mouse_move ? Receiver::Event::TYPE_MOUSE_MOVE : (event_mouse_down ? Receiver::Event::TYPE_MOUSE_DOWN : Receiver::Event::TYPE_MOUSE_UP);
	e.mouse.key   = event_mouse_key;
	e.mouse.x     = event_mouse_pos.x;
	e.mouse.y     = event_mouse_pos.y;
	e.mouse.which = 0;
	e.Push();
	
	// --------------------------------
	// mouse capture
	// --------------------------------
	
	SystemWIN* system = Holder<SystemWIN>::getHostage();
	
	static s32 s_click_count = 0;
	if ( ::GetCapture() != system->_hWnd ) {
		s_click_count = 0;
	}
	
	switch ( msg ) {
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_MBUTTONDOWN:
		{
			++s_click_count;
			::SetCapture(system->_hWnd);
		}
		break;
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MBUTTONUP:
		{
			--s_click_count;
			if ( s_click_count <= 0 ) {
				s_click_count = 0;
				::ReleaseCapture();
			}
		}
		break;
	}
}

LRESULT CALLBACK SystemWIN::WindowProc(
	HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	Application* app    = theApplication();
	SystemWIN*   system = Holder<SystemWIN>::getHostage();
	
	if ( hwnd == system->_hWnd && msg == system->_restoreMessage ) {
		system->RestoreWindow();
		::SetForegroundWindow(system->_hWnd);
		return FALSE;
	}
	
	switch ( msg ) {
	case WM_PAINT:
		{
			PAINTSTRUCT ps;
			::BeginPaint(hwnd, &ps);
			::EndPaint(hwnd, &ps);
		}
		return FALSE;
	case WM_ERASEBKGND:
		{
		}
		return FALSE;
	case WM_SETFOCUS:
		{
			Receiver::Event e;
			e.type = Receiver::Event::TYPE_FOCUS;
			e.focus.gain = true;
			e.Push(true);
		}
		return FALSE;
	case WM_KILLFOCUS:
		{
			Receiver::Event e;
			e.type = Receiver::Event::TYPE_FOCUS;
			e.focus.gain = false;
			e.Push(true);
		}
		return FALSE;
	case WM_ACTIVATEAPP:
		{
			bool gain = (wparam == TRUE);
			if ( system && gain ) {
				system->_updateClipCursor();
			}
			Receiver::Event e;
			e.type = Receiver::Event::TYPE_ACTIVE;
			e.active.gain = gain;
			e.Push(true);
		}
		return FALSE;
	case WM_DESTROY:
		{
			::PostQuitMessage(0);
		}
		return FALSE;
	case WM_SETCURSOR:
		{
			if ( system && app ) {
				if ( LOWORD(lparam) == HTCLIENT && (system->isWindowActive() || !app->getDesc().isNoFocusSuspend) ) {
					system->_updateCursor();
				} else {
					::SetCursor(::LoadCursor(NULL, IDC_ARROW));
				}
			}
		}
		return FALSE;
	case WM_SYSCOMMAND:
		{
			switch ( wparam ) {
			case SC_CLOSE:
				{
					Receiver::Event e;
					e.type = Receiver::Event::TYPE_QUIT;
					e.Push();
				}
				return FALSE;
			case SC_SCREENSAVE:
				{
					if ( app && !app->getDesc().isAllowScreensaver ) {
						return FALSE;
					}
				}
				break;
			case SC_MINIMIZE:
			case SC_MAXIMIZE:
			case SC_RESTORE:
				break;
			case SC_MONITORPOWER:
			case SC_HOTKEY:
			case SC_TASKLIST:
			case SC_SIZE:
				return FALSE;
			}
		}
		break;
	
	// Mouse
	
	case WM_MOUSEMOVE:
	case WM_LBUTTONUP: case WM_LBUTTONDOWN:
	case WM_RBUTTONUP: case WM_RBUTTONDOWN:
	case WM_MBUTTONUP: case WM_MBUTTONDOWN:
	case WM_XBUTTONUP: case WM_XBUTTONDOWN:
	case WM_MOUSEWHEEL:
		{
			MouseMsg(msg, wparam, lparam);
		}
		return FALSE;
	
	// Keyboard
	
	case WM_KEYDOWN:
	case WM_KEYUP:
		{
			KeyMsg(msg, wparam, lparam);
		}
		return FALSE;
	case WM_SYSKEYDOWN:
	case WM_SYSKEYUP:
		{
			KeyMsg(msg, wparam, lparam);
			// exit by alt+F4
			if ( wparam == VK_F4 ) {
				Receiver::Event e;
				e.type = Receiver::Event::TYPE_QUIT;
				e.Push();
			}
		}
		return FALSE;
	}
	return ::DefWindowProc(hwnd, msg, wparam, lparam);
}

bool SystemWIN::Init()
{
	bool success = (_initInstanceMutex() && _initWindow() && _initTimers());
	if ( !success ) {
		BME_LOG_ERROR("SystemWIN::Init() Window init failed!");
		return false;
	}
	
	if ( !RendererImpl::Initialize() ) {
		BME_LOG_ERROR("SystemWIN::Init() Renderer init failed!");
		return false;
	}
	
	return true;
}

void SystemWIN::DeInit()
{
	RendererImpl::Shutdown();
	_deinitTimers();
	_deinitWindow();
	_deinitInstanceMutex();
}

// ----------------------------------------------------------------------------
// 
// Window override
// 
// ----------------------------------------------------------------------------

void SystemWIN::setWindowCaption(const Str& value)
{
	WStr caption;
	Strings::MakeWide(caption, value.c_str());
	::SetWindowTextW(_hWnd, caption.c_str());
}

void SystemWIN::setWindowIcon(const Path& value)
{
	HICON icon = system_win_detail::LoadIconByPath(value);
	if ( !icon ) {
		icon = _defaultIcon;
	}
	if ( _currentIcon && _currentIcon != _defaultIcon ) {
		::DestroyIcon(_currentIcon);
		_currentIcon = NULL;
	}
	_currentIcon = icon;
	::SetClassLongPtr(_hWnd, GCLP_HICON, (LONG_PTR)icon);
}

void SystemWIN::setCursor(const Path& value, const pnt2u& hotSpot)
{
	HICON cursor = system_win_detail::LoadCursorByPath(value, hotSpot);
	if ( !cursor ) {
		cursor = _defaultCursor;
	}
	if ( _currentCursor && _currentCursor != _defaultCursor ) {
		::DestroyCursor(_currentCursor);
		_currentCursor = NULL;
	}
	_currentCursor = cursor;
	::SetClassLongPtr(_hWnd, GCLP_HCURSOR, (LONG_PTR)cursor);
	_updateCursor();
}

void SystemWIN::setCursorPosition(const pnt2u& value)
{
	vec2f cp = PointToScreen(vec2f(value));
	::POINT p = {
		Math::SmartCast<LONG>(cp.x),
		Math::SmartCast<LONG>(cp.y) };
	::ClientToScreen(_hWnd, &p);
	::SetCursorPos(p.x, p.y);
}

void SystemWIN::setCursorVisible(bool yesno)
{
	_cursorVisible = yesno;
	_updateCursor();
}

bool SystemWIN::isCursorVisible() const
{
	return _cursorVisible;
}

void SystemWIN::ShowWindow()
{
	if ( !isWindowVisible() ) {
		::ShowWindow(_hWnd, SW_SHOW);
		::UpdateWindow(_hWnd);
	}
	_updateClipCursor();
}

void SystemWIN::HideWindow()
{
	if ( isWindowVisible() ) {
		::ShowWindow(_hWnd, SW_HIDE);
	}
}

void SystemWIN::RestoreWindow()
{
	if ( isWindowMinimized() ) {
		::ShowWindow(_hWnd, SW_RESTORE);
		::SetFocus(_hWnd);
	}
	_updateClipCursor();
}

void SystemWIN::MinimizeWindow()
{
	if ( !isWindowMinimized() ) {
		::ShowWindow(_hWnd, SW_MINIMIZE);
	}
}

bool SystemWIN::isWindowVisible() const
{
	return ::IsWindowVisible(_hWnd) == TRUE;
}

bool SystemWIN::isWindowFocused() const
{
	return ::GetFocus() == _hWnd;
}

bool SystemWIN::isWindowActive() const
{
	return ::GetActiveWindow() == _hWnd;
}

bool SystemWIN::isWindowMinimized() const
{
	return TRUE == ::IsIconic(_hWnd);
}

void SystemWIN::ShowConsole() const
{
	::AllocConsole();
}

void SystemWIN::HideConsole() const
{
	::FreeConsole();
}

bool SystemWIN::ShowSoftKeyboard() const
{
	return false;
}

void SystemWIN::HideSoftKeyboard() const
{
}

bool SystemWIN::isSoftKeyboardVisible() const
{
	return false;
}

System::E_ORIENTATION SystemWIN::getDeviceOrientation() const
{
	if ( getDesktopResolution().x >= getDesktopResolution().y ) {
		return ORIENTATION_LANDSCAPE;
	} else {
		return ORIENTATION_PORTRAIT;
	}
}

// ----------------------------------------------------------------------------
// 
// SystemImpl override
// 
// ----------------------------------------------------------------------------

void SystemWIN::Update()
{
	if ( theApplication() && !theApplication()->getDesc().parentHandler ) {
		MSG msg;
		while ( ::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) ) {
			if ( msg.message == WM_QUIT ) {
				Receiver::Event e;
				e.type = Receiver::Event::TYPE_QUIT;
				e.Push();
			} else {
				::DispatchMessage(&msg);
			}
		}
	}
}

void SystemWIN::EndFrame()
{
#if BME_RENDERER == BME_RENDERER_OGL
	::SwapBuffers(_hDC);
#endif
	
	RendererImpl* renderer = Holder<RendererImpl>::getHostage();
	if ( renderer ) {
		renderer->EndFrame();
	}
}

bool SystemWIN::ToggleFullscreen(bool yesno)
{
	Application* app = theApplication();
	if ( !app ) {
		BME_LOG_ERROR("SystemWIN::ToggleScreenMode() application not found!");
		return false;
	}
	
	CalculateResolutions(
		pnt2u(::GetSystemMetrics(SM_CXSCREEN), ::GetSystemMetrics(SM_CYSCREEN)));
	
	LONG  styleW;
	rects clientSize;
	_changeWindowStyle(
		yesno,
		app->getDesc().parentHandler != 0,
		&styleW, &clientSize);
	
	if ( !::SetWindowLongPtr(_hWnd, GWL_STYLE, styleW) ) {
		return false;
	}
	
	if ( !::SetWindowPos(
		_hWnd, yesno ? HWND_TOPMOST : HWND_NOTOPMOST,
		Math::SmartCast<int>(clientSize.getLeft()),
		Math::SmartCast<int>(clientSize.getTop()),
		Math::SmartCast<int>(clientSize.getWidth()),
		Math::SmartCast<int>(clientSize.getHeight()),
		SWP_FRAMECHANGED | SWP_SHOWWINDOW) )
	{
		return false;
	}
	
	RendererImpl* renderer = Holder<RendererImpl>::getHostage();
	if ( renderer ) {
		renderer->Resize();
	}
	
	if ( !yesno ) {
		/// \todo for <=XP only?
		::ChangeDisplaySettings(NULL,NULL);
	}
	
	_updateClipCursor();
	return true;
}
	
bool SystemWIN::ToggleWidescreen(bool /*yesno*/)
{
	_updateClipCursor();
	return true;
}

void SystemWIN::NativeLog(const char* text, Logger::E_LEVEL level) const
{
	/// \todo need convert on stack, without allocs
	WStr wtext;
	Strings::MakeWide(wtext, text);
	
	using namespace system_win_detail;
	WriteToConsole(wtext, level);
	
#if BME_COMPILER == BME_COMPILER_MSVC
	::OutputDebugStringW(wtext.c_str());
#endif
}

size_t SystemWIN::getTicks() const
{
	DWORD now = ::timeGetTime();
	return Math::SmartCast<size_t>(now - _startTimer);
}

Inputer::Desc SystemWIN::getInputerDesc() const
{
	Inputer::Desc desc;
	desc.mouses    = 1;
	desc.keyboards = 1;
	desc.joysticks = 0;
	return desc;
}

ptrdiff_t SystemWIN::getHWND() const
{
	return reinterpret_cast<ptrdiff_t>(_hWnd);
}

} // namespace bme

#endif // BME_SYSTEM_WINDOWS
