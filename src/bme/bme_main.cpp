/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/bme_main.cpp
 *****************************************************************************/

#include <bme/bme_main.h>

using namespace bme;

// ----------------------------------------------------------------------------
//
// (Windows) BMEStart
//
// ----------------------------------------------------------------------------

#if BME_OS == BME_OS_WINDOWS
	
	void BMEStart(const Application::Desc& desc)
	{
		if ( !theApplication() )
			return;
		
		if ( theApplication()->Start(desc) )
		{
			if ( !desc.parentHandler )
				while ( theApplication()->Step() ) {}
		}
		
		if ( !desc.parentHandler )
			BMEShutdown();
	}
	
#endif // BME_OS_WINDOWS

// ----------------------------------------------------------------------------
//
// (Android && iOS && MacOSX) BMEStart
//
// ----------------------------------------------------------------------------

#if BME_OS == BME_OS_ANDROID || BME_OS == BME_OS_IOS || BME_OS == BME_OS_MACOSX
	
	void BMEStart(const Application::Desc& desc)
	{
		if ( !theApplication() )
			return;
		
		theApplication()->Start(desc);
	}
	
#endif // BME_OS_ANDROID || BME_OS_IOS || BME_OS_MACOSX

// ----------------------------------------------------------------------------
//
// Common
//
// ----------------------------------------------------------------------------

void BMEShutdown()
{
	if ( theApplication() )
	{
		theApplication()->Shutdown();
		delete theApplication();
	}
}
