/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/FilesImpl_Common.cpp
 *****************************************************************************/

#include <bme/core/Files.h>

namespace bme
{
namespace files_impl
{

// ----------------------------------------------------------------------------
// 
// Predefs
// 
// ----------------------------------------------------------------------------

class MMapFile_Memory;
class ReadFile_Memory;

// ----------------------------------------------------------------------------
// 
// MMapFile_Memory
// 
// ----------------------------------------------------------------------------

class MMapFile_Memory : public MMapFile{
	typedef cpp::intrusive_ptr<const ReadFile_Memory> TFile;
	TFile          _file;
	const void*    _data;
public:
	MMapFile_Memory(const ReadFile_Memory* file);
	virtual ~MMapFile_Memory();
	// MMapFile override ------------------------------------------------------
	size_t         getSize  () const;
	const void*    getData  () const;
	const Path&    getPath  () const;
	// ------------------------------------------------------------------------
};

// ----------------------------------------------------------------------------
// 
// ReadFile_Memory
// 
// ----------------------------------------------------------------------------

class ReadFile_Memory : public ReadFile{
	const void*    _buffer;
	size_t         _pos;
	size_t         _size;
	Path           _path;
	TBufferDeleter _deleter;
public:
	ReadFile_Memory(
		const void* buffer, size_t size,
		const Path& path, TBufferDeleter deleter);
	virtual ~ReadFile_Memory();
	const void*    getData  () const;
	// ReadFile override ------------------------------------------------------
	size_t         Read     ( void* dest, size_t size );
	bool           Seek     ( ptrdiff_t offset, bool relative );
	size_t         getPos   () const;
	size_t         getSize  () const;
	const Path&    getPath  () const;
	MMapFilePtr    MakeMMap () const;
	// ------------------------------------------------------------------------
};

// ----------------------------------------------------------------------------
// 
// MMapFile_Memory impl
// 
// ----------------------------------------------------------------------------

MMapFile_Memory::MMapFile_Memory(const ReadFile_Memory* file)
: _file(file)
, _data(file->getData())
{
}

MMapFile_Memory::~MMapFile_Memory()
{
}

size_t MMapFile_Memory::getSize() const
{
	BME_ASSERT(_file);
	return _file->getSize();
}

const void* MMapFile_Memory::getData() const
{
	return _data;
}

const Path& MMapFile_Memory::getPath() const
{
	BME_ASSERT(_file);
	return _file->getPath();
}

// ----------------------------------------------------------------------------
// 
// ReadFile_Memory impl
// 
// ----------------------------------------------------------------------------

ReadFile_Memory::ReadFile_Memory(
	const void* buffer, size_t size,
	const Path& path, TBufferDeleter deleter)
: _buffer(buffer)
, _pos(0)
, _size(size)
, _path(path)
, _deleter(deleter)
{
}

ReadFile_Memory::~ReadFile_Memory()
{
	if ( _deleter && _buffer ) {
		_deleter(const_cast<void*>(_buffer));
		_buffer = NULL;
	}
}

const void* ReadFile_Memory::getData() const
{
	return _buffer;
}

size_t ReadFile_Memory::Read(void* dest, size_t size)
{
	BME_ASSERT(dest && size);
	ptrdiff_t amount = Math::SmartCast<ptrdiff_t>(size);
	if ( getPos() + amount > getSize() ) {
		amount -= getPos() + amount - getSize();
	}
	if ( amount <= 0 ) {
		return 0;
	}
	const u8* p = static_cast<const u8*>(getData());
	BME_memcpy(dest, p + getPos(), amount);
	_pos += amount;
	return amount;
}

bool ReadFile_Memory::Seek(ptrdiff_t offset, bool relative)
{
	ptrdiff_t self_pos  = Math::SmartCast<ptrdiff_t>(getPos());
	ptrdiff_t self_size = Math::SmartCast<ptrdiff_t>(getSize());
	if ( relative ) {
		offset = self_pos + offset;
	}
	if ( offset < 0 || offset > self_size ) {
		return false;
	}
	_pos = Math::SmartCast<size_t>(offset);
	return true;
}

size_t ReadFile_Memory::getPos() const
{
	return _pos;
}

size_t ReadFile_Memory::getSize() const
{
	return _size;
}

const Path& ReadFile_Memory::getPath() const
{
	return _path;
}

MMapFilePtr ReadFile_Memory::MakeMMap() const
{
	return MMapFilePtr(new MMapFile_Memory(this));
}

} // namespace files_impl

// ----------------------------------------------------------------------------
// 
// Static interfaces files functions
// 
// ----------------------------------------------------------------------------

ReadFilePtr ReadFile::Create(
	const void* buffer, size_t size,
	const Path& path, TBufferDeleter deleter)
{
	BME_ASSERT(buffer && size);
	return ReadFilePtr(new files_impl::ReadFile_Memory(
		buffer, size, path, deleter));
}

} // namespace bme
