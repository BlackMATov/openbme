/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/SystemImpl.cpp
 *****************************************************************************/

#include "SystemImpl.h"
#include "InputerImpl.h"
#include "ReceiverImpl.h"

#include <bme/core/Camera.h>
#include <bme/core/Application.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// system_detail
// 
// ----------------------------------------------------------------------------

namespace system_detail
{
	class StandartReceiver : public Receiver
	{
	public:
		
		StandartReceiver() {}
		virtual ~StandartReceiver() {}
		
		virtual bool OnEvent(const Event& event)
		{
			Application* app     = theApplication();
			System*      system  = theSystem();
			InputerImpl* inputer = Holder<InputerImpl>::getHostage();
			switch ( event.type ) {
			case Event::TYPE_FOCUS:
				{
					if ( inputer && event.focus.gain ) {
						inputer->Reset();
					}
					BME_LOG_DEBUG_FMT(
						"Receiver::Event::TYPE_FOCUS: '%1%'",
						event.focus.gain);
				}
				break;
			case Event::TYPE_ACTIVE:
				{
					if ( inputer && event.active.gain ) {
						inputer->Reset();
					}
					if ( system && app && !event.active.gain && app->getDesc().isFullscreen ) {
						system->MinimizeWindow();
					}
					BME_LOG_DEBUG_FMT(
						"Receiver::Event::TYPE_ACTIVE: '%1%'",
						event.active.gain);
				}
				break;
			case Event::TYPE_RESET_DEVICE:
				{
					BME_LOG_DEBUG("Receiver::Event::TYPE_RESET_DEVICE");
				}
				break;
			case Event::TYPE_ROTATE_DEVICE:
				{
					BME_LOG_DEBUG("Receiver::Event::TYPE_ROTATE_DEVICE");
				}
				break;
			case Event::TYPE_SOFTKEYBOARD_CHANGE:
				{
					BME_LOG_DEBUG_FMT(
						"Receiver::Event::TYPE_SOFTKEYBOARD_CHANGE: '%1%'",
						event.softkeyboard.text);
				}
				break;
			case Event::TYPE_SOFTKEYBOARD_DONE:
				{
					BME_LOG_DEBUG_FMT(
						"Receiver::Event::TYPE_SOFTKEYBOARD_DONE: '%1%'",
						event.softkeyboard.text);
				}
				break;
			case Event::TYPE_MEMORY_WARNING:
				{
					BME_LOG_WARNING("Receiver::Event::TYPE_MEMORY_WARNING");
				}
				break;
			case Event::TYPE_QUIT:
				{
					if ( !app || app->OnQuit() ) {
						return false;
					}
				}
				break;
			default:
				// nothing
				break;
			}
			return true;
		}
	} standart_receiver;
} // namespace system_detail

// ----------------------------------------------------------------------------
// 
// SystemImpl
// 
// ----------------------------------------------------------------------------

SystemImpl::SystemImpl()
{
	EventEmitter* emitter = Holder<EventEmitter>::getHostage();
	if ( emitter ) {
		emitter->RegisterReceiver(&system_detail::standart_receiver);
	}
}

SystemImpl::~SystemImpl()
{
	EventEmitter* emitter = Holder<EventEmitter>::getHostage();
	if ( emitter ) {
		emitter->UnregisterReceiver(&system_detail::standart_receiver);
	}
}

vec2f SystemImpl::PointToScreen(const vec2f& point)
{
	if ( theApplication() ) {
		rectu vp = Camera().getCorrectViewport();
		const vec2f& app_res = vec2f(theApplication()->getDesc().resolution);
		return point * vec2f(vp.getSize()) / app_res + vec2f(vp.getTopLeft());
	}
	return vec2f::zero;
}

void SystemImpl::CalculateResolutions(const pnt2u& desktop_res)
{
	_desktopResolution = desktop_res;
	if ( !theApplication() || theApplication()->getDesc().isFullscreen ) {
		_windowResolution = desktop_res;
	} else {
		_windowResolution = theApplication()->getDesc().resolution;
		if ( theApplication()->getDesc().isAllowWindowAutoSize ) {
			ptrdiff_t kX = Math::SmartCast<ptrdiff_t>(_windowResolution.x) - Math::SmartCast<ptrdiff_t>(_desktopResolution.x);
			ptrdiff_t kY = Math::SmartCast<ptrdiff_t>(_windowResolution.y) - Math::SmartCast<ptrdiff_t>(_desktopResolution.y);
			
			/// \todo hardcode...
			ptrdiff_t min_window_border    = -50;
			ptrdiff_t minus_window_percent =  10;
			
			if ( kX >= min_window_border || kY >= min_window_border ) {
				f32 kA = Math::SmartCast<f32>(_windowResolution.x) / Math::SmartCast<f32>(_windowResolution.y);
				if ( kX > kY ) {
					_windowResolution.x = _desktopResolution.x -
						_desktopResolution.x / 100 * Math::Clamp<ptrdiff_t>(minus_window_percent, 0, 100);
					_windowResolution.y = Math::SmartCast<size_t>(_windowResolution.x / kA);
				} else {
					_windowResolution.y = _desktopResolution.y -
						_desktopResolution.y / 100 * Math::Clamp<ptrdiff_t>(minus_window_percent, 0, 100);
					_windowResolution.x = Math::SmartCast<size_t>(_windowResolution.y * kA);
				}
			}
		}
	}
}

void SystemImpl::CalculateResolutions(
	const pnt2u& window_res, const pnt2u& desktop_res)
{
	_windowResolution  = window_res;
	_desktopResolution = desktop_res;
}

pnt2u SystemImpl::getWindowResolution() const
{
	return _windowResolution;
}

pnt2u SystemImpl::getDesktopResolution() const
{
	return _desktopResolution;
}

Inputer::Desc SystemImpl::getInputerDesc() const
{
	return Inputer::Desc();
}

Joystick::Desc SystemImpl::getJoystickDesc(size_t /*index*/) const
{
	return Joystick::Desc();
}

f32 SystemImpl::getJoyAxisState(size_t /*index*/, size_t /*nAxis*/) const
{
	return 0.f;
}

vec2f SystemImpl::getJoyBallState(size_t /*index*/, size_t /*nBall*/) const
{
	return vec2f::zero;
}

bool SystemImpl::IsJoyHatDowned(size_t /*index*/, size_t /*nHat*/, E_JOYSTICK_HAT /*state*/) const
{
	return false;
}

} // namespace bme
