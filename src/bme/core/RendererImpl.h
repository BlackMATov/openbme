/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/RendererImpl.h
 *****************************************************************************/

#pragma once

#include <bme/core/Renderer.h>
#include <bme/core/Camera.h>
#include <bme/core/Logger.h>
#include <bme/core/Image.h>

#include "Batches.h"
#include "TextureImpl.h"
#include "RendererDevice.h"

namespace bme
{

class RendererImpl
	: public Renderer
	, public Holder<RendererImpl>
{
	RendererDevicePtr        _device;
	Camera                   _camera;
	Caps                     _caps;
	BatchContainer           _batches;
	SceneInfo                _sceneInfo;
	TTextureList             _textures;
	
	RendererImpl();
	virtual ~RendererImpl();
	
public:
	
	static bool              Initialize       ();
	static void              Shutdown         ();
	
	template < typename T >
	void                     AddToBatch       ( const T& prim );
	void                     EndFrame         ();
	void                     Resize           ();
	const RendererDevicePtr& getDevice        () const;
	
	void                     AddTexture       ( TextureImpl* texture );
	void                     RemoveTexture    ( TextureImpl* texture );
	void                     ReleaseTextures  ( bool rt_only );
	bool                     RecoveryTextures ();
	// Override ---------------------------------------------------------------
	Caps&                    getCaps          ();
	const Caps&              getCaps          () const;
	
	bool                     BeginScene       ();
	void                     ClearScene       ( const Color& color,
	                                            bool back_buffer,
	                                            bool z_buffer);
	const SceneInfo&         EndScene         ();
	
	void                     setCamera        ( const Camera& camera );
	const Camera&            getCamera        () const;
	
	void                     RenderObject     ( const Quad& prim );
	void                     RenderObject     ( const Triangle& prim );
	void                     RenderObject     ( const Line& prim );
	void                     RenderObject     ( const Material& material,
	                                            const RenderStates& states,
	                                            const Vertex* vertices, size_t vertices_count,
	                                            const u16* indices, size_t indices_count,
	                                            E_PRIMITIVE prim_type );
	void                     RenderBatches    ();
	// ------------------------------------------------------------------------
};

template < typename T >
void RendererImpl::AddToBatch(const T& prim)
{
	if ( !_batches.AddPrimitive(prim) ) {
		RenderBatches();
		if ( !_batches.AddPrimitive(prim) ) {
			BME_LOG_ERROR("RendererImpl::AddToBatch() can't added primitive to batches!");
		}
	}
}

} // namespace bme
