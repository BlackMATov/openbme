/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/ImagePVR.cpp
 *****************************************************************************/

#include "ImagePVR.h"
#include <bme/core/Files.h>

// library from depends
#include <powervr/PVRTTexture.h>
#include <powervr/PVRTDecompress.h>

namespace bme
{
namespace image_loaders
{
	static bool PVR_Decompress(ImageDesc& desc,
		const PVR_Texture_Header& header, const SmartBuffer& texture_data)
	{
	#if BME_RENDERER == BME_RENDERER_OGLES
		desc.pixels = texture_data;
		return true;
	#else
		const size_t data_size = desc.pitch * desc.size.y;
		if ( !desc.pixels.Allocate(data_size) ) {
			return false;
		}
		switch ( header.dwpfFlags & PVRTEX_PIXELTYPE )
		{
		case OGL_PVRTC2:
		case OGL_PVRTC4:
			{
				PVRTDecompressPVRTC(
					texture_data.getBuffer(),
					(header.dwpfFlags & PVRTEX_PIXELTYPE) == OGL_PVRTC2 ? 1 : 0,
					Math::SmartCast<const int>(desc.size.x),
					Math::SmartCast<const int>(desc.size.y),
					static_cast<unsigned char*>(desc.pixels.getBuffer()));
				Color::InPlace_RGBA8_To_A8R8G8B8(
					static_cast<u8*>(desc.pixels.getBuffer()), desc.size);
			}
			break;
		case OGL_RGB_888:
			{
				Color::RGB8_To_X8R8G8B8(
					static_cast<     u32*>( desc.pixels.getBuffer()), desc.size,
					static_cast<const u8*>(texture_data.getBuffer()), desc.size);
			}
			break;
		case OGL_RGBA_8888:
			{
				Color::RGBA8_To_A8R8G8B8(
					static_cast<     u32*>( desc.pixels.getBuffer()), desc.size,
					static_cast<const u8*>(texture_data.getBuffer()), desc.size);
			}
			break;
		default:
			// unsupported format
			return false;
		}
		return true;
	#endif
	}
	
	static ImageDesc PVR_PreLoader(
		const ReadFilePtr& file, PVR_Texture_Header& header)
	{
		ImageDesc desc;
		
		// read header
		if ( sizeof(PVR_Texture_Header) != file->Read(&header, sizeof(PVR_Texture_Header)) ) {
			return ImageDesc();
		}
		
		// check pvr magic
		if ( header.dwPVR != PVRTEX_IDENTIFIER ) {
			return ImageDesc();
		}
		
		// get and check size
		desc.size = pnt2u(header.dwWidth, header.dwHeight);
		const bool is_pvrtc2 = ((header.dwpfFlags & PVRTEX_PIXELTYPE) == OGL_PVRTC2);
		const bool is_pvrtc4 = ((header.dwpfFlags & PVRTEX_PIXELTYPE) == OGL_PVRTC4);
		if ( is_pvrtc2 || is_pvrtc4 ) {
			if ( desc.size.x != desc.size.y ) {
				return ImageDesc();
			}
			if ( (is_pvrtc2 && desc.size.x < 16) || (is_pvrtc4 && desc.size.x < 8) ) {
				return ImageDesc();
			}
			if ( !Math::IsPowerOfTwo(desc.size.x) || !Math::IsPowerOfTwo(desc.size.y) ) {
				return ImageDesc();
			}
		}
	#if BME_RENDERER == BME_RENDERER_OGLES
		// select and check format
		switch ( header.dwpfFlags & PVRTEX_PIXELTYPE ) {
		case OGL_PVRTC2:
			desc.pitch  = desc.size.x * 2 / 8;
			desc.format = header.dwAlphaBitMask ? FORMAT_RGBA_PVRTC2 : FORMAT_RGB_PVRTC2;
			break;
		case OGL_PVRTC4:
			desc.pitch  = desc.size.x * 4 / 8;
			desc.format = header.dwAlphaBitMask ? FORMAT_RGBA_PVRTC4 : FORMAT_RGB_PVRTC4;
			break;
		case OGL_RGB_888:
			desc.pitch  = desc.size.x * 3 * sizeof(u8);
			desc.format = FORMAT_RGB8;
			break;
		case OGL_RGBA_8888:
			desc.pitch  = desc.size.x * 4 * sizeof(u8);
			desc.format = FORMAT_RGBA8;
			break;
		default:
			// unsupported format
			return ImageDesc();
		}
	#else
		// select and check format
		switch ( header.dwpfFlags & PVRTEX_PIXELTYPE ) {
		case OGL_PVRTC2:
		case OGL_PVRTC4:
			desc.pitch  = desc.size.x * sizeof(u32);
			desc.format = header.dwAlphaBitMask ? FORMAT_A8R8G8B8 : FORMAT_X8R8G8B8;
			break;
		case OGL_RGB_888:
			desc.pitch  = desc.size.x * sizeof(u32);
			desc.format = FORMAT_X8R8G8B8;
			break;
		case OGL_RGBA_8888:
			desc.pitch  = desc.size.x * sizeof(u32);
			desc.format = FORMAT_A8R8G8B8;
			break;
		default:
			// unsupported format
			return ImageDesc();
		}
	#endif
		return desc;
	}
	
	ImageDesc PVR_Loader(const ReadFilePtr& file, bool preload)
	{
		PVR_Texture_Header header;
		ImageDesc desc = PVR_PreLoader(file, header);
		if ( desc.isEmpty() || preload ) {
			return desc;
		}
		
		SmartBuffer texture_data;
		void* buffer = texture_data.Allocate(header.dwTextureDataSize);
		if ( !buffer || texture_data.getSize() != file->Read(buffer, texture_data.getSize()) ) {
			return ImageDesc();
		}
		
		if ( !PVR_Decompress(desc, header, texture_data) ) {
			return ImageDesc();
		}
		
		return desc;
	}
} // namespace image_loaders
} // namespace bme
