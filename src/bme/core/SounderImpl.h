/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/SounderImpl.h
 *****************************************************************************/

#pragma once

#include <bme/core/Sounder.h>
#include "SoundImpl.h"

namespace bme
{

class SounderImpl
	: public Sounder
	, public Holder<SounderImpl>
{
	typedef cpp::map<StrId, f32>::type TVolumes;
	TVolumes     _groupVolumes;
	f32          _masterVolume;
	void         _updateVolumes();
	TChannelList _channels;
	
protected:
	
	SounderImpl();
	virtual ~SounderImpl();
	
public:
	
	static bool  Initialize      ();
	static void  Shutdown        ();
	
	void         Update          ();
	void         AddChannel      ( ChannelImpl* channel );
	void         RemoveChannel   ( ChannelImpl* channel );
	
	// Sounder override -------------------------------------------------------
	void         setMasterVolume ( f32 value );
	f32          getMasterVolume () const;
	
	void         setGroupVolume  ( f32 value, const StrId& group );
	f32          getGroupVolume  ( const StrId& group ) const;
	
	size_t       StopChannels    ( const StrId& group );
	size_t       PauseChannels   ( const StrId& group );
	size_t       ResumeChannels  ( const StrId& group );
	
	size_t       StopChannels    ( const SoundPtr& sound );
	size_t       PauseChannels   ( const SoundPtr& sound );
	size_t       ResumeChannels  ( const SoundPtr& sound );
	// ------------------------------------------------------------------------
};

} // namespace bme
