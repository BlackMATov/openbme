/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/Application.cpp
 *****************************************************************************/

#include <bme/core/Application.h>
#include <bme/core/Logger.h>

#include "EngineImpl.h"
#include "SystemImpl.h"

namespace bme
{

Application::Application()
{
}

Application::~Application()
{
}

bool Application::Start(const Desc& desc)
{
	_desc = desc;
	
	if ( !engine_impl::Initialize() ) {
		return false;
	}
	
	if ( !OnInit() ) {
		BME_LOG_ERROR("Application::Start() application initialize error!");
		return false;
	}
	
	SystemImpl* system = Holder<SystemImpl>::getHostage();
	if ( system ) {
		system->ShowWindow();
	}
	
	return true;
}

bool Application::Step()
{
	return engine_impl::Step();
}

void Application::Shutdown()
{
	OnShutdown();
	engine_impl::Shutdown();
}

bool Application::ToggleFullscreen(bool yesno)
{
	Desc last_desc = getDesc();
	_desc.isFullscreen = yesno;
	
	SystemImpl* system = Holder<SystemImpl>::getHostage();
	if ( system && system->ToggleFullscreen(yesno) ) {
		return true;
	}
	
	_desc = last_desc;
	return false;
}

bool Application::ToggleWidescreen(bool yesno)
{
	Desc last_desc = getDesc();
	_desc.isWidescreen = yesno;
	
	SystemImpl* system = Holder<SystemImpl>::getHostage();
	if ( system && system->ToggleWidescreen(yesno) ) {
		return true;
	}
	
	_desc = last_desc;
	return false;
}

const Application::Desc& Application::getDesc() const
{
	return _desc;
}

f32 Application::getTime() const
{
	return getTicks() * 0.001f;
}

f32 Application::getDeltaTime() const
{
	return engine_impl::eDeltaTime;
}

size_t Application::getTicks() const
{
	SystemImpl* system = Holder<SystemImpl>::getHostage();
	return system ? system->getTicks() : 0;
}

size_t Application::getFPS() const
{
	return engine_impl::eFPS;
}

} // namespace bme
