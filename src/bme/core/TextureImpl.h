/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/TextureImpl.h
 *****************************************************************************/

#pragma once

#include <bme/core/Texture.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// TextureList
// 
// ----------------------------------------------------------------------------

class TextureImpl;

typedef cpp::intrusive::list_base_hook<
	cpp::intrusive::link_mode<cpp::intrusive::auto_unlink>
> TextureListHook;

typedef cpp::intrusive::list<
	TextureImpl,
	cpp::intrusive::constant_time_size<false>
> TTextureList;

// ----------------------------------------------------------------------------
// 
// TextureImpl
// 
// ----------------------------------------------------------------------------

class TextureImpl
	: public Texture
	, public TextureListHook
{
	struct Desc{
		pnt2u    size;
		pnt2u    origSize;
		E_FORMAT format;
		bool     isRT;
		bool     isZB;
		
		Desc();
		bool CheckForRender();
		bool IsRTFormat() const;
	} _desc;
	
protected:
	
	// for impl override ------------------------------------------------------
	virtual bool   CreateImpl      ()                           = 0;
	virtual bool   CreateImpl      ( const ImageDesc&  desc )   = 0;
	virtual bool   CreateImpl      ( const TexturePtr& source ) = 0;
	// ------------------------------------------------------------------------
	
	TextureImpl();
	
public:
	
	static TextureImpl* Create();
	virtual ~TextureImpl();
	
	bool           Create          ( const ImageDesc& desc );
	bool           Create          ( const TexturePtr& source );
	bool           Create          ( const pnt2u& size, E_FORMAT format,
	                                 bool render_target, bool z_buffer );
	// for impl override ------------------------------------------------------
	virtual void   Release         () = 0;
	virtual bool   Recovery        () = 0;
	// Texture override -------------------------------------------------------
	const pnt2u&   getSize         () const;
	const pnt2u&   getOriginalSize () const;
	E_FORMAT       getFormat       () const;
	size_t         getMemoryUsage  () const;
	bool           isRenderTarget  () const;
	bool           isZBuffer       () const;
	// ------------------------------------------------------------------------
};

} // namespace bme
