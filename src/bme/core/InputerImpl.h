/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/InputerImpl.h
 *****************************************************************************/

#pragma once

#include <bme/core/Inputer.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// Common
// 
// ----------------------------------------------------------------------------

enum E_KEY_STATE
{
	KEY_STATE_EMPTY,
	KEY_STATE_DOWN,
	KEY_STATE_DOWNED,
	KEY_STATE_UP
};

// ----------------------------------------------------------------------------
// 
// MouseImpl
// 
// ----------------------------------------------------------------------------

class MouseImpl : public Mouse
{
	vec2f       _pos;
	E_KEY_STATE _keyStates[MOUSE_UNKNOWN];
	
public:
	
	MouseImpl();
	MouseImpl(const MouseImpl& other);
	MouseImpl& operator=(const MouseImpl& other);
	virtual ~MouseImpl();
	
	bool        DoKey          ( E_MOUSE key, bool down );
	bool        DoMove         ( const vec2f& pos );
	void        Reset          ();
	void        Update         ();
	
	// Mouse override ---------------------------------------------------------
	bool        IsKeyDown      ( E_MOUSE key ) const;
	bool        IsKeyUp        ( E_MOUSE key ) const;
	bool        IsKeyDowned    ( E_MOUSE key ) const;
	vec2f       getPosition    ()              const;
	// ------------------------------------------------------------------------
};

// ----------------------------------------------------------------------------
// 
// KeyboardImpl
// 
// ----------------------------------------------------------------------------

class KeyboardImpl : public Keyboard
{
	E_KEY_STATE _keyStates[KEY_UNKNOWN];
	
public:
	
	KeyboardImpl();
	KeyboardImpl(const KeyboardImpl& other);
	KeyboardImpl& operator=(const KeyboardImpl& other);
	virtual ~KeyboardImpl();
	
	bool        DoKey          ( E_KEY key, bool down );
	void        Reset          ();
	void        Update         ();
	
	// Keyboard override ------------------------------------------------------
	bool        IsKeyDown      ( E_KEY key ) const;
	bool        IsKeyUp        ( E_KEY key ) const;
	bool        IsKeyDowned    ( E_KEY key ) const;
	// ------------------------------------------------------------------------
};

// ----------------------------------------------------------------------------
// 
// JoystickImpl
// 
// ----------------------------------------------------------------------------

class JoystickImpl : public Joystick
{
	ptrdiff_t   _index;
	Desc        _desc;
	E_KEY_STATE _buttonStates[JOYSTICK_BUTTON_UNKNOWN];
	
public:
	
	JoystickImpl();
	JoystickImpl(const JoystickImpl& other);
	JoystickImpl& operator=(const JoystickImpl& other);
	virtual ~JoystickImpl();
	
	void        Init           ( size_t index );
	bool        isInited       () const;
	
	bool        DoButton       ( E_JOYSTICK_BUTTON button, bool down );
	void        Reset          ();
	void        Update         ();
	
	// Joystick override ------------------------------------------------------
	const Desc& getDesc        ()                                    const;
	f32         getAxisState   ( size_t nAxis )                      const;
	vec2f       getBallState   ( size_t nBall )                      const;
	
	bool        IsButtonDown   ( E_JOYSTICK_BUTTON button )          const;
	bool        IsButtonUp     ( E_JOYSTICK_BUTTON button )          const;
	bool        IsButtonDowned ( E_JOYSTICK_BUTTON button )          const;
	bool        IsHatDowned    ( size_t nHat, E_JOYSTICK_HAT state ) const;
	// ------------------------------------------------------------------------
};

// ----------------------------------------------------------------------------
// 
// InputerImpl
// 
// ----------------------------------------------------------------------------

class InputerImpl
	: public Inputer
	, public Holder<InputerImpl>
{
	typedef cpp::vector   <MouseImpl>::type TMouses;
	typedef cpp::vector<KeyboardImpl>::type TKeyboards;
	typedef cpp::vector<JoystickImpl>::type TJoysticks;
	
	Desc               _desc;
	mutable TMouses    _mouses;
	mutable TKeyboards _keyboards;
	mutable TJoysticks _joysticks;
	
public:
	
	InputerImpl();
	virtual ~InputerImpl();
	
	static bool Initialize     ();
	static void Shutdown       ();
	
	void        Reset          ();
	void        Update         ();
	
	// Inputer override -------------------------------------------------------
	const Desc& getDesc        ()               const;
	Mouse*      getMouse       ( size_t index ) const;
	Keyboard*   getKeyboard    ( size_t index ) const;
	Joystick*   getJoystick    ( size_t index ) const;
	// ------------------------------------------------------------------------
};

} // namespace bme
