/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/EngineImpl.h
 *****************************************************************************/

#pragma once

#include <bme/core/base.h>

namespace bme
{
namespace engine_impl
{
	extern f32        eDeltaTime;
	extern size_t     eFPS;
	
	bool Initialize   ();
	void Shutdown     ();
	bool Step         ();
	void UpdateTimers ();
} // namespace engine_impl
} // namespace bme
