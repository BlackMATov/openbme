/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/ImageSTBI.h
 *****************************************************************************/

#include <bme/core/Image.h>

namespace bme
{
namespace image_loaders
{
	ImageDesc STBI_Loader(const ReadFilePtr& file, bool preload);
} // namespace image_loaders
} // namespace bme
