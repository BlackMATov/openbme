/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/Files.cpp
 *****************************************************************************/

#include <bme/core/Files.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// ReadFile
// 
// ----------------------------------------------------------------------------

size_t ReadFile::ReadS(Str& v)
{
	size_t str_len = 0;
	size_t read_bytes = ReadV(str_len);
	if ( str_len > 0 ) {
		/// \todo need alloca or temp linear allocator here
		cpp::vector<Str::value_type>::type str_data(str_len);
		read_bytes += ReadV(&str_data[0], str_len);
		v = Str(&str_data[0], str_len);
	} else {
		v = Str();
	}
	return read_bytes;
}

size_t ReadFile::ReadS(StrId& v)
{
	size_t str_len = 0;
	size_t read_bytes = ReadV(str_len);
	if ( str_len > 0 ) {
		/// \todo need alloca or temp linear allocator here
		cpp::vector<Str::value_type>::type str_data(str_len);
		read_bytes += ReadV(&str_data[0], str_len);
		v = StrId(&str_data[0], str_len);
	} else {
		v = StrId();
	}
	return read_bytes;
}

// ----------------------------------------------------------------------------
// 
// WriteFile
// 
// ----------------------------------------------------------------------------

size_t WriteFile::WriteS(const Str& v)
{
	size_t str_len = v.length();
	size_t write_bytes = WriteV(str_len);
	if ( str_len > 0 ) {
		write_bytes += WriteV(v.c_str(), str_len);
	}
	return write_bytes;
}

size_t WriteFile::WriteS(const StrId& v)
{
	size_t str_len = v.length();
	size_t write_bytes = WriteV(str_len);
	if ( str_len > 0 ) {
		write_bytes += WriteV(v.c_str(), str_len);
	}
	return write_bytes;
}

} // namespace bme
