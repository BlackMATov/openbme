/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/FilesImpl_POSIX.cpp
 *****************************************************************************/

#include <bme/core/Files.h>

#if BME_FILESYSTEM == BME_FILESYSTEM_POSIX

#include <fcntl.h>
#include <sys/mman.h>

namespace bme
{
namespace files_impl
{

// ----------------------------------------------------------------------------
// 
// Predefs
// 
// ----------------------------------------------------------------------------

class MMapFile_POSIX;
class ReadFile_POSIX;
class WriteFile_POSIX;

// ----------------------------------------------------------------------------
// 
// MMapFile_POSIX
// 
// ----------------------------------------------------------------------------

class MMapFile_POSIX : public MMapFile{
	typedef cpp::intrusive_ptr<const ReadFile_POSIX> TFile;
	TFile       _file;
	void*       _data;
	void        _open     ();
	void        _close    ();
public:
	MMapFile_POSIX(const ReadFile_POSIX* file);
	virtual ~MMapFile_POSIX();
	bool        isOpen    () const;
	// MMapFile override ------------------------------------------------------
	size_t      getSize   () const;
	const void* getData   () const;
	const Path& getPath   () const;
	// ------------------------------------------------------------------------
};

// ----------------------------------------------------------------------------
// 
// ReadFile_POSIX
// 
// ----------------------------------------------------------------------------

class ReadFile_POSIX : public ReadFile{
	Path        _path;
	size_t      _size;
	int         _handle;
	void        _open     ();
	void        _close    ();
public:
	ReadFile_POSIX(const Path& path);
	virtual ~ReadFile_POSIX();
	bool        isOpen    () const;
	int         getHandle () const;
	// ReadFile override ------------------------------------------------------
	size_t      Read      ( void* dest, size_t size );
	bool        Seek      ( ptrdiff_t offset, bool relative );
	size_t      getPos    () const;
	size_t      getSize   () const;
	const Path& getPath   () const;
	MMapFilePtr MakeMMap  () const;
	// ------------------------------------------------------------------------
};

// ----------------------------------------------------------------------------
// 
// WriteFile_POSIX
// 
// ----------------------------------------------------------------------------

class WriteFile_POSIX : public WriteFile{
	Path        _path;
	int         _handle;
	void        _open     ( bool append );
	void        _close    ();
public:
	WriteFile_POSIX(const Path& path, bool append);
	virtual ~WriteFile_POSIX();
	bool        isOpen    () const;
	int         getHandle () const;
	// WriteFile override -----------------------------------------------------
	size_t      Write     ( const void* src, size_t size );
	bool        Seek      ( ptrdiff_t offset, bool relative );
	size_t      getPos    () const;
	const Path& getPath   () const;
	// ------------------------------------------------------------------------
};

// ----------------------------------------------------------------------------
// 
// MMapFile_POSIX impl
// 
// ----------------------------------------------------------------------------

void MMapFile_POSIX::_open()
{
	if ( !_file || !_file->isOpen() || _file->getSize() <= 0 ) {
		_close();
		return;
	}
	
	_data = ::mmap(
		NULL, _file->getSize(), PROT_READ, MAP_SHARED, _file->getHandle(), 0);
	if ( MAP_FAILED == _data ) {
		_close();
		return;
	}
}

void MMapFile_POSIX::_close()
{
	if ( MAP_FAILED != _data ) {
		::munmap(_data, _file->getSize());
		_data = MAP_FAILED;
	}
}

MMapFile_POSIX::MMapFile_POSIX(const ReadFile_POSIX* file)
: _file(file)
, _data(MAP_FAILED)
{
	_open();
}

MMapFile_POSIX::~MMapFile_POSIX()
{
	_close();
}

bool MMapFile_POSIX::isOpen() const
{
	return MAP_FAILED != getData();
}

size_t MMapFile_POSIX::getSize() const
{
	BME_ASSERT(_file);
	return _file->getSize();
}

const void* MMapFile_POSIX::getData() const
{
	return _data;
}

const Path& MMapFile_POSIX::getPath() const
{
	BME_ASSERT(_file);
	return _file->getPath();
}

// ----------------------------------------------------------------------------
// 
// ReadFile_POSIX impl
// 
// ----------------------------------------------------------------------------

void ReadFile_POSIX::_open()
{
	_handle = ::open(getPath().getNativePath().c_str(), O_RDONLY);
	if ( _handle < 0 ) {
		_close();
		return;
	}
	if ( ::lseek(_handle, 0, SEEK_END) < 0 ) {
		_close();
		return;
	}
	ptrdiff_t rlseek = ::lseek(_handle, 0, SEEK_CUR);
	if ( rlseek < 0 ) {
		_close();
		return;
	}
	_size = Math::SmartCast<size_t>(rlseek);
	if ( ::lseek(_handle, 0, SEEK_SET) < 0 ) {
		_close();
		return;
	}
}

void ReadFile_POSIX::_close()
{
	if ( _handle >= 0 ) {
		::close(_handle);
		_handle = -1;
	}
}

ReadFile_POSIX::ReadFile_POSIX(const Path& path)
: _path(path)
, _size(0)
, _handle(-1)
{
	_open();
}

ReadFile_POSIX::~ReadFile_POSIX()
{
	_close();
}

bool ReadFile_POSIX::isOpen() const
{
	return -1 != getHandle();
}
	
int ReadFile_POSIX::getHandle() const
{
	return _handle;
}

size_t ReadFile_POSIX::Read(void* dest, size_t size)
{
	BME_ASSERT(isOpen());
	ptrdiff_t rread = ::read(_handle, dest, size);
	return rread < 0 ? 0 : Math::SmartCast<size_t>(rread);
}

bool ReadFile_POSIX::Seek(ptrdiff_t offset, bool relative)
{
	BME_ASSERT(isOpen());
	ptrdiff_t rlseek = ::lseek(_handle, offset, relative ? SEEK_CUR : SEEK_SET);
	return rlseek >= 0;
}

size_t ReadFile_POSIX::getPos() const
{
	BME_ASSERT(isOpen());
	ptrdiff_t rlseek = ::lseek(_handle, 0, SEEK_CUR);
	BME_ASSERT(rlseek >= 0); /// \todo file get pos bug
	return Math::SmartCast<size_t>(rlseek);
}

size_t ReadFile_POSIX::getSize() const
{
	return _size;
}

const Path&	ReadFile_POSIX::getPath() const
{
	return _path;
}

MMapFilePtr ReadFile_POSIX::MakeMMap() const
{
	cpp::auto_ptr<MMapFile_POSIX> file(new MMapFile_POSIX(this));
	if ( file->isOpen() ) {
		return MMapFilePtr(file.release());
	}
	return MMapFilePtr();
}

// ----------------------------------------------------------------------------
// 
// WriteFile_POSIX impl
// 
// ----------------------------------------------------------------------------

void WriteFile_POSIX::_open(bool append)
{
	_handle = ::open(getPath().getNativePath().c_str(),
		append ? (O_WRONLY|O_CREAT|O_APPEND) : (O_WRONLY|O_CREAT|O_TRUNC),
		S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
}

void WriteFile_POSIX::_close()
{
	if ( _handle >= 0 ) {
		::close(_handle);
		_handle = -1;
	}
}

WriteFile_POSIX::WriteFile_POSIX(const Path& path, bool append)
: _path(path)
, _handle(-1)
{
	_open(append);
}

WriteFile_POSIX::~WriteFile_POSIX()
{
	_close();
}

bool WriteFile_POSIX::isOpen() const
{
	return -1 != getHandle();
}

int WriteFile_POSIX::getHandle() const
{
	return _handle;
}

size_t WriteFile_POSIX::Write(const void* src, size_t size)
{
	BME_ASSERT(isOpen());
	ptrdiff_t rwrite = ::write(_handle, src, size);
	return rwrite < 0 ? 0 : Math::SmartCast<size_t>(rwrite);
}

bool WriteFile_POSIX::Seek(ptrdiff_t offset, bool relative)
{
	BME_ASSERT(isOpen());
	ptrdiff_t rlseek = ::lseek(_handle, offset, relative ? SEEK_CUR : SEEK_SET);
	return rlseek >= 0;
}

size_t WriteFile_POSIX::getPos() const
{
	BME_ASSERT(isOpen());
	ptrdiff_t rlseek = ::lseek(_handle, 0, SEEK_CUR);
	BME_ASSERT(rlseek >= 0); /// \todo bug
	return Math::SmartCast<size_t>(rlseek);
}

const Path& WriteFile_POSIX::getPath() const
{
	return _path;
}

} // namespace files_impl

// ----------------------------------------------------------------------------
// 
// Static interfaces files functions
// 
// ----------------------------------------------------------------------------

ReadFilePtr ReadFile::Create(const Path& path)
{
	using namespace files_impl;
	cpp::auto_ptr<ReadFile_POSIX> file(new ReadFile_POSIX(path));
	if ( file->isOpen() ) {
		return ReadFilePtr(file.release());
	}
	return ReadFilePtr();
}

WriteFilePtr WriteFile::Create(const Path& path, bool append)
{
	using namespace files_impl;
	cpp::auto_ptr<WriteFile_POSIX> file(new WriteFile_POSIX(path, append));
	if ( file->isOpen() ) {
		return WriteFilePtr(file.release());
	}
	return WriteFilePtr();
}

} // namespace bme

#endif // BME_FILESYSTEM == BME_FILESYSTEM_POSIX
