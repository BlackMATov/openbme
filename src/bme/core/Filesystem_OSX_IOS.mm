/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/Filesystem_OSX_IOS.mm
 *****************************************************************************/

#include "FilesystemImpl.h"

#if BME_OS == BME_OS_MACOSX || BME_OS == BME_OS_IOS

#import <Foundation/Foundation.h>
#include <dirent.h>

namespace bme
{
namespace filesystem_impl
{
	bool RemoveImpl(const Path& path, bool /*folder*/)
	{
		NSString* filename = [[NSString alloc] initWithUTF8String:path.getNativePath().c_str()];
		BOOL      success  = [[NSFileManager defaultManager] removeItemAtPath : filename
																		error : nil];
		[filename release];
		return YES == success;
	}
	
	bool IsExistsImpl(const Path& path, bool folder)
	{
		BOOL      directory = NO;
		NSString* filename  = [[NSString alloc] initWithUTF8String:path.getNativePath().c_str()];
		BOOL      success   = [[NSFileManager defaultManager] fileExistsAtPath : filename
																   isDirectory : &directory];
		[filename release];
		return YES == success && directory == folder;
	}

	bool CreateDirectoryImpl(const Path& path)
	{
		NSString* filename = [[NSString alloc] initWithUTF8String:path.getNativePath().c_str()];
		BOOL      success  = [[NSFileManager defaultManager] createDirectoryAtPath : filename
													   withIntermediateDirectories : YES
																		attributes : nil
																			 error : nil];
		[filename release];
		return YES == success;
	}
	
	bool TraceDirectoryImpl(const Path& path, Filesystem::TraceFunc func, void* data)
	{
		DIR* dir = ::opendir(path.getNativePath().c_str());
		if ( dir )
		{
			while ( struct dirent* ent = ::readdir(dir) )
			{
				const char* fn     = ent->d_name;
				const bool  folder = DT_DIR == ent->d_type;
				if ( func && 0 != BME_strcmp(fn, ".") && 0 != BME_strcmp(fn, "..") )
				{
					if ( !func(path / fn, folder, data) )
						return false;
				}
			}
			::closedir(dir);
		}
		return true;
	}
	
	Path GetExeDirImpl()
	{
		NSString* filename = [[NSBundle mainBundle] executablePath];
		if ( filename )
		{
			Path exe_path = [filename UTF8String];
			return exe_path.getParentPath();
		}
		return Path();
	}
	
	Path GetResDirImpl()
	{
		NSString* filename = [[NSBundle mainBundle] resourcePath];
		if ( filename )
		{
			Path ret_value = [filename UTF8String];
			return ret_value / "assets";
		}
		return GetExeDirImpl() / "assets";
	}
	
	Path GetWrkDirImpl()
	{
		NSString* filename = [[NSFileManager defaultManager] currentDirectoryPath];
		if ( filename )
		{
			Path ret_value = [filename UTF8String];
			return ret_value;
		}
		return Path();
	}
	
	Path GetAppDirImpl()
	{
		NSArray *paths = NSSearchPathForDirectoriesInDomains(
			NSApplicationSupportDirectory, NSUserDomainMask, YES);
		if ( paths && [paths count] > 0 )
		{
			Path ret_value = [[paths objectAtIndex:0] UTF8String];
			return ret_value;
		}
		return Path();
	}
} // namespace filesystem_impl
} // namespace bme

#endif // BME_OS_MACOSX || BME_OS_IOS
