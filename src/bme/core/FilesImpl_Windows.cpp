/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/FilesImpl_Windows.cpp
 *****************************************************************************/

#include <bme/core/Files.h>

#if BME_FILESYSTEM == BME_FILESYSTEM_WINDOWS

#include <windows.h>

namespace bme
{
namespace files_impl
{

// ----------------------------------------------------------------------------
// 
// Predefs
// 
// ----------------------------------------------------------------------------

class MMapFile_Windows;
class ReadFile_Windows;
class WriteFile_Windows;

// ----------------------------------------------------------------------------
// 
// MMapFile_Windows
// 
// ----------------------------------------------------------------------------

class MMapFile_Windows : public MMapFile{
	typedef cpp::intrusive_ptr<const ReadFile_Windows> TFile;
	TFile       _file;
	void*       _data;
	HANDLE      _mapping;
	void        _open     ();
	void        _close    ();
public:
	MMapFile_Windows(const ReadFile_Windows* file);
	virtual ~MMapFile_Windows();
	bool        isOpen    () const;
	// MMapFile override ------------------------------------------------------
	size_t      getSize   () const;
	const void* getData   () const;
	const Path& getPath   () const;
	// ------------------------------------------------------------------------
};

// ----------------------------------------------------------------------------
// 
// ReadFile_Windows
// 
// ----------------------------------------------------------------------------

class ReadFile_Windows : public ReadFile{
	Path        _path;
	size_t      _size;
	HANDLE      _handle;
	void        _open     ();
	void        _close    ();
public:
	ReadFile_Windows(const Path& path);
	virtual ~ReadFile_Windows();
	bool        isOpen    () const;
	HANDLE      getHandle () const;
	// ReadFile override ------------------------------------------------------
	size_t      Read      ( void* dest, size_t size );
	bool        Seek      ( ptrdiff_t offset, bool relative );
	size_t      getPos    () const;
	size_t      getSize   () const;
	const Path&	getPath   () const;
	MMapFilePtr MakeMMap  () const;
	// ------------------------------------------------------------------------
};

// ----------------------------------------------------------------------------
// 
// WriteFile_Windows
// 
// ----------------------------------------------------------------------------

class WriteFile_Windows : public WriteFile{
	Path        _path;
	HANDLE      _handle;
	void        _open     ( bool append );
	void        _close    ();
public:
	WriteFile_Windows(const Path& path, bool append);
	virtual ~WriteFile_Windows();
	bool        isOpen    () const;
	HANDLE      getHandle () const;
	// WriteFile override -----------------------------------------------------
	size_t      Write     ( const void* src, size_t size );
	bool        Seek      ( ptrdiff_t offset, bool relative );
	size_t      getPos    () const;
	const Path& getPath   () const;
	// ------------------------------------------------------------------------
};

// ----------------------------------------------------------------------------
// 
// MMapFile_Windows impl
// 
// ----------------------------------------------------------------------------

void MMapFile_Windows::_open()
{
	if ( !_file || !_file->isOpen() || _file->getSize() <= 0 ) {
		_close();
		return;
	}
	
	_mapping = ::CreateFileMappingW(
		_file->getHandle(),
		NULL,
		PAGE_READONLY,
		0,
		0,
		NULL);
	if ( !_mapping ) {
		_close();
		return;
	}
	
	_data = ::MapViewOfFile(
		_mapping,
		FILE_MAP_READ,
		0,
		0,
		0);
	if ( !_data ) {
		_close();
		return;
	}
}

void MMapFile_Windows::_close()
{
	if ( _data ) {
		::UnmapViewOfFile(_data);
		_data = NULL;
	}
	
	if ( _mapping ) {
		::CloseHandle(_mapping);
		_mapping = NULL;
	}
}

MMapFile_Windows::MMapFile_Windows(const ReadFile_Windows* file)
: _file(file)
, _data(NULL)
, _mapping(NULL)
{
	_open();
}

MMapFile_Windows::~MMapFile_Windows()
{
	_close();
}

bool MMapFile_Windows::isOpen() const
{
	return NULL != getData();
}

size_t MMapFile_Windows::getSize() const
{
	BME_ASSERT(_file);
	return _file->getSize();
}

const void* MMapFile_Windows::getData() const
{
	return _data;
}

const Path& MMapFile_Windows::getPath() const
{
	BME_ASSERT(_file);
	return _file->getPath();
}

// ----------------------------------------------------------------------------
// 
// ReadFile_Windows impl
// 
// ----------------------------------------------------------------------------

void ReadFile_Windows::_open()
{
	// try 1
	{
		_handle = ::CreateFileW(
			getPath().getNativeWPath().c_str(),
			GENERIC_READ,
			FILE_SHARE_READ,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_READONLY,
			NULL);
	}
	
	// try 2 (open locked file)
	if ( INVALID_HANDLE_VALUE == _handle ) {
		_handle = ::CreateFileW(
			getPath().getNativeWPath().c_str(),
			GENERIC_READ,
			FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_READONLY,
			NULL);
	}
	
	if ( INVALID_HANDLE_VALUE == _handle ) {
		_close();
		return;
	}
	
	_size = ::GetFileSize(_handle, NULL);
	if ( INVALID_FILE_SIZE == _size ) {
		_close();
		return;
	}
}

void ReadFile_Windows::_close()
{
	if ( INVALID_HANDLE_VALUE != _handle ) {
		::CloseHandle(_handle);
		_handle = INVALID_HANDLE_VALUE;
	}
}

ReadFile_Windows::ReadFile_Windows(const Path& path)
: _path(path)
, _size(0)
, _handle(INVALID_HANDLE_VALUE)
{
	_open();
}

ReadFile_Windows::~ReadFile_Windows()
{
	_close();
}

bool ReadFile_Windows::isOpen() const
{
	return INVALID_HANDLE_VALUE != getHandle();
}

HANDLE ReadFile_Windows::getHandle() const
{
	return _handle;
}

size_t ReadFile_Windows::Read(void* dest, size_t size)
{
	BME_ASSERT(isOpen());
	DWORD read_bytes = 0;
	BOOL rread = ::ReadFile(
		_handle, dest, Math::SmartCast<DWORD>(size), &read_bytes, NULL);
	return FALSE == rread ? 0 : Math::SmartCast<size_t>(read_bytes);
}

bool ReadFile_Windows::Seek(ptrdiff_t offset, bool relative)
{
	BME_ASSERT(isOpen());
	DWORD r = ::SetFilePointer(
		_handle, Math::SmartCast<LONG>(offset), NULL,
		relative ? FILE_CURRENT : FILE_BEGIN);
	return INVALID_SET_FILE_POINTER != r;
}

size_t ReadFile_Windows::getPos() const
{
	BME_ASSERT(isOpen());
	DWORD r = ::SetFilePointer(_handle, 0, NULL, FILE_CURRENT);
	BME_ASSERT(INVALID_SET_FILE_POINTER != r); /// \todo file get pos bug
	return Math::SmartCast<size_t>(r);
}

size_t ReadFile_Windows::getSize() const
{
	return _size;
}

const Path&	ReadFile_Windows::getPath() const
{
	return _path;
}

MMapFilePtr ReadFile_Windows::MakeMMap() const
{
	cpp::auto_ptr<MMapFile_Windows> file(new MMapFile_Windows(this));
	if ( file->isOpen() ) {
		return MMapFilePtr(file.release());
	}
	return MMapFilePtr();
}

// ----------------------------------------------------------------------------
// 
// WriteFile_Windows impl
// 
// ----------------------------------------------------------------------------

void WriteFile_Windows::_open(bool append)
{
	_handle = ::CreateFileW(
		getPath().getNativeWPath().c_str(),
		GENERIC_WRITE,
		FILE_SHARE_WRITE,
		NULL,
		append ? OPEN_ALWAYS : CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL);
	
	if ( INVALID_HANDLE_VALUE == _handle ) {
		_close();
		return;
	}
	
	if ( append ) {
		if ( INVALID_SET_FILE_POINTER ==
			::SetFilePointer(_handle, 0, NULL, FILE_END) )
		{
			_close();
			return;
		}
	}
}

void WriteFile_Windows::_close()
{
	if ( INVALID_HANDLE_VALUE != _handle ) {
		::CloseHandle(_handle);
		_handle = INVALID_HANDLE_VALUE;
	}
}

WriteFile_Windows::WriteFile_Windows(const Path& path, bool append)
: _path(path)
, _handle(INVALID_HANDLE_VALUE)
{
	_open(append);
}

WriteFile_Windows::~WriteFile_Windows()
{
	_close();
}

bool WriteFile_Windows::isOpen() const
{
	return INVALID_HANDLE_VALUE != getHandle();
}

HANDLE WriteFile_Windows::getHandle() const
{
	return _handle;
}

size_t WriteFile_Windows::Write(const void* src, size_t size)
{
	BME_ASSERT(isOpen());
	DWORD write_bytes = 0;
	BOOL r = ::WriteFile(
		_handle, src, Math::SmartCast<DWORD>(size), &write_bytes, NULL);
	return FALSE == r ? 0 : Math::SmartCast<size_t>(write_bytes);
}

bool WriteFile_Windows::Seek(ptrdiff_t offset, bool relative)
{
	BME_ASSERT(isOpen());
	DWORD r = ::SetFilePointer(
		_handle, Math::SmartCast<LONG>(offset), NULL,
		relative ? FILE_CURRENT : FILE_BEGIN);
	return INVALID_SET_FILE_POINTER != r;
}

size_t WriteFile_Windows::getPos() const
{
	BME_ASSERT(isOpen());
	DWORD r = ::SetFilePointer(_handle, 0, NULL, FILE_CURRENT);
	BME_ASSERT(INVALID_SET_FILE_POINTER != r); /// \todo file get pos bug
	return Math::SmartCast<size_t>(r);
}

const Path& WriteFile_Windows::getPath() const
{
	return _path;
}

} // namespace files_impl

// ----------------------------------------------------------------------------
// 
// Static interfaces files functions
// 
// ----------------------------------------------------------------------------

ReadFilePtr ReadFile::Create(const Path& path)
{
	using namespace files_impl;
	cpp::auto_ptr<ReadFile_Windows> file(new ReadFile_Windows(path));
	if ( file->isOpen() ) {
		return ReadFilePtr(file.release());
	}
	return ReadFilePtr();
}

WriteFilePtr WriteFile::Create(const Path& path, bool append)
{
	using namespace files_impl;
	cpp::auto_ptr<WriteFile_Windows> file(new WriteFile_Windows(path, append));
	if ( file->isOpen() ) {
		return WriteFilePtr(file.release());
	}
	return WriteFilePtr();
}

} // namespace bme

#endif // BME_FILESYSTEM == BME_FILESYSTEM_WINDOWS
