/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/Archive.cpp
 *****************************************************************************/

#include <bme/core/Archive.h>

#include <bme/core/Files.h>
#include <bme/core/Filesystem.h>

// library from depends
#include <zlib/unzip.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// Archive
// 
// ----------------------------------------------------------------------------

Archive::Archive(const Path& path, const StrId& pass)
: _path(path), _pass(pass)
{
}

Archive::~Archive()
{
}

const Path& Archive::getPath() const
{
	return _path;
}

const StrId& Archive::getPass() const
{
	return _pass;
}

// ----------------------------------------------------------------------------
// 
// zip_archive_detail
// 
// ----------------------------------------------------------------------------

namespace zip_archive_detail
{
	struct FileWrapper
	{
		ReadFilePtr file;
	};
	
	static voidpf bme_zfopen64_file_func(
		voidpf /*opaque*/, const void* filename, int mode)
	{
		if ( ZLIB_FILEFUNC_MODE_READ == (mode & ZLIB_FILEFUNC_MODE_READWRITEFILTER) ) {
			if ( theFilesystem() ) {
				ReadFilePtr archive_file = theFilesystem()->OpenFile(
					static_cast<const char*>(filename),
					Filesystem::ORDER_DISK_ONLY);
				if ( archive_file ) {
					FileWrapper* wrapper = new FileWrapper;
					wrapper->file = archive_file;
					return wrapper;
				}
			}
		} else {
			BME_ASSERT_MSG(false, "unsupported zlib wrapper function");
		}
		return NULL;
	}
	
	static uLong bme_zfread_file_func(
		voidpf /*opaque*/, voidpf stream, void* buf, uLong size)
	{
		FileWrapper* wrapper = static_cast<FileWrapper*>(stream);
		size_t read_bytes = wrapper->file->Read(buf, Math::SmartCast<size_t>(size));
		return Math::SmartCast<uLong>(read_bytes);
	}
	
	static uLong bme_zfwrite_file_func(
		voidpf /*opaque*/, voidpf /*stream*/, const void* /*buf*/, uLong /*size*/)
	{
		BME_ASSERT_MSG(false, "unsupported zlib wrapper function");
		return 0;
	}
	
	static ZPOS64_T bme_zftell64_file_func(
		voidpf /*opaque*/, voidpf stream)
	{
		FileWrapper* wrapper = static_cast<FileWrapper*>(stream);
		return Math::SmartCast<ZPOS64_T>(wrapper->file->getPos());
	}
	
	static long bme_zfseek64_file_func(
		voidpf /*opaque*/, voidpf stream, ZPOS64_T offset, int origin)
	{
		FileWrapper* wrapper = static_cast<FileWrapper*>(stream);
		switch ( origin ) {
		case ZLIB_FILEFUNC_SEEK_CUR:
			if ( wrapper->file->Seek(Math::SmartCast<ptrdiff_t>(offset), true) ) {
				return 0;
			}
			break;
		case ZLIB_FILEFUNC_SEEK_END:
			if ( wrapper->file->Seek(Math::SmartCast<ptrdiff_t>(wrapper->file->getSize()), false) ) {
				return 0;
			}
			break;
		case ZLIB_FILEFUNC_SEEK_SET:
			if ( wrapper->file->Seek(Math::SmartCast<ptrdiff_t>(offset), false) ) {
				return 0;
			}
			break;
		default:
			BME_ASSERT_MSG(false, "unsupported zlib wrapper function");
			break;
		}
		return -1;
	}
	
	static int bme_zfclose_file_func(
		voidpf /*opaque*/, voidpf stream)
	{
		delete static_cast<FileWrapper*>(stream);
		return 0;
	}
	
	static int bme_zferror_file_func(
		voidpf /*opaque*/, voidpf /*stream*/)
	{
		return 0;
	}
	
	static unzFile bme_zipOpen(const Path& path)
	{
		zlib_filefunc64_def ffunc;
		ffunc.zopen64_file = bme_zfopen64_file_func;
		ffunc.zread_file   = bme_zfread_file_func;
		ffunc.zwrite_file  = bme_zfwrite_file_func;
		ffunc.ztell64_file = bme_zftell64_file_func;
		ffunc.zseek64_file = bme_zfseek64_file_func;
		ffunc.zclose_file  = bme_zfclose_file_func;
		ffunc.zerror_file  = bme_zferror_file_func;
		ffunc.opaque       = NULL;
		return unzOpen2_64(path.getNativePath().c_str(), &ffunc);
	}
	
	static void* archive_buffer_alloc(size_t size)
	{
		return BME_malloc(size);
	}
	
	static void archive_buffer_free(void* buffer)
	{
		if ( buffer ) {
			BME_free(buffer);
		}
	}
} // namespace zip_archive_detail

// ----------------------------------------------------------------------------
// 
// ZipArchive
// 
// ----------------------------------------------------------------------------

void ZipArchive::_fillPaths()
{
	using namespace zip_archive_detail;
	unzFile zip = bme_zipOpen(getPath());
	if ( zip ) {
		Path archive_parent_path = getPath().getParentPath();
		char zipName[FILENAME_MAX] = {0};
		int done = unzGoToFirstFile(zip);
		while ( UNZ_OK == done ) {
			unz_file_info64 file_info;
			if ( UNZ_OK == unzGetCurrentFileInfo64(
				zip, &file_info, zipName, sizeof(zipName), NULL, 0, NULL, 0) )
			{
				TPaths::value_type stored_path = cpp::make_pair(
					archive_parent_path / zipName,
					Math::SmartCast<size_t>(unzGetOffset64(zip)));
				char last_char = file_info.size_filename > 0 ?
					zipName[file_info.size_filename-1] : 0;
				if ( last_char != '\\' && last_char != '/' ) {
					_files.insert(stored_path);
				} else {
					_folders.insert(stored_path);
				}
			}
			done = unzGoToNextFile(zip);
		}
		unzClose(zip);
	}
}

ZipArchive::ZipArchive(const Path& path, const StrId& pass)
: Archive(path, pass)
{
	_fillPaths();
}

ZipArchive::~ZipArchive()
{
}

bool ZipArchive::isExists() const
{
	using namespace zip_archive_detail;
	unzFile zip = bme_zipOpen(getPath());
	if ( zip ) {
		unzClose(zip);
		return true;
	}
	return false;
}

ReadFilePtr ZipArchive::LoadFile(const Path& path) const
{
	using namespace zip_archive_detail;
	unzFile zip = bme_zipOpen(getPath());
	if ( !zip ) {
		return ReadFilePtr();
	}
	
	TPaths::const_iterator file_iter = _files.find(path);
	if ( _files.end() == file_iter ) {
		unzClose(zip);
		return ReadFilePtr();
	}
	
	size_t file_offset = (*file_iter).second;
	if ( UNZ_OK != unzSetOffset64(
		zip, Math::SmartCast<ZPOS64_T>(file_offset)) )
	{
		unzClose(zip);
		return ReadFilePtr();
	}
	
	unz_file_info64 file_info;
	if ( UNZ_OK != unzGetCurrentFileInfo64(
		zip, &file_info, NULL, 0, NULL, 0, NULL, 0) )
	{
		unzClose(zip);
		return ReadFilePtr();
	}
	
	if ( UNZ_OK != unzOpenCurrentFilePassword(
		zip, getPass().empty() ? NULL : getPass().c_str()) )
	{
		unzClose(zip);
		return ReadFilePtr();
	}
	
	void* ptr = archive_buffer_alloc(
		Math::SmartCast<size_t>(file_info.uncompressed_size));
	
	if ( !ptr ) {
		unzCloseCurrentFile(zip);
		unzClose(zip);
		return ReadFilePtr();
	}
	
	if ( unzReadCurrentFile(
		zip, ptr, Math::SmartCast<unsigned>(file_info.uncompressed_size)) < 0 )
	{
		unzCloseCurrentFile(zip);
		unzClose(zip);
		archive_buffer_free(ptr);
		return ReadFilePtr();
	} else {
		unzCloseCurrentFile(zip);
		unzClose(zip);
		return ReadFile::Create(
			ptr, Math::SmartCast<size_t>(file_info.uncompressed_size),
			path, &archive_buffer_free);
	}
}

bool ZipArchive::IsFileExists(const Path& path) const
{
	return _files.end() != _files.find(path);
}

bool ZipArchive::IsFolderExists(const Path& path) const
{
	return _folders.end() != _folders.find(path);
}

} // namespace bme
