/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/RendererImpl.cpp
 *****************************************************************************/

#include "RendererImpl.h"

#include <bme/core/Application.h>

namespace bme
{

RendererImpl::RendererImpl()
{
	_device.reset(RendererDevice::Create());
}

RendererImpl::~RendererImpl()
{
}

bool RendererImpl::Initialize()
{
	RendererImpl* self = new RendererImpl();
	return self->getDevice()->Initialize();
}

void RendererImpl::Shutdown()
{
	RendererImpl* renderer = Holder<RendererImpl>::getHostage();
	if ( renderer ) {
		renderer->ReleaseTextures(false);
		renderer->getDevice()->Shutdown();
		delete renderer;
	}
}

void RendererImpl::EndFrame()
{
	getDevice()->EndFrame();
}

void RendererImpl::Resize()
{
	getDevice()->Resize();
	setCamera(Camera());
}

const RendererDevicePtr& RendererImpl::getDevice() const
{
	return _device;
}

void RendererImpl::AddTexture(TextureImpl* texture)
{
	BME_ASSERT(texture && !texture->is_linked());
	_textures.push_back(*texture);
}

void RendererImpl::RemoveTexture(TextureImpl* texture)
{
	BME_ASSERT(texture && texture->is_linked());
	texture->unlink();
}

void RendererImpl::ReleaseTextures(bool rt_only)
{
	for ( TTextureList::iterator
		iter = _textures.begin(), end = _textures.end();
		iter != end; ++iter )
	{
		TextureImpl& tex = *iter;
		if ( !rt_only || tex.isRenderTarget() ) {
			tex.Release();
		}
	}
}

bool RendererImpl::RecoveryTextures()
{
	bool success = true;
	for ( TTextureList::iterator
		iter = _textures.begin(), end = _textures.end();
		iter != end; ++iter )
	{
		TextureImpl& tex = *iter;
		if ( !tex.Recovery() ) {
			success = false;
		}
	}
	return success;
}

// ----------------------------------------------------------------------------
// 
// Renderer override
// 
// ----------------------------------------------------------------------------

Renderer::Caps& RendererImpl::getCaps()
{
	return _caps;
}

const Renderer::Caps& RendererImpl::getCaps() const
{
	return _caps;
}

bool RendererImpl::BeginScene()
{
	bool success = getDevice()->BeginScene();
	if ( success ) {
		// fill scene to black
		getDevice()->setViewport(rectu(pnt2u(vec2f(theSystem()->getWindowResolution()))));
		getDevice()->ClearScene(Color::black, true, true);
		// reset camera and info
		setCamera(Camera());
		_sceneInfo.reset();
	}
	return success;
}

void RendererImpl::ClearScene(
	const Color& color, bool back_buffer, bool z_buffer)
{
	RenderBatches();
	getDevice()->ClearScene(color, back_buffer, z_buffer);
}

const Renderer::SceneInfo& RendererImpl::EndScene()
{
	RenderBatches();
	getDevice()->EndScene();
	return _sceneInfo;
}

void RendererImpl::setCamera(const Camera& camera)
{
	RenderBatches();
	
	_camera = camera;
	getDevice()->setTransform    ( RendererDevice::TRANSFORM_VIEW,       camera.getCorrectView() );
	getDevice()->setTransform    ( RendererDevice::TRANSFORM_WORLD,      camera.getWorld      () );
	getDevice()->setTransform    ( RendererDevice::TRANSFORM_PROJECTION, camera.getProjection () );
	getDevice()->setRenderTarget ( camera.getRenderTarget   () );
	getDevice()->setViewport     ( camera.getCorrectViewport() );
}

const Camera& RendererImpl::getCamera() const
{
	return _camera;
}

void RendererImpl::RenderObject(const Quad& prim)
{
	AddToBatch(prim);
}

void RendererImpl::RenderObject(const Triangle& prim)
{
	AddToBatch(prim);
}

void RendererImpl::RenderObject(const Line& prim)
{
	AddToBatch(prim);
}

void RendererImpl::RenderObject(
	const Material& material, const RenderStates& states,
	const Vertex* vertices, size_t vertices_count,
	const u16* indices, size_t indices_count, E_PRIMITIVE prim_type)
{
	RenderBatches();
	
	getDevice()->setStates(material, states, true);
	getDevice()->DrawPrimitives(
		vertices, vertices_count, indices, indices_count, prim_type);
	
	size_t prim_count = 0;
	if ( indices && indices_count > 0 ) {
		prim_count = indices_count / prim_type;
	} else {
		prim_count = vertices_count / prim_type;
	}
	
	switch ( prim_type ) {
	case Renderer::PRIMITIVE_LINES:
		_sceneInfo.nLines += prim_count;
		_sceneInfo.nDIP += 1;
		break;
	case Renderer::PRIMITIVE_TRIANGLES:
		_sceneInfo.nTriangles += prim_count;
		_sceneInfo.nDIP += 1;
		break;
	default:
		BME_ASSERT(false);
		break;
	}
}

void RendererImpl::RenderBatches()
{
	const BatchContainer::TBatches& batches = _batches.getBatches();
	for ( BatchContainer::TBatches::const_iterator
		iter = batches.begin(), end = batches.end();
		iter != end; ++iter )
	{
		const Batch&  batch     = *iter;
		const Vertex* vertex    = _batches.getFirstVertex();
		const u16*    quadIndex = _batches.getFirstQuadIndex();
		
		getDevice()->setStates(batch.material, batch.renderStates, true);
		
		switch( batch.vertPerPrim ) {
		case Quad::numVertices:
			{
				getDevice()->DrawPrimitives(
					vertex + batch.startVertex,
					batch.primCount * 4,
					quadIndex,
					batch.primCount * 6,
					PRIMITIVE_TRIANGLES);
				_sceneInfo.nTriangles += batch.primCount * 2;
			}
			break;
		case Triangle::numVertices:
			{
				getDevice()->DrawPrimitives(
					vertex + batch.startVertex,
					batch.primCount * 3,
					NULL, 0, PRIMITIVE_TRIANGLES);
				_sceneInfo.nTriangles += batch.primCount;
			}
			break;
		case Line::numVertices:
			{
				getDevice()->DrawPrimitives(
					vertex + batch.startVertex,
					batch.primCount * 2,
					NULL, 0, PRIMITIVE_LINES);
				_sceneInfo.nLines += batch.primCount;
			}
			break;
		default:
			BME_ASSERT(false);
		}
	}
	_sceneInfo.nDIP += batches.size();
	_batches.Clear();
}

} // namespace bme
