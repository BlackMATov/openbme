/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/LoggerImpl.cpp
 *****************************************************************************/

#include "LoggerImpl.h"
#include "SystemImpl.h"

#include <bme/core/Files.h>
#include <bme/core/Filesystem.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// logger_detail
// 
// ----------------------------------------------------------------------------

namespace logger_detail
{
	static const char* LogLevelToString(Logger::E_LEVEL level)
	{
		switch ( level ) {
		case Logger::LEVEL_DEBUG:
			return "d";
		case Logger::LEVEL_INFO:
			return "i";
		case Logger::LEVEL_COMPLETE:
			return "c";
		case Logger::LEVEL_WARNING:
			return "w";
		case Logger::LEVEL_ERROR:
			return "e";
		default:
			BME_ASSERT(false);
			return "";
		}
	}
}

// ----------------------------------------------------------------------------
// 
// LoggerImpl
// 
// ----------------------------------------------------------------------------

LoggerImpl::LoggerImpl()
: _minLevel(LEVEL_INFO)
, _outputType(OUTPUT_ALL)
, _writeHook(NULL)
{
#if BME_MODE == BME_MODE_DEBUG
	if ( theSystem() ) {
		theSystem()->ShowConsole();
	}
	
	if ( theFilesystem() ) {
		Path path = Path::app_dir;
		if ( theFilesystem()->CreateDirectories(path) ) {
			setLogfile(path / "log.txt");
		}
	}
#endif
}

LoggerImpl::~LoggerImpl()
{
}

bool LoggerImpl::Initialize()
{
	BME_ASSERT(!theLogger());
	new LoggerImpl();
	return true;
}

void LoggerImpl::Shutdown()
{
	delete theLogger();
}

bool LoggerImpl::setLogfile(const Path& path)
{
	MutexLockGuard<RecursiveMutex> guard(_mutex);
	WriteFilePtr file = WriteFile::Create(path, false);
	if ( file ) {
		::time_t tmp_time; ::time(&tmp_time);
		const char* tmp_time_str = ::asctime(::localtime(&tmp_time));
		const char* tmp_bme_ver = BME_VERSION_STR;
		
		char log_buffer[1024] = {0};
		Strings::Format(log_buffer, 1024,
			"---------- BMEngine log file -----------\n"
			"Engine version: %1%\n"
			"Local time and date: %2%"
			"----------------------------------------\n\n",
			tmp_bme_ver, tmp_time_str);
		file->Write(log_buffer, BME_strlen(log_buffer));
		
		_path = path;
		return true;
	} else {
		_path.Clear();
		return false;
	}
}

const Path& LoggerImpl::getLogfile() const
{
	MutexLockGuard<RecursiveMutex> guard(_mutex);
	return _path;
}

void LoggerImpl::setMinLevel(E_LEVEL level)
{
	MutexLockGuard<RecursiveMutex> guard(_mutex);
	_minLevel = level;
}

Logger::E_LEVEL LoggerImpl::getMinLevel() const
{
	MutexLockGuard<RecursiveMutex> guard(_mutex);
	return _minLevel;
}

void LoggerImpl::setOutputType(E_OUTPUT value)
{
	MutexLockGuard<RecursiveMutex> guard(_mutex);
	_outputType = value;
}

Logger::E_OUTPUT LoggerImpl::getOutputType() const
{
	MutexLockGuard<RecursiveMutex> guard(_mutex);
	return _outputType;
}

void LoggerImpl::setWriteHook(TWriteHook hook)
{
	MutexLockGuard<RecursiveMutex> guard(_mutex);
	_writeHook = hook;
}

Logger::TWriteHook LoggerImpl::getWriteHook() const
{
	MutexLockGuard<RecursiveMutex> guard(_mutex);
	return _writeHook;
}

void LoggerImpl::Write(const char* text, E_LEVEL level)
{
	if ( !text || !text[0] ) {
		return;
	}
	
	TWriteHook hook = getWriteHook();
	if ( hook && !hook(text, level) ) {
		return;
	}
	
	if ( level < getMinLevel() || OUTPUT_OFF == getOutputType() ) {
		return;
	}
	
	char log_text[BME_MAX_LOGGER_MESSAGE] = {0};
	Strings::Format(log_text, BME_MAX_LOGGER_MESSAGE,
		"- [%1%] %2%\n",
		logger_detail::LogLevelToString(level), text);
	
	MutexLockGuard<RecursiveMutex> guard(_mutex);
	
	// Native output
	if ( Logger::OUTPUT_NATIVE == getOutputType() || Logger::OUTPUT_ALL == getOutputType() ) {
		SystemImpl* system = Holder<SystemImpl>::getHostage();
		if ( system ) {
			system->NativeLog(log_text, level);
		}
	}
	
	// FILE
	if ( Logger::OUTPUT_FILE == getOutputType() || Logger::OUTPUT_ALL == getOutputType() ) {
		WriteFilePtr file = WriteFile::Create(getLogfile(), true);
		if ( file ) {
			file->Write(log_text, BME_strlen(log_text));
		}
	}
}

} // namespace bme
