/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/ReceiverImpl.cpp
 *****************************************************************************/

#include "ReceiverImpl.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// Receiver::Event
// 
// ----------------------------------------------------------------------------

Receiver::Event::Event()
: type(TYPE_UNKNOWN)
{
}

void Receiver::Event::Push(bool immediately)
{
	EventEmitter* emitter = Holder<EventEmitter>::getHostage();
	if ( emitter ) {
		emitter->PushEvent(*this, immediately);
	}
}

// ----------------------------------------------------------------------------
// 
// Receiver
// 
// ----------------------------------------------------------------------------

Receiver::Receiver()
{
	EventEmitter* emitter = Holder<EventEmitter>::getHostage();
	if ( emitter ) {
		emitter->RegisterReceiver(this);
	}
}

Receiver::~Receiver()
{
	EventEmitter* emitter = Holder<EventEmitter>::getHostage();
	if ( emitter ) {
		emitter->UnregisterReceiver(this);
	}
}

// ----------------------------------------------------------------------------
// 
// EventEmitter
// 
// ----------------------------------------------------------------------------

bool EventEmitter::_emitEvent(const Receiver::Event& event)
{
	bool success = true;
	for ( TReceiverList::iterator
		iter = _receivers.begin(), end = _receivers.end();
		iter != end; ++iter )
	{
		Receiver& receiver = *iter;
		if ( !receiver.OnEvent(event) ) {
			success = false;
		}
	}
	return success;
}

EventEmitter::EventEmitter()
{
	_events.reserve(BME_SYSTEM_EVENTS_RESERVE);
}

EventEmitter::~EventEmitter()
{
}

bool EventEmitter::Initialize()
{
	new EventEmitter();
	return true;
}

void EventEmitter::Shutdown()
{
	delete Holder<EventEmitter>::getHostage();
}

void EventEmitter::PushEvent(const Receiver::Event& event, bool immediately)
{
	if ( immediately ) {
		_emitEvent(event);
	} else {
		_events.push_back(event);
	}
}

bool EventEmitter::UpdateEvents()
{
	bool success = true;
	for ( TEvents::const_iterator
		iter = _events.begin(), end = _events.end();
		iter != end; ++iter )
	{
		if ( !_emitEvent(*iter) ) {
			success = false;
		}
	}
	_events.clear();
	return success;
}

void EventEmitter::RegisterReceiver(Receiver* receiver)
{
	BME_ASSERT(receiver && !receiver->is_linked());
	_receivers.push_back(*receiver);
}

void EventEmitter::UnregisterReceiver(Receiver* receiver)
{
	BME_ASSERT(receiver && receiver->is_linked());
	receiver->unlink();
}

} // namespace bme
