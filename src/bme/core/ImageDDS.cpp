/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/ImageDDS.cpp
 *****************************************************************************/

#include "ImageDDS.h"
#include <bme/core/Files.h>

#if BME_RENDERER != BME_RENDERER_OGLES

#define BME_MAKEFOURCC(ch0, ch1, ch2, ch3) \
	((u32)(u8)(ch0)        | \
	((u32)(u8)(ch1) << 8)  | \
	((u32)(u8)(ch2) << 16) | \
	((u32)(u8)(ch3) << 24) )

namespace bme
{
namespace image_loaders
{
	namespace dds_loader
	{
		enum DDSConsts {
			DDS_MAGIC = BME_MAKEFOURCC('D', 'D', 'S', ' '),
			DDS_DXT1  = BME_MAKEFOURCC('D', 'X', 'T', '1'),
			DDS_DXT3  = BME_MAKEFOURCC('D', 'X', 'T', '3'),
			DDS_DXT5  = BME_MAKEFOURCC('D', 'X', 'T', '5')
		};
		
		struct DDSPixelFormat {
			u32            dwSize;
			u32            dwFlags;
			u32            dwFourCC;
			u32            dwRGBBitCount;
			u32            dwRBitMask;
			u32            dwGBitMask;
			u32            dwBBitMask;
			u32            dwABitMask;
		};
		
		struct DDSHeader {
			u32            dwMagic;
			u32            dwSize;
			u32            dwFlags;
			u32            dwHeight;
			u32            dwWidth;
			u32            dwPitchOrLinearSize;
			u32            dwDepth;
			u32            dwMipMapCount;
			u32            dwReserved1[11];
			DDSPixelFormat ddspf;
			u32            dwCaps;
			u32            dwCaps2;
			u32            dwCaps3;
			u32            dwCaps4;
			u32            dwReserved2;
		};
	} // namespace dds_loader
	
	ImageDesc DDS_PreLoader(const ReadFilePtr& file, dds_loader::DDSHeader& header)
	{
		using namespace dds_loader;
		ImageDesc desc;
		
		// read header
		if ( sizeof(DDSHeader) != file->Read(&header, sizeof(DDSHeader)) ) {
			return ImageDesc();
		}
		
		// check dds magic
		if ( header.dwMagic != DDS_MAGIC ) {
			return ImageDesc();
		}
		
		// get and check size
		desc.size = pnt2u(header.dwWidth, header.dwHeight);
		if ( !Math::IsPowerOfTwo(desc.size.x) || !Math::IsPowerOfTwo(desc.size.y) ) {
			return ImageDesc();
		}
		
		// select and check format
		switch ( header.ddspf.dwFourCC ) {
		case DDS_DXT1:
			desc.format = FORMAT_DXT1;
			break;
		case DDS_DXT3:
			desc.format = FORMAT_DXT3;
			break;
		case DDS_DXT5:
			desc.format = FORMAT_DXT5;
			break;
		default:
			// unsupported format
			return ImageDesc();
		}
		
		// pitch
		size_t block_size  = (header.ddspf.dwFourCC == DDS_DXT1 ? 8 : 16);
		size_t block_width = (header.dwWidth + 3) / 4;
		desc.pitch         = (block_width * block_size);
		
		return desc;
	}
	
	ImageDesc DDS_Loader(const ReadFilePtr& file, bool preload)
	{
		using namespace dds_loader;
		
		DDSHeader header;
		ImageDesc desc = DDS_PreLoader(file, header);
		if ( desc.isEmpty() || preload ) {
			return desc;
		}
		
		const size_t block_size   = (header.ddspf.dwFourCC == DDS_DXT1 ? 8 : 16);
		const size_t block_width  = (header.dwWidth  + 3) / 4;
		const size_t block_height = (header.dwHeight + 3) / 4;
		const size_t data_size    = (block_width * block_height * block_size);
		
		SmartBuffer pixels;
		void* buffer = pixels.Allocate((data_size));
		if ( !buffer || data_size != file->Read(buffer, data_size) ) {
			return ImageDesc();
		}
		
		desc.pixels = pixels;
		return desc;
	}
} // namespace image_loaders
} // namespace bme
#endif // !BME_RENDERER_OGLES
