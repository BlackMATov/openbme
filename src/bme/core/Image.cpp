/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/Image.cpp
 *****************************************************************************/

#include <bme/core/Image.h>

#include <bme/core/Files.h>
#include <bme/core/Filesystem.h>

#include "ImageDDS.h"
#include "ImagePVR.h"
#include "ImageSTBI.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// image_loader_detail
// 
// ----------------------------------------------------------------------------

namespace image_loader_detail
{
	static ImageLoader::TLoaders s_loaders;
	static RecursiveMutex s_loaders_mutex;
} // namespace image_loader_detail

// ----------------------------------------------------------------------------
// 
// ImageDesc
// 
// ----------------------------------------------------------------------------

ImageDesc::ImageDesc()
: pitch(0), format(FORMAT_UNKNOWN)
{
}

bool ImageDesc::isEmpty() const
{
	return
		!isLoaded() && !isPreLoaded();
}

bool ImageDesc::isLoaded() const
{
	return
		isPreLoaded() && pixels;
}

bool ImageDesc::isPreLoaded() const
{
	return
		size.x != 0 && size.y != 0 &&
		pitch  != 0 && FORMAT_UNKNOWN != format;
}

size_t ImageDesc::BPPFromFormat(E_FORMAT format)
{
	switch ( format ) {
#if BME_RENDERER == BME_RENDERER_OGLES
	case FORMAT_RGB8:
		return 24;
	case FORMAT_RGBA8:
		return 32;
	case FORMAT_RGB_PVRTC2:
		return 2;
	case FORMAT_RGB_PVRTC4:
		return 4;
	case FORMAT_RGBA_PVRTC2:
		return 2;
	case FORMAT_RGBA_PVRTC4:
		return 4;
#else
	case FORMAT_X8R8G8B8:
	case FORMAT_A8R8G8B8:
		return 32;
	case FORMAT_DXT1:
		return 4;
	case FORMAT_DXT3:
		return 8;
	case FORMAT_DXT5:
		return 8;
#endif
	default:
		BME_ASSERT(false);
		return 0;
	}
}

// ----------------------------------------------------------------------------
// 
// ImageLoader
// 
// ----------------------------------------------------------------------------

ImageDesc ImageLoader::_load(const ReadFilePtr& file, bool preload) const
{
	for ( TLoaders::const_reverse_iterator
		iter = _loaders.rbegin(), end = _loaders.rend();
		iter != end; ++iter )
	{
		if ( file->Seek(0, false) ) {
			ImageDesc desc = (*iter)(file, preload);
			if ( !desc.isEmpty() ) {
				return desc;
			}
		}
	}
	return ImageDesc();
}

ImageLoader::ImageLoader()
{
	using namespace image_loader_detail;
	MutexLockGuard<RecursiveMutex> guard(s_loaders_mutex);
	if ( s_loaders.empty() ) {
		s_loaders.reserve(BME_IMAGE_LOADERS_RESERVE);
		s_loaders.push_back(&image_loaders::STBI_Loader);
		s_loaders.push_back(&image_loaders::PVR_Loader);
	#if BME_RENDERER != BME_RENDERER_OGLES
		s_loaders.push_back(&image_loaders::DDS_Loader);
	#endif
	}
	_loaders = s_loaders;
}

ImageDesc ImageLoader::Load(const Path& path, bool preload)
{
	ReadFilePtr file =
		theFilesystem() ? theFilesystem()->OpenFile(path) : ReadFilePtr();
	if ( file ) {
		ImageDesc desc = _load(file, preload);
		if ( !desc.isEmpty() ) {
			return desc;
		}
	}
	return ImageDesc();
}

ImageDesc ImageLoader::Load(const SmartBuffer& mem, bool preload)
{
	return Load(mem.getBuffer(), mem.getSize(), preload);
}

ImageDesc ImageLoader::Load(const void* data, size_t size, bool preload)
{
	if ( data && size ) {
		ReadFilePtr file = ReadFile::Create(data, size, Path(), NULL);
		if ( file ) {
			ImageDesc desc = _load(file, preload);
			if ( !desc.isEmpty() ) {
				return desc;
			}
		}
	}
	return ImageDesc();
}

void ImageLoader::Register(TLoader loader)
{
	if ( loader ) {
		using namespace image_loader_detail;
		MutexLockGuard<RecursiveMutex> guard(s_loaders_mutex);
		s_loaders.push_back(loader);
	}
}

bool ImageLoader::Unregister(TLoader loader)
{
	using namespace image_loader_detail;
	MutexLockGuard<RecursiveMutex> guard(s_loaders_mutex);
	TLoaders::iterator iter = cpp::find(s_loaders.begin(), s_loaders.end(), loader);
	if ( iter != s_loaders.end() ) {
		s_loaders.erase(iter);
		return true;
	}
	return false;
}

} // namespace bme
