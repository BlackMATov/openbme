/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/Filesystem_Windows.cpp
 *****************************************************************************/

#include "FilesystemImpl.h"

#if BME_OS == BME_OS_WINDOWS

#include <shlobj.h>
#include <windows.h>

namespace bme
{
namespace filesystem_impl
{
	bool RemoveImpl(const Path& path, bool /*folder*/)
	{
		wchar_t from_buffer[FILENAME_MAX] = {0};
		BME_wcscpy(from_buffer, path.getNativeWPath().c_str());
		::SHFILEOPSTRUCTW fileop;
		::ZeroMemory(&fileop, sizeof(fileop));
		fileop.wFunc  = FO_DELETE;
		fileop.pFrom  = from_buffer;
		fileop.fFlags = FOF_SILENT | FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_NOCONFIRMMKDIR;
		return 0 == ::SHFileOperationW(&fileop);
	}
	
	bool IsExistsImpl(const Path& path, bool folder)
	{
		DWORD attributes = ::GetFileAttributesW(path.getNativeWPath().c_str());
		if ( INVALID_FILE_ATTRIBUTES != attributes ) {
			if ( folder && (attributes & FILE_ATTRIBUTE_DIRECTORY) ) {
				return true;
			}
			if ( !folder && !(attributes & FILE_ATTRIBUTE_DIRECTORY) ) {
				return true;
			}
		}
		return false;
	}
	
	bool CreateDirectoryImpl(const Path& path)
	{
		return
			!path.isEmpty() &&
			0 != ::CreateDirectoryW(path.getNativeWPath().c_str(), NULL);
	}
	
	bool TraceDirectoryImpl(const Path& path, Filesystem::TraceFunc func, void* data)
	{
		Path search_path = path / "*";
		WIN32_FIND_DATAW entw;
		HANDLE dir = ::FindFirstFileW(search_path.getNativeWPath().c_str(), &entw);
		if ( INVALID_HANDLE_VALUE != dir ) {
			do {
				const WCHAR* fn     = entw.cFileName;
				const DWORD  attr   = entw.dwFileAttributes;
				const bool   folder = !!(attr & FILE_ATTRIBUTE_DIRECTORY);
				if ( func && 0 != BME_wcscmp(fn, L".") && 0 != BME_wcscmp(fn, L"..") ) {
					if ( !func(path / fn, folder, data) ) {
						return false;
					}
				}
			} while(::FindNextFileW(dir, &entw) != 0);
			::FindClose(dir);
		}
		return true;
	}
	
	Path GetExeDirImpl()
	{
		wchar_t buffer[FILENAME_MAX] = {0};
		DWORD n = ::GetModuleFileNameW(NULL, buffer, FILENAME_MAX);
		if ( n > 0 && n < sizeof(buffer) ) {
			return Path(buffer).getParentPath();
		}
		return Path();
	}
	
	Path GetResDirImpl()
	{
		return GetExeDirImpl() / "assets";
	}
	
	Path GetWrkDirImpl()
	{
		wchar_t buffer[FILENAME_MAX] = {0};
		DWORD n = ::GetCurrentDirectoryW(sizeof(buffer), buffer);
		if ( n > 0 && n < sizeof(buffer) ) {
			return Path(buffer);
		}
		return Path();
	}
	
	Path GetAppDirImpl()
	{
		wchar_t buffer[FILENAME_MAX] = {0};
		if ( SUCCEEDED(::SHGetFolderPathW(0, CSIDL_APPDATA | CSIDL_FLAG_CREATE, 0, 0, buffer)) ) {
			return Path(buffer);
		}
		return Path();
	}
} // namespace filesystem_impl
} // namespace bme

#endif // BME_OS_WINDOWS
