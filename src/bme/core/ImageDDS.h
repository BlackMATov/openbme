/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/ImageDDS.h
 *****************************************************************************/

#include <bme/core/Image.h>

#if BME_RENDERER != BME_RENDERER_OGLES

namespace bme
{
namespace image_loaders
{
	ImageDesc DDS_Loader(const ReadFilePtr& file, bool preload);
} // namespace image_loaders
} // namespace bme

#endif // !BME_RENDERER_OGLES
