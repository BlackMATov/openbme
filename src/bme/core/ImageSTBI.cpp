/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/ImageSTBI.cpp
 *****************************************************************************/

#include "ImageSTBI.h"
#include <bme/core/Files.h>

// library from depends
#include <stb_image/stb_image.h>

namespace bme
{
namespace image_loaders
{
	namespace detail
	{
		static bool bme_format_from_stb(int channels,
			E_FORMAT& outFormat, size_t& outPixelSize)
		{
			switch ( channels ) {
			#if BME_RENDERER == BME_RENDERER_OGLES
				case 1:
				case 3:
				{
					outFormat    = FORMAT_RGB8;
					outPixelSize = 3 * sizeof(u8);
					return true;
				}
				case 2:
				case 4:
				{
					outFormat    = FORMAT_RGBA8;
					outPixelSize = 4 * sizeof(u8);
					return true;
				}
			#else
				case 1:
				case 3:
				{
					outFormat    = FORMAT_X8R8G8B8;
					outPixelSize = sizeof(u32);
					return true;
				}
				case 2:
				case 4:
				{
					outFormat    = FORMAT_A8R8G8B8;
					outPixelSize = sizeof(u32);
					return true;
				}
			#endif
				default:
					BME_ASSERT(false);
					return false;
			}
		}
		
		static ImageDesc STBI_PreLoader(const void* data, size_t size)
		{
			int width = 0, height = 0, channels = 0;
			if ( 1 == stbi_info_from_memory(
				static_cast<const stbi_uc*>(data), size,
				&width, &height, &channels))
			{
				ImageDesc desc;
				desc.size = pnt2u(width, height);
				if ( bme_format_from_stb(channels, desc.format, desc.pitch) ) {
					desc.pitch *= desc.size.x;
					return desc;
				}
			}
			return ImageDesc();
		}
	} // namespace detail
#if BME_RENDERER == BME_RENDERER_OGLES
	ImageDesc STBI_Loader(const ReadFilePtr& file, bool preload)
	{
		MMapFilePtr mmap_file = file ? file->MakeMMap() : MMapFilePtr();
		if ( !mmap_file ) {
			return ImageDesc();
		}
		
		using namespace detail;
		if ( preload ) {
			return STBI_PreLoader(mmap_file->getData(), mmap_file->getSize());
		}
		
		int width = 0, height = 0, channels = 0;
		stbi_uc* result = stbi_load_from_memory(
			static_cast<const stbi_uc*>(mmap_file->getData()),
			mmap_file->getSize(),
			&width, &height, &channels, 0);
		
		if ( !result || (channels < 1 || channels > 4) ) {
			stbi_image_free(result);
			return ImageDesc();
		}
		
		ImageDesc desc;
		bool success = false;
		
		switch ( channels ) {
		case 1:
		case 3:
			{
				desc.size = pnt2u(width, height);
				if ( bme_format_from_stb(channels, desc.format, desc.pitch) ) {
					desc.pitch *= desc.size.x;
					desc.pixels = SmartBuffer(desc.pitch * desc.size.y);
					const u8* src = reinterpret_cast<const u8*>(result);
					u8* dest = reinterpret_cast<u8*>(desc.pixels.getBuffer());
					if ( src && dest ) {
						success = true;
						if ( channels == 1 ) {
							Color::G8_To_RGB8(dest, desc.size, src, desc.size);
						} else {
							Color::RGB8_To_RGB8(dest, desc.size, src, desc.size);
						}
					}
				}
			}
			break;
		case 2:
		case 4:
			{
				desc.size = pnt2u(width, height);
				if ( bme_format_from_stb(channels, desc.format, desc.pitch) ) {
					desc.pitch *= desc.size.x;
					desc.pixels = SmartBuffer(desc.pitch * desc.size.y);
					const u8* src = reinterpret_cast<const u8*>(result);
					u8* dest = reinterpret_cast<u8*>(desc.pixels.getBuffer());
					if ( src && dest ) {
						success = true;
						if ( channels == 2 ) {
							Color::GA8_To_RGBA8(dest, desc.size, src, desc.size);
						} else {
							Color::RGBA8_To_RGBA8(dest, desc.size, src, desc.size);
						}
					}
				}
			}
			break;
		default:
			BME_ASSERT(false);
		}
		stbi_image_free(result);
		return success ? desc : ImageDesc();
	}
#else
	ImageDesc STBI_Loader(const ReadFilePtr& file, bool preload)
	{
		MMapFilePtr mmap_file = file ? file->MakeMMap() : MMapFilePtr();
		if ( !mmap_file ) {
			return ImageDesc();
		}
		
		using namespace detail;
		if ( preload ) {
			return STBI_PreLoader(mmap_file->getData(), mmap_file->getSize());
		}
		
		int width = 0, height = 0, channels = 0;
		stbi_uc* result = stbi_load_from_memory(
			static_cast<const stbi_uc*>(mmap_file->getData()),
			mmap_file->getSize(),
			&width, &height, &channels, 0);
		
		if ( !result || (channels < 1 || channels > 4) ) {
			stbi_image_free(result);
			return ImageDesc();
		}
		
		ImageDesc desc;
		bool success = false;
		
		switch ( channels ) {
		case 1:
		case 3:
			{
				desc.size = pnt2u(width, height);
				if ( bme_format_from_stb(channels, desc.format, desc.pitch) ) {
					desc.pitch *= desc.size.x;
					desc.pixels = SmartBuffer(desc.pitch * desc.size.y);
					const u8* src = reinterpret_cast<const u8*>(result);
					u32* dest = reinterpret_cast<u32*>(desc.pixels.getBuffer());
					if ( src && dest ) {
						success = true;
						if ( channels == 1 ) {
							Color::G8_To_X8R8G8B8(dest, desc.size, src, desc.size);
						} else {
							Color::RGB8_To_X8R8G8B8(dest, desc.size, src, desc.size);
						}
					}
				}
			}
			break;
		case 2:
		case 4:
			{
				desc.size = pnt2u(width, height);
				if ( bme_format_from_stb(channels, desc.format, desc.pitch) ) {
					desc.pitch *= desc.size.x;
					desc.pixels = SmartBuffer(desc.pitch * desc.size.y);
					const u8* src = reinterpret_cast<const u8*>(result);
					u32* dest = reinterpret_cast<u32*>(desc.pixels.getBuffer());
					if ( src && dest ) {
						success = true;
						if ( channels == 2 ) {
							Color::GA8_To_A8R8G8B8(dest, desc.size, src, desc.size);
						} else {
							Color::RGBA8_To_A8R8G8B8(dest, desc.size, src, desc.size);
						}
					}
				}
			}
			break;
		default:
			BME_ASSERT(false);
		}
		stbi_image_free(result);
		return success ? desc : ImageDesc();
	}
#endif
} // namespace image_loaders
} // namespace bme
