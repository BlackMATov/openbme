/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/FilesystemImpl.cpp
 *****************************************************************************/

#include "FilesystemImpl.h"

#include <bme/core/Files.h>
#include <bme/core/Logger.h>
#include <bme/core/Archive.h>
#include <bme/core/Application.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// filesystem_impl_detail
// 
// ----------------------------------------------------------------------------

namespace filesystem_impl_detail
{
	static void* file_buffer_alloc(size_t size)
	{
		return BME_malloc(size);
	}
	
	static void file_buffer_free(void* buffer)
	{
		if ( buffer ) {
			BME_free(buffer);
		}
	}
	
	struct TraceData
	{
		void* userData;
		Filesystem::TraceFunc userFunc;
	};
	
	static bool recursive_trace(const Path& path, bool folder, void* data)
	{
		TraceData* trace_data = static_cast<TraceData*>(data);
		void* userData = trace_data->userData;
		Filesystem::TraceFunc userFunc = trace_data->userFunc;
		if ( userFunc && !userFunc(path, folder, userData) ) {
			return false;
		}
		else if ( folder ) {
			return filesystem_impl::TraceDirectoryImpl(
				path, &recursive_trace, trace_data);
		}
		return true;
	}
} // namespace filesystem_impl_detail

// ----------------------------------------------------------------------------
// 
// Filesystem::FileTask
// 
// ----------------------------------------------------------------------------

void Filesystem::FileTask::_run()
{
	_file = theFilesystem()
		? theFilesystem()->ReadFile(getPath(), getMode())
		: ReadFilePtr();
}

void Filesystem::FileTask::_reset()
{
	_file.reset();
}

Filesystem::FileTask::FileTask(const Path& path, E_ORDER_MODE mode)
: _path(path)
, _mode(mode)
{
}

Filesystem::FileTask::~FileTask()
{
}

const Path& Filesystem::FileTask::getPath() const
{
	return _path;
}

Filesystem::E_ORDER_MODE Filesystem::FileTask::getMode() const
{
	return _mode;
}

const ReadFilePtr& Filesystem::FileTask::getFile()
{
	Wait();
	return _file;
}

// ----------------------------------------------------------------------------
// 
// Private FilesystemImpl
// 
// ----------------------------------------------------------------------------

ReadFilePtr FilesystemImpl::_openFileFromDisk(const Path& path) const
{
	return ReadFile::Create(path);
}

ReadFilePtr FilesystemImpl::_openFileFromArchive(const Path& path) const
{
	MutexLockGuard<RecursiveMutex> guard(_archMutex);
	for ( TArchives::const_reverse_iterator
		iter = _archives.rbegin(), end = _archives.rend();
		iter != end; ++iter )
	{
		const ArchivePtr& archive = (*iter);
		ReadFilePtr file = archive ? archive->LoadFile(path) : ReadFilePtr();
		if ( file ) {
			return file;
		}
	}
	return ReadFilePtr();
}

ReadFilePtr FilesystemImpl::_openFileCommon(
	const Path& path, E_ORDER_MODE mode) const
{
	// disk order
	if ( mode == ORDER_DISK_FIRST || mode == ORDER_DISK_ONLY ) {
		ReadFilePtr file = _openFileFromDisk(path);
		if ( file || mode == ORDER_DISK_ONLY ) {
			return file;
		}
	}
	// find in archives
	{
		ReadFilePtr file = _openFileFromArchive(path);
		if ( file || mode == ORDER_DISK_FIRST || mode == ORDER_ARCHIVE_ONLY ) {
			return file;
		}
	}
	// find in filesystem
	return _openFileFromDisk(path);
}

bool FilesystemImpl::_isExistsOnDisk(const Path& path, bool folder) const
{
	MutexLockGuard<RecursiveMutex> guard(_implMutex);
	return filesystem_impl::IsExistsImpl(path, folder);
}

bool FilesystemImpl::_isExistsOnArchive(const Path& path, bool folder) const
{
	MutexLockGuard<RecursiveMutex> guard(_archMutex);
	for ( TArchives::const_reverse_iterator
		iter = _archives.rbegin(), end = _archives.rend();
		iter != end; ++iter )
	{
		const ArchivePtr& archive = (*iter);
		if ( archive ) {
			if ( folder ) {
				return archive->IsFolderExists(path);
			} else {
				return archive->IsFileExists(path);
			}
		}
	}
	return false;
}

bool FilesystemImpl::_isExistsCommon(
	const Path& path, E_ORDER_MODE mode, bool folder) const
{
	// disk order
	if ( mode == ORDER_DISK_FIRST || mode == ORDER_DISK_ONLY ) {
		bool success = _isExistsOnDisk(path, folder);
		if ( success || mode == ORDER_DISK_ONLY ) {
			return success;
		}
	}
	// find in archives
	{
		bool success = _isExistsOnArchive(path, folder);
		if ( success || mode == ORDER_DISK_FIRST || mode == ORDER_ARCHIVE_ONLY ) {
			return success;
		}
	}
	// find in filesystem
	return _isExistsOnDisk(path, folder);
}

bool FilesystemImpl::_attachArchive(const ArchivePtr& archive)
{
	if ( archive && archive->isExists() ) {
		MutexLockGuard<RecursiveMutex> guard(_archMutex);
		if ( !_isArchiveAttached(archive->getPath()) ) {
			_archives.push_back(archive);
			return true;
		}
	}
	return false;
}

bool FilesystemImpl::_isArchiveAttached(const Path& path) const
{
	MutexLockGuard<RecursiveMutex> guard(_archMutex);
	for ( TArchives::const_iterator
		iter = _archives.begin(), end = _archives.end();
		iter != end; ++iter )
	{
		const ArchivePtr& archive = (*iter);
		if ( archive && archive->getPath() == path ) {
			return true;
		}
	}
	return false;
}

bool FilesystemImpl::_detachArchive(const Path& path)
{
	MutexLockGuard<RecursiveMutex> guard(_archMutex);
	for ( TArchives::iterator
		iter = _archives.begin(), end = _archives.end();
		iter != end; ++iter )
	{
		const ArchivePtr& archive = (*iter);
		if ( archive && archive->getPath() == path ) {
			_archives.erase(iter);
			return true;
		}
	}
	return false;
}

void FilesystemImpl::_detachAllArchives()
{
	MutexLockGuard<RecursiveMutex> guard(_archMutex);
	_archives.clear();
}

// ----------------------------------------------------------------------------
// 
// Public FilesystemImpl
// 
// ----------------------------------------------------------------------------

FilesystemImpl::FilesystemImpl()
{
	_jobber.reset(new Jobber(1));
	_archives.reserve(BME_ARCHIVES_RESERVE);
	Path::BindAlias("exe_dir", GetExecuteDir ());
	Path::BindAlias("res_dir", GetResourceDir());
	Path::BindAlias("wrk_dir", GetWorkingDir ());
	Path::BindAlias("app_dir", GetAppDataDir ());
}

FilesystemImpl::~FilesystemImpl()
{
}

bool FilesystemImpl::Initialize()
{
	BME_ASSERT(!theFilesystem());
	new FilesystemImpl();
	return true;
}

void FilesystemImpl::Shutdown()
{
	delete theFilesystem();
}

bool FilesystemImpl::RemoveFile(const Path& path) const
{
	if ( !IsFileExists(path, ORDER_DISK_ONLY) ) {
		return true;
	}
	MutexLockGuard<RecursiveMutex> guard(_implMutex);
	return filesystem_impl::RemoveImpl(path, false);
}

bool FilesystemImpl::RemoveFolder(const Path& path) const
{
	if ( !IsFolderExists(path, ORDER_DISK_ONLY) ) {
		return true;
	}
	MutexLockGuard<RecursiveMutex> guard(_implMutex);
	return filesystem_impl::RemoveImpl(path, true);
}

bool FilesystemImpl::CreateDirectories(const Path& path) const
{
	if ( path.isEmpty() || IsFolderExists(path, ORDER_DISK_ONLY) ) {
		return true;
	}
	if ( !CreateDirectories(path.getParentPath()) ) {
		return false;
	}
	MutexLockGuard<RecursiveMutex> guard(_implMutex);
	return filesystem_impl::CreateDirectoryImpl(path);
}

void FilesystemImpl::TraceDirectory(
	const Path& path, TraceFunc func, void* data) const
{
	MutexLockGuard<RecursiveMutex> guard(_implMutex);
	filesystem_impl::TraceDirectoryImpl(path, func, data);
}

void FilesystemImpl::TraceDirectories(
	const Path& path, TraceFunc func, void* data) const
{
	filesystem_impl_detail::TraceData trace_data;
	trace_data.userData = data;
	trace_data.userFunc = func;
	MutexLockGuard<RecursiveMutex> guard(_implMutex);
	filesystem_impl::TraceDirectoryImpl(
		path, &filesystem_impl_detail::recursive_trace, &trace_data);
}

ReadFilePtr FilesystemImpl::OpenFile(const Path& path, E_ORDER_MODE mode) const
{
	return _openFileCommon(path, mode);
}

ReadFilePtr FilesystemImpl::ReadFile(const Path& path, E_ORDER_MODE mode) const
{
	ReadFilePtr file = OpenFile(path, mode);
	if ( file ) {
		using namespace filesystem_impl_detail;
		const size_t size = file->getSize();
		void* mem = file_buffer_alloc(size);
		if ( mem ) {
			if ( size == file->Read(mem, size) ) {
				return ReadFile::Create(
					mem, size, path, &file_buffer_free);
			} else {
				file_buffer_free(mem);
			}
		}
	}
	return ReadFilePtr();
}

Filesystem::FileTaskPtr FilesystemImpl::ReadFileAsync(
	const Path& path, E_ORDER_MODE mode) const
{
	if ( IsFileExists(path, mode) ) {
		FileTaskPtr task = FileTaskPtr(new FileTask(path, mode));
		_jobber->AddTask(task);
		return task;
	}
	return FileTaskPtr();
}

bool FilesystemImpl::IsFileExists(const Path& path, E_ORDER_MODE mode) const
{
	return _isExistsCommon(path, mode, false);
}

bool FilesystemImpl::IsFolderExists(const Path& path, E_ORDER_MODE mode) const
{
	return _isExistsCommon(path, mode, true);
}

Path FilesystemImpl::GetExecuteDir() const
{
	MutexLockGuard<RecursiveMutex> guard(_implMutex);
	return filesystem_impl::GetExeDirImpl();
}

Path FilesystemImpl::GetResourceDir() const
{
	MutexLockGuard<RecursiveMutex> guard(_implMutex);
	return filesystem_impl::GetResDirImpl();
}

Path FilesystemImpl::GetWorkingDir() const
{
	MutexLockGuard<RecursiveMutex> guard(_implMutex);
	return filesystem_impl::GetWrkDirImpl();
}

Path FilesystemImpl::GetAppDataDir() const
{
	// %USER_FOLDER%\\AppData\\Roaming\\CompName\\GameName
	// %WORK_DIR%\\AppData\\CompName\\GameName
	// %WORK_DIR%
	
	Path dir;
	
	{
		MutexLockGuard<RecursiveMutex> guard(_implMutex);
		dir = filesystem_impl::GetAppDirImpl();
	}
	
	if ( dir.isEmpty() || !CreateDirectories(dir) ) {
		dir = GetWorkingDir() / Path("AppData");
	}
	
	Application* app = theApplication();
	if ( app ) {
		const StrId& comp_name = app->getDesc().compName;
		const StrId& game_name = app->getDesc().gameName;
		dir /= Path(comp_name.c_str()) / Path(game_name.c_str());
	}
	
	if ( !IsFolderExists(dir, ORDER_DISK_ONLY) && !CreateDirectories(dir) ) {
		dir = GetWorkingDir();
	}
	
	return dir;
}

bool FilesystemImpl::AttachArchive(const ArchivePtr& archive)
{
	bool success = _attachArchive(archive);
	BME_LOG_COMPLETE_OR_ERROR_FMT(
		success,
		"Attaching archive by path (%1%)",
		archive ? archive->getPath() : Path());
	return success;
}

bool FilesystemImpl::IsArchiveAttached(const Path& path) const
{
	return _isArchiveAttached(path);
}

bool FilesystemImpl::DetachArchive(const Path& path)
{
	bool success = _detachArchive(path);
	BME_LOG_COMPLETE_OR_ERROR_FMT(
		success,
		"Detaching archive by path (%1%)",
		path);
	return success;
}

void FilesystemImpl::DetachAllArchives()
{
	_detachAllArchives();
	BME_LOG_COMPLETE("Detaching all archives");
}

} // namespace bme
