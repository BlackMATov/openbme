/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/EngineImpl.cpp
 *****************************************************************************/

#include "EngineImpl.h"

#include "LoggerImpl.h"
#include "SystemImpl.h"
#include "InputerImpl.h"
#include "SounderImpl.h"
#include "ReceiverImpl.h"
#include "FilesystemImpl.h"

#include <bme/core/Application.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// engine_impl
// 
// ----------------------------------------------------------------------------

namespace engine_impl
{
	// --------------------------------
	// Extern variables
	// --------------------------------
	
	f32    eDeltaTime = 0.f;
	size_t eFPS       = 0;
	
	// --------------------------------
	// Functions
	// --------------------------------
	
	bool Initialize()
	{
		if ( !EventEmitter::Initialize() ) {
			BME_LOG_ERROR("Engine::Initialize() event emitter initialize error!");
			return false;
		}
		
		if ( !FilesystemImpl::Initialize() ) {
			BME_LOG_ERROR("Engine::Initialize() filesystem initialize error!");
			return false;
		}
		
		if ( !LoggerImpl::Initialize() ) {
			BME_LOG_ERROR("Engine::Initialize() logger initialize error!");
			return false;
		}
		
	#if BME_SOUNDER != BME_SOUNDER_NONE
		if ( !SounderImpl::Initialize() ) {
			BME_LOG_ERROR("Engine::Initialize() sounder initialize error!");
			return false;
		}
	#endif
		
		if ( !SystemImpl::Initialize() ) {
			BME_LOG_ERROR("Engine::Initialize() system initialize error!");
			return false;
		}
		
		if ( !InputerImpl::Initialize() ) {
			BME_LOG_ERROR("Engine::Initialize() inputer initialize error!");
			return false;
		}
		
		return true;
	}
	
	void Shutdown()
	{
		InputerImpl::Shutdown();
		SystemImpl::Shutdown();
	#if BME_SOUNDER != BME_SOUNDER_NONE
		SounderImpl::Shutdown();
	#endif
		LoggerImpl::Shutdown();
		FilesystemImpl::Shutdown();
		EventEmitter::Shutdown();
	}
	
	bool Step()
	{
		Application*  app     = theApplication();
		InputerImpl*  inputer = Holder<InputerImpl>::getHostage();
		SystemImpl*   system  = Holder<SystemImpl>::getHostage();
		SounderImpl*  sounder = Holder<SounderImpl>::getHostage();
		EventEmitter* emitter = Holder<EventEmitter>::getHostage();
		
		if ( !(app && inputer && system && sounder && emitter) ) {
			BME_LOG_ERROR("engine_impl::Step() any system failed!");
			return false;
		}
		
		inputer->Update();
		system->Update();
		sounder->Update();
		
		UpdateTimers();
		
		if ( !emitter->UpdateEvents() ) {
			return false;
		}
		
		if ( (!app->getDesc().isNoFocusSuspend || system->isWindowFocused()) ||
			 (app->getDesc().parentHandler) )
		{
			if ( !app->OnUpdate() ) {
				return false;
			}
			app->OnRender();
			system->EndFrame();
		}
		return true;
	}
	
	void UpdateTimers()
	{	
		SystemImpl* system = Holder<SystemImpl>::getHostage();
		if ( system ) {
			static size_t last_time = 0;
			// -= delta time =-
			{
				// min 1ms update time
				size_t dt;
				do { dt = system->getTicks() - last_time; } while (dt < 1);
				
				eDeltaTime = dt * 0.001f;
				last_time = system->getTicks();
				
				// safetyLongDeltaTime ( < 5FPS -> 100FPS )
				if ( eDeltaTime > 1.f / 5.f ) {
					eDeltaTime = 1.f / 100.f;
				}
			}
			// -= FPS =-
			{
				static size_t nFPS = 0;
				static size_t time_update_fps = 0;
				
				nFPS++;
				if ( last_time - time_update_fps >= 1000 ) {
					time_update_fps = last_time;
					eFPS = nFPS;
					nFPS = 0;
				}
			}
		} else {
			eDeltaTime = 0.f;
			eFPS = 0;
		}
	}
} // namespace engine_impl
} // namespace bme
