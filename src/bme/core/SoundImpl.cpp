/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/SoundImpl.cpp
 *****************************************************************************/

#include "SoundImpl.h"
#include "SounderImpl.h"

#include <bme/core/Logger.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// sound_impl_detail
// 
// ----------------------------------------------------------------------------

namespace sound_impl_detail
{
	f32 ConvertVolumeLocalToGlobal(f32 volume, const StrId& group)
	{
		f32 ret_value = volume;
		if ( theSounder() ) {
			ret_value *= theSounder()->getMasterVolume();
			ret_value *= theSounder()->getGroupVolume(group);
		}
		return ret_value;
	}
} // namespace sound_impl_detail

// ----------------------------------------------------------------------------
// 
// Sound
// 
// ----------------------------------------------------------------------------

SoundPtr Sound::Get(const Path& path, bool stream)
{
	SoundPtr ret_value;
	if ( theSounder() && theSounder()->isValid() ) {
		cpp::auto_ptr<SoundImpl> snd(SoundImpl::Create());
		if ( snd->Create(path, stream) ) {
			ret_value = SoundPtr(snd.release());
		}
	}
	BME_LOG_COMPLETE_OR_ERROR_FMT(
		ret_value,
		"Create sound from path (%1%, stream: %2%)",
		path, stream);
	return ret_value;
}

SoundPtr Sound::Create(const SmartBuffer& mem)
{
	return Sound::Create(mem.getBuffer(), mem.getSize());
}

SoundPtr Sound::Create(const void* data, size_t size)
{
	SoundPtr ret_value;
	if ( theSounder() && theSounder()->isValid() ) {
		cpp::auto_ptr<SoundImpl> snd(SoundImpl::Create());
		if ( snd->Create(data, size) ) {
			ret_value = SoundPtr(snd.release());
		}
	}
	BME_LOG_COMPLETE_OR_ERROR(
		ret_value,
		"Create sound from memory");
	return ret_value;
}

// ----------------------------------------------------------------------------
// 
// SoundImpl
// 
// ----------------------------------------------------------------------------

SoundImpl::SoundImpl()
: _length(0.f)
, _streamed(false)
{
}

SoundImpl::~SoundImpl()
{
}

bool SoundImpl::Create(const SmartBuffer& mem)
{
	_streamed = false;
	return CreateImpl(mem.getBuffer(), mem.getSize(), _length);
}

bool SoundImpl::Create(const void* data, size_t size)
{
	_streamed = false;
	return CreateImpl(data, size, _length);
}

bool SoundImpl::Create(const Path& path, bool stream)
{
	_streamed = stream;
	return CreateImpl(path, _length);
}

ChannelPtr SoundImpl::Play(f32 volume, bool loop, const StrId& group)
{
	if ( theSounder() && theSounder()->isValid() ) {
		cpp::auto_ptr<ChannelImpl> channel(ChannelImpl::Create(group, this));
		if ( channel->Create(volume, loop) ) {
			return ChannelPtr(channel.release());
		}
	}
	return ChannelPtr();
}

size_t SoundImpl::Stop()
{
	return theSounder() ?
		theSounder()->StopChannels(this) : 0;
}

size_t SoundImpl::Pause()
{
	return theSounder() ?
		theSounder()->PauseChannels(this) : 0;
}

size_t SoundImpl::Resume()
{
	return theSounder() ?
		theSounder()->ResumeChannels(this) : 0;
}

f32 SoundImpl::getLength() const
{
	return _length;
}

bool SoundImpl::isStreamed() const
{
	return _streamed;
}

// ----------------------------------------------------------------------------
// 
// ChannelImpl
// 
// ----------------------------------------------------------------------------

ChannelImpl::ChannelImpl(const StrId& group, const SoundPtr &sound)
: _panning(0.f)
, _pitch(1.f)
, _volume(1.f)
, _group(group)
, _sound(sound)
{
	SounderImpl* sounder = Holder<SounderImpl>::getHostage();
	if ( sounder ) {
		sounder->AddChannel(this);
	}
}

ChannelImpl::~ChannelImpl()
{
	SounderImpl* sounder = Holder<SounderImpl>::getHostage();
	if ( sounder ) {
		sounder->RemoveChannel(this);
	}
}

bool ChannelImpl::Create(f32 volume, bool loop)
{
	if ( CreateImpl() ) {
		setProperty(PROPERTY_VOLUME, volume);
		setLooped(loop);
		return PlayImpl();
	}
	return false;
}

void ChannelImpl::setProperty(E_PROPERTY prop, f32 value)
{
	switch ( prop ) {
	case PROPERTY_PANNING:
		{
			_panning = Math::Clamp(value, -1.f, 1.f);
			setPropertyImpl(prop, _panning);
		}
		break;
	case PROPERTY_PITCH:
		{
			_pitch = cpp::max(value, 0.f);
			setPropertyImpl(prop, _pitch);
		}
		break;
	case PROPERTY_VOLUME:
		{
			using sound_impl_detail::ConvertVolumeLocalToGlobal;
			_volume = Math::Clamp(value, 0.f, 1.f);
			setPropertyImpl(
				prop, ConvertVolumeLocalToGlobal(_volume, getGroup()));
		}
		break;
	case PROPERTY_POSITION:
		{
			f32 position_impl = Math::Clamp(value, 0.f, getLength());
			setPropertyImpl(prop, position_impl);
		}
		break;
	default:
		BME_ASSERT(false);
		break;
	}
}

f32 ChannelImpl::getProperty(E_PROPERTY prop) const
{
	switch ( prop ) {
	case PROPERTY_PANNING:  return _panning;
	case PROPERTY_PITCH:    return _pitch;
	case PROPERTY_VOLUME:   return _volume;
	case PROPERTY_POSITION: return getPropertyImpl(prop);
	default:
		BME_ASSERT(false);
		return 0.f;
	}
}

const StrId& ChannelImpl::getGroup() const
{
	return _group;
}

const SoundPtr& ChannelImpl::getSound() const
{
	return _sound;
}

f32 ChannelImpl::getLength() const
{
	BME_ASSERT(getSound());
	return getSound()->getLength();
}

bool ChannelImpl::isStreamed() const
{
	BME_ASSERT(getSound());
	return getSound()->isStreamed();
}

} // namespace bme
