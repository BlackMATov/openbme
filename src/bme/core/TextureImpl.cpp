/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/TextureImpl.cpp
 *****************************************************************************/

#include "TextureImpl.h"
#include "RendererImpl.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// texture_impl_detail
// 
// ----------------------------------------------------------------------------

namespace texture_impl_detail
{
	static const char* ColorFormatAsStr(E_FORMAT format)
	{
		switch ( format ) {
	#if BME_RENDERER == BME_RENDERER_OGLES
		case FORMAT_RGB8:        return "FORMAT_RGB8";
		case FORMAT_RGBA8:       return "FORMAT_RGBA8";
		case FORMAT_RGB_PVRTC2:  return "FORMAT_RGB_PVRTC2";
		case FORMAT_RGB_PVRTC4:  return "FORMAT_RGB_PVRTC4";
		case FORMAT_RGBA_PVRTC2: return "FORMAT_RGBA_PVRTC2";
		case FORMAT_RGBA_PVRTC4: return "FORMAT_RGBA_PVRTC4";
	#else
		case FORMAT_X8R8G8B8:    return "FORMAT_X8R8G8B8";
		case FORMAT_A8R8G8B8:    return "FORMAT_A8R8G8B8";
		case FORMAT_DXT1:        return "FORMAT_DXT1";
		case FORMAT_DXT3:        return "FORMAT_DXT3";
		case FORMAT_DXT5:        return "FORMAT_DXT5";
	#endif
		case FORMAT_UNKNOWN:     return "FORMAT_UNKNOWN";
		default:
			BME_ASSERT(false);
			return "";
		}
	}
} // namespace texture_impl_detail

// ----------------------------------------------------------------------------
// 
// Strings
// 
// ----------------------------------------------------------------------------

namespace Strings
{
	template <>
	struct Arg<TexturePtr>
	{
		TexturePtr value;
		Arg(const TexturePtr& value) : value(value) {}
		static ptrdiff_t Write(
			char* dest, size_t size, const Arg<TexturePtr>& self)
		{
			using namespace texture_impl_detail;
			const TexturePtr& tex = self.value;
			char buf[BME_MAX_LOGGER_MESSAGE] = {0};
			Strings::Format(
				buf, BME_MAX_LOGGER_MESSAGE, "(%1%,%2%,%3%KB)",
				tex ? tex->getSize() : pnt2u::zero,
				ColorFormatAsStr(tex ? tex->getFormat() : FORMAT_UNKNOWN),
				tex ? tex->getMemoryUsage() / 1024 : 0);
			return BME_snprintf(dest, size, "%s", buf);
		}
	};
	
	template <>
	struct Arg<ImageDesc>
	{
		ImageDesc value;
		Arg(const ImageDesc& value) : value(value) {}
		static ptrdiff_t Write(
			char* dest, size_t size, const Arg<ImageDesc>& self)
		{
			using namespace texture_impl_detail;
			const ImageDesc& desc = self.value;
			char buf[BME_MAX_LOGGER_MESSAGE] = {0};
			Strings::Format(
				buf, BME_MAX_LOGGER_MESSAGE, "(%1%,%2%)",
				desc.size,
				ColorFormatAsStr(desc.format));
			return BME_snprintf(dest, size, "%s", buf);
		}
	};
} // namespace Strings

// ----------------------------------------------------------------------------
// 
// Texture
// 
// ----------------------------------------------------------------------------

TexturePtr Texture::Get(const Path& path)
{
	TexturePtr ret_value;
	ImageLoader image_loader;
	ImageDesc desc = image_loader.Load(path, false);
	if ( desc.isLoaded() ) {
		cpp::auto_ptr<TextureImpl> tex(TextureImpl::Create());
		if ( tex->Create(desc) ) {
			ret_value = TexturePtr(tex.release());
		}
	}
	BME_LOG_COMPLETE_OR_ERROR_FMT(
		ret_value,
		"Get texture from path (%1%,%2%)",
		path,
		ret_value);
	return ret_value;
}

TexturePtr Texture::Create(const ImageDesc& desc)
{
	TexturePtr ret_value;
	if ( desc.isLoaded() ) {
		cpp::auto_ptr<TextureImpl> tex(TextureImpl::Create());
		if ( tex->Create(desc) ) {
			ret_value = TexturePtr(tex.release());
		}
	}
	using namespace texture_impl_detail;
	BME_LOG_COMPLETE_OR_ERROR_FMT(
		ret_value,
		"Create texture from desc %1%",
		desc);
	return ret_value;
}

TexturePtr Texture::Create(const SmartBuffer& mem)
{
	return Texture::Create(mem.getBuffer(), mem.getSize());
}

TexturePtr Texture::Create(const void* data, size_t size)
{
	TexturePtr ret_value;
	ImageLoader image_loader;
	ImageDesc desc = image_loader.Load(data, size, false);
	if ( desc.isLoaded() ) {
		cpp::auto_ptr<TextureImpl> tex(TextureImpl::Create());
		if ( tex->Create(desc) ) {
			ret_value = TexturePtr(tex.release());
		}
	}
	using namespace texture_impl_detail;
	BME_LOG_COMPLETE_OR_ERROR_FMT(
		ret_value,
		"Create texture from memory %1%",
		ret_value);
	return ret_value;
}

TexturePtr Texture::Create(const TexturePtr& source)
{
	TexturePtr ret_value;
	if ( source ) {
		cpp::auto_ptr<TextureImpl> tex(TextureImpl::Create());
		if ( tex->Create(source) ) {
			ret_value = TexturePtr(tex.release());
		}
	}
	using namespace texture_impl_detail;
	BME_LOG_COMPLETE_OR_ERROR_FMT(
		ret_value,
		"Create copy texture %1%",
		ret_value);
	return ret_value;
}

TexturePtr Texture::Create(
	const pnt2u& size, E_FORMAT format, bool render_target, bool z_buffer)
{
	TexturePtr ret_value;
	if ( size.x > 0 && size.y > 0 && FORMAT_UNKNOWN != format ) {
		cpp::auto_ptr<TextureImpl> tex(TextureImpl::Create());
		if ( tex->Create(size, format, render_target, z_buffer) ) {
			ret_value = TexturePtr(tex.release());
		}
	}
	using namespace texture_impl_detail;
	BME_LOG_COMPLETE_OR_ERROR_FMT(
		ret_value,
		"Creating new texture (%1%,%2%,RT:%3%,ZB:%4%)",
		size,
		ColorFormatAsStr(format),
		render_target,
		z_buffer);
	return ret_value;
}

// ----------------------------------------------------------------------------
// 
// TextureImpl::Desc
// 
// ----------------------------------------------------------------------------

TextureImpl::Desc::Desc()
: size(pnt2u::zero)
, origSize(pnt2u::zero)
, format(FORMAT_UNKNOWN)
, isRT(false)
, isZB(false)
{
}

bool TextureImpl::Desc::CheckForRender()
{
	// check renderer
	if ( !theRenderer() ) {
		return false;
	}
	
	// check render target support and format
	if ( isRT ) {
		if ( !theRenderer()->getCaps().render_to_texture ) {
			return false;
		}
		if ( !IsRTFormat() ) {
			return false;
		}
	}
	
	// check npot size
	if ( !theRenderer()->getCaps().npot_textures ) {
		size.x = Math::NextPowerOfTwo(size.x);
		size.y = Math::NextPowerOfTwo(size.y);
	}
	
	// check max size
	if ( size.x <= 0 || size.y <= 0 ||
		 size.x > theRenderer()->getCaps().max_texture_size.x ||
		 size.y > theRenderer()->getCaps().max_texture_size.y )
	{
		return false;
	}
	
	return true;
}

bool TextureImpl::Desc::IsRTFormat() const
{
	switch ( format ) {
#if BME_RENDERER == BME_RENDERER_OGLES
	case FORMAT_RGB8:
	case FORMAT_RGBA8:
		return true;
	case FORMAT_RGB_PVRTC2:
	case FORMAT_RGB_PVRTC4:
	case FORMAT_RGBA_PVRTC2:
	case FORMAT_RGBA_PVRTC4:
		return false;
#else
	case FORMAT_X8R8G8B8:
	case FORMAT_A8R8G8B8:
		return true;
	case FORMAT_DXT1:
	case FORMAT_DXT3:
	case FORMAT_DXT5:
		return false;
#endif
	default:
		BME_ASSERT(false);
		return false;
	}
}

// ----------------------------------------------------------------------------
// 
// TextureImpl
// 
// ----------------------------------------------------------------------------

TextureImpl::TextureImpl()
{
	RendererImpl* renderer = Holder<RendererImpl>::getHostage();
	if ( renderer ) {
		renderer->AddTexture(this);
	}
}

TextureImpl::~TextureImpl()
{
	RendererImpl* renderer = Holder<RendererImpl>::getHostage();
	if ( renderer ) {
		renderer->RemoveTexture(this);
	}
}

bool TextureImpl::Create(const ImageDesc& desc)
{
	if ( !desc.isEmpty() ) {
		_desc.size     = desc.size;
		_desc.origSize = desc.size;
		_desc.format   = desc.format;
		if ( _desc.CheckForRender() && CreateImpl(desc) ) {
			return true;
		}
	}
	return false;
}

bool TextureImpl::Create(const TexturePtr& source)
{
	if ( source ) {
		_desc.size     = source->getSize();
		_desc.origSize = source->getOriginalSize();
		_desc.format   = source->getFormat();
		if ( _desc.CheckForRender() && CreateImpl(source) ) {
			return true;
		}
	}
	return false;
}

bool TextureImpl::Create(
	const pnt2u& size, E_FORMAT format, bool render_target, bool z_buffer)
{
	{
		_desc.size     = size;
		_desc.origSize = size;
		_desc.format   = format;
		_desc.isRT     = render_target;
		_desc.isZB     = z_buffer;
		if ( _desc.CheckForRender() && CreateImpl() ) {
			return true;
		}
	}
	return false;
}

const pnt2u& TextureImpl::getSize() const
{
	return _desc.size;
}

const pnt2u& TextureImpl::getOriginalSize() const
{
	return _desc.origSize;
}

E_FORMAT TextureImpl::getFormat() const
{
	return _desc.format;
}

size_t TextureImpl::getMemoryUsage() const
{
	return
		getSize().x * getSize().y *
		ImageDesc::BPPFromFormat(getFormat()) / 8;
}

bool TextureImpl::isRenderTarget() const
{
	return _desc.isRT;
}

bool TextureImpl::isZBuffer() const
{
	return _desc.isZB;
}

} // namespace bme
