/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/Camera.cpp
 *****************************************************************************/

#include <bme/core/Camera.h>

#include <bme/core/System.h>
#include <bme/core/Application.h>

#include "RendererDevice.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// camera_detail
// 
// ----------------------------------------------------------------------------

namespace camera_detail
{
	static rectu GetTrueViewport()
	{
		if ( !theApplication() || !theSystem() ) {
			return rectu::zero;
		}
		
		vec2f app_res  = vec2f(theApplication()->getDesc().resolution);
		bool  app_wide = theApplication()->getDesc().isWidescreen;
		vec2f win_res  = vec2f(theSystem()->getWindowResolution());
		
		f32 app_aspect = app_res.x / app_res.y;
		f32 win_aspect = win_res.x / win_res.y;
		
		if ( app_res != win_res && !app_wide ) {
			vec2f true_res = app_aspect < win_aspect
				? vec2f(win_res.y*app_aspect, win_res.y)
				: vec2f(win_res.x, win_res.x/app_aspect);
			return rectu(pnt2u((win_res - true_res)/2.f), pnt2u(true_res));
		} else {
			return rectu(pnt2u(win_res));
		}
	}
}

// ----------------------------------------------------------------------------
// 
// Camera
// 
// ----------------------------------------------------------------------------

Camera::Camera()
{
	if ( theSystem() && theApplication() ) {
		To2D(vec2f(theSystem()->getWindowResolution()));
		setViewport(rectu(theApplication()->getDesc().resolution));
	}
}

Camera::Camera(const Camera& other)
{
	_view         = other._view;
	_world        = other._world;
	_projection   = other._projection;
	_viewport     = other._viewport;
	_renderTarget = other._renderTarget;
}

Camera& Camera::operator=(const Camera& other)
{
	if ( this != &other ) {
		_view         = other._view;
		_world        = other._world;
		_projection   = other._projection;
		_viewport     = other._viewport;
		_renderTarget = other._renderTarget;
	}
	return *this;
}

Camera::Camera(const TexturePtr& target)
: _renderTarget(target)
{
	if ( target ) {
		To2D(vec2f(target->getOriginalSize()));
		setViewport(rectu(target->getOriginalSize()));
	} else if ( theSystem() && theApplication() ) {
		To2D(vec2f(theSystem()->getWindowResolution()));
		setViewport(rectu(theApplication()->getDesc().resolution));
	}
}

void Camera::To2D(const vec2f& size)
{
	f32 dx    = -size.x / 2.f + RendererDevice::getTexelOffset().x;
	f32 dy    =  size.y / 2.f - RendererDevice::getTexelOffset().y;
	f32 zNear = -1.f;
	f32 zFar  =  1.f;
	
	_projection  = mat4f::MakeScale(vec3f(1.f, -1.f, 1.f));
	_projection *= mat4f::MakeTranslate(vec3f(dx, dy, 0.f));
	_projection *= mat4f::MakeOrthogonal(size, zNear, zFar);
	
	if ( !getRenderTarget() && theApplication() ) {
		vec2f app_res = vec2f(theApplication()->getDesc().resolution);
		_view *= mat4f::MakeScale(size / app_res);
	}
}

/** \todo IMPLME
void Camera::To3D(const vec2f& size, f32 fov, f32 aspect, f32 zNear, f32 zFar)
{
	if ( math::IsZero(aspect) ) {
		aspect = size.x / size.y;
	}
	projection = mat4f::MakePerspective(fov, aspect, zNear, zFar);
}*/

void Camera::setWorld(const mat4f& value)
{
	_world = value;
}

void Camera::setViewport(const rectu& value)
{
	_viewport = value;
	
	if ( getRenderTarget() ) {
		_viewport = value.Intersection(rectu(getRenderTarget()->getSize()));
		if ( _viewport.getArea() == 0 ) {
			_viewport = rectu(getRenderTarget()->getOriginalSize());
		}
	} else if ( theApplication() ) {
		_viewport = value.Intersection(rectu(theApplication()->getDesc().resolution));
		if ( _viewport.getArea() == 0 ) {
			_viewport = rectu(theApplication()->getDesc().resolution);
		}
	}
}

const mat4f& Camera::getWorld() const
{
	return _world;
}

const mat4f& Camera::getProjection() const
{
	return _projection;
}

const rectu& Camera::getViewport() const
{
	return _viewport;
}

const TexturePtr& Camera::getRenderTarget() const
{
	return _renderTarget;
}

const mat4f& Camera::getCorrectView() const
{
	return _view;
}

rectu Camera::getCorrectViewport() const
{
	if ( getRenderTarget() || !theApplication() ) {
		return _viewport;
	}
	
	rectu true_vp = camera_detail::GetTrueViewport();
	vec2f aspects = vec2f(theApplication()->getDesc().resolution) / vec2f(true_vp.getSize());
	
	f32 nx  = true_vp.getLeft() + getViewport().getLeft  () / aspects.x;
	f32 nx2 = true_vp.getLeft() + getViewport().getRight () / aspects.x;
	f32 ny  = true_vp.getTop () + getViewport().getTop   () / aspects.y;
	f32 ny2 = true_vp.getTop () + getViewport().getBottom() / aspects.y;
	rectu ret_value(pnt2u(vec2f(nx,ny)), pnt2u(vec2f(nx2-nx,ny2-ny)));
	return true_vp.Intersection(ret_value);
}

} // namespace bme
