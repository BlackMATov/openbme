/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/RendererDevice.h
 *****************************************************************************/

#pragma once

#include <bme/core/Renderer.h>
#include <bme/core/Primitives.h>

namespace bme
{

class RendererImpl;

class RendererDevice : public RefCounted
{
protected:
	
	mat4f                  _worldMat;
	mat4f                  _viewMat;
	mat4f                  _projMat;
	rectu                  _viewport;
	
	Material               _currentMaterial;
	RenderStates           _currentRenderStates;
	TexturePtr             _currentRenderTarget;
	
	pnt2u                  _getMaxCurrentRTSize() const;
	
public:
	
	enum E_TRANSFORM {
		TRANSFORM_VIEW,
		TRANSFORM_WORLD,
		TRANSFORM_PROJECTION
	};
	
	virtual ~RendererDevice() {}
	static RendererImpl* getOwner();
	
	const mat4f&           getTransform       ( E_TRANSFORM transform ) const;
	const rectu&           getViewport        () const;
	void                   RetryCurrentStates ();
	void                   ResetCurrentState  ();
	
	// for override and impl to renderer specific -----------------------------
	static const vec2f&    getTexelOffset     ();
	static RendererDevice* Create             ();
	
	virtual bool           Initialize         () = 0;
	virtual void           Shutdown           () = 0;
	virtual void           EndFrame           () = 0;
	virtual	void           Resize             () = 0;
	virtual void           DeviceLost         () = 0;
	
	virtual bool           BeginScene         () = 0;
	virtual void           ClearScene         ( const Color& color, bool back_buffer, bool z_buffer ) = 0;
	virtual bool           EndScene           () = 0;
	
	virtual void           setTransform       ( E_TRANSFORM transform, const mat4f& mat ) = 0;
	virtual void           setViewport        ( const rectu& vp ) = 0;
	
	virtual bool           setRenderTarget    ( const TexturePtr& texture ) = 0;
	virtual void           setStates          ( const Material& material, const RenderStates& states, bool lazy ) = 0;
	virtual void           DrawPrimitives     ( const Vertex* vertices, size_t vertices_count,
	                                            const u16* indices, size_t indices_count,
	                                            Renderer::E_PRIMITIVE type ) = 0;
	// ------------------------------------------------------------------------
};

typedef cpp::intrusive_ptr<RendererDevice> RendererDevicePtr;

} // namespace bme
