/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/InputerImpl.cpp
 *****************************************************************************/

#include "InputerImpl.h"
#include "SystemImpl.h"
#include "ReceiverImpl.h"

// for mouse position correction
#include <bme/core/Camera.h>
#include <bme/core/Application.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// inputer_detail
// 
// ----------------------------------------------------------------------------

namespace inputer_detail
{
	static void KeyProcess(E_KEY_STATE& key)
	{
		switch ( key ) {
		case KEY_STATE_EMPTY:
			// nothing
			break;
		case KEY_STATE_DOWN:
			key = KEY_STATE_DOWNED;
			break;
		case KEY_STATE_DOWNED:
			// nothing
			break;
		case KEY_STATE_UP:
			key = KEY_STATE_EMPTY;
			break;
		default:
			BME_ASSERT(false);
			break;
		}
	}
	
	class StandartReceiver : public Receiver
	{
	public:
		
		StandartReceiver() {}
		virtual ~StandartReceiver() {}
		
		virtual bool OnEvent(const Event& event)
		{
			switch ( event.type ) {
			case Event::TYPE_MOUSE_DOWN:
			case Event::TYPE_MOUSE_UP:
			case Event::TYPE_MOUSE_MOVE:
				{
					MouseImpl* mouse = static_cast<MouseImpl*>(theMouse(event.mouse.which));
					if ( mouse ) {
						if ( event.mouse.key != MOUSE_WHEELDOWN && event.mouse.key != MOUSE_WHEELUP ) {
							mouse->DoMove(
								vec2f(event.mouse.x, event.mouse.y));
						}
						if ( event.type != Event::TYPE_MOUSE_MOVE ) {
							mouse->DoKey(
								event.mouse.key, event.type == Event::TYPE_MOUSE_DOWN);
						}
					}
				}
				break;
			case Event::TYPE_KEYBOARD_DOWN:
			case Event::TYPE_KEYBOARD_UP:
				{
					KeyboardImpl* keyboard = static_cast<KeyboardImpl*>(theKeyboard(event.keyboard.which));
					if ( keyboard ) {
						keyboard->DoKey(
							event.keyboard.key,
							event.type == Event::TYPE_KEYBOARD_DOWN);
					}
				}
				break;
			case Event::TYPE_JOYSTICK_DOWN:
			case Event::TYPE_JOYSTICK_UP:
				{
					JoystickImpl* joystick = static_cast<JoystickImpl*>(theJoystick(event.joystick.which));
					if ( joystick ) {
						joystick->DoButton(
							event.joystick.button,
							event.type == Event::TYPE_JOYSTICK_DOWN);
					}
				}
				break;
			default:
				// nothing
				break;
			}
			return true;
		}
	} standart_receiver;
}

// ----------------------------------------------------------------------------
// 
// MouseImpl
// 
// ----------------------------------------------------------------------------

MouseImpl::MouseImpl()
{
	Reset();
}

MouseImpl::MouseImpl(const MouseImpl& other)
{
	_pos = other._pos;
	cpp::copy(other._keyStates, other._keyStates + MOUSE_UNKNOWN, _keyStates);
}

MouseImpl& MouseImpl::operator=(const MouseImpl& other)
{
	if ( this != &other ) {
		_pos = other._pos;
		cpp::copy(other._keyStates, other._keyStates + MOUSE_UNKNOWN, _keyStates);
	}
	return *this;
}

MouseImpl::~MouseImpl()
{
}

bool MouseImpl::DoKey(E_MOUSE key, bool down)
{
	if ( key == MOUSE_UNKNOWN ) {
		return false;
	}
	if ( (down && _keyStates[key] == KEY_STATE_UP) || (!down && _keyStates[key] == KEY_STATE_EMPTY) ) {
		return false;
	}
	_keyStates[key] = down ? KEY_STATE_DOWN : KEY_STATE_UP;
	return true;
}

bool MouseImpl::DoMove(const vec2f& pos)
{
	_pos = pos;
	return true;
}

void MouseImpl::Reset()
{
	cpp::fill(_keyStates, _keyStates + MOUSE_UNKNOWN, KEY_STATE_EMPTY);
}

void MouseImpl::Update()
{
	cpp::for_each(_keyStates, _keyStates + MOUSE_UNKNOWN, inputer_detail::KeyProcess);
	_keyStates[MOUSE_WHEELDOWN] = KEY_STATE_EMPTY;
	_keyStates[MOUSE_WHEELUP  ] = KEY_STATE_EMPTY;
}

bool MouseImpl::IsKeyDown(E_MOUSE key) const
{
	return (_keyStates[key] == KEY_STATE_DOWN);
}

bool MouseImpl::IsKeyUp(E_MOUSE key) const
{
	return (_keyStates[key] == KEY_STATE_UP);
}

bool MouseImpl::IsKeyDowned(E_MOUSE key) const
{
	return (_keyStates[key] == KEY_STATE_DOWN || _keyStates[key] == KEY_STATE_DOWNED);
}

vec2f MouseImpl::getPosition() const
{
	vec2f ret_value = _pos;
	if ( theApplication() ) {
		rectu vp      = Camera().getCorrectViewport();
		vec2f app_res = vec2f(theApplication()->getDesc().resolution);
		
		ret_value -= vec2f(vp.getTopLeft());
		ret_value *= app_res / vec2f(vp.getSize());
		
		ret_value.x = cpp::max(0.f,             ret_value.x);
		ret_value.y = cpp::max(0.f,             ret_value.y);
		ret_value.x = cpp::min(app_res.x - 1.f, ret_value.x);
		ret_value.y = cpp::min(app_res.y - 1.f, ret_value.y);
	}
	return ret_value;
}

// ----------------------------------------------------------------------------
// 
// KeyboardImpl
// 
// ----------------------------------------------------------------------------

KeyboardImpl::KeyboardImpl()
{
	Reset();
}

KeyboardImpl::KeyboardImpl(const KeyboardImpl& other)
{
	cpp::copy(other._keyStates, other._keyStates + KEY_UNKNOWN, _keyStates);
}

KeyboardImpl& KeyboardImpl::operator=(const KeyboardImpl& other)
{
	if ( this != &other ) {
		cpp::copy(other._keyStates, other._keyStates + KEY_UNKNOWN, _keyStates);
	}
	return *this;
}

KeyboardImpl::~KeyboardImpl()
{
}

bool KeyboardImpl::DoKey(E_KEY key, bool down)
{
	if ( key == KEY_UNKNOWN ) {
		return false;
	}
	if ( (down && _keyStates[key] == KEY_STATE_UP) || (!down && _keyStates[key] == KEY_STATE_EMPTY) ) {
		return false;
	}
	_keyStates[key] = down ? KEY_STATE_DOWN : KEY_STATE_UP;
	return true;
}

void KeyboardImpl::Reset()
{
	cpp::fill(_keyStates, _keyStates + KEY_UNKNOWN, KEY_STATE_EMPTY);
}

void KeyboardImpl::Update()
{
	cpp::for_each(_keyStates, _keyStates + KEY_UNKNOWN, inputer_detail::KeyProcess);
}

bool KeyboardImpl::IsKeyDown(E_KEY key) const
{
	return (_keyStates[key] == KEY_STATE_DOWN);
}

bool KeyboardImpl::IsKeyUp(E_KEY key) const
{
	return (_keyStates[key] == KEY_STATE_UP);
}

bool KeyboardImpl::IsKeyDowned(E_KEY key) const
{
	return (_keyStates[key] == KEY_STATE_DOWN || _keyStates[key] == KEY_STATE_DOWNED);
}

// ----------------------------------------------------------------------------
// 
// JoystickImpl
// 
// ----------------------------------------------------------------------------

JoystickImpl::JoystickImpl()
: _index(-1)
{
	Reset();
}

JoystickImpl::JoystickImpl(const JoystickImpl& other)
{
	_index = other._index;
	_desc = other._desc;
	cpp::copy(other._buttonStates, other._buttonStates + JOYSTICK_BUTTON_UNKNOWN, _buttonStates);
}

JoystickImpl& JoystickImpl::operator=(const JoystickImpl& other)
{
	if ( this != &other ) {
		_index = other._index;
		_desc = other._desc;
		cpp::copy(other._buttonStates, other._buttonStates + JOYSTICK_BUTTON_UNKNOWN, _buttonStates);
	}
	return *this;
}

JoystickImpl::~JoystickImpl()
{
}

void JoystickImpl::Init(size_t index)
{
	_index = Math::SmartCast<ptrdiff_t>(index);
	SystemImpl* system = Holder<SystemImpl>::getHostage();
	if ( system ) {
		_desc = system->getJoystickDesc(index);
	}
}

bool JoystickImpl::isInited() const
{
	return _index >= 0;
}

bool JoystickImpl::DoButton(E_JOYSTICK_BUTTON button, bool down)
{
	if ( button == JOYSTICK_BUTTON_UNKNOWN ) {
		return false;
	}
	if ( (down && _buttonStates[button] == KEY_STATE_UP) || (!down && _buttonStates[button] == KEY_STATE_EMPTY) ) {
		return false;
	}
	_buttonStates[button] = down ? KEY_STATE_DOWN : KEY_STATE_UP;
	return true;
}

void JoystickImpl::Reset()
{
	cpp::fill(_buttonStates, _buttonStates + JOYSTICK_BUTTON_UNKNOWN, KEY_STATE_EMPTY);
}

void JoystickImpl::Update()
{
	cpp::for_each(_buttonStates, _buttonStates + JOYSTICK_BUTTON_UNKNOWN, inputer_detail::KeyProcess);
}

const Joystick::Desc& JoystickImpl::getDesc() const
{
	return _desc;
}

f32 JoystickImpl::getAxisState(size_t nAxis) const
{
	SystemImpl* system = Holder<SystemImpl>::getHostage();
	if ( system && isInited() ) {
		return system->getJoyAxisState(_index, nAxis);
	}
	return 0.f;
}

vec2f JoystickImpl::getBallState(size_t nBall) const
{
	SystemImpl* system = Holder<SystemImpl>::getHostage();
	if ( system && isInited() ) {
		return system->getJoyBallState(_index, nBall);
	}
	return vec2f::zero;
}

bool JoystickImpl::IsButtonDown(E_JOYSTICK_BUTTON button) const
{
	return (_buttonStates[button] == KEY_STATE_DOWN);
}

bool JoystickImpl::IsButtonUp(E_JOYSTICK_BUTTON button) const
{
	return (_buttonStates[button] == KEY_STATE_UP);
}

bool JoystickImpl::IsButtonDowned(E_JOYSTICK_BUTTON button) const
{
	return (_buttonStates[button] == KEY_STATE_DOWN || _buttonStates[button] == KEY_STATE_DOWNED);
}

bool JoystickImpl::IsHatDowned(size_t nHat, E_JOYSTICK_HAT state) const
{
	SystemImpl* system = Holder<SystemImpl>::getHostage();
	if ( system && isInited() ) {
		return system->IsJoyHatDowned(_index, nHat, state);
	}
	return false;
}

// ----------------------------------------------------------------------------
// 
// InputerImpl
// 
// ----------------------------------------------------------------------------

InputerImpl::InputerImpl()
{
	SystemImpl* system = Holder<SystemImpl>::getHostage();
	if ( system ) {
		_desc = system->getInputerDesc();
		_mouses.resize(_desc.mouses);
		_keyboards.resize(_desc.keyboards);
		_joysticks.resize(_desc.joysticks);
		for ( size_t i = 0; i < _desc.joysticks; ++i ) {
			_joysticks[i].Init(i);
		}
	}
	EventEmitter* emitter = Holder<EventEmitter>::getHostage();
	if ( emitter ) {
		emitter->RegisterReceiver(&inputer_detail::standart_receiver);
	}
}

InputerImpl::~InputerImpl()
{
	EventEmitter* emitter = Holder<EventEmitter>::getHostage();
	if ( emitter ) {
		emitter->UnregisterReceiver(&inputer_detail::standart_receiver);
	}
}

bool InputerImpl::Initialize()
{
	BME_ASSERT(!theInputer());
	new InputerImpl();
	return true;
}

void InputerImpl::Shutdown()
{
	delete theInputer();
}

const Inputer::Desc& InputerImpl::getDesc() const
{
	return _desc;
}

Mouse* InputerImpl::getMouse(size_t index) const
{
	return index < _mouses.size() ? &_mouses[index] : NULL;
}

Keyboard* InputerImpl::getKeyboard(size_t index) const
{
	return index < _keyboards.size() ? &_keyboards[index] : NULL;
}

Joystick* InputerImpl::getJoystick(size_t index) const
{
	return index < _joysticks.size() ? &_joysticks[index] : NULL;
}

void InputerImpl::Reset()
{
	for ( size_t i = 0; i < _mouses.size(); ++i ) {
		_mouses[i].Reset();
	}
	for ( size_t i = 0; i < _keyboards.size(); ++i ) {
		_keyboards[i].Reset();
	}
	for ( size_t i = 0; i < _joysticks.size(); ++i ) {
		_joysticks[i].Reset();
	}
}

void InputerImpl::Update()
{
	for ( size_t i = 0; i < _mouses.size(); ++i ) {
		_mouses[i].Update();
	}
	for ( size_t i = 0; i < _keyboards.size(); ++i ) {
		_keyboards[i].Update();
	}
	for ( size_t i = 0; i < _joysticks.size(); ++i ) {
		_joysticks[i].Update();
	}
}

} // namespace bme
