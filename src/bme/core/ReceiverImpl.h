/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/ReceiverImpl.h
 *****************************************************************************/

#include <bme/core/Receiver.h>

namespace bme
{

class EventEmitter : public Holder<EventEmitter>
{
	typedef cpp::vector<Receiver::Event>::type TEvents;
	
	TEvents        _events;
	TReceiverList  _receivers;
	bool           _emitEvent          ( const Receiver::Event& event );
	
public:
	
	EventEmitter();
	virtual ~EventEmitter();
	
	static bool    Initialize         ();
	static void    Shutdown           ();
	
	void           PushEvent          ( const Receiver::Event& event, bool immediately );
	bool           UpdateEvents       ();
	void           RegisterReceiver   ( Receiver* receiver );
	void           UnregisterReceiver ( Receiver* receiver );
};

} // namespace bme
