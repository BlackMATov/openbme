/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/FilesystemImpl.h
 *****************************************************************************/

#pragma once

#include <bme/core/Filesystem.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// OS specific functions
// 
// ----------------------------------------------------------------------------

namespace filesystem_impl
{
	bool RemoveImpl          ( const Path& path, bool folder );
	bool IsExistsImpl        ( const Path& path, bool folder );
	bool CreateDirectoryImpl ( const Path& path );
	bool TraceDirectoryImpl  ( const Path& path,
	                           Filesystem::TraceFunc func, void* data );
	
	Path GetExeDirImpl       ();
	Path GetResDirImpl       ();
	Path GetWrkDirImpl       ();
	Path GetAppDirImpl       ();
}

// ----------------------------------------------------------------------------
// 
// FilesystemImpl
// 
// ----------------------------------------------------------------------------

class FilesystemImpl
	: public Filesystem
	, public Holder<FilesystemImpl>
{
	typedef cpp::vector<ArchivePtr>::type TArchives;
	JobberPtr      _jobber;
	TArchives      _archives;
	RecursiveMutex _archMutex;
	RecursiveMutex _implMutex;
	
	ReadFilePtr    _openFileFromDisk    ( const Path& path ) const;
	ReadFilePtr    _openFileFromArchive ( const Path& path ) const;
	ReadFilePtr    _openFileCommon      ( const Path& path, E_ORDER_MODE mode ) const;
	
	bool           _isExistsOnDisk      ( const Path& path, bool folder ) const;
	bool           _isExistsOnArchive   ( const Path& path, bool folder ) const;
	bool           _isExistsCommon      ( const Path& path, E_ORDER_MODE mode, bool folder ) const;
	
	bool           _attachArchive       ( const ArchivePtr& archive );
	bool           _isArchiveAttached   ( const Path& path ) const;
	bool           _detachArchive       ( const Path& path );
	void           _detachAllArchives   ();
	
public:
	
	FilesystemImpl();
	virtual ~FilesystemImpl();
	
	static bool    Initialize           ();
	static void    Shutdown             ();
	
	// Filesystem override ----------------------------------------------------
	bool           RemoveFile           ( const Path& path ) const;
	bool           RemoveFolder         ( const Path& path ) const;
	bool           CreateDirectories    ( const Path& path ) const;
	
	void           TraceDirectory       ( const Path& path,
	                                      TraceFunc func, void* data ) const;
	void           TraceDirectories     ( const Path& path,
	                                      TraceFunc func, void* data ) const;
	
	ReadFilePtr    OpenFile             ( const Path& path,
	                                      E_ORDER_MODE mode ) const;
	ReadFilePtr    ReadFile             ( const Path& path,
	                                      E_ORDER_MODE mode ) const;
	FileTaskPtr    ReadFileAsync        ( const Path& path,
	                                      E_ORDER_MODE mode ) const;
	
	bool           IsFileExists         ( const Path& path,
	                                      E_ORDER_MODE mode ) const;
	bool           IsFolderExists       ( const Path& path,
	                                      E_ORDER_MODE mode ) const;
	
	Path           GetExecuteDir        () const;
	Path           GetResourceDir       () const;
	Path           GetWorkingDir        () const;
	Path           GetAppDataDir        () const;
	
	bool           AttachArchive        ( const ArchivePtr& archive );
	bool           IsArchiveAttached    ( const Path& path ) const;
	bool           DetachArchive        ( const Path& path );
	void           DetachAllArchives    ();
	// ------------------------------------------------------------------------
};

} // namespace bme
