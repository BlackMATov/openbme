/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/SystemImpl.h
 *****************************************************************************/

#pragma once

#include <bme/core/System.h>
#include <bme/core/Logger.h>
#include <bme/core/Inputer.h>

namespace bme
{

class SystemImpl
	: public System
	, public Holder<SystemImpl>
{
	pnt2u                  _windowResolution;
	pnt2u                  _desktopResolution;
	
protected:
	
	vec2f                  PointToScreen        ( const vec2f& point );
	void                   CalculateResolutions ( const pnt2u& desktop_res );
	void                   CalculateResolutions ( const pnt2u& window_res,
	                                              const pnt2u& desktop_res );
	
public:
	
	SystemImpl();
	virtual ~SystemImpl();
	
	// System resolutions impl ------------------------------------------------
	virtual pnt2u          getWindowResolution  () const;
	virtual pnt2u          getDesktopResolution () const;
	// ------------------------------------------------------------------------
	
	// for override and impl to system specific -------------------------------
	static bool            Initialize           ();
	static void            Shutdown             ();
	
	virtual void           Update               () = 0;
	virtual void           EndFrame             () = 0;
	virtual bool           ToggleFullscreen     ( bool yesno ) = 0;
	virtual bool           ToggleWidescreen     ( bool yesno ) = 0;
	virtual void           NativeLog            ( const char* text, Logger::E_LEVEL level ) const = 0;
	virtual size_t         getTicks             () const = 0;
	
	virtual Inputer::Desc  getInputerDesc       ()                                                  const;
	virtual Joystick::Desc getJoystickDesc      ( size_t index )                                    const;
	virtual f32            getJoyAxisState      ( size_t index, size_t nAxis )                      const;
	virtual vec2f          getJoyBallState      ( size_t index, size_t nBall )                      const;
	virtual bool           IsJoyHatDowned       ( size_t index, size_t nHat, E_JOYSTICK_HAT state ) const;
	
#if BME_OS == BME_OS_WINDOWS
	virtual ptrdiff_t      getHWND              () const = 0;
#endif
	// ------------------------------------------------------------------------
};

} // namespace bme
