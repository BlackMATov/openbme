/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/LoggerImpl.h
 *****************************************************************************/

#pragma once

#include <bme/core/Logger.h>

namespace bme
{

class LoggerImpl
	: public Logger
	, public Holder<LoggerImpl>
{
	Path           _path;
	E_LEVEL        _minLevel;
	E_OUTPUT       _outputType;
	TWriteHook     _writeHook;
	RecursiveMutex _mutex;
	
public:
	
	LoggerImpl();
	virtual ~LoggerImpl();
	
	static bool Initialize    ();
	static void Shutdown      ();
	
	// Logger override --------------------------------------------------------
	bool        setLogfile    ( const Path& path );
	const Path& getLogfile    () const;
	
	void        setMinLevel   ( E_LEVEL level );
	E_LEVEL     getMinLevel   () const;
	
	void        setOutputType ( E_OUTPUT value );
	E_OUTPUT    getOutputType () const;
	
	void        setWriteHook  ( TWriteHook hook );
	TWriteHook  getWriteHook  () const;
	
	void        Write         ( const char* text, E_LEVEL level );
	// ------------------------------------------------------------------------
};

} // namespace bme
