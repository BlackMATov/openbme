/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/SounderImpl.cpp
 *****************************************************************************/

#include "SounderImpl.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// sounder_detail
// 
// ----------------------------------------------------------------------------

namespace sounder_detail
{
	template < typename TIter >
	static size_t ApplyBySound(
		TIter begin, TIter end,
		const SoundPtr& sound, bool(Channel::*func)())
	{
		size_t ret_value = 0;
		for ( ; begin != end; ++begin ) {
			Channel& channel = *begin;
			if ( !sound || sound == channel.getSound() ) {
				++ret_value;
				(channel.*func)();
			}
		}
		return ret_value;
	}
	
	template < typename TIter >
	static size_t ApplyByGroup(
		TIter begin, TIter end,
		const StrId& group, bool(Channel::*func)())
	{
		size_t ret_value = 0;
		for ( ; begin != end; ++begin ) {
			Channel& channel = *begin;
			if ( group.empty() || group == channel.getGroup() ) {
				++ret_value;
				(channel.*func)();
			}
		}
		return ret_value;
	}
} // sounder_detail

// ----------------------------------------------------------------------------
// 
// SounderImpl
// 
// ----------------------------------------------------------------------------

void SounderImpl::_updateVolumes()
{
	for ( TChannelList::iterator
		iter = _channels.begin(), end = _channels.end();
		iter != end; ++iter )
	{
		Channel& channel = *iter;
		channel.setProperty(
			Channel::PROPERTY_VOLUME,
			channel.getProperty(Channel::PROPERTY_VOLUME));
	}
}

SounderImpl::SounderImpl()
: _masterVolume(1.f)
{
}

SounderImpl::~SounderImpl()
{
}

void SounderImpl::Update()
{
}

void SounderImpl::AddChannel(ChannelImpl* channel)
{
	BME_ASSERT(channel && !channel->is_linked());
	_channels.push_back(*channel);
}

void SounderImpl::RemoveChannel(ChannelImpl* channel)
{
	BME_ASSERT(channel && channel->is_linked());
	channel->unlink();
}

void SounderImpl::setMasterVolume(f32 value)
{
	_masterVolume = Math::Clamp(value, 0.f, 1.f);
	_updateVolumes();
}

f32 SounderImpl::getMasterVolume() const
{
	return _masterVolume;
}

void SounderImpl::setGroupVolume(f32 value, const StrId& group)
{
	_groupVolumes[group] = Math::Clamp(value, 0.f, 1.f);
	_updateVolumes();
}

f32 SounderImpl::getGroupVolume(const StrId& group) const
{
	TVolumes::const_iterator iter = _groupVolumes.find(group);
	if ( _groupVolumes.end() != iter ) {
		return (*iter).second;
	}
	return 1.f;
}

size_t SounderImpl::StopChannels(const StrId& group)
{
	using sounder_detail::ApplyByGroup;
	return ApplyByGroup(_channels.begin(), _channels.end(), group, &Channel::Stop);
}

size_t SounderImpl::PauseChannels(const StrId& group)
{
	using sounder_detail::ApplyByGroup;
	return ApplyByGroup(_channels.begin(), _channels.end(), group, &Channel::Pause);
}

size_t SounderImpl::ResumeChannels(const StrId& group)
{
	using sounder_detail::ApplyByGroup;
	return ApplyByGroup(_channels.begin(), _channels.end(), group, &Channel::Resume);
}

size_t SounderImpl::StopChannels(const SoundPtr& sound)
{
	using sounder_detail::ApplyBySound;
	return ApplyBySound(_channels.begin(), _channels.end(), sound, &Channel::Stop);
}

size_t SounderImpl::PauseChannels(const SoundPtr& sound)
{
	using sounder_detail::ApplyBySound;
	return ApplyBySound(_channels.begin(), _channels.end(), sound, &Channel::Pause);
}

size_t SounderImpl::ResumeChannels(const SoundPtr& sound)
{
	using sounder_detail::ApplyBySound;
	return ApplyBySound(_channels.begin(), _channels.end(), sound, &Channel::Resume);
}

} // namespace bme
