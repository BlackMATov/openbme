/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/RendererDevice.cpp
 *****************************************************************************/

#include "RendererDevice.h"

#include <bme/core/System.h>
#include <bme/core/Logger.h>
#include <bme/core/Camera.h>
#include <bme/core/Texture.h>

namespace bme
{

pnt2u RendererDevice::_getMaxCurrentRTSize() const
{
	if ( _currentRenderTarget ) {
		return _currentRenderTarget->getSize();
	} else if ( theSystem() ) {
		return theSystem()->getWindowResolution();
	}
	BME_LOG_ERROR("RendererDevice::_getMaxCurrentRTSize() can't get theSystem");
	return pnt2u::zero;
}

RendererImpl* RendererDevice::getOwner()
{
	return Holder<RendererImpl>::getHostage();
}

const mat4f& RendererDevice::getTransform(E_TRANSFORM transform) const
{
	switch(transform) {
	case TRANSFORM_VIEW:
		return _viewMat;
	case TRANSFORM_WORLD:
		return _worldMat;
	case TRANSFORM_PROJECTION:
		return _projMat;
	default:
		BME_ASSERT(false);
		return mat4f::identity;
	}
}

const rectu& RendererDevice::getViewport() const
{
	return _viewport;
}

void RendererDevice::RetryCurrentStates()
{
	setStates(_currentMaterial, _currentRenderStates, false);
	theRenderer()->setCamera(theRenderer()->getCamera());
}

void RendererDevice::ResetCurrentState()
{
	setStates(Material(), RenderStates(), false);
	theRenderer()->setCamera(Camera());
}

} // namespace bme
