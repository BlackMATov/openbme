/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/SoundImpl.h
 *****************************************************************************/

#pragma once

#include <bme/core/Sound.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// SoundImpl
// 
// ----------------------------------------------------------------------------

class SoundImpl : public Sound
{
	f32          _length;
	bool         _streamed;
	
protected:
	
	// for impl override ------------------------------------------------------
	virtual bool CreateImpl ( const Path& path,
	                          f32& outLength ) = 0;
	virtual bool CreateImpl ( const void* data, size_t size,
	                          f32& outLength ) = 0;
	// ------------------------------------------------------------------------
	
	SoundImpl();
	
public:
	
	static SoundImpl* Create();
	virtual ~SoundImpl();
	
	bool         Create     ( const SmartBuffer& mem );
	bool         Create     ( const void* data, size_t size );
	bool         Create     ( const Path& path, bool stream );
	
	// Sound override ---------------------------------------------------------
	ChannelPtr   Play       ( f32 volume, bool loop, const StrId& group );
	size_t       Stop       ();
	size_t       Pause      ();
	size_t       Resume     ();
	
	f32          getLength  () const;
	bool         isStreamed () const;
	// ------------------------------------------------------------------------
};

// ----------------------------------------------------------------------------
// 
// ChannelList
// 
// ----------------------------------------------------------------------------

class ChannelImpl;

typedef cpp::intrusive::list_base_hook<
	cpp::intrusive::link_mode<cpp::intrusive::auto_unlink>
> ChannelListHook;

typedef cpp::intrusive::list<
	ChannelImpl,
	cpp::intrusive::constant_time_size<false>
> TChannelList;

// ----------------------------------------------------------------------------
// 
// ChannelImpl
// 
// ----------------------------------------------------------------------------

class ChannelImpl
	: public Channel
	, public ChannelListHook
{
	f32             _panning;
	f32             _pitch;
	f32             _volume;
	StrId           _group;
	SoundPtr        _sound;
	
protected:
	
	virtual bool    PlayImpl        () = 0;
	virtual bool    CreateImpl      () = 0;
	virtual void    setPropertyImpl ( E_PROPERTY prop, f32 value ) = 0;
	virtual f32     getPropertyImpl ( E_PROPERTY prop ) const = 0;
	
	ChannelImpl(const StrId& group, const SoundPtr &sound);
	
public:
	
	static ChannelImpl* Create(const StrId& group, const SoundPtr &sound);
	virtual ~ChannelImpl();
	
	bool            Create          ( f32 volume, bool loop );
	
	// Channel override -------------------------------------------------------
	void            setProperty     ( E_PROPERTY prop, f32 value );
	f32             getProperty     ( E_PROPERTY prop )	const;
	const StrId&    getGroup        () const;
	const SoundPtr& getSound        () const;
	f32             getLength       () const;
	bool            isStreamed      () const;
	// ------------------------------------------------------------------------
};

} // namespace bme
