/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/core/Batches.h
 *****************************************************************************/

#pragma once

#include <bme/core/Primitives.h>

namespace bme
{

struct Batch
{
	size_t       primCount;
	size_t       startVertex;
	
	size_t       vertPerPrim;
	Material     material;
	RenderStates renderStates;
	
	Batch()
	: primCount(0), startVertex(0), vertPerPrim(0) {}
	
	Batch(
		size_t startVertex, size_t vertPerPrim,
		const Material& material, const RenderStates& renderStates)
	: primCount(0), startVertex(startVertex)
	, vertPerPrim(vertPerPrim), material(material), renderStates(renderStates) {}
	
	template < typename T >
	bool IsTypePrimitiveEqual(const T& prim) const
	{
		return
			 vertPerPrim == prim.numVertices &&
			    material == prim.material &&
			renderStates == prim.renderStates;
	}
};

class BatchContainer
{
public:
	
	typedef cpp::vector <Batch>::type TBatches;
	typedef cpp::vector<Vertex>::type TVertices;
	typedef cpp::vector   <u16>::type TIndices;
	
private:
	
	TBatches  _batches;
	TVertices _vertices;
	TIndices  _quadIndices;
	
	template < typename TPrim >
	bool IsAddedPossible(const TPrim&)
	{
		return
			_batches.size() < _batches.capacity() &&
			_vertices.size() + TPrim::numVertices <= _vertices.capacity();
	}
	
	bool IsAddedPossible(const Quad&)
	{
		if ( !_batches.empty() && _batches.back().vertPerPrim != Quad::numVertices )
			return false;
		return
			_batches.size() < _batches.capacity() &&
			_vertices.size() + Quad::numVertices <= _vertices.capacity();
	}
	
	template < typename TPrim >
	void AddPrimitiveVertices(const TPrim& prim)
	{
		_vertices.resize(_vertices.size() + prim.numVertices);
		BME_memcpy(&_vertices[_vertices.size() - prim.numVertices],
			&prim.vertices[0], prim.numVertices * sizeof(Vertex));
	}
	
	void AddPrimitiveVertices(const Quad& prim)
	{
		_vertices.resize(_vertices.size() + prim.numVertices);
		BME_memcpy(&_vertices[_vertices.size() - prim.numVertices],
			&prim.vertices[0], prim.numVertices * sizeof(Vertex));
	}
	
public:
	
	BatchContainer()
	{
		BME_ASSERT_MSG(
			BME_MAX_BATCH_VERTICES <= cpp::bounds<u16>::highest(),
			"BME_MAX_BATCH_VERTICES must be <= max short value");
		
		_batches    .reserve(BME_MAX_BATCHES);
		_vertices   .reserve(BME_MAX_BATCH_VERTICES);
		_quadIndices.reserve(BME_MAX_BATCH_VERTICES/4*6);
		
		for ( size_t i = 0, e = _quadIndices.capacity()/6; i < e; ++i ) {
			_quadIndices.push_back(static_cast<u16>(4*i + 0));
			_quadIndices.push_back(static_cast<u16>(4*i + 1));
			_quadIndices.push_back(static_cast<u16>(4*i + 2));
			_quadIndices.push_back(static_cast<u16>(4*i + 2));
			_quadIndices.push_back(static_cast<u16>(4*i + 3));
			_quadIndices.push_back(static_cast<u16>(4*i + 0));
		}
	}
	
	void Clear()
	{
		_batches.clear();
		_vertices.clear();
	}
	
	bool isEmpty() const
	{
		return _batches.empty();
	}
	
	const TBatches& getBatches() const
	{
		return _batches;
	}
	
	const Vertex* getFirstVertex() const
	{
		return _vertices.empty() ? NULL : &_vertices[0];
	}
	
	size_t getVerticesCount() const
	{
		return _vertices.size();
	}
	
	const u16* getFirstQuadIndex() const
	{
		return _quadIndices.empty() ? NULL : &_quadIndices[0];
	}
	
	size_t getQuadIndicesCount() const
	{
		return _quadIndices.size();
	}
	
	template < typename T >
	bool AddPrimitive(const T& prim)
	{
		if ( !IsAddedPossible(prim) ) {
			return false;
		}
		if ( _batches.empty() || !_batches.back().IsTypePrimitiveEqual(prim) ) {
			_batches.push_back(
				Batch(_vertices.size(), prim.numVertices, prim.material, prim.renderStates));
		}
		AddPrimitiveVertices(prim);
		++_batches.back().primCount;
		return true;
	}
};

} // namespace bme
