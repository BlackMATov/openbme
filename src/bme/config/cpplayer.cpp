/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/config/cpplayer.cpp
 *****************************************************************************/

#include <bme/config/cpplayer.h>
#include <bme/config/clayer.h>

// ----------------------------------------------------------------------------
// 
// For BOOST_ENABLE_ASSERT_HANDLER
// 
// ----------------------------------------------------------------------------

namespace boost
{
	void assertion_failed(
		char const * /*expr*/,
		char const * /*function*/,
		char const * /*file*/,
		long         /*line*/)
	{
		BME_ASSERT_MSG(false, "boost assertion failed");
	}
	
	void assertion_failed_msg(
		char const * /*expr*/,
		char const * /*msg*/,
		char const * /*function*/,
		char const * /*file*/,
		long         /*line*/)
	{
		BME_ASSERT_MSG(false, "boost assertion failed");
	}
} // namespace boost
