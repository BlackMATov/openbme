/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/math/vec2f.cpp
 *****************************************************************************/

#include <bme/math/vec2f.h>
#include <bme/math/vec3f.h>

namespace bme
{

const vec2f vec2f::zero    = vec2f( 0.f,  0.f);
const vec2f vec2f::one     = vec2f( 1.f,  1.f);
const vec2f vec2f::neg     = vec2f(-1.f, -1.f);
const vec2f vec2f::one_neg = vec2f( 1.f, -1.f);
const vec2f vec2f::neg_one = vec2f(-1.f,  1.f);
const vec2f vec2f::one_x   = vec2f( 1.f,  0.f);
const vec2f vec2f::one_y   = vec2f( 0.f,  1.f);
const vec2f vec2f::neg_x   = vec2f(-1.f,  0.f);
const vec2f vec2f::neg_y   = vec2f( 0.f, -1.f);

vec2f::vec2f()                   : x(0), y(0) {}
vec2f::vec2f(f32 n)              : x(n), y(n) {}
vec2f::vec2f(f32 x, f32 y)       : x(x), y(y) {}
vec2f::vec2f(const vec2f& other) : x(other.x), y(other.y) {}
vec2f::vec2f(const vec3f& other) : x(other.x), y(other.y) {}

} // namespace bme
