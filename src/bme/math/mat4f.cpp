/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/math/mat4f.cpp
 *****************************************************************************/

#include <bme/math/mat4f.h>

namespace bme
{

const mat4f mat4f::zero     = mat4f::MakeZero();
const mat4f mat4f::identity = mat4f::MakeIdentity();

mat4f& mat4f::Zero()
{
	BME_memset(&m, 0, sizeof(m));
	return *this;
}

mat4f& mat4f::Identity()
{
	Zero();
	a[ 0] =
	a[ 5] =
	a[10] =
	a[15] = 1.f;
	return *this;
}

mat4f& mat4f::Scale(const vec2f& v)
{
	Zero();
	a[ 0] = v.x;
	a[ 5] = v.y;
	a[10] = 1.f;
	a[15] = 1.f;
	return *this;
}

mat4f& mat4f::Scale(const vec3f& v)
{
	Zero();
	a[ 0] = v.x;
	a[ 5] = v.y;
	a[10] = v.z;
	a[15] = 1.f;
	return *this;
}

mat4f& mat4f::Translate(const vec2f& v)
{
	Identity();
	m._41 = v.x;
	m._42 = v.y;
	m._43 = 0.f;
	return *this;
}

mat4f& mat4f::Translate(const vec3f& v)
{
	Identity();
	m._41 = v.x;
	m._42 = v.y;
	m._43 = v.z;
	return *this;
}

mat4f& mat4f::Rotation(f32 angle, const vec2f& v)
{
	return Rotation(angle, vec3f(v.x, v.y, 0.f));
}

mat4f& mat4f::Rotation(f32 angle, const vec3f& v)
{
	f32 x   = v.x;
	f32 y   = v.y;
	f32 z   = v.z;
	f32 px  = x * x;
	f32 py  = y * y;
	f32 pz  = z * z;
	f32 cs  = cpp::cos(angle);
	f32 sn  = cpp::sin(angle);
	f32 ics = 1.f - cs;
	f32 xym = x * y * ics;
	f32 xzm = x * z * ics;
	f32 yzm = y * z * ics;
	f32 xsn = x * sn;
	f32 ysn = y * sn;
	f32 zsn = z * sn;
	a[ 0]   = px * ics + cs;
	a[ 1]   = xym - zsn;
	a[ 2]   = xzm + ysn;
	a[ 3]   = 0.f;
	a[ 4]   = xym + zsn;
	a[ 5]   = py * ics + cs;
	a[ 6]   = yzm - xsn;
	a[ 7]   = 0.f;
	a[ 8]   = xzm - ysn;
	a[ 9]   = yzm + xsn;
	a[10]   = pz * ics + cs;
	a[11]   = 0.f;
	a[12]   = 0.f;
	a[13]   = 0.f;
	a[14]   = 0.f;
	a[15]   = 1.f;
	return *this;
}

mat4f& mat4f::Orthogonal(const vec2f& size, f32 znear, f32 zfar)
{
	BME_ASSERT(!Math::IsEquals(size.x, 0.f));
	BME_ASSERT(!Math::IsEquals(size.y, 0.f));
	BME_ASSERT(!Math::IsEquals(znear, zfar));
	Zero();
	a[ 0] = 2.f / size.x;
	a[ 5] = 2.f / size.y;
	a[10] = 1.f / (zfar - znear);
	a[15] = 1.f;
	a[14] = znear / (znear - zfar);
	return *this;
}

mat4f& mat4f::Perspective(f32 fov, f32 aspect, f32 znear, f32 zfar)
{
	BME_ASSERT(!Math::IsEquals(aspect, 0.f));
	BME_ASSERT(!Math::IsEquals(znear, zfar));
	Zero();
	f32 h = 1.f / cpp::tan(fov * 0.5f);
	f32 w = h / aspect;
	a[ 0] = w;
	a[ 5] = h;
	a[10] = zfar / (zfar - znear);
	a[11] = 1.f;
	a[14] = -znear * zfar / (zfar - znear);
	return *this;
}

mat4f& mat4f::Inverse(const mat4f& in_m, bool* success)
{
	f32 d =
		(in_m.m._11 * in_m.m._22 - in_m.m._12 * in_m.m._21) * (in_m.m._33 * in_m.m._44 - in_m.m._34 * in_m.m._43) -
		(in_m.m._11 * in_m.m._23 - in_m.m._13 * in_m.m._21) * (in_m.m._32 * in_m.m._44 - in_m.m._34 * in_m.m._42) +
		(in_m.m._11 * in_m.m._24 - in_m.m._14 * in_m.m._21) * (in_m.m._32 * in_m.m._43 - in_m.m._33 * in_m.m._42) +
		(in_m.m._12 * in_m.m._23 - in_m.m._13 * in_m.m._22) * (in_m.m._31 * in_m.m._44 - in_m.m._34 * in_m.m._41) -
		(in_m.m._12 * in_m.m._24 - in_m.m._14 * in_m.m._22) * (in_m.m._31 * in_m.m._43 - in_m.m._33 * in_m.m._41) +
		(in_m.m._13 * in_m.m._24 - in_m.m._14 * in_m.m._23) * (in_m.m._31 * in_m.m._42 - in_m.m._32 * in_m.m._41);
	
	if ( Math::IsZero(d) ) {
		if ( success ) {
			*success = false;
		}
		return Translate(vec3f(-m._41, -m._42, -m._43));
	}
	
	d = 1.0f / d;
	
	m._11 = d * (
		in_m.m._22 * (in_m.m._33 * in_m.m._44 - in_m.m._34 * in_m.m._43) +
		in_m.m._23 * (in_m.m._34 * in_m.m._42 - in_m.m._32 * in_m.m._44) +
		in_m.m._24 * (in_m.m._32 * in_m.m._43 - in_m.m._33 * in_m.m._42));
	m._12 = d * (
		in_m.m._32 * (in_m.m._13 * in_m.m._44 - in_m.m._14 * in_m.m._43) +
		in_m.m._33 * (in_m.m._14 * in_m.m._42 - in_m.m._12 * in_m.m._44) +
		in_m.m._34 * (in_m.m._12 * in_m.m._43 - in_m.m._13 * in_m.m._42));
	m._13 = d * (
		in_m.m._42 * (in_m.m._13 * in_m.m._24 - in_m.m._14 * in_m.m._23) +
		in_m.m._43 * (in_m.m._14 * in_m.m._22 - in_m.m._12 * in_m.m._24) +
		in_m.m._44 * (in_m.m._12 * in_m.m._23 - in_m.m._13 * in_m.m._22));
	m._14 = d * (
		in_m.m._12 * (in_m.m._24 * in_m.m._33 - in_m.m._23 * in_m.m._34) +
		in_m.m._13 * (in_m.m._22 * in_m.m._34 - in_m.m._24 * in_m.m._32) +
		in_m.m._14 * (in_m.m._23 * in_m.m._32 - in_m.m._22 * in_m.m._33));
	m._21 = d * (
		in_m.m._23 * (in_m.m._31 * in_m.m._44 - in_m.m._34 * in_m.m._41) +
		in_m.m._24 * (in_m.m._33 * in_m.m._41 - in_m.m._31 * in_m.m._43) +
		in_m.m._21 * (in_m.m._34 * in_m.m._43 - in_m.m._33 * in_m.m._44));
	m._22 = d * (
		in_m.m._33 * (in_m.m._11 * in_m.m._44 - in_m.m._14 * in_m.m._41) +
		in_m.m._34 * (in_m.m._13 * in_m.m._41 - in_m.m._11 * in_m.m._43) +
		in_m.m._31 * (in_m.m._14 * in_m.m._43 - in_m.m._13 * in_m.m._44));
	m._23 = d * (
		in_m.m._43 * (in_m.m._11 * in_m.m._24 - in_m.m._14 * in_m.m._21) +
		in_m.m._44 * (in_m.m._13 * in_m.m._21 - in_m.m._11 * in_m.m._23) +
		in_m.m._41 * (in_m.m._14 * in_m.m._23 - in_m.m._13 * in_m.m._24));
	m._24 = d * (
		in_m.m._13 * (in_m.m._24 * in_m.m._31 - in_m.m._21 * in_m.m._34) +
		in_m.m._14 * (in_m.m._21 * in_m.m._33 - in_m.m._23 * in_m.m._31) +
		in_m.m._11 * (in_m.m._23 * in_m.m._34 - in_m.m._24 * in_m.m._33));
	m._31 = d * (
		in_m.m._24 * (in_m.m._31 * in_m.m._42 - in_m.m._32 * in_m.m._41) +
		in_m.m._21 * (in_m.m._32 * in_m.m._44 - in_m.m._34 * in_m.m._42) +
		in_m.m._22 * (in_m.m._34 * in_m.m._41 - in_m.m._31 * in_m.m._44));
	m._32 = d * (
		in_m.m._34 * (in_m.m._11 * in_m.m._42 - in_m.m._12 * in_m.m._41) +
		in_m.m._31 * (in_m.m._12 * in_m.m._44 - in_m.m._14 * in_m.m._42) +
		in_m.m._32 * (in_m.m._14 * in_m.m._41 - in_m.m._11 * in_m.m._44));
	m._33 = d * (
		in_m.m._44 * (in_m.m._11 * in_m.m._22 - in_m.m._12 * in_m.m._21) +
		in_m.m._41 * (in_m.m._12 * in_m.m._24 - in_m.m._14 * in_m.m._22) +
		in_m.m._42 * (in_m.m._14 * in_m.m._21 - in_m.m._11 * in_m.m._24));
	m._34 = d * (
		in_m.m._14 * (in_m.m._22 * in_m.m._31 - in_m.m._21 * in_m.m._32) +
		in_m.m._11 * (in_m.m._24 * in_m.m._32 - in_m.m._22 * in_m.m._34) +
		in_m.m._12 * (in_m.m._21 * in_m.m._34 - in_m.m._24 * in_m.m._31));
	m._41 = d * (
		in_m.m._21 * (in_m.m._33 * in_m.m._42 - in_m.m._32 * in_m.m._43) +
		in_m.m._22 * (in_m.m._31 * in_m.m._43 - in_m.m._33 * in_m.m._41) +
		in_m.m._23 * (in_m.m._32 * in_m.m._41 - in_m.m._31 * in_m.m._42));
	m._42 = d * (
		in_m.m._31 * (in_m.m._13 * in_m.m._42 - in_m.m._12 * in_m.m._43) +
		in_m.m._32 * (in_m.m._11 * in_m.m._43 - in_m.m._13 * in_m.m._41) +
		in_m.m._33 * (in_m.m._12 * in_m.m._41 - in_m.m._11 * in_m.m._42));
	m._43 = d * (
		in_m.m._41 * (in_m.m._13 * in_m.m._22 - in_m.m._12 * in_m.m._23) +
		in_m.m._42 * (in_m.m._11 * in_m.m._23 - in_m.m._13 * in_m.m._21) +
		in_m.m._43 * (in_m.m._12 * in_m.m._21 - in_m.m._11 * in_m.m._22));
	m._44 = d * (
		in_m.m._11 * (in_m.m._22 * in_m.m._33 - in_m.m._23 * in_m.m._32) +
		in_m.m._12 * (in_m.m._23 * in_m.m._31 - in_m.m._21 * in_m.m._33) +
		in_m.m._13 * (in_m.m._21 * in_m.m._32 - in_m.m._22 * in_m.m._31));
	
	if ( success ) {
		*success = true;
	}
	return *this;
}

} // namespace bme
