/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/math/vec3f.cpp
 *****************************************************************************/

#include <bme/math/vec3f.h>
#include <bme/math/vec2f.h>

namespace bme
{

const vec3f vec3f::zero  = vec3f( 0.f, 0.f, 0.f);
const vec3f vec3f::one   = vec3f( 1.f, 1.f, 1.f);
const vec3f vec3f::neg   = vec3f(-1.f,-1.f,-1.f);
const vec3f vec3f::one_x = vec3f( 1.f, 0.f, 0.f);
const vec3f vec3f::one_y = vec3f( 0.f, 1.f, 0.f);
const vec3f vec3f::one_z = vec3f( 0.f, 0.f, 1.f);
const vec3f vec3f::neg_x = vec3f(-1.f, 0.f, 0.f);
const vec3f vec3f::neg_y = vec3f( 0.f,-1.f, 0.f);
const vec3f vec3f::neg_z = vec3f( 0.f, 0.f,-1.f);

vec3f::vec3f()                          : x(0), y(0), z(0) {}
vec3f::vec3f(f32 n)                     : x(n), y(n), z(n) {}
vec3f::vec3f(f32 x, f32 y, f32 z)       : x(x), y(y), z(z) {}
vec3f::vec3f(const vec3f& other)        : x(other.x), y(other.y), z(other.z) {}
vec3f::vec3f(const vec2f& other, f32 z) : x(other.x), y(other.y), z(z) {}

} // namespace bme
