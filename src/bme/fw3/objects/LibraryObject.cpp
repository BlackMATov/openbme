/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/LibraryObject.cpp
 *****************************************************************************/

#include <bme/fw3/objects/LibraryObject.h>
#include <bme/fw3/assets/LibraryAsset.h>

namespace bme
{
namespace fw3
{

LibraryObject::LibraryObject()
{
}

LibraryObject::LibraryObject(const LibraryAssetPtr& asset)
: _asset(asset)
{
}

LibraryObject::~LibraryObject()
{
}

void LibraryObject::setName(const StrId& value)
{
	_name = value;
}

const StrId& LibraryObject::getName() const
{
	return _name;
}

const LibraryAssetPtr& LibraryObject::getAsset() const
{
	return _asset;
}

} // namespace fw3
} // namespace bme
