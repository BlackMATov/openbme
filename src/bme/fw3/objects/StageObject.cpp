/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/StageObject.cpp
 *****************************************************************************/

#include <bme/fw3/objects/StageObject.h>

namespace bme
{
namespace fw3
{

// ----------------------------------------------------------------------------
// 
// Private
// 
// ----------------------------------------------------------------------------

bool StageObject::OnEvent(const Receiver::Event& /*event*/)
{
	return true;
}

// ----------------------------------------------------------------------------
// 
// Protected
// 
// ----------------------------------------------------------------------------

void StageObject::_postRenderSelf()
{
}

void StageObject::_postUpdateSelf(f32 /*dt*/)
{
}

StageObject::StageObject()
{
}

// ----------------------------------------------------------------------------
// 
// Public
// 
// ----------------------------------------------------------------------------

StageObjectPtr StageObject::Create()
{
	return StageObjectPtr(new StageObject());
}

StageObject::~StageObject()
{
}

} // namespace fw3
} // namespace bme
