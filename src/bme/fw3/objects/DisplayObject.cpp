/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/DisplayObject.cpp
 *****************************************************************************/

#include <bme/fw3/objects/DisplayObject.h>
#include <bme/fw3/objects/StageObject.h>

namespace bme
{
namespace fw3
{

// ----------------------------------------------------------------------------
// 
// DisplayObject::Transform
// 
// ----------------------------------------------------------------------------

DisplayObject::Transform::Transform()
: alpha(1.f)
, visible(true)
, blend(RenderStates::BLEND_ALPHA) {}

DisplayObject::Transform DisplayObject::Transform::operator*(
	const DisplayObject::Transform& other) const
{
	// other is parent
	Transform temp;
	temp.matrix  = matrix   * other.matrix;
	temp.alpha   = alpha    * other.alpha;
	temp.visible = visible && other.visible;
	temp.blend   = RenderStates::BLEND_ALPHA == other.blend ? blend : other.blend;
	return temp;
}

bool DisplayObject::Transform::operator==(const Transform& other) const
{
	return
		matrix  == other.matrix &&
		Math::IsEquals(alpha, other.alpha) &&
		visible == other.visible &&
		blend   == other.blend;
}

bool DisplayObject::Transform::operator!=(const Transform& other) const
{
	return
		matrix  != other.matrix ||
		!Math::IsEquals(alpha, other.alpha) ||
		visible != other.visible ||
		blend   != other.blend;
}

// ----------------------------------------------------------------------------
// 
// DisplayObject::ChildDef
// 
// ----------------------------------------------------------------------------

DisplayObject::ChildDef::ChildDef(const DisplayObjectPtr& child)
: child(child)
{
}

bool DisplayObject::ChildDef::operator==(const ChildDef& rhs) const
{
	return child == rhs.child;
}

bool DisplayObject::ChildDef::operator!=(const ChildDef& rhs) const
{
	return child != rhs.child;
}

// ----------------------------------------------------------------------------
// 
// Private
// 
// ----------------------------------------------------------------------------

void DisplayObject::_updateLocalBounds() const
{
	const rectf& self_bounds = getSelfBounds();
	if ( self_bounds != rectf::zero ) {
		vec2f tl = vec2f(getSelfBounds().getTopLeft());
		vec2f tr = vec2f(getSelfBounds().getTopRight());
		vec2f bl = vec2f(getSelfBounds().getBottomLeft());
		vec2f br = vec2f(getSelfBounds().getBottomRight());
		
		const Transform& transform = getGlobalTransform();
		transform.matrix.ApplyToVector(tl);
		transform.matrix.ApplyToVector(tr);
		transform.matrix.ApplyToVector(bl);
		transform.matrix.ApplyToVector(br);
		
		_localBounds = rectf(tl.x, tl.y, tl.x, tl.y);
		_localBounds.AddPoint(pnt2f(tr));
		_localBounds.AddPoint(pnt2f(bl));
		_localBounds.AddPoint(pnt2f(br));
	}
	
	_invalidLocalBounds = false;
	_localBoundsUpdated();
}

void DisplayObject::_updateGlobalBounds() const
{
	_globalBounds = getLocalBounds();
	for ( TChildren::const_iterator
		iter = _children.begin(), end = _children.end();
		iter != end; ++iter )
	{
		const DisplayObjectPtr& child = (*iter).child;
		if ( child ) {
			const rectf& child_bounds = child->getGlobalBounds();
			if ( child_bounds != rectf::zero ) {
				if ( _globalBounds == rectf::zero ) {
					_globalBounds = child_bounds;
				} else {
					_globalBounds.Merge(child_bounds);
				}
			}
		}
	}
	
	_invalidGlobalBounds = false;
	_globalBoundsUpdated();
}

void DisplayObject::_updateLocalTransform() const
{
	_localTransform.matrix =
		mat4f::MakeScale     (getScale   ())               *
		mat4f::MakeRotation  (getRotation(), vec3f::neg_z) *
		mat4f::MakeTranslate (getPosition());
	
	_invalidLocalTransform = false;
	_localTransformUpdated();
}

void DisplayObject::_updateGlobalTransform() const
{
	if ( getParent() ) {
		_globalTransform = getLocalTransform() * getParent()->getGlobalTransform();
	} else {
		_globalTransform = getLocalTransform();
	}
	
	_invalidGlobalTransform = false;
	_globalTransformUpdated();
}

void DisplayObject::_setParent(DisplayObject* parent)
{
	_parent = parent;
	_invalidateGlobalTransform();
	
	/** \todo EVENT
	if ( parent ) {
		Event e(Event::ON_ADDED, false, false);
		e.SendTo(this);
	} else {
		Event e(Event::ON_REMOVED, false, false);
		e.SendTo(this);
	}*/
}

// ----------------------------------------------------------------------------
// 
// Protected
// 
// ----------------------------------------------------------------------------

void DisplayObject::_invalidateLocalBounds()
{
	_invalidLocalBounds = true;
	_invalidateGlobalBounds();
}

void DisplayObject::_invalidateGlobalBounds()
{
	if ( !_invalidGlobalBounds ) {
		_invalidGlobalBounds = true;
		if ( getParent() ) {
			getParent()->_invalidateGlobalBounds();
		}
	}
}

void DisplayObject::_invalidateLocalTransform()
{
	_invalidLocalTransform = true;
	_invalidateGlobalTransform();
}

void DisplayObject::_invalidateGlobalTransform()
{
	if ( !_invalidGlobalTransform ) {
		_invalidGlobalTransform = true;
		for ( TChildren::const_iterator
			iter = _children.begin(), end = _children.end();
			iter != end; ++iter )
		{
			const DisplayObjectPtr& child = (*iter).child;
			if ( child ) {
				child->_invalidateGlobalTransform();
			}
		}
	}
	_invalidateLocalBounds();
}

void DisplayObject::_localBoundsUpdated() const
{
}

void DisplayObject::_globalBoundsUpdated() const
{
}

void DisplayObject::_localTransformUpdated() const
{
}

void DisplayObject::_globalTransformUpdated() const
{
}

void DisplayObject::_renderSelf()
{
}

void DisplayObject::_updateSelf(f32 /*dt*/)
{
}

void DisplayObject::_postRenderSelf()
{
}

void DisplayObject::_postUpdateSelf(f32 /*dt*/)
{
}

const rectf& DisplayObject::_getSelfBounds() const
{
	return rectf::zero;
}

DisplayObject::DisplayObject()
: LibraryObject()
, _position(vec2f::zero)
, _scale(vec2f::one)
, _rotation(0.f)
, _invalidLocalBounds(true)
, _invalidGlobalBounds(true)
, _invalidLocalTransform(true)
, _invalidGlobalTransform(true)
, _parent(NULL)
, _childrenNum(0)
{
}

DisplayObject::DisplayObject(const LibraryAssetPtr& asset)
: LibraryObject(asset)
, _position(vec2f::zero)
, _scale(vec2f::one)
, _rotation(0.f)
, _invalidLocalBounds(true)
, _invalidGlobalBounds(true)
, _invalidLocalTransform(true)
, _invalidGlobalTransform(true)
, _parent(NULL)
, _childrenNum(0)
{
}

// ----------------------------------------------------------------------------
// 
// Public
// 
// ----------------------------------------------------------------------------

const size_t DisplayObject::bad_index = size_t(-1);

DisplayObjectPtr DisplayObject::Create()
{
	return DisplayObjectPtr(new DisplayObject());
}

DisplayObject::~DisplayObject()
{
	BME_ASSERT(!_parent);
	RemoveChildren();
}

// ----------------------------------------------------------------------------
// 
// Events
// 
// ----------------------------------------------------------------------------

void DisplayObject::Render()
{
	if ( !isVisible() || Math::IsZero(getAlpha()) ) {
		return;
	}
	
	// safe RemoveFromParent
	DisplayObjectPtr copy_self(this);
	
	_renderSelf();
	for ( TChildren::iterator
		iter = _children.begin(), end = _children.end();
		iter != end; )
	{
		const DisplayObjectPtr& child = (*iter).child;
		if ( child ) {
			child->Render();
			++iter;
		} else {
			iter = _children.erase(iter);
		}
	}
	_postRenderSelf();
}

void DisplayObject::Update(f32 dt)
{
	// safe RemoveFromParent
	DisplayObjectPtr copy_self(this);
	
	_updateSelf(dt);
	for ( TChildren::iterator
		iter = _children.begin(), end = _children.end();
		iter != end; )
	{
		const DisplayObjectPtr& child = (*iter).child;
		if ( child ) {
			child->Update(dt);
			++iter;
		} else {
			iter = _children.erase(iter);
		}
	}
	_postUpdateSelf(dt);
}

// ----------------------------------------------------------------------------
// 
// Transformations
// 
// ----------------------------------------------------------------------------

const DisplayObject::Transform& DisplayObject::getLocalTransform() const
{
	if ( _invalidLocalTransform ) {
		_updateLocalTransform();
	}
	return _localTransform;
}

const DisplayObject::Transform& DisplayObject::getGlobalTransform() const
{
	if ( _invalidGlobalTransform ) {
		_updateGlobalTransform();
	}
	return _globalTransform;
}

// ----------------------------------------------------------------------------
// 
// Bounds
// 
// ----------------------------------------------------------------------------

const rectf& DisplayObject::getSelfBounds() const
{
	return _getSelfBounds();
}

const rectf& DisplayObject::getLocalBounds() const
{
	if ( _invalidLocalBounds ) {
		_updateLocalBounds();
	}
	return _localBounds;
}

const rectf& DisplayObject::getGlobalBounds() const
{
	if ( _invalidGlobalBounds ) {
		_updateGlobalBounds();
	}
	return _globalBounds;
}

// ----------------------------------------------------------------------------
// 
// Position
// 
// ----------------------------------------------------------------------------

f32 DisplayObject::getX() const
{
	return _position.x;
}

f32 DisplayObject::getY() const
{
	return _position.y;
}

const vec2f& DisplayObject::getPosition() const
{
	return _position;
}

void DisplayObject::setX(f32 value)
{
	_position.x = value;
	_invalidateLocalTransform();
}

void DisplayObject::setY(f32 value)
{
	_position.y = value;
	_invalidateLocalTransform();
}

void DisplayObject::setPosition(f32 x, f32 y)
{
	_position.x = x;
	_position.y = y;
	_invalidateLocalTransform();
}

void DisplayObject::setPosition(const vec2f& value)
{
	_position = value;
	_invalidateLocalTransform();
}

void DisplayObject::MoveX(f32 value)
{
	_position.x += value;
	_invalidateLocalTransform();
}

void DisplayObject::MoveY(f32 value)
{
	_position.y += value;
	_invalidateLocalTransform();
}

void DisplayObject::Move(f32 x, f32 y)
{
	_position.x += x;
	_position.y += y;
	_invalidateLocalTransform();
}

void DisplayObject::Move(const vec2f& value)
{
	_position += value;
	_invalidateLocalTransform();
}

// ----------------------------------------------------------------------------
// 
// Scale
// 
// ----------------------------------------------------------------------------

f32 DisplayObject::getScaleX() const
{
	return _scale.x;
}

f32 DisplayObject::getScaleY() const
{
	return _scale.y;
}

const vec2f& DisplayObject::getScale() const
{
	return _scale;
}

void DisplayObject::setScaleX(f32 value)
{
	_scale.x = value;
	_invalidateLocalTransform();
}

void DisplayObject::setScaleY(f32 value)
{
	_scale.y = value;
	_invalidateLocalTransform();
}

void DisplayObject::setScale(f32 scale)
{
	_scale = vec2f(scale);
	_invalidateLocalTransform();
}

void DisplayObject::setScale(f32 x, f32 y)
{
	_scale.x = x;
	_scale.y = y;
	_invalidateLocalTransform();
}

void DisplayObject::setScale(const vec2f& value)
{
	_scale = value;
	_invalidateLocalTransform();
}

void DisplayObject::ScaleX(f32 value)
{
	_scale.x += value;
	_invalidateLocalTransform();
}

void DisplayObject::ScaleY(f32 value)
{
	_scale.y += value;
	_invalidateLocalTransform();
}

void DisplayObject::Scale(f32 scale)
{
	_scale = vec2f(scale);
	_invalidateLocalTransform();
}

void DisplayObject::Scale(f32 x, f32 y)
{
	_scale.x += x;
	_scale.y += y;
	_invalidateLocalTransform();
}

void DisplayObject::Scale(const vec2f& value)
{
	_scale += value;
	_invalidateLocalTransform();
}

// ----------------------------------------------------------------------------
// 
// Rotation
// 
// ----------------------------------------------------------------------------

f32 DisplayObject::getRotation() const
{
	return _rotation;
}

void DisplayObject::setRotation(f32 value)
{
	_rotation = value;
	_invalidateLocalTransform();
}

void DisplayObject::Rotate(f32 value)
{
	_rotation += value;
	_invalidateLocalTransform();
}

// ----------------------------------------------------------------------------
// 
// Size
// 
// ----------------------------------------------------------------------------

f32 DisplayObject::getWidth() const
{
	return getSize().x;
}

f32 DisplayObject::getHeight() const
{
	return getSize().y;
}

vec2f DisplayObject::getSize() const
{
	return vec2f(getGlobalBounds().getSize());
}

void DisplayObject::setWidth(f32 value)
{
	setSize(value, getHeight());
}

void DisplayObject::setHeight(f32 value)
{
	setSize(getWidth(), value);
}

void DisplayObject::setSize(f32 w, f32 h)
{
	setSize(vec2f(w, h));
}

void DisplayObject::setSize(const vec2f& value)
{
	vec2f size = getSize();
	if ( !Math::IsZero(size.x) && !Math::IsZero(size.y) ) {
		setScale(value / size * getScale());
	}
}

void DisplayObject::ResizeWidth(f32 value)
{
	setWidth(getWidth() + value);
}

void DisplayObject::ResizeHeight(f32 value)
{
	setHeight(getHeight() + value);
}

void DisplayObject::Resize(f32 x, f32 y)
{
	setSize(getSize() + vec2f(x,y));
}

void DisplayObject::Resize(const vec2f& value)
{
	setSize(getSize() + value);
}

// ----------------------------------------------------------------------------
// 
// Visible
// 
// ----------------------------------------------------------------------------

void DisplayObject::setAlpha(f32 value)
{
	_localTransform.alpha = value;
	_invalidateLocalTransform();
}

void DisplayObject::setVisible(bool yesno)
{
	_localTransform.visible = yesno;
	_invalidateLocalTransform();
}

void DisplayObject::setBlend(RenderStates::E_BLEND blend)
{
	_localTransform.blend = blend;
	_invalidateLocalTransform();
}

f32 DisplayObject::getAlpha() const
{
	return getLocalTransform().alpha;
}

bool DisplayObject::isVisible() const
{
	return getLocalTransform().visible;
}

RenderStates::E_BLEND DisplayObject::getBlend() const
{
	return getLocalTransform().blend;
}

// ----------------------------------------------------------------------------
// 
// Mouse
// 
// ----------------------------------------------------------------------------

f32 DisplayObject::getMouseX() const
{
	return getMousePosition().x;
}

f32 DisplayObject::getMouseY() const
{
	return getMousePosition().y;
}

vec2f DisplayObject::getMousePosition() const
{
	if ( theMouse() ) {
		vec2f p = LocalToGlobal(vec2f::zero);
		return theMouse()->getPosition() - p;
	}
	return vec2f::zero;
}

// ----------------------------------------------------------------------------
// 
// Methods
// 
// ----------------------------------------------------------------------------

vec2f DisplayObject::GlobalToLocal(const vec2f& point) const
{
	const Transform& trans = getGlobalTransform();
	return mat4f::MakeInverse(trans.matrix).ApplyToVector(point);
}

vec2f DisplayObject::LocalToGlobal(const vec2f& point) const
{
	return getGlobalTransform().matrix.ApplyToVector(point);
}

// ----------------------------------------------------------------------------
// 
// Container
// 
// ----------------------------------------------------------------------------

DisplayObjectPtr DisplayObject::getRoot() const
{
	DisplayObjectPtr root = DisplayObjectPtr(const_cast<DisplayObject*>(this));
	while ( root->getParent() ) {
		root = root->getParent();
	}
	return root;
}

DisplayObjectPtr DisplayObject::getParent() const
{
	return DisplayObjectPtr(_parent);
}

void DisplayObject::AddChild(const DisplayObjectPtr& child)
{
	BME_ASSERT(child && this != child.get());
	DisplayObjectPtr my_child = child;
	my_child->RemoveFromParent();
	
	_invalidateGlobalBounds();
	++_childrenNum;
	_children.push_back(my_child);
	my_child->_setParent(this);
}

bool DisplayObject::RemoveChild(const DisplayObjectPtr& child)
{
	BME_ASSERT(child && this != child.get());
	DisplayObjectPtr child_parent = child->getParent();
	if ( child_parent && this == child_parent.get() ) {
		TChildren::iterator iter = cpp::find(
			_children.begin(), _children.end(), child);
		BME_ASSERT(iter != _children.end());
		ChildDef& child_def = *iter;
		DisplayObjectPtr my_child = child_def.child;
		BME_ASSERT(child == my_child);
		_invalidateGlobalBounds();
		--_childrenNum;
		child_def.child.reset();
		my_child->_setParent(NULL);
		return true;
	}
	return false;
}

void DisplayObject::RemoveChildren()
{
	for ( TChildren::iterator
		iter = _children.begin(), end = _children.end();
		iter != end; ++iter )
	{
		ChildDef& child_def = *iter;
		if ( child_def.child ) {
			DisplayObjectPtr my_child = child_def.child;
			_invalidateGlobalBounds();
			--_childrenNum;
			child_def.child.reset();
			my_child->_setParent(NULL);
		}
	}
}

void DisplayObject::RemoveFromParent()
{
	if ( getParent() ) {
		getParent()->RemoveChild(this);
	}
}

DisplayObjectPtr DisplayObject::GetChildByName(
	const StrId& name, bool recursive) const
{
	for ( TChildren::const_iterator
		iter = _children.begin(), end = _children.end();
		iter != end; ++iter )
	{
		const ChildDef& def = (*iter);
		if ( def.child && def.child->getName() == name ) {
			return def.child;
		}
	}
	
	if ( recursive )
	{
		for ( TChildren::const_iterator
			iter = _children.begin(), end = _children.end();
			iter != end; ++iter )
		{
			const ChildDef& child_def = (*iter);
			if ( child_def.child ) {
				DisplayObjectPtr found_child =
					child_def.child->GetChildByName(name, recursive);
				if ( found_child ) {
					return found_child;
				}
			}
		}
	}
	
	return DisplayObjectPtr();
}

DisplayObjectPtr DisplayObject::GetChildByIndex(size_t index) const
{
	size_t i = 0;
	for ( TChildren::const_iterator
		iter = _children.begin(), end = _children.end();
		iter != end; ++iter )
	{
		const ChildDef& def = (*iter);
		if ( def.child ) {
			if ( i == index ) {
				return def.child;
			} else {
				++i;
			}
		}
	}
	return DisplayObjectPtr();
}

size_t DisplayObject::GetChildIndex(const DisplayObjectPtr& child) const
{
	if ( !child || this == child || this != child->getParent() ) {
		return DisplayObject::bad_index;
	}
	
	size_t i = 0;
	for ( TChildren::const_iterator
		iter = _children.begin(), end = _children.end();
		iter != end; ++iter )
	{
		const ChildDef& def = (*iter);
		if ( def.child ) {
			if ( def.child == child ) {
				return i;
			} else {
				++i;
			}
		}
	}
	return DisplayObject::bad_index;
}

bool DisplayObject::IsHasChild(
	const DisplayObjectPtr& child, bool recursive) const
{
	if ( !child || this == child || !child->getParent() ) {
		return false;
	}
	
	if ( this == child->getParent() ) {
		return true;
	}
	
	if ( recursive ) {
		for ( TChildren::const_iterator
			iter = _children.begin(), end = _children.end();
			iter != end; ++iter )
		{
			const ChildDef& def = (*iter);
			if ( def.child && def.child->IsHasChild(child, recursive) ) {
				return true;
			}
		}
	}
	
	return false;
}

const DisplayObject::TChildren& DisplayObject::getChildren() const
{
	return _children;
}

size_t DisplayObject::getNumChildren(bool recursive) const
{
	size_t ret_value = _childrenNum;
	if ( recursive ) {
		for ( TChildren::const_iterator
			iter = _children.begin(), end = _children.end();
			iter != end; ++iter )
		{
			const ChildDef& child_def = (*iter);
			const DisplayObjectPtr& child = child_def.child;
			if ( child ) {
				ret_value += child->getNumChildren(recursive);
			}
		}
	}
	return ret_value;
}

} // namespace fw3
} // namespace bme
