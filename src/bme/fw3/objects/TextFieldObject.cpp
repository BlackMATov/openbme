/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/TextFieldObject.cpp
 *****************************************************************************/

#include <bme/fw3/objects/TextFieldObject.h>

namespace bme
{
namespace fw3
{

// ----------------------------------------------------------------------------
// 
// Private
// 
// ----------------------------------------------------------------------------

void TextFieldObject::_updateQuads() const
{
	_quads.clear();
	_selfBounds = rectf::zero;
	_invalidQuads = false;
	
	const Transform& transform = getGlobalTransform();
	FontAssetPtr font_asset = cpp::static_pointer_cast<FontAsset>(getAsset());
	if ( !font_asset ) {
		return;
	}
	
	// update quads common
	{
		QuadHelper qh;
		qh.quad.material.filter = isSmoothing();
		qh.quad.renderStates.blend = transform.blend;
		qh.CalculateColors(Color(
			getColor().getR(), getColor().getG(), getColor().getB(),
			getColor().getA() * Math::Clamp(transform.alpha, 0.f, 1.f)));
		_quads.resize(_glyphs.size(), qh);
	}
	
	f32  text_left   = cpp::bounds<f32>::highest();
	f32  text_top    = cpp::bounds<f32>::highest();
	f32  text_right  = cpp::bounds<f32>::lowest();
	f32  text_bottom = cpp::bounds<f32>::lowest();
	bool has_bound   = false;
	
	// update quads geometry
	{
		vec2f dpos;
		
		f32 text_height = 0.f;
		dpos.y = _calcVAlignmentOffset(&text_height);
		
		f32 vspace_added_size = 0.f;
		if ( getVAlignment() == VALIGN_JUSTIFY ) {
			dpos.y = 0.f;
			if ( _strings.size() > 0 ) {
				vspace_added_size = (_getMaxStringHeight() - text_height)/(_strings.size() - 1);
			}
		}
		
		{
			text_top    = dpos.y;
			text_bottom = text_top + text_height;
			
			if ( _strings.size() > 0 ) {
				text_bottom += vspace_added_size * (_strings.size() - 1);
			}
		}
		
		for ( size_t i = 0, e = _strings.size(); i != e; ++i ) {
			const StringDesc& strDesc = _strings[i];
			f32 str_width = strDesc.width;
			
			dpos.x = _calcHAlignmentOffset(str_width);
			
			f32 hspace_added_size = 0.f;
			if ( getHAlignment() >= HALIGN_JUSTIFY_LEFT && getHAlignment() <= HALIGN_JUSTIFY_FULL &&
				(strDesc.justifiable || getHAlignment() == HALIGN_JUSTIFY_FULL) )
			{
				dpos.x = 0.f;
				if ( strDesc.space_count > 0 ) {
					hspace_added_size = (_getMaxStringWidth() - strDesc.width)/strDesc.space_count;
				}
			}
			
			{
				text_left  = cpp::min(
					text_left, dpos.x);
				text_right = cpp::max(
					text_right, text_left + str_width + strDesc.space_count * hspace_added_size);
			}
			
			for ( size_t i = strDesc.start, e = strDesc.start + strDesc.length; i < e; ++i ) {
				QuadHelper& qh = _quads[i];
				GlyphDesc& glyph_desc = _glyphs[i];
				if ( glyph_desc.glyph ) {
					has_bound = true;
					
					dpos.x += glyph_desc.kern * _internalScale;
					
					if ( glyph_desc.glyph->id == ' ' ) {
						dpos.x += hspace_added_size;
					}
					
					qh.quad.material.texture = font_asset->FindPage(glyph_desc.glyph->page);
					qh.CalculateTexCoords(glyph_desc.glyph->pageCoords);
					
					qh.CalculateVertices(
						mat4f::MakeTranslate(dpos + glyph_desc.glyph->offset * _internalScale) * transform.matrix,
						vec2f(glyph_desc.glyph->pageCoords.getSize()) * _internalScale, vec2f::zero, false, false);
					
					dpos.x += glyph_desc.glyph->advance * _internalScale;
				}
			}
			
			dpos.y += (getLineSpacing() + _info.lineHeight) * _internalScale + vspace_added_size;
		}
	}
	
	// update self bounds
	if ( has_bound ) {
		bool fixed_width  = !Math::IsZero(getFixedWidth ());
		bool fixed_height = !Math::IsZero(getFixedHeight());
		_selfBounds = rectf(
			!fixed_width  ? text_left   : 0.f,
			!fixed_height ? text_top    : 0.f,
			!fixed_width  ? text_right  : getFixedWidth (),
			!fixed_height ? text_bottom : getFixedHeight());
	}
}

void TextFieldObject::_updateGlyphs() const
{
	_glyphs.clear();
	_invalidGlyphs = false;
	
	FontAssetPtr font_asset = cpp::static_pointer_cast<FontAsset>(getAsset());
	if ( !font_asset ) {
		return;
	}
	
	// update glyphs
	{
		_glyphs.resize(_utext.length());
		for ( size_t i = 0, e = _utext.length(); i != e; ++i ) {
			FontAsset::TChar symbol = _utext[i];
			const FontAsset::Glyph* glyph = font_asset->FindGlyph(symbol);
			_glyphs[i].glyph = glyph;
			
			if ( i > 0 ) {
				_glyphs[i].kern = Math::SmartCast<f32>(
					font_asset->FindKerning(_glyphs[i-1].glyph, glyph));
			}
			
			if ( !glyph && symbol != '\n' ) {
				BME_LOG_WARNING_FMT(
					"TextFieldObject::_updateQuads() not found symbol(%1%) in font asset(%2%)",
					symbol, font_asset->getPath());
			}
		}
	}
}

void TextFieldObject::_updateStrings() const
{
	_strings.clear();
	_strings.reserve(32);
	_internalScale  = 1.f;
	_maxStringWidth = 0.f;
	_invalidStrings = false;
	
	FontAssetPtr font_asset = cpp::static_pointer_cast<FontAsset>(getAsset());
	if ( !font_asset ) {
		return;
	}
	
	f32 last_bad_scale      = 1.f;
	f32 last_good_scale     = 0.01f;
	f32 internal_scale_step = 0.01f;
	
// ------------------------------------
UPDATE_AGAIN_TAG:
// ------------------------------------
	
	_strings.clear();
	_strings.push_back(StringDesc(0));
	StringDesc* str_back = &_strings.back();
	_maxStringWidth = 0.f;
	
	size_t last_space       = size_t(-1);
	f32    last_space_width = 0.f;
	size_t last_space_count = 0;
	size_t space_count      = 0;
	
	for ( size_t i = 0, e = _utext.length(); i != e; ++i ) {
		FontAsset::TChar symbol = _utext[i];
		const GlyphDesc& glyph_desc = _glyphs[i];
		
		if ( symbol == ' ' ) {
			last_space       = i;
			last_space_width = str_back->width;
			last_space_count = space_count;
			++space_count;
		}
		
		bool new_line = false;
		
		if ( symbol == '\n' ) {
			new_line              = true;
			str_back->length      = i - str_back->start;
			str_back->space_count = space_count;
			str_back->justifiable = false;
		} else if ( glyph_desc.glyph ) {
			// for 'new line by symbol'
			//float last_width = str_back->width;
			
			str_back->width +=
				(glyph_desc.kern + glyph_desc.glyph->advance) * _internalScale;
			
			if ( !Math::IsZero(getFixedWidth()) && str_back->width > getFixedWidth() ) {
				if ( size_t(-1) != last_space ) {
					new_line              = true;
					str_back->width       = last_space_width;
					str_back->length      = last_space - str_back->start;
					str_back->space_count = last_space_count;
					str_back->justifiable = true;
					
					i = last_space;
				}
				/* 'new line by symbol'
				else if ( i - str_back->start > 0 )
				{
					new_line              = true;
					str_back->width       = last_width;
					str_back->length      = i - str_back->start;
					str_back->space_count = space_count;
					str_back->justifiable = true;
					
					--i;
				}*/
				else {
					if ( !Math::IsEquals(last_bad_scale, last_good_scale) ) {
						goto TRY_SCALE_DOWN_TAG;
					}
				}
			}
		}
		
		if ( i == e - 1 ) {
			new_line              = true;
			str_back->length      = i + 1 - str_back->start;
			str_back->space_count = space_count;
			str_back->justifiable = false;
		}
		
		if ( new_line ) {
			_maxStringWidth = cpp::max(_getMaxStringWidth(), str_back->width);
			
			if ( i < e - 1 ) {
				_strings.push_back(StringDesc(i + 1));
				str_back = &_strings.back();
			}
			
			last_space       = size_t(-1);
			last_space_width = 0.f;
			last_space_count = 0;
			space_count      = 0;
			
			if ( !Math::IsZero(getFixedHeight()) && _getStringHeight() > getFixedHeight() ) {
				if ( !Math::IsEquals(last_bad_scale, last_good_scale) ) {
					goto TRY_SCALE_DOWN_TAG;
				}
			}
		}
	}
	
	if ( !Math::IsZero(getFixedWidth()) || !Math::IsZero(getFixedHeight()) ) {
		if ( cpp::abs(last_good_scale-last_bad_scale) > internal_scale_step ) {
			goto TRY_SCALE_UP_TAG;
		} else if ( !Math::IsEquals(last_bad_scale, last_good_scale) ) {
			_internalScale = last_good_scale;
			last_bad_scale = last_good_scale;
			goto UPDATE_AGAIN_TAG;
		}
	}
	
	return;
	
// ------------------------------------
TRY_SCALE_DOWN_TAG:
// ------------------------------------
	{
		last_bad_scale = cpp::min(last_bad_scale, _internalScale);
		_internalScale = 0.5f * (_internalScale + last_good_scale);
		goto UPDATE_AGAIN_TAG;
	}
// ------------------------------------
TRY_SCALE_UP_TAG:
// ------------------------------------
	{
		last_good_scale = cpp::max(last_good_scale, _internalScale);
		_internalScale = 0.5f * (_internalScale + last_bad_scale);
		goto UPDATE_AGAIN_TAG;
	}
}

void TextFieldObject::_invalidateText(bool quads, bool glyphs, bool strings)
{
	if ( quads ) {
		_invalidQuads = true;
	}
	
	if ( glyphs ) {
		_invalidGlyphs = true;
	}
	
	if ( strings ) {
		_invalidStrings = true;
	}
	
	_invalidateLocalBounds();
}

void TextFieldObject::_checkInvalidateText() const
{
	if ( _invalidGlyphs ) {
		_updateGlyphs();
	}
	
	if ( _invalidStrings ) {
		_updateStrings();
	}
	
	if ( _invalidQuads ) {
		_updateQuads();
	}
}

f32 TextFieldObject::_calcHAlignmentOffset(f32 str_width) const
{
	switch ( getHAlignment() ) {
	case HALIGN_LEFT:
	case HALIGN_JUSTIFY_LEFT:
		return 0.f;
	case HALIGN_CENTER:
	case HALIGN_JUSTIFY_CENTER:
		return 0.5f * _getMaxStringWidth() - 0.5f * str_width;
	case HALIGN_RIGHT:
	case HALIGN_JUSTIFY_RIGHT:
		return _getMaxStringWidth() - str_width;
	case HALIGN_JUSTIFY_FULL:
		return 0.f;
	default:
		BME_ASSERT(false);
		return 0.f;
	}
}

f32 TextFieldObject::_calcVAlignmentOffset(f32* out_text_height) const
{
	FontAssetPtr font_asset = cpp::static_pointer_cast<FontAsset>(getAsset());
	if ( !font_asset ) {
		if ( out_text_height ) {
			*out_text_height = 0.f;
		}
		return 0.f;
	}
	
	f32 text_height = _getStringHeight();
	if ( out_text_height ) {
		*out_text_height = text_height;
	}
	
	switch ( getVAlignment() ) {
	case VALIGN_TOP:
		return 0.f;
	case VALIGN_CENTER:
		return 0.5f * _getMaxStringHeight() - 0.5f * text_height;
	case VALIGN_BOTTOM:
		return _getMaxStringHeight() - text_height;
	case VALIGN_JUSTIFY:
		return 0.f;
	default:
		BME_ASSERT(false);
		return 0.f;
	}
}

f32 TextFieldObject::_getStringHeight() const
{
	FontAssetPtr font_asset = cpp::static_pointer_cast<FontAsset>(getAsset());
	if ( font_asset && _strings.size() > 0 ) {
		f32 lines_height = font_asset->getInfo().lineHeight * _strings.size() * _internalScale;
		return lines_height + getLineSpacing() * (_strings.size() - 1) * _internalScale;
	}
	return 0.f;
}

f32 TextFieldObject::_getMaxStringWidth() const
{
	if ( Math::IsZero(getFixedWidth()) ) {
		return _maxStringWidth;
	} else {
		return getFixedWidth();
	}
}

f32 TextFieldObject::_getMaxStringHeight() const
{
	if ( Math::IsZero(getFixedHeight()) ) {
		return _getStringHeight();
	} else {
		return getFixedHeight();
	}
}

// ----------------------------------------------------------------------------
// 
// Protected
// 
// ----------------------------------------------------------------------------

void TextFieldObject::_invalidateLocalTransform()
{
	DisplayObject::_invalidateLocalTransform();
	_invalidateText(true, false, false);
}

void TextFieldObject::_invalidateGlobalTransform()
{
	DisplayObject::_invalidateGlobalTransform();
	_invalidateText(true, false, false);
}

void TextFieldObject::_updateSelf(f32 dt)
{
	DisplayObject::_updateSelf(dt);
}

void TextFieldObject::_renderSelf()
{
	DisplayObject::_renderSelf();
	_checkInvalidateText();
	for ( size_t i = 0; i < _quads.size(); ++i ) {
		_quads[i].Render();
	}
}

const rectf& TextFieldObject::_getSelfBounds() const
{
	_checkInvalidateText();
	return _selfBounds;
}

TextFieldObject::TextFieldObject(const LibraryAssetPtr& asset)
: DisplayObject(asset)
, _color(Color::white)
, _smoothing(true)
, _halign(HALIGN_LEFT)
, _valign(VALIGN_TOP)
, _fixedSize(vec2f::zero)
, _lineSpacing(1.f)
, _internalScale(1.f)
, _maxStringWidth(0.f)
, _invalidQuads(true)
, _invalidGlyphs(true)
, _invalidStrings(true)
{
	if ( asset ) {
		BME_ASSERT(cpp::dynamic_pointer_cast<FontAsset>(asset));
		FontAssetPtr font_asset = cpp::static_pointer_cast<FontAsset>(asset);
		_info = font_asset->getInfo();
		setSmoothing(font_asset->isSmoothing());
	}
}

// ----------------------------------------------------------------------------
// 
// Public
// 
// ----------------------------------------------------------------------------

TextFieldObjectPtr TextFieldObject::Create(const LibraryAssetPtr& asset)
{
	if ( asset && !cpp::dynamic_pointer_cast<FontAsset>(asset) ) {
		return TextFieldObjectPtr();
	}
	return TextFieldObjectPtr(new TextFieldObject(asset));
}

TextFieldObjectPtr TextFieldObject::Create(const BMFontAsset::Desc& desc)
{
	BMFontAssetPtr asset = BMFontAsset::Create(desc, true);
	return asset
		? TextFieldObjectPtr(new TextFieldObject(asset))
		: TextFieldObjectPtr();
}

TextFieldObject::~TextFieldObject()
{
}

void TextFieldObject::setText(const Str& value)
{
	_text = value;
	Strings::MakeUtf32(_utext, getText().c_str());
	_invalidateText(true, true, true);
}

const Str& TextFieldObject::getText() const
{
	return _text;
}

void TextFieldObject::setColor(const Color& value)
{
	_color = value;
	_invalidateText(true, false, false);
}

const Color& TextFieldObject::getColor() const
{
	return _color;
}

void TextFieldObject::setSmoothing(bool yesno)
{
	_smoothing = yesno;
	_invalidateText(true, false, false);
}

bool TextFieldObject::isSmoothing() const
{
	return _smoothing;
}

void TextFieldObject::setHAlignment(E_HALIGN value)
{
	setAlignment(value, getVAlignment());
}

void TextFieldObject::setVAlignment(E_VALIGN value)
{
	setAlignment(getHAlignment(), value);
}

void TextFieldObject::setAlignment(E_HALIGN halign, E_VALIGN valign)
{
	_halign = halign;
	_valign = valign;
	_invalidateText(true, false, false);
}

TextFieldObject::E_HALIGN TextFieldObject::getHAlignment() const
{
	return _halign;
}

TextFieldObject::E_VALIGN TextFieldObject::getVAlignment() const
{
	return _valign;
}

void TextFieldObject::setFixedWidth(f32 value)
{
	setFixedSize(value, getFixedHeight());
}

void TextFieldObject::setFixedHeight(f32 value)
{
	setFixedSize(getFixedWidth(), value);
}

void TextFieldObject::setFixedSize(f32 w, f32 h)
{
	setFixedSize(vec2f(w, h));
}

void TextFieldObject::setFixedSize(const vec2f& value)
{
	_fixedSize = value;
	_fixedSize.x = cpp::max(0.f, _fixedSize.x);
	_fixedSize.y = cpp::max(0.f, _fixedSize.y);
	_invalidateText(true, false, true);
}

f32 TextFieldObject::getFixedWidth() const
{
	return _fixedSize.x;
}

f32 TextFieldObject::getFixedHeight() const
{
	return _fixedSize.y;
}

const vec2f& TextFieldObject::getFixedSize() const
{
	return _fixedSize;
}

void TextFieldObject::setLineSpacing(f32 value)
{
	_lineSpacing = cpp::max(0.f, value);
	_invalidateText(true, false, true);
}

f32 TextFieldObject::getLineSpacing() const
{
	return _lineSpacing;
}

} // namespace fw3
} // namespace bme
