/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/BitmapObject.cpp
 *****************************************************************************/

#include <bme/fw3/objects/BitmapObject.h>

namespace bme
{
namespace fw3
{

// ----------------------------------------------------------------------------
// 
// Private
// 
// ----------------------------------------------------------------------------

bool BitmapObject::_updateQuad() const
{
	_invalidQuad = false;
	if ( getTexture() && rectf::zero != getTexCoords() ) {
		const Color& color = getColor();
		const Transform& transform = getGlobalTransform();
		_quad.quad.material.texture   = getTexture();
		_quad.quad.material.filter    = isSmoothing();
		_quad.quad.renderStates.blend = transform.blend;
		_quad.CalculateColors(Color(
			color.getR(), color.getG(), color.getB(),
			color.getA() * Math::Clamp(transform.alpha, 0.f, 1.f)));
		_quad.CalculateTexCoords(getTexCoords());
		_quad.CalculateVertices(transform.matrix, vec2f(getTexCoords().getSize()));
		return true;
	} else {
		_quad = QuadHelper();
		return false;
	}
}

void BitmapObject::_updateBounds()
{
	_selfBounds = rectf(getTexCoords().getSize());
	_invalidateLocalBounds();
}

// ----------------------------------------------------------------------------
// 
// Protected
// 
// ----------------------------------------------------------------------------

void BitmapObject::_invalidateLocalTransform()
{
	DisplayObject::_invalidateLocalTransform();
	_invalidQuad = true;
}

void BitmapObject::_invalidateGlobalTransform()
{
	DisplayObject::_invalidateGlobalTransform();
	_invalidQuad = true;
}

void BitmapObject::_updateSelf(f32 dt)
{
	DisplayObject::_updateSelf(dt);
}

void BitmapObject::_renderSelf()
{
	DisplayObject::_renderSelf();
	if ( _invalidQuad ) {
		_updateQuad();
	}
	_quad.Render();
}

const rectf& BitmapObject::_getSelfBounds() const
{
	return _selfBounds;
}

BitmapObject::BitmapObject(const LibraryAssetPtr& asset)
: DisplayObject(asset)
, _color(Color::white)
, _smoothing(true)
, _invalidQuad(true)
{
	if ( asset ) {
		BME_ASSERT(cpp::dynamic_pointer_cast<BitmapAsset>(asset));
		BitmapAssetPtr bitmap_asset = cpp::static_pointer_cast<BitmapAsset>(asset);
		setColor(bitmap_asset->getColor());
		setSmoothing(bitmap_asset->isSmoothing());
		setTexture(bitmap_asset->getTexture(), bitmap_asset->getTexCoords());
	}
}

// ----------------------------------------------------------------------------
// 
// Public
// 
// ----------------------------------------------------------------------------

BitmapObjectPtr BitmapObject::Create(const LibraryAssetPtr& asset)
{
	if ( asset && !cpp::dynamic_pointer_cast<BitmapAsset>(asset) ) {
		return BitmapObjectPtr();
	}
	return BitmapObjectPtr(new BitmapObject(asset));
}

BitmapObjectPtr BitmapObject::Create(const BitmapAsset::Desc& desc)
{
	BitmapAssetPtr asset = BitmapAsset::Create(desc, true);
	return asset
		? BitmapObjectPtr(new BitmapObject(asset))
		: BitmapObjectPtr();
}

BitmapObject::~BitmapObject()
{
	_quad = QuadHelper();
}

void BitmapObject::setColor(const Color& value)
{
	_color = value;
	_invalidQuad = true;
}

const Color& BitmapObject::getColor() const
{
	return _color;
}

void BitmapObject::setSmoothing(bool yesno)
{
	_smoothing = yesno;
	_invalidQuad = true;
}

bool BitmapObject::isSmoothing() const
{
	return _smoothing;
}

void BitmapObject::setTexture(const TexturePtr& texture, const rectf& texcoords)
{
	_texture   = texture;
	_texcoords = texcoords;
	
	if ( getTexture() && rectf::zero == getTexCoords() ) {
		_texcoords = rectf(pnt2f(vec2f(getTexture()->getOriginalSize())));
	}
	
	_updateBounds();
	_invalidQuad = true;
}

const TexturePtr& BitmapObject::getTexture() const
{
	return _texture;
}

const rectf& BitmapObject::getTexCoords() const
{
	return _texcoords;
}

} // namespace fw3
} // namespace bme
