/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/SharedObjectImpl.h
 *****************************************************************************/

#include <bme/fw3/common/SharedObject.h>

namespace bme
{
namespace fw3
{

// ----------------------------------------------------------------------------
// 
// shared_object_detail
// 
// ----------------------------------------------------------------------------

namespace shared_object_detail
{
	// --------------------------------
	// predefs
	// --------------------------------
	
	template < typename T >
	struct type2value;
	
	struct value_place;
	
	template < typename T >
	struct value_holder;
	
	// --------------------------------
	// type2value
	// --------------------------------
	
	template < typename T >
	struct type2value {};
	
	template <> struct type2value<s32> {
		static const SharedObject::E_VALUE value = SharedObject::VALUE_INT;
		static s32 def() { return 0; } };
	
	template <> struct type2value<f32> {
		static const SharedObject::E_VALUE value = SharedObject::VALUE_FLOAT;
		static f32 def() { return 0.f; } };
	
	template <> struct type2value<vec2f> {
		static const SharedObject::E_VALUE value = SharedObject::VALUE_VEC2;
		static const vec2f& def() { return vec2f::zero; } };
	
	template <> struct type2value<vec3f> {
		static const SharedObject::E_VALUE value = SharedObject::VALUE_VEC3;
		static const vec3f& def() { return vec3f::zero; } };
	
	template <> struct type2value<Color> {
		static const SharedObject::E_VALUE value = SharedObject::VALUE_COLOR;
		static const Color& def() { return Color::white; } };
	
	template <> struct type2value<Str> {
		static const SharedObject::E_VALUE value = SharedObject::VALUE_TEXT;
		static Str def() { return Str(); } };
	
	template <> struct type2value<SmartBuffer> {
		static const SharedObject::E_VALUE value = SharedObject::VALUE_BIN;
		static SmartBuffer def() { return SmartBuffer(); } };
	
	// --------------------------------
	// value_place
	// --------------------------------
	
	struct value_place
	{
		StrId name;
		SharedObject::E_VALUE type;
		
		value_place(const StrId& name, SharedObject::E_VALUE type)
			: name(name), type(type) {}
		virtual ~value_place() {}
		
		template < typename T >
		value_holder<T>* castTo()
		{
		#if BME_MODE == BME_MODE_DEBUG
			value_holder<T>* holder = dynamic_cast<value_holder<T>*>(this);
			BME_ASSERT(holder);
		#else
			value_holder<T>* holder = static_cast<value_holder<T>*>(this);
		#endif
			return holder;
		}
		
		template < typename T >
		const value_holder<T>* castTo() const
		{
			return const_cast<value_place*>(this)->castTo<T>();
		}
	};
	
	// --------------------------------
	// value_holder
	// --------------------------------
	
	template < typename T >
	struct value_holder : public value_place
	{
		T value;
		value_holder(const StrId& name, SharedObject::E_VALUE type, const T& value)
			: value_place(name, type), value(value) {}
		virtual ~value_holder() {}
	};
}

// ----------------------------------------------------------------------------
// 
// SharedObjectImpl
// 
// ----------------------------------------------------------------------------

class SharedObjectImpl : public SharedObject
{
	void _clearUp();
	shared_object_detail::value_place*
		_findValue(E_VALUE type, const StrId& name);
	const shared_object_detail::value_place*
		_findValue(E_VALUE type, const StrId& name) const;
	
private:
	
	typedef cpp::vector<shared_object_detail::value_place*>::type TValues;
	typedef cpp::map<StrId, TValues>::type                        TSections;
	typedef cpp::vector<SharedObjectPtr>::type                    TSharedObjects;
	
	Path                  _path;
	StrId                 _section;
	TSections             _sections;
	static TSharedObjects _sharedObjects;
	
public:
	
	SharedObjectImpl(const Path& path);
	virtual ~SharedObjectImpl();
	static SharedObjectPtr GetOrCreate(const Path& path);
	
	template < typename T >
	T            getValue    ( const StrId& name ) const;
	template < typename T >
	void         setValue    ( const StrId& name, const T& value );
	
	// Override ---------------------------------------------------------------
	void         Clear       ();
	void         Flush       () const;
	
	void         setSection  ( const StrId& section );
	const StrId& getSection  () const;
	
	bool         RemoveValue ( const StrId& name, E_VALUE type );
	bool         IsHasValue  ( const StrId& name, E_VALUE type ) const;
	
	s32          GetInt      ( const StrId& name ) const { return getValue<s32>        (name); }
	f32          GetFloat    ( const StrId& name ) const { return getValue<f32>        (name); }
	vec2f        GetVec2     ( const StrId& name ) const { return getValue<vec2f>      (name); }
	vec3f        GetVec3     ( const StrId& name ) const { return getValue<vec3f>      (name); }
	Color        GetColor    ( const StrId& name ) const { return getValue<Color>      (name); }
	Str          GetString   ( const StrId& name ) const { return getValue<Str>        (name); }
	SmartBuffer  GetBin      ( const StrId& name ) const { return getValue<SmartBuffer>(name); }
	
	void         SetInt      ( const StrId& name, s32                value ) { setValue(name, value); }
	void         SetFloat    ( const StrId& name, f32                value ) { setValue(name, value); }
	void         SetVec2     ( const StrId& name, const vec2f&       value ) { setValue(name, value); }
	void         SetVec3     ( const StrId& name, const vec3f&       value ) { setValue(name, value); }
	void         SetColor    ( const StrId& name, const Color&       value ) { setValue(name, value); }
	void         SetString   ( const StrId& name, const Str&         value ) { setValue(name, value); }
	void         SetBin      ( const StrId& name, const SmartBuffer& value ) { setValue(name, value); }
	// ------------------------------------------------------------------------
};

// ----------------------------------------------------------------------------
// 
// SharedObjectImpl impl
// 
// ----------------------------------------------------------------------------

template < typename T >
T SharedObjectImpl::getValue(const StrId& name) const
{
	using namespace shared_object_detail;
	const value_place* vp = _findValue(type2value<T>::value, name);
	if ( vp ) {
		return vp->castTo<T>()->value;
	} else {
		return type2value<T>::def();
	}
}

template < typename T >
void SharedObjectImpl::setValue(const StrId& name, const T& value)
{
	using namespace shared_object_detail;
	value_place* vp = _findValue(type2value<T>::value, name);
	if ( vp ) {
		vp->castTo<T>()->value = value;
	} else {
		value_holder<T>* holder = new value_holder<T>(name, type2value<T>::value, value);
		_sections[getSection()].push_back(holder);
	}
}

} // namespace fw3
} // namespace bme
