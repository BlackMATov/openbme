/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/XmlParser.cpp
 *****************************************************************************/

#include <bme/fw3/common/XmlParser.h>

namespace bme
{
namespace fw3
{
	using namespace pugi;
	
	// ------------------------------------------------------------------------
	// 
	// LoadXmlDocument
	// 
	// ------------------------------------------------------------------------
	
	bool LoadXmlDocument(const Path& path, xml_document& out_doc)
	{
		ReadFilePtr file = theFilesystem() ?
			theFilesystem()->OpenFile(path) : ReadFilePtr();
		
		MMapFilePtr mmap_file = file ?
			file->MakeMMap() : MMapFilePtr();
		
		if ( !mmap_file ) {
			BME_LOG_ERROR_FMT(
				"LoadXmlDocument() path is wrong (\"%1%\")",
				path);
			return false;
		}
		
		return LoadXmlDocument(
			mmap_file->getData(), mmap_file->getSize(), out_doc);
	}
	
	bool LoadXmlDocument(const SmartBuffer& mem, xml_document& out_doc)
	{
		return LoadXmlDocument(
			mem.getBuffer(), mem.getSize(), out_doc);
	}
	
	bool LoadXmlDocument(const void* data, size_t size, xml_document& out_doc)
	{
		if ( !data || !size ) {
			BME_LOG_ERROR("LoadXmlDocument() memory is empty");
			return false;
		}
		xml_parse_result result = out_doc.load_buffer(data, size);
		if ( !result ) {
			BME_LOG_ERROR_FMT(
				"LoadXmlDocument()\nParse error message: %1%",
				result.description());
			return false;
		}
		return true;
	}
	
	// ------------------------------------------------------------------------
	// 
	// GetXmlAttribute
	// 
	// ------------------------------------------------------------------------
	
	void GetXmlAttribute(
		const pugi::xml_node& node, const char* name,
		bool& value, bool def)
	{
		xml_attribute attr = node.attribute(name);
		value = attr.as_bool(def);
	}
	
	void GetXmlAttribute(
		const pugi::xml_node& node, const char* name,
		Str& value, const Str& def)
	{
		xml_attribute attr = node.attribute(name);
		value = attr.as_string(def.c_str());
	}
	
	void GetXmlAttribute(
		const pugi::xml_node& node, const char* name,
		WStr& value, const WStr& def)
	{
		xml_attribute attr = node.attribute(name);
		if ( attr.empty() || !Strings::MakeWide(value, attr.value()) ) {
			value = def;
		}
	}
	
	void GetXmlAttribute(
		const pugi::xml_node& node, const char* name,
		Path& value, const Path& def)
	{
		xml_attribute attr = node.attribute(name);
		value = attr.empty() ? def : attr.value();
	}
	
	void GetXmlAttribute(
		const pugi::xml_node& node, const char* name,
		StrId& value, const StrId& def)
	{
		xml_attribute attr = node.attribute(name);
		value = attr.empty() ? def : attr.value();
	}
	
	void GetXmlAttribute(
		const pugi::xml_node& node, const char* name,
		Material::E_ADDRESS& value, Material::E_ADDRESS def)
	{
		xml_attribute attr = node.attribute(name);
		if ( 0 == BME_strcmp(attr.value(), "WRAP") ) {
			value = Material::ADDRESS_WRAP;
		} else if ( 0 == BME_strcmp(attr.value(), "CLAMP") ) {
			value = Material::ADDRESS_CLAMP;
		} else {
			value = def;
		}
	}
	
	void GetXmlAttribute(
		const pugi::xml_node& node, const char* name,
		RenderStates::E_BLEND& value, RenderStates::E_BLEND def)
	{
		xml_attribute attr = node.attribute(name);
		if ( 0 == BME_strcmp(attr.value(), "ALPHA") ) {
			value = RenderStates::BLEND_ALPHA;
		} else if ( 0 == BME_strcmp(attr.value(), "ADD") ) {
			value = RenderStates::BLEND_ADD;
		} else if ( 0 == BME_strcmp(attr.value(), "MULTIPLY") ) {
			value = RenderStates::BLEND_MULTIPLY;
		} else {
			value = def;
		}
	}
} // namespace fw3
} // namespace bme
