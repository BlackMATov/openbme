/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/ExcelXmlTable.cpp
 *****************************************************************************/

#include <bme/fw3/common/ExcelXmlTable.h>
#include <bme/fw3/common/XmlParser.h>

namespace bme
{
namespace fw3
{

// ----------------------------------------------------------------------------
// 
// Private
// 
// ----------------------------------------------------------------------------

ExcelXmlTable::CellDesc::CellDesc(const pnt2u& cell, const StrId& data)
: cell(cell), data(data)
{
}

ExcelXmlTable::CellDesc::CellDesc(u32 row, u32 column, const StrId& data)
: cell(column, row), data(data)
{
}

bool ExcelXmlTable::CellDesc::operator<(const CellDesc& other) const
{
	return cell < other.cell;
}

ExcelXmlTable::ExcelXmlTable(const Path& path)
: _path(path)
, _reloadCount(0)
{
}

// ----------------------------------------------------------------------------
// 
// Public
// 
// ----------------------------------------------------------------------------

ExcelXmlTable::~ExcelXmlTable()
{
}

ExcelXmlTablePtr ExcelXmlTable::Create(const Path& path)
{
	ExcelXmlTablePtr ret_value = ExcelXmlTablePtr(new ExcelXmlTable(path));
	return ret_value->Reload() ? ret_value : ExcelXmlTablePtr();
}

bool ExcelXmlTable::Reload(const StrId& worksheet)
{
	// ------------------------------------------------------------------------
	
	pugi::xml_document doc;
	if ( !LoadXmlDocument(getPath(), doc) ) {
		return false;
	}
	
	pugi::xml_node root_node = doc.first_child();
	if ( !root_node ) {
		return false;
	}
	
	// ------------------------------------------------------------------------
	
	if ( !worksheet.empty() ) {
		_worksheetsDescs.erase(worksheet);
	} else {
		_worksheetsDescs.clear();
	}
	
	// ------------------------------------------------------------------------
	
	for ( pugi::xml_node worksheet_node = root_node.child("Worksheet");
		worksheet_node; worksheet_node = worksheet_node.next_sibling("Worksheet") )
	{
		StrId worksheet_name = worksheet_node.attribute("ss:Name").value();
		if ( worksheet_name.empty() ) {
			continue;
		}
		
		if ( !worksheet.empty() && worksheet != worksheet_name ) {
			continue;
		}
		
		pugi::xml_node table_node = worksheet_node.child("Table");
		if ( !table_node ) {
			continue;
		}
		
		// ----------------------------
		// Add new worksheet
		// ----------------------------
		
		WorksheetDesc& worksheet = _worksheetsDescs[worksheet_name];
		TCells& worksheet_cells = worksheet.cells;
		
		// ----------------------------
		// Reserve
		// ----------------------------
		
		pugi::xml_attribute expanded_row_count_attr =
			table_node.attribute("ss:ExpandedRowCount");
		pugi::xml_attribute expanded_column_count_attr =
			table_node.attribute("ss:ExpandedColumnCount");
		
		if ( expanded_row_count_attr && expanded_column_count_attr ) {
			size_t expanded_row_count =
				Math::SmartCast<size_t>(expanded_row_count_attr.as_uint());
			size_t expanded_column_count =
				Math::SmartCast<size_t>(expanded_column_count_attr.as_uint());
			if ( expanded_row_count && expanded_column_count ) {
				worksheet_cells.reserve(expanded_row_count * expanded_column_count);
			}
		}
		
		// ----------------------------
		// Fill cells
		// ----------------------------
		
		u32 row_index = 0;
		for ( pugi::xml_node row_node = table_node.child("Row");
			row_node; row_node = row_node.next_sibling("Row") )
		{
			GetXmlAttribute(row_node, "ss:Index", row_index, row_index + 1);
			
			u32 column_index = 0;
			for ( pugi::xml_node cell_node = row_node.child("Cell");
				cell_node; cell_node = cell_node.next_sibling("Cell") )
			{
				GetXmlAttribute(cell_node, "ss:Index", column_index, column_index + 1);
				
				pugi::xml_node data_node = cell_node.child("Data");
				if ( data_node ) {
					if ( 0 == BME_strcmp(data_node.attribute("ss:Type").value(), "String") ||
						 0 == BME_strcmp(data_node.attribute("ss:Type").value(), "Number") )
					{
						StrId value_str = data_node.child_value();
						if ( !value_str.empty() ) {
							worksheet_cells.push_back(
								CellDesc(row_index, column_index, value_str));
							worksheet.maxcell.x = cpp::max(worksheet.maxcell.x, column_index);
							worksheet.maxcell.y = cpp::max(worksheet.maxcell.y, row_index);
						}
					}
				}
			}
		}
		
		// ----------------------------
		// Sort cells
		// ----------------------------
		
		cpp::sort(worksheet_cells.begin(), worksheet_cells.end());
	}
	
	// ------------------------------------------------------------------------
	
	_worksheetsNames.clear();
	_worksheetsNames.reserve(_worksheetsDescs.size());
	
	for ( TWorksheetsDescs::const_iterator
		iter = _worksheetsDescs.begin(), end = _worksheetsDescs.end();
		iter != end; ++iter )
	{
		_worksheetsNames.push_back((*iter).first);
	}
	
	// ------------------------------------------------------------------------
	
	++_reloadCount;
	return true;
}

const Path& ExcelXmlTable::getPath() const
{
	return _path;
}

size_t ExcelXmlTable::getReloadCount() const
{
	return _reloadCount;
}

const ExcelXmlTable::TWorksheets& ExcelXmlTable::getWorksheets() const
{
	return _worksheetsNames;
}

StrId ExcelXmlTable::GetCellData(const StrId& worksheet, const pnt2u& position) const
{
	TWorksheetsDescs::const_iterator worksheet_iter = _worksheetsDescs.find(worksheet);
	if ( worksheet_iter != _worksheetsDescs.end() ) {
		const TCells& cells = (*worksheet_iter).second.cells;
		TCells::const_iterator cell_iter =
			cpp::lower_bound(cells.begin(), cells.end(), CellDesc(position));
		if ( cell_iter != cells.end() && position == (*cell_iter).cell ) {
			return (*cell_iter).data;
		}
	}
	return StrId();
}

StrId ExcelXmlTable::GetCellData(const StrId& worksheet, size_t row, size_t column) const
{
	return GetCellData(worksheet, pnt2u(column, row));
}

size_t ExcelXmlTable::getMaxRow(const StrId& worksheet) const
{
	return getMaxCell(worksheet).y;
}

size_t ExcelXmlTable::getMaxColumn(const StrId& worksheet) const
{
	return getMaxCell(worksheet).x;
}

const pnt2u& ExcelXmlTable::getMaxCell(const StrId& worksheet) const
{
	TWorksheetsDescs::const_iterator worksheet_iter = _worksheetsDescs.find(worksheet);
	if ( worksheet_iter != _worksheetsDescs.end() ) {
		return (*worksheet_iter).second.maxcell;
	}
	return pnt2u::zero;
}

} // namespace fw3
} // namespace bme
