/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/SharedObjectImpl.cpp
 *****************************************************************************/

#include "SharedObjectImpl.h"

namespace bme
{
namespace fw3
{

static const char* DEFAULT_SHARED_OBJECT_SECTION = "Common";

// ----------------------------------------------------------------------------
// 
// SharedObject
// 
// ----------------------------------------------------------------------------

SharedObjectPtr SharedObject::Get()
{
	Path dir = Path::app_dir;
	if ( theFilesystem() && !theFilesystem()->CreateDirectories(dir) ) {
		dir = Path::wrk_dir;
	}
	dir /= Path("shared_object.bso");
	return SharedObjectImpl::GetOrCreate(dir);
}

// ----------------------------------------------------------------------------
// 
// shared_object_detail
// 
// ----------------------------------------------------------------------------

namespace shared_object_detail
{
	// --------------------------------
	// Write
	// --------------------------------
	
	static void _writeTValueToFile(const WriteFilePtr& file, s32 v)
	{
		file->WriteV(v);
	}
	
	static void _writeTValueToFile(const WriteFilePtr& file, f32 v)
	{
		file->WriteV(v);
	}
	
	static void _writeTValueToFile(const WriteFilePtr& file, const vec2f& v)
	{
		file->WriteV(v.x);
		file->WriteV(v.y);
	}
	
	static void _writeTValueToFile(const WriteFilePtr& file, const vec3f& v)
	{
		file->WriteV(v.x);
		file->WriteV(v.y);
		file->WriteV(v.z);
	}
	
	static void _writeTValueToFile(const WriteFilePtr& file, const Color& v)
	{
		file->WriteV(v.getARGB());
	}
	
	static void _writeTValueToFile(const WriteFilePtr& file, const Str& v)
	{
		file->WriteS(v);
	}
	
	static void _writeTValueToFile(const WriteFilePtr& file, const SmartBuffer& v)
	{
		file->WriteV(v.getSize());
		file->Write(v.getBuffer(), v.getSize());
	}
	
	template < typename T >
	void _writeTValueToFile(const WriteFilePtr& file, const value_place* vp)
	{
		file->WriteV(static_cast<s32>(vp->type));
		file->WriteS(vp->name);
		_writeTValueToFile(file, vp->castTo<T>()->value);
	}
	
	static void _writeValueToFile(const WriteFilePtr& file, const value_place* vp)
	{
		switch ( vp->type ) {
		case SharedObject::VALUE_INT:
			_writeTValueToFile<s32>(file, vp);
			break;
		case SharedObject::VALUE_FLOAT:
			_writeTValueToFile<f32>(file, vp);
			break;
		case SharedObject::VALUE_VEC2:
			_writeTValueToFile<vec2f>(file, vp);
			break;
		case SharedObject::VALUE_VEC3:
			_writeTValueToFile<vec3f>(file, vp);
			break;
		case SharedObject::VALUE_COLOR:
			_writeTValueToFile<Color>(file, vp);
			break;
		case SharedObject::VALUE_TEXT:
			_writeTValueToFile<Str>(file, vp);
			break;
		case SharedObject::VALUE_BIN:
			_writeTValueToFile<SmartBuffer>(file, vp);
			break;
		default:
			BME_ASSERT(false);
		}
	}
	
	// --------------------------------
	// Read
	// --------------------------------
	
	template < typename T >
	void _readTValueFromFile(SharedObjectImpl* impl,
		const ReadFilePtr& file, const StrId& name);
	
	template <>
	void _readTValueFromFile<s32>(SharedObjectImpl* impl,
		const ReadFilePtr& file, const StrId& name)
	{
		s32 v;
		file->ReadV(v);
		impl->setValue(name, v);
	}
	
	template <>
	void _readTValueFromFile<f32>(SharedObjectImpl* impl,
		const ReadFilePtr& file, const StrId& name)
	{
		f32 v;
		file->ReadV(v);
		impl->setValue(name, v);
	}
	
	template <>
	void _readTValueFromFile<vec2f>(SharedObjectImpl* impl,
		const ReadFilePtr& file, const StrId& name)
	{
		vec2f v;
		file->ReadV(v.x);
		file->ReadV(v.y);
		impl->setValue(name, v);
	}
	
	template <>
	void _readTValueFromFile<vec3f>(SharedObjectImpl* impl,
		const ReadFilePtr& file, const StrId& name)
	{
		vec3f v;
		file->ReadV(v.x);
		file->ReadV(v.y);
		file->ReadV(v.z);
		impl->setValue(name, v);
	}
	
	template <>
	void _readTValueFromFile<Color>(SharedObjectImpl* impl,
		const ReadFilePtr& file, const StrId& name)
	{
		u32 v = 0;
		file->ReadV(v);
		impl->setValue(name, Color(v));
	}
	
	template <>
	void _readTValueFromFile<Str>(SharedObjectImpl* impl,
		const ReadFilePtr& file, const StrId& name)
	{
		Str v;
		file->ReadS(v);
		impl->setValue(name, v);
	}
	
	template <>
	void _readTValueFromFile<SmartBuffer>(SharedObjectImpl* impl,
		const ReadFilePtr& file, const StrId& name)
	{
		size_t size = 0;
		file->ReadV(size);
		SmartBuffer buf(size);
		file->Read(buf.getBuffer(), size);
		impl->setValue(name, buf);
	}
	
	// --------------------------------
	// Load
	// --------------------------------
	
	static void _load(const ReadFilePtr& file, SharedObjectImpl* impl)
	{
		StrId last_section = impl->getSection();
		while ( file->getPos() < file->getSize() ) {
			StrId sec_name;
			file->ReadS(sec_name);
			
			size_t num_values = 0;
			file->ReadV(num_values);
			
			impl->setSection(sec_name);
			
			for ( size_t i = 0; i < num_values; ++i ) {
				u32 type_v;
				file->ReadV(type_v);
				
				StrId name_v;
				file->ReadS(name_v);
				
				switch ( type_v ) {
				case SharedObject::VALUE_INT:
					_readTValueFromFile<s32>(impl, file, name_v);
					break;
				case SharedObject::VALUE_FLOAT:
					_readTValueFromFile<f32>(impl, file, name_v);
					break;
				case SharedObject::VALUE_VEC2:
					_readTValueFromFile<vec2f>(impl, file, name_v);
					break;
				case SharedObject::VALUE_VEC3:
					_readTValueFromFile<vec3f>(impl, file, name_v);
					break;
				case SharedObject::VALUE_COLOR:
					_readTValueFromFile<Color>(impl, file, name_v);
					break;
				case SharedObject::VALUE_TEXT:
					_readTValueFromFile<Str>(impl, file, name_v);
					break;
				case SharedObject::VALUE_BIN:
					_readTValueFromFile<SmartBuffer>(impl, file, name_v);
					break;
				default:
					BME_ASSERT(false);
				}
			}
		}
		impl->setSection(last_section);
	}
}

// ----------------------------------------------------------------------------
// 
// SharedObjectImpl
// 
// ----------------------------------------------------------------------------

SharedObjectImpl::TSharedObjects SharedObjectImpl::_sharedObjects;

SharedObjectImpl::SharedObjectImpl(const Path& path)
: _path(path), _section(DEFAULT_SHARED_OBJECT_SECTION)
{
	ReadFilePtr file = theFilesystem() ?
		theFilesystem()->OpenFile(path, Filesystem::ORDER_DISK_ONLY) : ReadFilePtr();
	if ( file ) {
		shared_object_detail::_load(file, this);
	} else {
		Flush();
	}
}

SharedObjectImpl::~SharedObjectImpl()
{
	Flush();
	_clearUp();
}

void SharedObjectImpl::_clearUp()
{
	for ( TSections::iterator
		iter = _sections.begin(), end = _sections.end();
		iter != end; ++iter )
	{
		TValues& values = (*iter).second;
		while ( !values.empty() ) {
			delete values.back();
			values.pop_back();
		}
	}
	_sections.clear();
}

shared_object_detail::value_place* SharedObjectImpl::_findValue(
	E_VALUE type, const StrId& name)
{
	TSections::const_iterator iter = _sections.find(getSection());
	if ( iter != _sections.end() ) {
		const TValues& values = (*iter).second;
		for ( TValues::const_iterator
			iter = values.begin(), end = values.end();
			iter != end; ++iter )
		{
			shared_object_detail::value_place* place = *iter;
			if ( place->type == type && place->name == name ) {
				return place;
			}
		}
	}
	return NULL;
}

const shared_object_detail::value_place* SharedObjectImpl::_findValue(
	E_VALUE type, const StrId& name) const
{
	return const_cast<SharedObjectImpl*>(this)->_findValue(type,name);
}

SharedObjectPtr	SharedObjectImpl::GetOrCreate(const Path& path)
{
	// find
	for ( TSharedObjects::const_iterator
		iter = _sharedObjects.begin(), end = _sharedObjects.end();
		iter != end; ++iter )
	{
		SharedObjectImpl* impl = static_cast<SharedObjectImpl*>((*iter).get());
		if ( impl->_path == path ) {
			return *iter;
		}
	}
	
	// create new
	SharedObjectPtr ptr(new SharedObjectImpl(path));
	_sharedObjects.push_back(ptr);
	return ptr;
}

void SharedObjectImpl::Clear()
{
	_clearUp();
	Flush();
}

void SharedObjectImpl::Flush() const
{
	WriteFilePtr file = !_path.isEmpty() ?
		WriteFile::Create(_path, false) : WriteFilePtr();
	if ( !file ) {
		BME_LOG_ERROR_FMT(
			"SharedObjectImpl::Flush() can't create shared file!\nPath: %1%",
			_path);
		return;
	}
	
	for ( TSections::const_iterator
		iter = _sections.begin(), end = _sections.end();
		iter != end; ++iter )
	{
		const TValues& values = (*iter).second;
		file->WriteS((*iter).first);
		file->WriteV(values.size());
		for ( TValues::const_iterator
			iter = values.begin(), end = values.end();
			iter != end; ++iter )
		{
			shared_object_detail::_writeValueToFile(file, *iter);
		}
	}
}

void SharedObjectImpl::setSection(const StrId& section)
{
	_section = section;
	if ( _section.empty() ) {
		_section = DEFAULT_SHARED_OBJECT_SECTION;
	}
}

const StrId& SharedObjectImpl::getSection() const
{
	return _section;
}

bool SharedObjectImpl::RemoveValue(const StrId& name, E_VALUE type)
{
	TSections::iterator iter = _sections.find(getSection());
	if ( iter != _sections.end() ) {
		TValues& values = (*iter).second;
		for ( TValues::iterator
			iter = values.begin(), end = values.end();
			iter != end; ++iter )
		{
			shared_object_detail::value_place* place = (*iter);
			if ( place->type == type && place->name == name ) {
				delete place;
				values.erase(iter);
				return true;
			}
		}
	}
	return false;
}

bool SharedObjectImpl::IsHasValue(const StrId& name, E_VALUE type) const
{
	return _findValue(type, name) != NULL;
}

} // namespace fw3
} // namespace bme
