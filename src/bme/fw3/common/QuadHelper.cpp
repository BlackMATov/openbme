/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/QuadHelper.cpp
 *****************************************************************************/

#include <bme/fw3/common/QuadHelper.h>

namespace bme
{
namespace fw3
{

void QuadHelper::CalculateTexCoords(
	const rectf& coords)
{
	CalculateTexCoords(
		vec2f(coords.getTopLeft()),
		vec2f(coords.getSize()));
}

void QuadHelper::CalculateTexCoords(
	const vec2f& coords, const vec2f& size)
{
	if ( quad.material.texture ) {
		const vec2f& tex_size = vec2f(quad.material.texture->getSize());
		vec2f        p1       = coords / tex_size;
		vec2f        p2       = (coords + size) / tex_size;
		CalculateTexCoords(
			p1.x, p1.y,
			p2.x, p1.y,
			p2.x, p2.y,
			p1.x, p2.y);
	} else {
		CalculateTexCoords(
			0.f, 0.f,
			1.f, 0.f,
			1.f, 1.f,
			0.f, 1.f);
	}
}

void QuadHelper::CalculateTexCoords(
	f32 x0, f32 y0, f32 x1, f32 y1, f32 x2, f32 y2, f32 x3, f32 y3)
{
	quad.vertices[0].tx = x0; quad.vertices[0].ty = y0;
	quad.vertices[1].tx = x1; quad.vertices[1].ty = y1;
	quad.vertices[2].tx = x2; quad.vertices[2].ty = y2;
	quad.vertices[3].tx = x3; quad.vertices[3].ty = y3;
}

void QuadHelper::CalculateVertices(
	f32 x0, f32 y0, f32 x1, f32 y1)
{
	quad.vertices[0].x = x0; quad.vertices[0].y = y0;
	quad.vertices[1].x = x1; quad.vertices[1].y = y0;
	quad.vertices[2].x = x1; quad.vertices[2].y = y1;
	quad.vertices[3].x = x0; quad.vertices[3].y = y1;
}

void QuadHelper::CalculateVertices(
	f32 x0, f32 y0, f32 x1, f32 y1, f32 x2, f32 y2, f32 x3, f32 y3)
{
	quad.vertices[0].x = x0; quad.vertices[0].y = y0;
	quad.vertices[1].x = x1; quad.vertices[1].y = y1;
	quad.vertices[2].x = x2; quad.vertices[2].y = y2;
	quad.vertices[3].x = x3; quad.vertices[3].y = y3;
}

void QuadHelper::CalculateVertices(
	const vec2f& pos, const vec2f& size)
{
	quad.vertices[0].x = pos.x;          quad.vertices[0].y = pos.y;
	quad.vertices[1].x = pos.x + size.x; quad.vertices[1].y = pos.y;
	quad.vertices[2].x = pos.x + size.x; quad.vertices[2].y = pos.y + size.y;
	quad.vertices[3].x = pos.x;          quad.vertices[3].y = pos.y + size.y;
}

void QuadHelper::CalculateVertices(
	const vec2f& pos, const vec2f& size,
	f32 rot, const vec2f& hotSpot,
	const vec2f& scale, bool flipX, bool flipY)
{
	f32 x1 = (-hotSpot.x)         * scale.x;
	f32 y1 = (-hotSpot.y)         * scale.y;
	f32 x2 = (size.x - hotSpot.x) * scale.x;
	f32 y2 = (size.y - hotSpot.y) * scale.y;
	
	if ( flipX ) {
		cpp::swap(x1, x2);
	}
	
	if ( flipY ) {
		cpp::swap(y1, y2);
	}
	
	if ( Math::IsZero(rot) ) {
		quad.vertices[0].x = x1 + pos.x; quad.vertices[0].y = y1 + pos.y;
		quad.vertices[1].x = x2 + pos.x; quad.vertices[1].y = y1 + pos.y;
		quad.vertices[2].x = x2 + pos.x; quad.vertices[2].y = y2 + pos.y;
		quad.vertices[3].x = x1 + pos.x; quad.vertices[3].y = y2 + pos.y;
	} else {
		f32 cos_rot = cpp::cos(rot);
		f32 sin_rot = cpp::sin(rot);
		
		quad.vertices[0].x = x1*cos_rot - y1*sin_rot + pos.x;
		quad.vertices[0].y = x1*sin_rot + y1*cos_rot + pos.y;
		
		quad.vertices[1].x = x2*cos_rot - y1*sin_rot + pos.x;
		quad.vertices[1].y = x2*sin_rot + y1*cos_rot + pos.y;
		
		quad.vertices[2].x = x2*cos_rot - y2*sin_rot + pos.x;
		quad.vertices[2].y = x2*sin_rot + y2*cos_rot + pos.y;
		
		quad.vertices[3].x = x1*cos_rot - y2*sin_rot + pos.x;
		quad.vertices[3].y = x1*sin_rot + y2*cos_rot + pos.y;
	}
}

void QuadHelper::CalculateVertices(
	const mat4f& matrix, const vec2f& size)
{
	vec2f p1(0.f,    0.f);
	vec2f p2(size.x, 0.f);
	vec2f p3(size.x, size.y);
	vec2f p4(0.f,    size.y);
	
	matrix.ApplyToVector(p1);
	matrix.ApplyToVector(p2);
	matrix.ApplyToVector(p3);
	matrix.ApplyToVector(p4);
	
	quad.vertices[0].x = p1.x; quad.vertices[0].y = p1.y;
	quad.vertices[1].x = p2.x; quad.vertices[1].y = p2.y;
	quad.vertices[2].x = p3.x; quad.vertices[2].y = p3.y;
	quad.vertices[3].x = p4.x; quad.vertices[3].y = p4.y;
}

void QuadHelper::CalculateVertices(
	const mat4f& matrix, const vec2f& size,
	const vec2f& hotSpot, bool flip_x, bool flip_y)
{
	vec2f p1(-hotSpot.x,         -hotSpot.y);
	vec2f p2(size.x - hotSpot.x, -hotSpot.y);
	vec2f p3(size.x - hotSpot.x, size.y - hotSpot.y);
	vec2f p4(-hotSpot.x,         size.y - hotSpot.y);
	
	if ( flip_x ) {
		cpp::swap(p1, p2);
	}
	
	if ( flip_y ) {
		cpp::swap(p3, p4);
	}
	
	matrix.ApplyToVector(p1);
	matrix.ApplyToVector(p2);
	matrix.ApplyToVector(p3);
	matrix.ApplyToVector(p4);
	
	vec2f hs = matrix.ApplyToVector(hotSpot, false);
	quad.vertices[0].x = p1.x + hs.x; quad.vertices[0].y = p1.y + hs.y;
	quad.vertices[1].x = p2.x + hs.x; quad.vertices[1].y = p2.y + hs.y;
	quad.vertices[2].x = p3.x + hs.x; quad.vertices[2].y = p3.y + hs.y;
	quad.vertices[3].x = p4.x + hs.x; quad.vertices[3].y = p4.y + hs.y;
}

void QuadHelper::CalculateColors(
	const Color& color)
{
	quad.vertices[0].color =
	quad.vertices[1].color =
	quad.vertices[2].color =
	quad.vertices[3].color = color.getARGB();
}

void QuadHelper::CalculateColors(
	const Color& c1, const Color& c2,
	const Color& c3, const Color& c4)
{
	quad.vertices[0].color = c1.getARGB();
	quad.vertices[1].color = c2.getARGB();
	quad.vertices[2].color = c3.getARGB();
	quad.vertices[3].color = c4.getARGB();
}

void QuadHelper::Render() const
{
	theRenderer()->RenderObject(quad);
}

} // namespace fw3
} // namespace bme
