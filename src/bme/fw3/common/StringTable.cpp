/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/StringTable.cpp
 *****************************************************************************/

#include <bme/fw3/common/StringTable.h>
#include <bme/fw3/common/ExcelXmlTable.h>

namespace bme
{
namespace fw3
{

void StringTable::_load(const ExcelXmlTablePtr& table)
{
	if ( table )
	{
		const ExcelXmlTable::TWorksheets& worksheets = table->getWorksheets();
		for ( ExcelXmlTable::TWorksheets::const_iterator
			iter = worksheets.begin(), end = worksheets.end();
			iter != end; ++iter )
		{
			const StrId& worksheet = (*iter);
			for ( size_t i = 1, e = table->getMaxRow(worksheet) ; i <= e; ++i ) {
				StrId id_word    = table->GetCellData(worksheet, i, 1);
				StrId value_word = table->GetCellData(worksheet, i, 2);
				if ( !id_word.empty() && !value_word.empty() ) {
					_sections[worksheet][id_word] = value_word;
				}
			}
		}
	}
}

StringTable::StringTable()
{
}

StringTable::~StringTable()
{
}

StringTablePtr StringTable::Create(const Path& path)
{
	ExcelXmlTablePtr table = ExcelXmlTable::Create(path);
	if ( !table ) {
		BME_LOG_ERROR_FMT(
			"StringTable::Create() String table file is wrong. (\"%1%\")",
			path);
		return StringTablePtr();
	}
	StringTablePtr tmp = StringTablePtr(new StringTable());
	tmp->_load(table);
	return tmp;
}

const StrId& StringTable::GetString(const StrId& id, const StrId& section) const
{
	StrId& str = _sections[section][id];
	if ( str.empty() ) {
		str = "*none*";
		BME_LOG_ERROR_FMT(
			"StringTable::GetString(). Not found value of string. id='%1%' section='%2%'",
			id, section);
	}
	return str;
}

} // namespace fw3
} // namespace bme
