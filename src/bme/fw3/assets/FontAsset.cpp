/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/FontAsset.cpp
 *****************************************************************************/

#include <bme/fw3/assets/FontAsset.h>
#include <bme/fw3/common/XmlParser.h>

namespace bme
{
namespace fw3
{

// ----------------------------------------------------------------------------
// 
// Private
// 
// ----------------------------------------------------------------------------

bool FontAsset::_checkPreLoad() const
{
	if ( getPath().isEmpty() ) {
		return false;
	}
	
#if BME_MODE == BME_MODE_DEBUG
	if ( !theFilesystem() || !theFilesystem()->IsFileExists(getPath()) ) {
		return false;
	}
#endif
	
	return true;
}

// ----------------------------------------------------------------------------
// 
// Protected
// 
// ----------------------------------------------------------------------------

bool FontAsset::_loadSelf()
{
	if ( !LibraryAsset::_loadSelf() ) {
		return false;
	}
	return true;
}

void FontAsset::_freeSelf()
{
	LibraryAsset::_freeSelf();
}

bool FontAsset::_preLoadSelf(const Desc& desc)
{
	if ( !LibraryAsset::_preLoadSelf(desc) ) {
		return false;
	}
	
	_path      = desc.path;
	_smoothing = desc.smoothing;
	
	return _checkPreLoad();
}

bool FontAsset::_preLoadSelf(
	const pugi::xml_node& node, const LibraryPtr& library)
{
	if ( !LibraryAsset::_preLoadSelf(node, library) ) {
		return false;
	}
	
	BME_GET_XML_ATTRIBUTE(node, "path",      _path);
	BME_GET_XML_ATTRIBUTE(node, "smoothing", _smoothing);
	
	return _checkPreLoad();
}

FontAsset::FontAsset()
: _smoothing(true)
{
}

// ----------------------------------------------------------------------------
// 
// Public
// 
// ----------------------------------------------------------------------------

FontAsset::~FontAsset()
{
}

const Path& FontAsset::getPath() const
{
	return _path;
}

bool FontAsset::isSmoothing() const
{
	return _smoothing;
}

} // namespace fw3
} // namespace bme
