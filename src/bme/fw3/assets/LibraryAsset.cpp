/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/LibraryAsset.cpp
 *****************************************************************************/

#include <bme/fw3/assets/LibraryAsset.h>
#include <bme/fw3/common/XmlParser.h>

namespace bme
{
namespace fw3
{

// ----------------------------------------------------------------------------
// 
// Protected
// 
// ----------------------------------------------------------------------------

bool LibraryAsset::_loadSelf()
{
	return true;
}

void LibraryAsset::_freeSelf()
{
}

bool LibraryAsset::_preLoadSelf(const Desc& desc)
{
	_name = desc.name;
	return true;
}

bool LibraryAsset::_preLoadSelf(
	const pugi::xml_node& node, const LibraryPtr& /*library*/)
{
	BME_GET_XML_ATTRIBUTE(node, "name", _name);
	return true;
}

LibraryAsset::LibraryAsset()
: _isLoaded(false)
{
}

// ----------------------------------------------------------------------------
// 
// Public
// 
// ----------------------------------------------------------------------------

LibraryAsset::~LibraryAsset()
{
}

bool LibraryAsset::Load()
{
	if ( isLoaded() || _loadSelf() ) {
		_isLoaded = true;
		return true;
	}
	_freeSelf();
	return false;
}

void LibraryAsset::Free()
{
	if ( isLoaded() ) {
		_freeSelf();
		_isLoaded = false;
	}
}

bool LibraryAsset::isLoaded() const
{
	return _isLoaded;
}

const StrId& LibraryAsset::getName() const
{
	return _name;
}

} // namespace fw3
} // namespace bme
