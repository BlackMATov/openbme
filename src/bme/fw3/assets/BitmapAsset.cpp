/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/BitmapAsset.cpp
 *****************************************************************************/

#include <bme/fw3/assets/BitmapAsset.h>
#include <bme/fw3/common/XmlParser.h>

#include <bme/fw3/assets/Library.h>
#include <bme/fw3/assets/TextureAsset.h>

namespace bme
{
namespace fw3
{

// ----------------------------------------------------------------------------
// 
// Private
// 
// ----------------------------------------------------------------------------

bool BitmapAsset::_checkPreLoad() const
{
	if ( !_textureAsset ) {
		return false;
	}
	return true;
}

// ----------------------------------------------------------------------------
// 
// Protected
// 
// ----------------------------------------------------------------------------

bool BitmapAsset::_loadSelf()
{
	if ( !LibraryAsset::_loadSelf() ) {
		return false;
	}
	return _textureAsset ? _textureAsset->Load() : false;
}

void BitmapAsset::_freeSelf()
{
	LibraryAsset::_freeSelf();
}

bool BitmapAsset::_preLoadSelf(const Desc& desc)
{
	if ( !LibraryAsset::_preLoadSelf(desc) ) {
		return false;
	}
	
	_color     = desc.color;
	_smoothing = desc.smoothing;
	_texcoords = desc.texcoords;
	
	TextureAsset::Desc tex_desc;
	tex_desc.path = desc.path;
	_textureAsset = TextureAsset::Create(tex_desc, false);
	
	return _checkPreLoad();
}

bool BitmapAsset::_preLoadSelf(
	const pugi::xml_node& node, const LibraryPtr& library)
{
	if ( !LibraryAsset::_preLoadSelf(node, library) ) {
		return false;
	}
	
	BME_GET_XML_ATTRIBUTE(node, "color",     _color);
	BME_GET_XML_ATTRIBUTE(node, "smoothing", _smoothing);
	BME_GET_XML_ATTRIBUTE(node, "texcoords", _texcoords);
	
	StrId texture_asset_name;
	BME_GET_XML_ATTRIBUTE(node, "texture", texture_asset_name);
	_textureAsset = library->FindAsset<TextureAsset>(texture_asset_name);
	
	return _checkPreLoad();
}

BitmapAsset::BitmapAsset()
: _color(Color::white)
, _smoothing(true)
{
}

// ----------------------------------------------------------------------------
// 
// Public
// 
// ----------------------------------------------------------------------------

BitmapAssetPtr BitmapAsset::Create(const Desc& desc, bool with_load)
{
	BitmapAssetPtr ret_value = BitmapAssetPtr(new BitmapAsset());
	if ( !ret_value->_preLoadSelf(desc) ) {
		ret_value.reset();
	}
	if ( ret_value && with_load && !ret_value->Load() ) {
		ret_value.reset();
	}
	return ret_value;
}

BitmapAssetPtr BitmapAsset::Create(
	const pugi::xml_node& node, const LibraryPtr& library)
{
	BitmapAssetPtr ret_value = BitmapAssetPtr(new BitmapAsset());
	if ( !ret_value->_preLoadSelf(node, library) ) {
		ret_value.reset();
	}
	return ret_value;
}

BitmapAsset::~BitmapAsset()
{
}

const Color& BitmapAsset::getColor() const
{
	return _color;
}

bool BitmapAsset::isSmoothing() const
{
	return _smoothing;
}

const rectf& BitmapAsset::getTexCoords() const
{
	return _texcoords;
}

const TexturePtr& BitmapAsset::getTexture() const
{
	static TexturePtr empty_texture;
	return _textureAsset ? _textureAsset->getTexture() : empty_texture;
}

} // namespace fw3
} // namespace bme
