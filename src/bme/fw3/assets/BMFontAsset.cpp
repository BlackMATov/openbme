/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/BMFontAsset.cpp
 *****************************************************************************/

#include <bme/fw3/assets/BMFontAsset.h>
#include <bme/fw3/common/XmlParser.h>

namespace bme
{
namespace fw3
{

// ----------------------------------------------------------------------------
// 
// Private
// 
// ----------------------------------------------------------------------------

BMFontAsset::BMGlyph::BMGlyph(TChar glyph_id)
: firstKerning(size_t(-1))
{
	id = glyph_id;
}

bool BMFontAsset::BMGlyph::operator<(const BMGlyph& other) const
{
	return id < other.id;
}

BMFontAsset::KerningPair::KerningPair(TChar first, TChar second, ptrdiff_t amount)
: first(first), second(second), amount(amount)
{
}

bool BMFontAsset::KerningPair::operator<(const KerningPair& other) const
{
	return
		(first < other.first) || (first == other.first && second < other.second);
}

bool BMFontAsset::_parse()
{
	_clear();
	
	pugi::xml_document doc;
	if ( !LoadXmlDocument(getPath(), doc) ) {
		return false;
	}
	
	pugi::xml_node root_node = doc.child("font");
	if ( !root_node ) {
		return false;
	}
	
	// COMMON
	{
		pugi::xml_node common_node = root_node.child("common");
		if ( !common_node ) {
			return false;
		}
		
		BME_GET_XML_ATTRIBUTE(common_node, "base",       _info.base);
		BME_GET_XML_ATTRIBUTE(common_node, "lineHeight", _info.lineHeight);
		BME_GET_XML_ATTRIBUTE(common_node, "scaleW",     _info.pageSize.x);
		BME_GET_XML_ATTRIBUTE(common_node, "scaleH",     _info.pageSize.y);
		BME_GET_XML_ATTRIBUTE(common_node, "pages",      _info.pages);
		
		if ( _info.pages <= 0 ) {
			return false;
		}
		
		_pages.resize(_info.pages);
	}
	
	// PAGES
	{
		pugi::xml_node pages_node = root_node.child("pages");
		if ( !pages_node ) {
			return false;
		}
		
		for ( pugi::xml_node page_node = pages_node.child("page");
			page_node; page_node = page_node.next_sibling("page") )
		{
			size_t page_id   = page_node.attribute("id").as_uint();
			Path   page_path = Path(page_node.attribute("file").value());
			
			if ( page_id >= _pages.size() ) {
				return false;
			}
			
			Path tex_path = getPath().getParentPath() / page_path;
			_pages[page_id] = Texture::Get(tex_path);
		}
	}
	
	// CHARS
	{
		pugi::xml_node chars_node = root_node.child("chars");
		if ( !chars_node ) {
			return false;
		}
		
		size_t count = 0;
		BME_GET_XML_ATTRIBUTE(chars_node, "count", count);
		if ( count > 0 ) {
			_glyphs.reserve(count);
			for ( pugi::xml_node char_node = chars_node.child("char");
				 char_node; char_node = char_node.next_sibling("char") )
			{
				BMGlyph glyph;
				ptrdiff_t id = char_node.attribute("id").as_int();
				f32 x = 0.f, y = 0.f, w = 0.f, h = 0.f;
				BME_GET_XML_ATTRIBUTE(char_node, "x",        x);
				BME_GET_XML_ATTRIBUTE(char_node, "y",        y);
				BME_GET_XML_ATTRIBUTE(char_node, "width",    w);
				BME_GET_XML_ATTRIBUTE(char_node, "height",   h);
				BME_GET_XML_ATTRIBUTE(char_node, "xoffset",  glyph.offset.x);
				BME_GET_XML_ATTRIBUTE(char_node, "yoffset",  glyph.offset.y);
				BME_GET_XML_ATTRIBUTE(char_node, "xadvance", glyph.advance);
				BME_GET_XML_ATTRIBUTE(char_node, "page",     glyph.page);
				glyph.pageCoords = rectf(x, y, x + w, y + h);
				if ( id >= 0 ) {
					glyph.id = Math::SmartCast<TChar>(id);
					_glyphs.push_back(glyph);
				}
			}
			cpp::sort(_glyphs.begin(), _glyphs.end());
		}
	}
	
	// KERNING
	{
		pugi::xml_node kernings_node = root_node.child("kernings");
		if ( kernings_node ) {
			size_t count = 0;
			BME_GET_XML_ATTRIBUTE(kernings_node, "count", count);
			if ( count > 0 ) {
				_kerningPairs.reserve(count);
				for ( pugi::xml_node kerning_node = kernings_node.child("kerning");
					 kerning_node; kerning_node = kerning_node.next_sibling("kerning") )
				{
					KerningPair kerning_pair;
					BME_GET_XML_ATTRIBUTE(kerning_node, "first",  kerning_pair.first);
					BME_GET_XML_ATTRIBUTE(kerning_node, "second", kerning_pair.second);
					BME_GET_XML_ATTRIBUTE(kerning_node, "amount", kerning_pair.amount);
					_kerningPairs.push_back(kerning_pair);
				}
				cpp::sort(_kerningPairs.begin(), _kerningPairs.end());
			}
		}
	}
	
	// KERNINGS FLAGS
	{
		size_t current_glyph = 0;
		for ( size_t i = 0, e = _kerningPairs.size(); i < e; ++i ) {
			TChar kerning_glyph = _kerningPairs[i].first;
			while ( current_glyph < _glyphs.size() ) {
				BMGlyph& glyph = _glyphs[current_glyph];
				if ( glyph.id > kerning_glyph ) {
					break;
				}
				
				++current_glyph;
				
				if ( glyph.id == kerning_glyph ) {
					glyph.firstKerning = i;
					break;
				}
			}
		}
	}
	
	return true;
}

void BMFontAsset::_clear()
{
	_info = Info();
	_pages.clear();
	_glyphs.clear();
	_kerningPairs.clear();
}

// ----------------------------------------------------------------------------
// 
// Protected
// 
// ----------------------------------------------------------------------------

bool BMFontAsset::_loadSelf()
{
	if ( !FontAsset::_loadSelf() ) {
		return false;
	}
	return _parse();
}

void BMFontAsset::_freeSelf()
{
	FontAsset::_freeSelf();
	_clear();
}

bool BMFontAsset::_preLoadSelf(const Desc& desc)
{
	if ( !FontAsset::_preLoadSelf(desc) ) {
		return false;
	}
	return true;
}

bool BMFontAsset::_preLoadSelf(
	const pugi::xml_node& node, const LibraryPtr& library)
{
	if ( !FontAsset::_preLoadSelf(node, library) ) {
		return false;
	}
	return true;
}

BMFontAsset::BMFontAsset()
{
}

// ----------------------------------------------------------------------------
// 
// Public
// 
// ----------------------------------------------------------------------------

BMFontAssetPtr BMFontAsset::Create(const Desc& desc, bool with_load)
{
	BMFontAssetPtr ret_value = BMFontAssetPtr(new BMFontAsset());
	if ( !ret_value->_preLoadSelf(desc) ) {
		ret_value.reset();
	}
	if ( ret_value && with_load && !ret_value->Load() ) {
		ret_value.reset();
	}
	return ret_value;
}

BMFontAssetPtr BMFontAsset::Create(
	const pugi::xml_node& node, const LibraryPtr& library)
{
	BMFontAssetPtr ret_value = BMFontAssetPtr(new BMFontAsset());
	if ( !ret_value->_preLoadSelf(node, library) ) {
		ret_value.reset();
	}
	return ret_value;
}

BMFontAsset::~BMFontAsset()
{
}

const FontAsset::Info& BMFontAsset::getInfo() const
{
	return _info;
}

const FontAsset::Glyph* BMFontAsset::FindGlyph(TChar symbol) const
{
	TGlyphs::const_iterator iter =
		cpp::lower_bound(_glyphs.begin(), _glyphs.end(),
		BMGlyph(symbol));
	if ( iter != _glyphs.end() && iter->id == symbol ) {
		return &(*iter);
	}
	return NULL;
}

TexturePtr BMFontAsset::FindPage(size_t page) const
{
	return page < _pages.size()
		? _pages[page]
		: TexturePtr();
}

ptrdiff_t BMFontAsset::FindKerning(const Glyph* first, const Glyph* second) const
{
	const BMGlyph* bmfirst = static_cast<const BMGlyph*>(first);
	if ( !first || !second || !bmfirst ) {
		return 0;
	}
	if ( size_t(-1) == bmfirst->firstKerning ||
		_kerningPairs.size() <= bmfirst->firstKerning )
	{
		return 0;
	}
	TKerningPairs::const_iterator iter =
		cpp::lower_bound(
			_kerningPairs.begin() + bmfirst->firstKerning, _kerningPairs.end(),
			KerningPair(first->id, second->id));
	if ( iter != _kerningPairs.end() &&
		iter->first == first->id && iter->second == second->id )
	{
		return iter->amount;
	}
	return 0;
}

} // namespace fw3
} // namespace bme
