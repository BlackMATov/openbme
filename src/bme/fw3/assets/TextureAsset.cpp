/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/TextureAsset.cpp
 *****************************************************************************/

#include <bme/fw3/assets/TextureAsset.h>
#include <bme/fw3/common/XmlParser.h>

namespace bme
{
namespace fw3
{

// ----------------------------------------------------------------------------
// 
// Private
// 
// ----------------------------------------------------------------------------

bool TextureAsset::_checkPreLoad() const
{
	if ( getPath().isEmpty() ) {
		return false;
	}
	
#if BME_MODE == BME_MODE_DEBUG
	if ( !theFilesystem() || !theFilesystem()->IsFileExists(getPath()) ) {
		return false;
	}
#endif
	
	return true;
}

// ----------------------------------------------------------------------------
// 
// Protected
// 
// ----------------------------------------------------------------------------

bool TextureAsset::_loadSelf()
{
	if ( !LibraryAsset::_loadSelf() ) {
		return false;
	}
	_texture = Texture::Get(getPath());
	return _texture ? true : false;
}

void TextureAsset::_freeSelf()
{
	LibraryAsset::_freeSelf();
	_texture.reset();
}

bool TextureAsset::_preLoadSelf(const Desc& desc)
{
	if ( !LibraryAsset::_preLoadSelf(desc) ) {
		return false;
	}
	_path = desc.path;
	return _checkPreLoad();
}

bool TextureAsset::_preLoadSelf(
	const pugi::xml_node& node, const LibraryPtr& library)
{
	if ( !LibraryAsset::_preLoadSelf(node, library) ) {
		return false;
	}
	BME_GET_XML_ATTRIBUTE(node, "path", _path);
	return _checkPreLoad();
}

TextureAsset::TextureAsset()
{
}

// ----------------------------------------------------------------------------
// 
// Public
// 
// ----------------------------------------------------------------------------

TextureAssetPtr TextureAsset::Create(const Desc& desc, bool with_load)
{
	TextureAssetPtr ret_value = TextureAssetPtr(new TextureAsset());
	if ( !ret_value->_preLoadSelf(desc) ) {
		ret_value.reset();
	}
	if ( ret_value &&  with_load && !ret_value->Load() ) {
		ret_value.reset();
	}
	return ret_value;
}

TextureAssetPtr TextureAsset::Create(
	const pugi::xml_node& node, const LibraryPtr& library)
{
	TextureAssetPtr ret_value = TextureAssetPtr(new TextureAsset());
	if ( !ret_value->_preLoadSelf(node, library) ) {
		ret_value.reset();
	}
	return ret_value;
}

TextureAsset::~TextureAsset()
{
}

const Path& TextureAsset::getPath() const
{
	return _path;
}

const TexturePtr& TextureAsset::getTexture() const
{
	return _texture;
}

} // namespace fw3
} // namespace bme
