/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/fw3/Library.cpp
 *****************************************************************************/

#include <bme/fw3/assets/Library.h>
#include <bme/fw3/common/XmlParser.h>

#include <bme/fw3/assets/BitmapAsset.h>
#include <bme/fw3/assets/BMFontAsset.h>
#include <bme/fw3/assets/TextureAsset.h>

namespace bme
{
namespace fw3
{

// ----------------------------------------------------------------------------
// 
// library_detail
// 
// ----------------------------------------------------------------------------

namespace library_detail
{
	static Library::TLoaders s_loaders;
	static RecursiveMutex s_loaders_mutex;
	
	static LibraryAssetPtr CreateAsset(
		const Path& path, const Library::TLoaders& loaders,
		const pugi::xml_node& node, const LibraryPtr& library,
		StrId& outType, StrId& outName)
	{
		StrId type;
		BME_GET_XML_ATTRIBUTE(node, "type", type);
		if ( type.empty() ) {
			BME_LOG_ERROR_FMT(
				"Type asset not specified (Path: %1%)",
				path);
			return LibraryAssetPtr();
		}
		Library::TLoaders::const_iterator iter = loaders.find(type);
		if ( iter == loaders.end() ) {
			BME_LOG_ERROR_FMT(
				"Asset loader not found (Type: %1%, Path: %2%)",
				type, path);
			return LibraryAssetPtr();
		}
		LibraryAssetPtr asset = (*iter).second(node, library);
		if ( !asset ) {
			BME_LOG_ERROR_FMT(
				"Asset loader error (Type: %1%, Path: %2%)",
				type, path);
			return LibraryAssetPtr();
		}
		const StrId& name = asset->getName();
		if ( name.empty() ) {
			BME_LOG_ERROR_FMT(
				"Name asset not specified (Type: %1%, Path: %2%)",
				type, path);
			return LibraryAssetPtr();
		}
		outType = type;
		outName = name;
		return asset;
	}
} // namespace library_detail

// ----------------------------------------------------------------------------
// 
// Library
// 
// ----------------------------------------------------------------------------

bool Library::_load(const Path& path)
{
	pugi::xml_document doc;
	if ( !LoadXmlDocument(path, doc) ) {
		return false;
	}
	
	pugi::xml_node root_node = doc.child("library");
	if ( !root_node ) {
		return false;
	}
	
	LibraryPtr library = LibraryPtr(this);
	
	for ( pugi::xml_node asset_node = root_node.child("asset");
		asset_node; asset_node = asset_node.next_sibling("asset") )
	{
		StrId asset_type;
		StrId asset_name;
		LibraryAssetPtr asset = library_detail::CreateAsset(
			path, _loaders, asset_node, library,
			asset_type, asset_name);
		if ( asset ) {
			if ( _findAsset(asset_name) ) {
				BME_LOG_ERROR_FMT(
					"Dublicate asset name (Type: %1%, Name: %2%, Path: %3%)",
					asset_type, asset_name, path);
			} else {
				_assets.insert(cpp::make_pair(asset_name, asset));
				BME_LOG_COMPLETE_FMT(
					"Preload asset (Type: %1%, Name: %2%)",
					asset_type, asset->getName());
			}
		}
	}
	return true;
}

LibraryAssetPtr Library::_findAsset(const StrId& name) const
{
	TAssets::const_iterator iter = _assets.find(name);
	return iter != _assets.end()
		? (*iter).second
		: LibraryAssetPtr();
}

void Library::_register(const StrId& type, TLoader loader)
{
	if ( !type.empty() && loader ) {
		using namespace library_detail;
		MutexLockGuard<RecursiveMutex> guard(s_loaders_mutex);
		s_loaders.insert(cpp::make_pair(type,loader));
	}
}

bool Library::_unregister(const StrId& type)
{
	using namespace library_detail;
	MutexLockGuard<RecursiveMutex> guard(s_loaders_mutex);
	TLoaders::iterator iter = s_loaders.find(type);
	if ( iter != s_loaders.end() ) {
		s_loaders.erase(iter);
		return true;
	}
	return false;
}

LibraryPtr Library::Create(const Path& path)
{
	LibraryPtr ret_value = LibraryPtr(new Library());
	if ( !ret_value->_load(path) ) {
		ret_value.reset();
	}
	return ret_value;
}

Library::Library()
{
	using namespace library_detail;
	MutexLockGuard<RecursiveMutex> quard(s_loaders_mutex);
	if ( s_loaders.empty() ) {
		Register< BitmapAsset>();
		Register< BMFontAsset>();
		Register<TextureAsset>();
	}
	_loaders = s_loaders;
}

Library::~Library()
{
}

} // namespace fw3
} // namespace bme
