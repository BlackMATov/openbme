/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/utils/Jobber.cpp
 *****************************************************************************/

#include <bme/utils/Jobber.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// JobberTask
// 
// ----------------------------------------------------------------------------

void JobberTask::_run()
{
	BME_ASSERT_MSG(false, "JobberTask::_run not impl!");
}

void JobberTask::_reset()
{
	BME_ASSERT_MSG(false, "JobberTask::_reset not impl!");
}

void JobberTask::_done()
{
}

JobberTask::JobberTask()
{
}

JobberTask::~JobberTask()
{
}

void JobberTask::Run()
{
	MutexLockGuard<RecursiveMutex> guard(_mutex);
	if ( !isReady() ) {
		_run();
		_done();
		_ready.set();
	}
}

void JobberTask::Reset()
{
	MutexLockGuard<RecursiveMutex> guard(_mutex);
	if ( isReady() ) {
		_reset();
		_ready.reset();
	}
}

void JobberTask::Wait()
{
	if ( !isReady() ) {
		Run();
	}
}

bool JobberTask::isReady() const
{
	return _ready.is();
}

// ----------------------------------------------------------------------------
// 
// Jobber
// 
// ----------------------------------------------------------------------------

void Jobber::_threadMain()
{
	for ( ; ; ) {
		if ( !_processTask() ) {
			MutexLockGuard<RecursiveMutex> guard(_sleepMutex);
			if(_isCanceled()) {
				break;
			}
			_sleepCond.Wait(_sleepMutex);
		}
	}
}

void Jobber::_threadMain(void* arg)
{
	Jobber* jobber = static_cast<Jobber*>(arg);
	jobber->_threadMain();
}

void Jobber::_wakeUp(bool all)
{
	MutexLockGuard<RecursiveMutex> guard(_sleepMutex);
	if ( all ) {
		_sleepCond.NotifyAll();
	} else {
		_sleepCond.NotifyOne();
	}
}

bool Jobber::_processTask()
{
	JobberTaskPtr task = _getNextTask();
	if ( task ) {
		task->Run();
		--_tasksNum;
		return true;
	}
	return false;
}

JobberTaskPtr Jobber::_getNextTask()
{
	MutexLockGuard<RecursiveMutex> guard(_tasksMutex);
	if ( !_tasks.empty() ) {
		JobberTaskPtr task = _tasks.front();
		_tasks.pop_front();
		return task;
	}
	return JobberTaskPtr();
}

bool Jobber::_isSkipAfter() const
{
	return _skipAfter.is();
}

bool Jobber::_isCanceled() const
{
	return _canceled.is();
}

Jobber::Jobber(size_t num_threads, bool skip_after)
: _skipAfter(skip_after)
, _tasks(BME_MAX_JOBBER_TASKS)
{
	if ( num_threads > 0 ) {
		_threads.resize(num_threads);
		for ( size_t i = 0; i < num_threads; ++i ) {
			_threads[i].reset(new Thread(&Jobber::_threadMain, this));
		}
	}
}

Jobber::~Jobber()
{
	_canceled.set();
	if ( _isSkipAfter() ) {
		MutexLockGuard<RecursiveMutex> guard(_tasksMutex);
		while ( !_tasks.empty() ) {
			_tasks.pop_back();
			--_tasksNum;
		}
	}
	_wakeUp(true);
	WaitAll();
	while ( !_threads.empty() ) {
		ThreadPtr thread = _threads.back();
		thread->Join();
		_threads.pop_back();
	}
}

void Jobber::WaitAll()
{
	while ( !isEmpty() ) {
		_processTask();
	}
}

void Jobber::WaitAll(WaitFunc func, void* data)
{
	WaitAll();
	if ( func ) {
		MutexLockGuard<RecursiveMutex> guard(_tasksMutex);
		WaitAll();
		func(data);
	}
}

void Jobber::AddTask(const JobberTaskPtr& task)
{
	MutexLockGuard<RecursiveMutex> guard(_tasksMutex);
	while ( _tasks.full() ) {
		_processTask();
	}
	++_tasksNum;
	_tasks.push_back(task);
	_wakeUp(false);
}

bool Jobber::isEmpty() const
{
	return 0 == _tasksNum.get();
}

} // namespace bme
