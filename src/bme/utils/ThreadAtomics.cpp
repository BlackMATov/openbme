/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/utils/ThreadAtomics.cpp
 *****************************************************************************/

#include <bme/utils/ThreadAtomics.h>

#if BME_OS == BME_OS_WINDOWS
#	include <windows.h>
#elif BME_OS == BME_OS_MACOSX || BME_OS == BME_OS_IOS
#	include <libkern/OSAtomic.h>
#endif

namespace bme
{

// ----------------------------------------------------------------------------
// 
// AtomicCounter
// 
// ----------------------------------------------------------------------------

// ------------------------------------
// BME_OS_WINDOWS
// ------------------------------------

#if BME_OS == BME_OS_WINDOWS
AtomicCounter::AtomicCounter()
: _value(0)
{
}

AtomicCounter::AtomicCounter(const AtomicCounter& other)
: _value(other.get())
{
}

AtomicCounter& AtomicCounter::operator=(const AtomicCounter& other)
{
	if ( this != &other ) {
		set(other.get());
	}
	return *this;
}

AtomicCounter::~AtomicCounter()
{
}

AtomicCounter::AtomicCounter(s32 value)
: _value(value)
{
}

AtomicCounter& AtomicCounter::operator=(s32 value)
{
	set(value);
	return *this;
}

s32 AtomicCounter::operator++()
{
	LONG res = ::InterlockedIncrement(
		reinterpret_cast<volatile LONG*>(&_value));
	return static_cast<s32>(res);
}

s32 AtomicCounter::operator++(int)
{
	s32 res = ++(*this);
	return --res;
}

s32 AtomicCounter::operator--()
{
	LONG res = ::InterlockedDecrement(
		reinterpret_cast<volatile LONG*>(&_value));
	return static_cast<s32>(res);
}

s32 AtomicCounter::operator--(int)
{
	s32 res = --(*this);
	return ++res;
}

s32 AtomicCounter::get() const
{
	return _value;
}

void AtomicCounter::set(s32 value)
{
	::InterlockedExchange(
		reinterpret_cast<volatile LONG*>(&_value),
		static_cast<LONG>(value));
}
// ------------------------------------
// BME_OS_MACOSX || BME_OS_IOS
// ------------------------------------
#elif BME_OS == BME_OS_MACOSX || BME_OS == BME_OS_IOS
AtomicCounter::AtomicCounter()
: _value(0)
{
}

AtomicCounter::AtomicCounter(const AtomicCounter& other)
: _value(other.get())
{
}

AtomicCounter& AtomicCounter::operator=(const AtomicCounter& other)
{
	if ( this != &other ) {
		set(other.get());
	}
	return *this;
}

AtomicCounter::~AtomicCounter()
{
}

AtomicCounter::AtomicCounter(s32 value)
: _value(value)
{
}

AtomicCounter& AtomicCounter::operator=(s32 value)
{
	set(value);
	return *this;
}

s32 AtomicCounter::operator++()
{
	return ::OSAtomicIncrement32(&_value);
}

s32 AtomicCounter::operator++(int)
{
	s32 res = ++(*this);
	return --res;
}

s32 AtomicCounter::operator--()
{
	return ::OSAtomicDecrement32(&_value);
}

s32 AtomicCounter::operator--(int)
{
	s32 res = --(*this);
	return ++res;
}

s32 AtomicCounter::get() const
{
	return _value;
}

void AtomicCounter::set(s32 value)
{
	/// \todo ::OSAtomicTestAndSet ?
	_value = value;
}
// ------------------------------------
// POSIX
// ------------------------------------
#else
AtomicCounter::AtomicCounter()
: _value(0)
{
}

AtomicCounter::AtomicCounter(const AtomicCounter& other)
: _value(other.get())
{
}

AtomicCounter& AtomicCounter::operator=(const AtomicCounter& other)
{
	if ( this != &other ) {
		set(other.get());
	}
	return *this;
}

AtomicCounter::~AtomicCounter()
{
}

AtomicCounter::AtomicCounter(s32 value)
: _value(value)
{
}

AtomicCounter& AtomicCounter::operator=(s32 value)
{
	set(value);
	return *this;
}

s32 AtomicCounter::operator++()
{
	return __sync_add_and_fetch(&_value, 1);
}

s32 AtomicCounter::operator++(int)
{
	return __sync_fetch_and_add(&_value, 1);
}

s32 AtomicCounter::operator--()
{
	return __sync_sub_and_fetch(&_value, 1);
}

s32 AtomicCounter::operator--(int)
{
	return __sync_fetch_and_sub(&_value, 1);
}

s32 AtomicCounter::get() const
{
	return _value;
}

void AtomicCounter::set(s32 value)
{
	__sync_lock_test_and_set(&_value, value);
}
#endif

// ----------------------------------------------------------------------------
// 
// AtomicFlag
// 
// ----------------------------------------------------------------------------

AtomicFlag::AtomicFlag()
: _value(0)
{
}

AtomicFlag::AtomicFlag(const AtomicFlag& other)
: _value(other._value)
{
}

AtomicFlag& AtomicFlag::operator=(const AtomicFlag& other)
{
	if ( this != &other ) {
		_value = other._value;
	}
	return *this;
}

AtomicFlag::~AtomicFlag()
{
}

AtomicFlag::AtomicFlag(bool value)
: _value(value ? 1 : 0)
{
}

AtomicFlag& AtomicFlag::operator=(bool value)
{
	_value.set(value ? 1 : 0);
	return *this;
}

bool AtomicFlag::is() const
{
	return 0 != _value.get();
}

void AtomicFlag::set()
{
	_value.set(1);
}

void AtomicFlag::reset()
{
	_value.set(0);
}

} // namespace bme
