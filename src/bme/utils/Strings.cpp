/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/utils/Strings.cpp
 *****************************************************************************/

#include <bme/utils/Strings.h>

#include <bme/utils/Path.h>
#include <bme/utils/StrId.h>
#include <bme/utils/Color.h>
#include <bme/utils/SmartBuffer.h>

// libraries from depends
#include <utf8cpp/utf8.h>
#include <cyoencode/CyoDecode.h>
#include <cyoencode/CyoEncode.h>

namespace bme
{
namespace Strings
{
	// ------------------------------------------------------------------------
	// 
	// strings_detail
	// 
	// ------------------------------------------------------------------------
	
	namespace strings_detail
	{
		const char* const default_seps = " ,|;";
		
		template < typename T >
		size_t t_str_len(const T* str)
		{
			const T* end = str;
			for ( ; *end; ++end ) {
				continue;
			}
			return end - str;
		}
		
		template < typename WCharType >
		static typename cpp::enable_if_c<2 == sizeof(WCharType), bool>::type
			Utf8ToWide( WStr& dest, const char* src )
		{
			dest.clear();
			size_t src_len = strings_detail::t_str_len(src);
			dest.reserve(src_len);
			try {
				utf8::utf8to16(src, src + src_len, cpp::back_inserter(dest));
				return true;
			} catch (utf8::exception&) {
				dest.clear();
				return false;
			}
		}
		
		template < typename WCharType >
		static typename cpp::enable_if_c<4 == sizeof(WCharType), bool>::type
			Utf8ToWide( WStr& dest, const char* src)
		{
			dest.clear();
			size_t src_len = strings_detail::t_str_len(src);
			dest.reserve(src_len);
			try {
				utf8::utf8to32(src, src + src_len, cpp::back_inserter(dest));
				return true;
			} catch (utf8::exception&) {
				dest.clear();
				return false;
			}
		}
		
		template < typename WCharType >
		static typename cpp::enable_if_c<2 == sizeof(WCharType), bool>::type
			WideToUtf8( Str& dest, const wchar_t* src)
		{
			dest.clear();
			size_t src_len = strings_detail::t_str_len(src);
			dest.reserve(src_len*sizeof(WCharType)/sizeof(char));
			try {
				utf8::utf16to8(src, src + src_len, cpp::back_inserter(dest));
				return true;
			} catch (utf8::exception&) {
				dest.clear();
				return false;
			}
		}
		
		template < typename WCharType >
		static typename cpp::enable_if_c<4 == sizeof(WCharType), bool>::type
			WideToUtf8( Str& dest, const wchar_t* src)
		{
			dest.clear();
			size_t src_len = strings_detail::t_str_len(src);
			dest.reserve(src_len*sizeof(WCharType)/sizeof(char));
			try {
				utf8::utf32to8(src, src + src_len, cpp::back_inserter(dest));
				return true;
			} catch (utf8::exception&) {
				dest.clear();
				return false;
			}
		}
		
		static bool Utf8To32(UStr& dest, const char* src)
		{
			dest.clear();
			size_t src_len = strings_detail::t_str_len(src);
			try {
				dest.reserve(src_len);
				utf8::utf8to32(src, src + src_len, cpp::back_inserter(dest));
				return true;
			} catch (utf8::exception&) {
				dest.clear();
				return false;
			}
		}
		
		static bool Utf32To8(Str& dest, const u32* src)
		{
			dest.clear();
			size_t src_len = strings_detail::t_str_len(src);
			try {
				dest.reserve(src_len*sizeof(u32)/sizeof(u8));
				utf8::utf32to8(src, src + src_len, cpp::back_inserter(dest));
				return true;
			} catch (utf8::exception&) {
				dest.clear();
				return false;
			}
		}
		
		// 2 f32
		
		static bool ParseValues(
			const char* str,
			f32* outValue1, f32* outValue2)
		{
			char tmp_str[256] = {0};
			BME_strcpy(tmp_str, str);
			
			// v1
			
			char* token = BME_strtok(tmp_str, default_seps);
			if ( !token ) {
				return false;
			}
			
			f32 v1 = 0.f;
			if ( !Strings::ParseToType(v1, token) )
				return false;
			
			// v2
			
			token = BME_strtok(NULL, default_seps);
			if ( !token ) {
				return false;
			}
			
			f32 v2 = 0.f;
			if ( !Strings::ParseToType(v2, token) ) {
				return false;
			}
			
			// return
			
			if ( outValue1 ) *outValue1 = v1;
			if ( outValue2 ) *outValue2 = v2;
			return true;
		}
		
		// 3 f32
		
		static bool ParseValues(
			const char* str,
			f32* outValue1, f32* outValue2, f32* outValue3)
		{
			char tmp_str[256] = {0};
			BME_strcpy(tmp_str, str);
			
			// v1
			
			char* token = BME_strtok(tmp_str, default_seps);
			if ( !token ) {
				return false;
			}
			
			f32 v1 = 0.f;
			if ( !Strings::ParseToType(v1, token) ) {
				return false;
			}
			
			// v2
			
			token = BME_strtok(NULL, default_seps);
			if ( !token ) {
				return false;
			}
			
			f32 v2 = 0.f;
			if ( !Strings::ParseToType(v2, token) ) {
				return false;
			}
			
			// v3
			
			token = BME_strtok(NULL, default_seps);
			if ( !token ) {
				return false;
			}
			
			f32 v3 = 0.f;
			if ( !Strings::ParseToType(v3, token) ) {
				return false;
			}
			
			// return
			
			if ( outValue1 ) *outValue1 = v1;
			if ( outValue2 ) *outValue2 = v2;
			if ( outValue3 ) *outValue3 = v3;
			return true;
		}
		
		// 4 f32
		
		static bool ParseValues(
			const char* str,
			f32* outValue1, f32* outValue2, f32* outValue3, f32* outValue4)
		{
			char tmp_str[256] = {0};
			BME_strcpy(tmp_str, str);
			
			// v1
			
			char* token = BME_strtok(tmp_str, default_seps);
			if ( !token ) {
				return false;
			}
			
			f32 v1 = 0.f;
			if ( !Strings::ParseToType(v1, token) ) {
				return false;
			}
			
			// v2
			
			token = BME_strtok(NULL, default_seps);
			if ( !token ) {
				return false;
			}
			
			f32 v2 = 0.f;
			if ( !Strings::ParseToType(v2, token) ) {
				return false;
			}
			
			// v3
			
			token = BME_strtok(NULL, default_seps);
			if ( !token ) {
				return false;
			}
			
			f32 v3 = 0.f;
			if ( !Strings::ParseToType(v3, token) ) {
				return false;
			}
			
			// v4
			
			token = BME_strtok(NULL, default_seps);
			if ( !token ) {
				return false;
			}
			
			f32 v4 = 0.f;
			if ( !Strings::ParseToType(v4, token) ) {
				return false;
			}
			
			// return
			
			if ( outValue1 ) *outValue1 = v1;
			if ( outValue2 ) *outValue2 = v2;
			if ( outValue3 ) *outValue3 = v3;
			if ( outValue4 ) *outValue4 = v4;
			return true;
		}
		
		// 2 s32
		
		static bool ParseValues(
			const char* str,
			s32* outValue1, s32* outValue2)
		{
			f32 v1 = 0.f, v2 = 0.f;
			if ( !ParseValues(str, &v1, &v2) ) {
				return false;
			}
			if ( outValue1 ) *outValue1 = Math::SmartCast<s32>(v1);
			if ( outValue2 ) *outValue2 = Math::SmartCast<s32>(v2);
			return true;
		}
		
		// 4 s32
		
		static bool ParseValues(
			const char* str,
			s32* outValue1, s32* outValue2, s32* outValue3, s32* outValue4)
		{
			f32 v1 = 0.f, v2 = 0.f, v3 = 0.f, v4 = 0.f;
			if ( !ParseValues(str, &v1, &v2, &v3, &v4) ) {
				return false;
			}
			if ( outValue1 ) *outValue1 = Math::SmartCast<s32>(v1);
			if ( outValue2 ) *outValue2 = Math::SmartCast<s32>(v2);
			if ( outValue3 ) *outValue3 = Math::SmartCast<s32>(v3);
			if ( outValue4 ) *outValue4 = Math::SmartCast<s32>(v4);
			return true;
		}
		
		// 2 u32
		
		static bool ParseValues(
			const char* str,
			u32* outValue1, u32* outValue2)
		{
			f32 v1 = 0.f, v2 = 0.f;
			if ( !ParseValues(str, &v1, &v2)
				|| v1 < 0.f || v2 < 0.f ) {
				return false;
			}
			if ( outValue1 ) *outValue1 = Math::SmartCast<u32>(v1);
			if ( outValue2 ) *outValue2 = Math::SmartCast<u32>(v2);
			return true;
		}
		
		// 4 u32
		
		static bool ParseValues(
			const char* str,
			u32* outValue1, u32* outValue2, u32* outValue3, u32* outValue4)
		{
			f32 v1 = 0.f, v2 = 0.f, v3 = 0.f, v4 = 0.f;
			if ( !ParseValues(str, &v1, &v2, &v3, &v4)
				|| v1 < 0.f || v2 < 0.f || v3 < 0.f || v4 < 0.f ) {
				return false;
			}
			if ( outValue1 ) *outValue1 = Math::SmartCast<u32>(v1);
			if ( outValue2 ) *outValue2 = Math::SmartCast<u32>(v2);
			if ( outValue3 ) *outValue3 = Math::SmartCast<u32>(v3);
			if ( outValue4 ) *outValue4 = Math::SmartCast<u32>(v4);
			return true;
		}
	}
	
	// ------------------------------------------------------------------------
	// 
	// Unicode conversions
	// 
	// ------------------------------------------------------------------------
	
	// --------------------------------
	// MakeUtf8
	// --------------------------------
	
	bool MakeUtf8(Str& dest, const char* src)
	{
		dest = src;
		return true;
	}
	
	bool MakeUtf8(Str& dest, const wchar_t* src)
	{
		return strings_detail::WideToUtf8<wchar_t>(dest, src);
	}
	
	bool MakeUtf8(Str& dest, const u32* src)
	{
		return strings_detail::Utf32To8(dest, src);
	}
	
	// --------------------------------
	// MakeWide
	// --------------------------------
	
	bool MakeWide(WStr& dest, const char* src)
	{
		return strings_detail::Utf8ToWide<wchar_t>(dest, src);
	}
	
	bool MakeWide(WStr& dest, const wchar_t* src)
	{
		dest = src;
		return true;
	}
	
	bool MakeWide(WStr& dest, const u32* src)
	{
		Str utf8;
		if ( !strings_detail::Utf32To8(utf8, src) ) {
			dest.clear();
			return false;
		}
		return strings_detail::Utf8ToWide<wchar_t>(dest, utf8.c_str());
	}
	
	// --------------------------------
	// MakeUtf32
	// --------------------------------
	
	bool MakeUtf32(UStr& dest, const char* src)
	{
		return strings_detail::Utf8To32(dest, src);
	}
	
	bool MakeUtf32(UStr& dest, const wchar_t* src)
	{
		Str utf8;
		if ( !strings_detail::WideToUtf8<wchar_t>(utf8, src) ) {
			dest.clear();
			return false;
		}
		return strings_detail::Utf8To32(dest, utf8.c_str());
	}
	
	bool MakeUtf32(UStr& dest, const u32* src)
	{
		dest = src;
		return true;
	}
	
	// ------------------------------------------------------------------------
	// 
	// Wildcards
	// 
	// ------------------------------------------------------------------------
	
	bool WildcardCheck(const char* wild, const char* string)
	{
		BME_ASSERT(wild && string);
		const char *cp = NULL, *mp = NULL;
		while ( *string && *wild != '*' ) {
			if ( *wild != *string && *wild != '?' ) {
				return false;
			}
			++wild;
			++string;
		}
		while ( *string ) {
			if ( *wild == '*' ) {
				if ( !*++wild ) {
					return true;
				}
				mp = wild;
				cp = string + 1;
			} else if ( *wild == *string || *wild == '?' ) {
				++wild;
				++string;
			} else {
				wild = mp;
				string = cp++;
			}
		}
		while ( *wild == '*' ) {
			++wild;
		}
		return !*wild;
	}
	
	// ------------------------------------------------------------------------
	// 
	// Numeric conversions
	// 
	// ------------------------------------------------------------------------
	
	bool ParseToType(pnt2u& dest, const char* str)
	{
		u32 v1 = 0, v2 = 0;
		if ( !strings_detail::ParseValues(str, &v1, &v2) ) {
			return false;
		}
		dest = pnt2u(v1, v2);
		return true;
	}
	
	bool ParseToType(pnt2s& dest, const char* str)
	{
		s32 v1 = 0, v2 = 0;
		if ( !strings_detail::ParseValues(str, &v1, &v2) ) {
			return false;
		}
		dest = pnt2s(v1, v2);
		return true;
	}
	
	bool ParseToType(pnt2f& dest, const char* str)
	{
		f32 v1 = 0, v2 = 0;
		if ( !strings_detail::ParseValues(str, &v1, &v2) ) {
			return false;
		}
		dest = pnt2f(v1, v2);
		return true;
	}
	
	bool ParseToType(rectu& dest, const char* str)
	{
		u32 v1 = 0, v2 = 0, v3 = 0, v4 = 0;
		if ( !strings_detail::ParseValues(str, &v1, &v2, &v3, &v4) ) {
			return false;
		}
		dest = rectu(v1, v2, v3, v4);
		return true;
	}
	
	bool ParseToType(rects& dest, const char* str)
	{
		s32 v1 = 0, v2 = 0, v3 = 0, v4 = 0;
		if ( !strings_detail::ParseValues(str, &v1, &v2, &v3, &v4) ) {
			return false;
		}
		dest = rects(v1, v2, v3, v4);
		return true;
	}
	
	bool ParseToType(rectf& dest, const char* str)
	{
		f32 v1 = 0.f, v2 = 0.f, v3 = 0.f, v4 = 0.f;
		if ( !strings_detail::ParseValues(str, &v1, &v2, &v3, &v4) ) {
			return false;
		}
		dest = rectf(v1, v2, v3, v4);
		return true;
	}
	
	bool ParseToType(vec2f& dest, const char* str)
	{
		f32 v1 = 0.f, v2 = 0.f;
		if ( !strings_detail::ParseValues(str, &v1, &v2) ) {
			return false;
		}
		dest = vec2f(v1, v2);
		return true;
	}
	
	bool ParseToType(vec3f& dest, const char* str)
	{
		f32 v1 = 0.f, v2 = 0.f, v3 = 0.f;
		if ( !strings_detail::ParseValues(str, &v1, &v2, &v3) ) {
			return false;
		}
		dest = vec3f(v1, v2, v3);
		return true;
	}
	
	bool ParseToType(Color& dest, const char* str)
	{
		f32 v1 = 0.f, v2 = 0.f, v3 = 0.f, v4 = 0.f;
		if ( strings_detail::ParseValues(str, &v1, &v2, &v3, &v4) ) {
			dest = Color(v1, v2, v3, v4);
			return true;
		}
		if ( strings_detail::ParseValues(str, &v1, &v2, &v3) ) {
			dest = Color(v1, v2, v3, 1.f);
			return true;
		}
		if ( strings_detail::ParseValues(str, &v1, &v2) ) {
			dest = Color(v1, v1, v1, v2);
			return true;
		}
		return false;
	}
	
	bool ParseToType(bool& dest, const char* str)
	{
		if ( !str ) {
			return false;
		}
		char s = *str;
		dest =
			(s == '1' ||
			 s == 't' || s == 'T' ||
			 s == 'y' || s == 'Y');
		return true;
	}
	
	bool ParseToType(Str& dest, const char* str)
	{
		if ( !str ) {
			return false;
		}
		dest = str;
		return true;
	}
	
	bool ParseToType(StrId& dest, const char* str)
	{
		if ( !str ) {
			return false;
		}
		dest = str;
		return true;
	}
	
	bool ParseToType(Path& dest, const char* str)
	{
		if ( !str ) {
			return false;
		}
		dest = str;
		return true;
	}
	
	// ------------------------------------------------------------------------
	// 
	// Base64 conversions
	// 
	// ------------------------------------------------------------------------
	
	bool Base64Encode(Str& dest, const void* src, size_t size)
	{
		if ( !src || !size ) {
			return false;
		}
		SmartBuffer mem(cyoBase64EncodeGetLength(size));
		if ( !mem ) {
			return false;
		}
		char* mem_buffer = static_cast<char*>(mem.getBuffer());
		size_t real_size = cyoBase64Encode(mem_buffer, src, size);
		if ( !real_size ) {
			return false;
		}
		dest = Str(mem_buffer, mem_buffer + real_size);
		return true;
	}
	
	bool Base64Decode(SmartBuffer& dest, const char* src, size_t size)
	{
		if ( 0 != cyoBase64Validate(src, size) ) {
			return false;
		}
		SmartBuffer mem(cyoBase64DecodeGetLength(size));
		if ( !mem ) {
			return false;
		}
		size_t real_size = cyoBase64Decode(mem.getBuffer(), src, size);
		if ( !real_size ) {
			return false;
		}
		void* dest_buffer = dest.Allocate(real_size);
		if ( !dest_buffer ) {
			return false;
		}
		BME_memcpy(dest_buffer, mem.getBuffer(), real_size);
		return true;
	}
} // namespace Strings
} // namespace bme
