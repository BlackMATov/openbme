/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/utils/Path.cpp
 *****************************************************************************/

#include <bme/utils/Path.h>

#include <bme/utils/Strings.h>
#include <bme/utils/StringsFormat.h>

namespace bme
{

Path::TAliases Path::_s_aliases;
RecursiveMutex Path::_s_mutex;
const Str Path::exe_dir("<exe_dir>");
const Str Path::res_dir("<res_dir>");
const Str Path::wrk_dir("<wrk_dir>");
const Str Path::app_dir("<app_dir>");

void Path::_s_apply_aliases(Str& dest)
{
	MutexLockGuard<RecursiveMutex> guard(_s_mutex);
	for ( ; ; ) {
		const Str last_dest = dest;
		for ( TAliases::const_iterator
			iter = _s_aliases.begin(), end = _s_aliases.end();
			iter != end; ++iter )
		{
			const StrId& src = (*iter).first;
			const Str& native_src = (*iter).second.getNativePath();
			cpp::replace_all(dest, src.c_str(), native_src.c_str());
		}
		if ( last_dest == dest ) {
			break;
		}
	}
}

void Path::_normalize()
{
	for ( Str::size_type i = 0; i < _data.length(); ++i ) {
		if ( _data[i] == '\\' ) {
			_data[i] = '/';
		}
	}
	_s_apply_aliases(_data);
}

Path::Path(const char* path)
{
	if ( path ) {
		Strings::MakeUtf8(_data, path);
	}
	_normalize();
}

Path::Path(const wchar_t* path)
{
	if ( path ) {
		Strings::MakeUtf8(_data, path);
	}
	_normalize();
}

Path::Path(const Str& path)
{
	if ( !path.empty() ) {
		Strings::MakeUtf8(_data, path.c_str());
	}
	_normalize();
}

Path::Path(const WStr& path)
{
	if ( !path.empty() ) {
		Strings::MakeUtf8(_data, path.c_str());
	}
	_normalize();
}

Path::Path()
{
}

Path::Path(const Path& other)
{
	_data = other._data;
}

Path& Path::operator=(const Path& other)
{
	if ( this != &other ) {
		_data = other._data;
	}
	return *this;
}

Path& Path::operator/=(const Path& other)
{
	if ( !_data.empty() && _data[_data.length()-1] != '/' ) {
		_data += '/';
	}
	_data += other._data;
	return *this;
}

void Path::Clear()
{
	_data.clear();
}

bool Path::isEmpty() const
{
	return _data.empty();
}

Path& Path::RemoveFilename()
{
	const Str filename = getFilename();
	if ( !filename.empty() ) {
		_data.erase(_data.length()-filename.length());
	}
	return *this;
}

Path& Path::RemoveExtension()
{
	const Str extension = getExtension();
	if ( !extension.empty() ) {
		_data.erase(_data.length()-extension.length());
	}
	return *this;
}

Path& Path::ReplaceFilename(const Str& source)
{
	RemoveFilename();
	_data += source;
	return *this;
}

Path& Path::ReplaceExtension(const Str& source)
{
	RemoveExtension();
	_data += source;
	return *this;
}

Str Path::getFilename() const
{
	const Str::size_type pos = _data.rfind('/');
	if ( pos == Str::npos ) {
		return _data;
	}
	return pos >= _data.length() - 1
		? Str()
		: _data.substr(pos+1, _data.length() - 1);
}

Str Path::getExtension() const
{
	const Str filename = getFilename();
	const Str::size_type pos = filename.rfind('.');
	return Str::npos == pos || filename == "." || filename == ".."
		? Str()
		: filename.substr(pos, filename.length() - 1);
}

Path Path::getParentPath() const
{
	const Str::size_type pos = _data.rfind('/');
	if ( Str::npos != pos ) {
		return Path(_data.substr(0, pos));
	}
	return Path();
}

bool Path::isHasFilename() const
{
	return !getFilename().empty();
}

bool Path::isHasExtension() const
{
	return !getExtension().empty();
}

bool Path::isHasParentPath() const
{
	return !getParentPath().isEmpty();
}

bool Path::isAbsolute() const
{
	// windows style ("//USER-PC/Users") || ("C:/docs")
	if ( _data.length() >= 2 &&
		((_data[0] == '/' && _data[1] == '/') || (_data[1] == ':')) )
	{
		return true;
	}
	// unix style ("/temp")
	if ( !_data.empty() && _data[0] == '/' ) {
		return true;
	}
	return false;
}

bool Path::isRelative() const
{
	return !isAbsolute();
}

const Str& Path::getNativePath() const
{
	return _data;
}

WStr Path::getNativeWPath() const
{
	WStr wide;
	Strings::MakeWide(wide, _data.c_str());
	return wide;
}

void Path::BindAlias(const StrId& id, const Path& path)
{
	StrId alias_id = BME_StrId_Make(1024, "%1%%2%%3%", StrId("<"), id, StrId(">"));
	MutexLockGuard<RecursiveMutex> guard(_s_mutex);
	_s_aliases[alias_id] = path;
}

bool Path::IsHasAlias(const StrId& id)
{
	StrId alias_id = BME_StrId_Make(1024, "%1%%2%%3%", StrId("<"), id, StrId(">"));
	MutexLockGuard<RecursiveMutex> guard(_s_mutex);
	return _s_aliases.find(alias_id) != _s_aliases.end();
}

} // namespace bme
