/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/utils/StringsFormatImpl.cpp
 *****************************************************************************/

#include <bme/utils/StringsFormatImpl.h>
#include <bme/utils/StringsFormat.h>

namespace bme
{
namespace Strings
{
	namespace detail
	{
	#if BME_COMPILER == BME_COMPILER_MSVC
		static int msvc_vsnprintf(char* dest, size_t size, const char* format, va_list ap)
		{
			int count = -1;
			if ( size != 0 ) {
				count = _vsnprintf_s(dest, size, _TRUNCATE, format, ap);
			}
			if ( count == -1 ) {
				count = _vscprintf(format, ap);
			}
			return count;
		}
		
		int msvc_snprintf(char* dest, size_t size, const char* format, ...)
		{
			int count;
			va_list ap;
			va_start(ap, format);
			count = msvc_vsnprintf(dest, size, format, ap);
			va_end(ap);
			return count;
		}
	#endif
	}
	
	ptrdiff_t Arg<bool>::Write(char* dest, size_t size, const Arg<bool>& self)
	{
		return BME_snprintf(dest, size, "%s",
			self.value ? "true" : "false");
	}
	
	ptrdiff_t Arg<char*>::Write(char* dest, size_t size, const Arg<char*>& self)
	{
		return BME_snprintf(dest, size, "%s",
			self.value);
	}
	
	ptrdiff_t Arg<const char*>::Write(char* dest, size_t size, const Arg<const char*>& self)
	{
		return BME_snprintf(dest, size, "%s",
			self.value);
	}
	
	ptrdiff_t Arg<Str>::Write(char* dest, size_t size, const Arg<Str>& self)
	{
		return BME_snprintf(dest, size, "%s",
			self.value.c_str());
	}
	
	ptrdiff_t Arg<StrId>::Write(char* dest, size_t size, const Arg<StrId>& self)
	{
		return BME_snprintf(dest, size, "%s",
			self.value.c_str());
	}
	
	ptrdiff_t Arg<Path>::Write(char* dest, size_t size, const Arg<Path>& self)
	{
		return BME_snprintf(dest, size, "%s",
			self.value.getNativePath().c_str());
	}
	
	ptrdiff_t Arg<vec2f>::Write(char* dest, size_t size, const Arg<vec2f>& self)
	{
		char buf[BME_MAX_LOGGER_MESSAGE] = {0};
		Strings::Format(
			buf, BME_MAX_LOGGER_MESSAGE, "(%1%,%2%)",
			Arg<f32>(self.value.x, self.width, self.precision),
			Arg<f32>(self.value.y, self.width, self.precision));
		return BME_snprintf(dest, size, "%s", buf);
	}
	
	ptrdiff_t Arg<vec3f>::Write(char* dest, size_t size, const Arg<vec3f>& self)
	{
		char buf[BME_MAX_LOGGER_MESSAGE] = {0};
		Strings::Format(
			buf, BME_MAX_LOGGER_MESSAGE, "(%1%,%2%,%3%)",
			Arg<f32>(self.value.x, self.width, self.precision),
			Arg<f32>(self.value.y, self.width, self.precision),
			Arg<f32>(self.value.z, self.width, self.precision));
		return BME_snprintf(dest, size, "%s", buf);
	}
	
	ptrdiff_t Arg<pnt2u>::Write(char* dest, size_t size, const Arg<pnt2u>& self)
	{
		char buf[BME_MAX_LOGGER_MESSAGE] = {0};
		Strings::Format(
			buf, BME_MAX_LOGGER_MESSAGE, "(%1%,%2%)",
			Arg<pnt2u::valuesT>(self.value.x, self.width),
			Arg<pnt2u::valuesT>(self.value.y, self.width));
		return BME_snprintf(dest, size, "%s", buf);
	}
	
	ptrdiff_t Arg<pnt2s>::Write(char* dest, size_t size, const Arg<pnt2s>& self)
	{
		char buf[BME_MAX_LOGGER_MESSAGE] = {0};
		Strings::Format(
			buf, BME_MAX_LOGGER_MESSAGE, "(%1%,%2%)",
			Arg<pnt2s::valuesT>(self.value.x, self.width),
			Arg<pnt2s::valuesT>(self.value.y, self.width));
		return BME_snprintf(dest, size, "%s", buf);
	}
	
	ptrdiff_t Arg<pnt2f>::Write(char* dest, size_t size, const Arg<pnt2f>& self)
	{
		char buf[BME_MAX_LOGGER_MESSAGE] = {0};
		Strings::Format(
			buf, BME_MAX_LOGGER_MESSAGE, "(%1%,%2%)",
			Arg<pnt2f::valuesT>(self.value.x, self.width, self.precision),
			Arg<pnt2f::valuesT>(self.value.y, self.width, self.precision));
		return BME_snprintf(dest, size, "%s", buf);
	}
	
	ptrdiff_t Arg<rectu>::Write(char* dest, size_t size, const Arg<rectu>& self)
	{
		char buf[BME_MAX_LOGGER_MESSAGE] = {0};
		Strings::Format(
			buf, BME_MAX_LOGGER_MESSAGE, "(%1%,%2%,%3%,%4%)",
			Arg<rectu::valuesT>(self.value.getLeft  (), self.width),
			Arg<rectu::valuesT>(self.value.getTop   (), self.width),
			Arg<rectu::valuesT>(self.value.getRight (), self.width),
			Arg<rectu::valuesT>(self.value.getBottom(), self.width));
		return BME_snprintf(dest, size, "%s", buf);
	}
	
	ptrdiff_t Arg<rects>::Write(char* dest, size_t size, const Arg<rects>& self)
	{
		char buf[BME_MAX_LOGGER_MESSAGE] = {0};
		Strings::Format(
			buf, BME_MAX_LOGGER_MESSAGE, "(%1%,%2%,%3%,%4%)",
			Arg<rects::valuesT>(self.value.getLeft  (), self.width),
			Arg<rects::valuesT>(self.value.getTop   (), self.width),
			Arg<rects::valuesT>(self.value.getRight (), self.width),
			Arg<rects::valuesT>(self.value.getBottom(), self.width));
		return BME_snprintf(dest, size, "%s", buf);
	}
	
	ptrdiff_t Arg<rectf>::Write(char* dest, size_t size, const Arg<rectf>& self)
	{
		char buf[BME_MAX_LOGGER_MESSAGE] = {0};
		Strings::Format(
			buf, BME_MAX_LOGGER_MESSAGE, "(%1%,%2%,%3%,%4%)",
			Arg<rectf::valuesT>(self.value.getLeft  (), self.width, self.precision),
			Arg<rectf::valuesT>(self.value.getTop   (), self.width, self.precision),
			Arg<rectf::valuesT>(self.value.getRight (), self.width, self.precision),
			Arg<rectf::valuesT>(self.value.getBottom(), self.width, self.precision));
		return BME_snprintf(dest, size, "%s", buf);
	}
} // namespace Strings
} // namespace bme
