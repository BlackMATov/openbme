/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/utils/SmartBuffer.cpp
 *****************************************************************************/

#include <bme/utils/SmartBuffer.h>
#include <bme/utils/RefCounted.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// SmartBuffer::Data
// 
// ----------------------------------------------------------------------------

class SmartBuffer::Data : public RefCounted
{
	size_t _size;
	void*  _buffer;
	
public:
	
	Data(size_t size)
	: _size(0), _buffer(NULL)
	{
		void* new_buffer = BME_malloc(size);
		if ( new_buffer ) {
			_size = size;
			_buffer = new_buffer;
		}
	}
	
	~Data()
	{
		if ( _buffer ) {
			BME_free(_buffer);
			_buffer = NULL;
		}
	}
	
	void* getBuffer()
	{
		return _buffer;
	}
	
	const void* getBuffer() const
	{
		return _buffer;
	}
	
	size_t getSize() const
	{
		return _size;
	}
	
	bool isValid() const
	{
		return _buffer != NULL;
	}
};

// ----------------------------------------------------------------------------
// 
// SmartBuffer
// 
// ----------------------------------------------------------------------------

SmartBuffer::SmartBuffer(size_t size)
{
	Allocate(size);
}

SmartBuffer::SmartBuffer(const void* src, size_t size)
{
	void* dest = Allocate(size);
	if ( dest && src && size ) {
		BME_memcpy(dest, src, size);
	}
}

SmartBuffer::SmartBuffer()
{
}

SmartBuffer::SmartBuffer(const SmartBuffer& other)
{
	_data = other._data;
}

SmartBuffer& SmartBuffer::operator=(const SmartBuffer& other)
{
	if ( this != &other ) {
		_data = other._data;
	}
	return *this;
}

SmartBuffer::~SmartBuffer()
{
}

SmartBuffer::operator safe_bool() const
{
	return (_data && _data->isValid()) ? &dummy::nonnull : 0;
}

bool SmartBuffer::operator!() const
{
	return !safe_bool(*this);
}

void* SmartBuffer::Allocate(size_t size)
{
	Release();
	if ( size ) {
		DataPtr new_data(new Data(size));
		if ( new_data && new_data->isValid() ) {
			_data = new_data;
		}
	}
	return getBuffer();
}

void SmartBuffer::Release()
{
	_data.reset();
}

void SmartBuffer::Clear(s32 value)
{
	void* dest = getBuffer();
	if ( dest ) {
		BME_memset(dest, value, getSize());
	}
}

bool SmartBuffer::IsMemEquals(const SmartBuffer& other) const
{
	return IsMemEquals(other.getBuffer(), other.getSize());
}

bool SmartBuffer::IsMemEquals(const void* buffer, size_t size) const
{
	BME_ASSERT(0 == size || buffer);
	BME_ASSERT(0 == getSize() || getBuffer());
	return
		size == getSize() &&
		(0 == size || 0 == BME_memcmp(buffer, getBuffer(), size));
}

void* SmartBuffer::getBuffer()
{
	return _data ? _data->getBuffer() : NULL;
}

const void* SmartBuffer::getBuffer() const
{
	return _data ? _data->getBuffer() : NULL;
}

size_t SmartBuffer::getSize() const
{
	return _data ? _data->getSize() : 0;
}

} // namespace bme
