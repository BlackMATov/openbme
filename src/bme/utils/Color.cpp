/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/utils/Color.cpp
 *****************************************************************************/

#include <bme/utils/Color.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// color_detail
// 
// ----------------------------------------------------------------------------

namespace color_detail
{
	template < typename LT, typename RT >
	static bool is_not_intersect(
		const LT* dest, size_t dest_size,
		const RT* src,  size_t src_size)
	{
		const u8* l1 = reinterpret_cast<const u8*>(dest);
		const u8* l2 = l1 + dest_size * sizeof(LT);
		const u8* r1 = reinterpret_cast<const u8*>(src);
		const u8* r2 = r1 + src_size * sizeof(RT);
		return !(l2 > r1 && l1 < r2);
	}
}

// ----------------------------------------------------------------------------
// 
// Color
// 
// ----------------------------------------------------------------------------

const Color Color::black   = Color(0,0,0,1);
const Color Color::white   = Color(1,1,1,1);
const Color Color::red     = Color(1,0,0,1);
const Color Color::green   = Color(0,1,0,1);
const Color Color::blue    = Color(0,0,1,1);
const Color Color::yellow  = Color(1,1,0,1);
const Color Color::magenta = Color(1,0,1,1);
const Color Color::cyan    = Color(0,1,1,1);

#define CONVERT_PREDEFINE(dest_type, src_type, dest_components, src_components)\
	BME_ASSERT(\
		dest && src &&\
		color_detail::is_not_intersect(\
			dest, dest_size.x * dest_size.y * (dest_components),\
			src,  src_size.x  * src_size.y  * (src_components)));\
	const size_t  src_pitch =  src_size.x *  src_components;\
	const size_t dest_pitch = dest_size.x * dest_components;\
	const size_t min_width  = cpp::min(dest_size.x, src_size.x);\
	const size_t min_height = cpp::min(dest_size.y, src_size.y);\
	(void)src_pitch;(void)dest_pitch;(void)min_width;(void)min_height;\
	for ( size_t i = 0; i < min_height; ++i ) {\
		const src_type* begin_line_src  = src;\
		dest_type*      begin_line_dest = dest;\
		for ( size_t j = 0; j < min_width; ++j ) {

#define CONVERT_POSDEFINE()\
	}\
	src  = begin_line_src  +  src_pitch;\
	dest = begin_line_dest + dest_pitch;\
	}

void Color::G8_To_X8R8G8B8(
	u32* dest, const pnt2u& dest_size, const u8* src, const pnt2u& src_size)
{
	CONVERT_PREDEFINE(u32,u8,1,1)
		u8 g = *src++;
		*dest++ = (0xFF << 24) + (g << 16) + (g << 8) + g;
	CONVERT_POSDEFINE()
}

void Color::GA8_To_A8R8G8B8(
	u32* dest, const pnt2u& dest_size, const u8* src, const pnt2u& src_size)
{
	CONVERT_PREDEFINE(u32,u8,1,2)
		u8 g = *src++;
		u8 a = *src++;
		*dest++ = (a << 24) + (g << 16) + (g << 8) + g;
	CONVERT_POSDEFINE()
}

void Color::RGB8_To_X8R8G8B8(
	u32* dest, const pnt2u& dest_size, const u8* src, const pnt2u& src_size)
{
	CONVERT_PREDEFINE(u32,u8,1,3)
		u8 r = *src++;
		u8 g = *src++;
		u8 b = *src++;
		*dest++ = (0xFF << 24) + (r << 16) + (g << 8) + b;
	CONVERT_POSDEFINE()
}

void Color::RGBA8_To_A8R8G8B8(
	u32* dest, const pnt2u& dest_size, const u8* src, const pnt2u& src_size)
{
	CONVERT_PREDEFINE(u32,u8,1,4)
		u8 r = *src++;
		u8 g = *src++;
		u8 b = *src++;
		u8 a = *src++;
		*dest++ = (a << 24) + (r << 16) + (g << 8) + b;
	CONVERT_POSDEFINE()
}

void Color::G8_To_RGB8(
	u8* dest, const pnt2u& dest_size, const u8* src, const pnt2u& src_size)
{
	CONVERT_PREDEFINE(u8,u8,3,1)
		u8 g = *src++;
		*dest++ = g;
		*dest++ = g;
		*dest++ = g;
	CONVERT_POSDEFINE()
}

void Color::GA8_To_RGBA8(
	u8* dest, const pnt2u& dest_size, const u8* src, const pnt2u& src_size)
{
	CONVERT_PREDEFINE(u8,u8,4,2)
		u8 g = *src++;
		u8 a = *src++;
		*dest++ = g;
		*dest++ = g;
		*dest++ = g;
		*dest++ = a;
	CONVERT_POSDEFINE()
}

void Color::X8R8G8B8_To_RGB8(
	u8* dest, const pnt2u& dest_size, const u32* src, const pnt2u& src_size)
{
	CONVERT_PREDEFINE(u8,u32,3,1)
		u32 xrgb = *src++;
		*dest++ = (xrgb >> 16) & 0xFF;
		*dest++ = (xrgb >>  8) & 0xFF;
		*dest++ = (xrgb      ) & 0xFF;
	CONVERT_POSDEFINE()
}

void Color::A8R8G8B8_To_RGBA8(
	u8* dest, const pnt2u& dest_size, const u32* src, const pnt2u& src_size)
{
	CONVERT_PREDEFINE(u8,u32,4,1)
		u32 argb = *src++;
		*dest++ = (argb >> 16) & 0xFF;
		*dest++ = (argb >>  8) & 0xFF;
		*dest++ = (argb      ) & 0xFF;
		*dest++ = (argb >> 24);
	CONVERT_POSDEFINE()
}

void Color::RGB8_To_RGB8(
	u8* dest, const pnt2u& dest_size, const u8* src, const pnt2u& src_size)
{
	BME_ASSERT(dest && src && color_detail::is_not_intersect(
		dest, dest_size.x * dest_size.y * 3,
		src,   src_size.x *  src_size.y * 3));
	
	const size_t src_pitch  =  src_size.x * 3;
	const size_t dest_pitch = dest_size.x * 3;
	const size_t min_pitch  = cpp::min(src_pitch, dest_pitch);
	const size_t min_height = cpp::min(dest_size.y, src_size.y);
	
	if ( src_pitch == dest_pitch ) {
		BME_memcpy(dest, src, min_pitch * min_height);
	} else {
		for ( size_t i = 0; i < min_height; ++i ) {
			BME_memcpy(dest, src, min_pitch);
			src  +=  src_pitch;
			dest += dest_pitch;
		}
	}
}

void Color::RGB8_To_RGBA8(
	u8* dest, const pnt2u& dest_size, const  u8* src, const pnt2u& src_size)
{
	CONVERT_PREDEFINE(u8,u8,4,3)
		*dest++ = *src++;
		*dest++ = *src++;
		*dest++ = *src++;
		*dest++ = 0xFF;
	CONVERT_POSDEFINE()
}

void Color::RGBA8_To_RGB8(
	u8* dest, const pnt2u& dest_size, const u8* src, const pnt2u& src_size)
{
	CONVERT_PREDEFINE(u8,u8,3,4)
		*dest++ = *src++;
		*dest++ = *src++;
		*dest++ = *src++;
		++src;
	CONVERT_POSDEFINE()
}

void Color::RGBA8_To_RGBA8(
	u8* dest, const pnt2u& dest_size, const u8* src, const pnt2u& src_size)
{
	BME_ASSERT(dest && src && color_detail::is_not_intersect(
		dest, dest_size.x * dest_size.y * 4,
		src,   src_size.x *  src_size.y * 4));
	
	const size_t src_pitch  =  src_size.x * 4;
	const size_t dest_pitch = dest_size.x * 4;
	const size_t min_pitch  = cpp::min(src_pitch,  dest_pitch);
	const size_t min_height = cpp::min(src_size.y, dest_size.y);
	
	if ( src_pitch == dest_pitch ) {
		BME_memcpy(dest, src, min_pitch * min_height);
	} else {
		for ( size_t i = 0; i < min_height; ++i ) {
			BME_memcpy(dest, src, min_pitch);
			src  +=  src_pitch;
			dest += dest_pitch;
		}
	}
}

void Color::X8R8G8B8_To_X8R8G8B8(
	u32* dest, const pnt2u& dest_size, const u32* src, const pnt2u& src_size)
{
	BME_ASSERT(dest && src && color_detail::is_not_intersect(
		dest, dest_size.x * dest_size.y,
		src,   src_size.x *  src_size.y));
	
	const size_t src_pitch  =  src_size.x;
	const size_t dest_pitch = dest_size.x;
	const size_t min_pitch  = cpp::min(src_pitch,  dest_pitch);
	const size_t min_height = cpp::min(src_size.y, dest_size.y);
	
	if ( src_pitch == dest_pitch ) {
		BME_memcpy(dest, src, min_pitch * min_height * sizeof(u32));
	} else {
		for ( size_t i = 0; i < min_height; ++i ) {
			BME_memmove(dest, src, min_pitch * sizeof(u32));
			src  +=  src_size.x;
			dest += dest_size.x;
		}
	}
}

void Color::A8R8G8B8_To_A8R8G8B8(
	u32* dest, const pnt2u& dest_size, const u32* src, const pnt2u& src_size)
{
	X8R8G8B8_To_X8R8G8B8(dest, dest_size, src, src_size);
}

u32* Color::InPlace_RGBA8_To_A8R8G8B8(
	u8* dest_src, const pnt2u& size)
{
	BME_ASSERT(dest_src);
	const u8* src    = dest_src;
	u32*      dest   = reinterpret_cast<u32*>(dest_src);
	size_t    pixels = size.x * size.y;
	for ( size_t i = 0; i < pixels; ++i ) {
		u8 r = *src++;
		u8 g = *src++;
		u8 b = *src++;
		u8 a = *src++;
		*dest++ = (a << 24) + (r << 16) + (g << 8) + b;
	}
	return dest - pixels;
}

u8* Color::InPlace_A8R8G8B8_To_RGBA8(
	u32* dest_src, const pnt2u& size)
{
	BME_ASSERT(dest_src);
	const u32* src    = dest_src;
	u8*        dest   = reinterpret_cast<u8*>(dest_src);
	size_t     pixels = size.x * size.y;
	for ( size_t i = 0; i < pixels; ++i ) {
		u32 argb = *src++;
		*dest++ = (argb >> 16) & 0xFF;
		*dest++ = (argb >>  8) & 0xFF;
		*dest++ = (argb      ) & 0xFF;
		*dest++ = (argb >> 24);
	}
	return dest - pixels * 4;
}

} // namespace bme
