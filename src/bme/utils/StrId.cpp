/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/utils/StrId.cpp
 *****************************************************************************/

#include <bme/utils/StrId.h>

namespace bme
{

StrId::StrId(const char* str)
{
	if ( str ) {
		_data = TData(str);
	}
}

StrId::StrId(const char* str, size_t n)
{
	if ( str && n > 0 ) {
		_data = TData(str, n);
	}
}

StrId::StrId()
{
}

StrId::StrId(const StrId& other)
{
	_data = other._data;
}

StrId& StrId::operator=(const StrId& other)
{
	if ( this != &other ) {
		_data = other._data;
	}
	return *this;
}

const char* StrId::data() const
{
	return _data.data();
}

const char* StrId::c_str() const
{
	return _data.c_str();
}

bool StrId::empty() const
{
	return _data.empty();
}

size_t StrId::size() const
{
	return _data.size();
}

size_t StrId::length() const
{
	return _data.length();
}

} // namespace bme
