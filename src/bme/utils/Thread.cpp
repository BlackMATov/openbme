/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/utils/Thread.cpp
 *****************************************************************************/

#include <bme/utils/Thread.h>

// library from depends
#include <tinythread/tinythread.h>

namespace bme
{

// ----------------------------------------------------------------------------
// 
// Thread::Impl
// 
// ----------------------------------------------------------------------------

class Thread::Impl : public RefCounted
{
	tthread::thread _thread;
	
public:
	
	Impl()
	{
	}
	
	Impl(TFunction func, TFunctionArg arg)
	: _thread(func, arg)
	{
	}
	
	~Impl()
	{
	}
	
	void Join()
	{
		_thread.join();
	}
	
	bool isJoinable() const
	{
		return _thread.joinable();
	}
};

// ----------------------------------------------------------------------------
// 
// Thread
// 
// ----------------------------------------------------------------------------

Thread::Thread()
: _impl(new Impl())
{
}

Thread::Thread(TFunction func, TFunctionArg arg)
: _impl(new Impl(func, arg))
{
}

Thread::~Thread()
{
}

void Thread::Join()
{
	_impl->Join();
}

bool Thread::isJoinable() const
{
	return _impl->isJoinable();
}

void Thread::YieldThis()
{
	tthread::this_thread::yield();
}

void Thread::SleepThis(size_t ms)
{
	tthread::this_thread::sleep_for(tthread::chrono::milliseconds(ms));
}

size_t Thread::GetProcessors()
{
	return Math::SmartCast<size_t>(tthread::thread::hardware_concurrency());
}

// ----------------------------------------------------------------------------
// 
// Mutex::Impl, RecursiveMutex::Impl, ConditionVar::Impl
// 
// ----------------------------------------------------------------------------

class Mutex::Impl : public RefCounted
{
	tthread::mutex _mutex;
	
public:
	Impl        () {}
	~Impl       () {}
	void Lock   ()                               { _mutex.lock();   }
	void Unlock ()                               { _mutex.unlock(); }
	void Wait   (tthread::condition_variable& c) { c.wait(_mutex);  }
};

class RecursiveMutex::Impl : public RefCounted
{
	tthread::recursive_mutex _mutex;
	
public:
	Impl        () {}
	~Impl       () {}
	void Lock   ()                               { _mutex.lock();   }
	void Unlock ()                               { _mutex.unlock(); }
	void Wait   (tthread::condition_variable& c) { c.wait(_mutex);  }
};

class ConditionVar::Impl : public RefCounted
{
	tthread::condition_variable _condition;
	
public:
	Impl           () {}
	~Impl          () {}
	
	template < typename T >
	void Wait      (T& mutex) { mutex->Wait(_condition); }
	void NotifyOne ()         { _condition.notify_one(); }
	void NotifyAll ()         { _condition.notify_all(); }
};

// ----------------------------------------------------------------------------
// 
// Mutex, RecursiveMutex, ConditionVar
// 
// ----------------------------------------------------------------------------

Mutex::Mutex                    () : _impl(new Impl())  {}
Mutex::~Mutex                   ()                      {}
void Mutex::Lock                () const                { _impl->Lock();   }
void Mutex::Unlock              () const                { _impl->Unlock(); }

RecursiveMutex::RecursiveMutex  () : _impl(new Impl())  {}
RecursiveMutex::~RecursiveMutex ()                      {}
void RecursiveMutex::Lock       () const                { _impl->Lock();   }
void RecursiveMutex::Unlock     () const                { _impl->Unlock(); }

ConditionVar::ConditionVar      () : _impl(new Impl())  {}
ConditionVar::~ConditionVar     ()                      {}
void ConditionVar::Wait         (Mutex& mutex)          { _impl->Wait(mutex._impl); }
void ConditionVar::Wait         (RecursiveMutex& mutex) { _impl->Wait(mutex._impl); }
void ConditionVar::NotifyOne    ()                      { _impl->NotifyOne();       }
void ConditionVar::NotifyAll    ()                      { _impl->NotifyAll();       }

} // namespace bme
