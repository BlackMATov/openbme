/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/bme/utils/Randomizer.cpp
 *****************************************************************************/

#include <bme/utils/Randomizer.h>

namespace bme
{

s32 Randomizer::_generateImpl(s32 min, s32 max) const
{
	_seed = 214013 * _seed + 2531011;
	return min + (_seed ^ _seed >> 15) % (max - min + 1);
}

f32 Randomizer::_generateImpl(f32 min, f32 max) const
{
	_seed = 214013 * _seed + 2531011;
	return min + (_seed >> 16) * (1.0f / 65535.0f) * (max - min);
}

Randomizer::Randomizer()
: _seed(Math::SmartCast<u32>(::time(NULL)))
{
}

Randomizer::Randomizer(const Randomizer& other)
: _seed(other._seed)
{
}

Randomizer& Randomizer::operator=(const Randomizer& other)
{
	if ( this != &other ) {
		_seed = other._seed;
	}
	return *this;
}

Randomizer::~Randomizer()
{
}

void Randomizer::setSeed(u32 value)
{
	_seed = value;
}

u32 Randomizer::getSeed() const
{
	return _seed;
}

} // namespace bme
