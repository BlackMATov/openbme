/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_macosx/SystemMACOSX_cocoa.mm
 *****************************************************************************/

#import "SystemMACOSX_cocoa.h"

#if BME_SYSTEM == BME_SYSTEM_MACOSX

// ----------------------------------------------------------------------------
// 
// BMECocoaMain
// 
// ----------------------------------------------------------------------------

#ifdef BME_WITH_MAIN

int main(int argc, char *argv[])
{
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	BMECocoaDelegate* delegate = [[[BMECocoaDelegate alloc] init] autorelease];
	[[NSApplication sharedApplication] setDelegate:delegate];
	[NSApp run];
	
	[pool release];	
	return 0;
}

#endif // BME_WITH_MAIN

// ----------------------------------------------------------------------------
// 
// bme::cocoa_detail
// 
// ----------------------------------------------------------------------------

namespace bme { namespace cocoa_detail
{
	BMECocoaView* GetView()
	{
		return [GetDelegate() view];
	}
	
	BMECocoaWindow* GetWindow()
	{
		return [GetDelegate() window];
	}
	
	BMECocoaDelegate* GetDelegate()
	{
		BMECocoaDelegate* delegate = (BMECocoaDelegate*)[NSApp delegate];
		return delegate;
	}
	
	NSApplication* GetApplication()
	{
		return NSApp;
	}
	
	void DoMouseEvent(NSView* view, NSEvent* event)
	{
		using namespace bme;
		
		NSPoint mouse_loc = [view convertPoint:[event locationInWindow] fromView:view];
		vec2f   mouse_pos = vec2f(mouse_loc.x, [view frame].size.height - mouse_loc.y);
		
		switch ( [event type] ) {
			case NSMouseMoved:
			case NSLeftMouseDragged:
			case NSRightMouseDragged:
			case NSOtherMouseDragged:
				{
					Receiver::Event e;
					e.type        = Receiver::Event::TYPE_MOUSE_MOVE;
					e.mouse.key   = MOUSE_UNKNOWN;
					e.mouse.x     = mouse_pos.x;
					e.mouse.y     = mouse_pos.y;
					e.mouse.which = 0;
					e.Push();
				}
				break;
			case NSScrollWheel:
				{
					Receiver::Event e;
					e.type        = Receiver::Event::TYPE_MOUSE_DOWN;
					e.mouse.key   = [event deltaY] < 0.f ? MOUSE_WHEELDOWN : MOUSE_WHEELUP;
					e.mouse.x     = mouse_pos.x;
					e.mouse.y     = mouse_pos.y;
					e.mouse.which = 0;
					e.Push();
				}
				break;
			case NSLeftMouseDown:
			case NSRightMouseDown:
			case NSOtherMouseDown:
				{
					E_MOUSE mouse = ConvertFromMouseButton([event buttonNumber]);
					if ( MOUSE_UNKNOWN != mouse ) {
						Receiver::Event e;
						e.type        = Receiver::Event::TYPE_MOUSE_DOWN;
						e.mouse.key   = mouse;
						e.mouse.x     = mouse_pos.x;
						e.mouse.y     = mouse_pos.y;
						e.mouse.which = 0;
						e.Push();
					}
				}
				break;
			case NSLeftMouseUp:
			case NSRightMouseUp:
			case NSOtherMouseUp:
				{
					E_MOUSE mouse = ConvertFromMouseButton([event buttonNumber]);
					if ( MOUSE_UNKNOWN != mouse ) {
						Receiver::Event e;
						e.type        = Receiver::Event::TYPE_MOUSE_UP;
						e.mouse.key   = mouse;
						e.mouse.x     = mouse_pos.x;
						e.mouse.y     = mouse_pos.y;
						e.mouse.which = 0;
						e.Push();
					}
				}
				break;
		}
	}
	
	void DoKeyEvent(NSEvent* event, bool down, bool flags)
	{
		using namespace bme;
		E_KEY key = ConvertFromKeyCode([event keyCode]);
		if ( KEY_UNKNOWN != key ) {
			if ( !flags && [event modifierFlags] & NSCommandKeyMask ) {
				if ( down ) {
					if ( key == KEY_Q ) {
						Receiver::Event e;
						e.type = Receiver::Event::TYPE_QUIT;
						e.Push();
					} else if ( key == KEY_M ) {
						[GetDelegate() Minimize];
					} else if ( key == KEY_H ) {
						[GetDelegate() Hide];
					}
				}
			} else {
				u32 charKey = 0;
				
				if ( !flags ) {
					UStr utf32;
					Strings::MakeUtf32(utf32, [[event characters] UTF8String]);
					charKey = utf32.empty() ? 0 : utf32[0];
				}
				
				Receiver::Event e;
				e.type             = down ? Receiver::Event::TYPE_KEYBOARD_DOWN : Receiver::Event::TYPE_KEYBOARD_UP;
				e.keyboard.key     = key;
				e.keyboard.charKey = charKey;
				e.keyboard.which = 0;
				e.Push();
			}
		}
	}
	
	E_MOUSE ConvertFromMouseButton(unsigned short mouseButton)
	{
		switch (mouseButton) {
			case 0: return MOUSE_LEFT;
			case 1: return MOUSE_RIGHT;
			case 2: return MOUSE_MIDLE;
			case 3: return MOUSE_X1MOUSE;
			case 4: return MOUSE_X2MOUSE;
		}
		return MOUSE_UNKNOWN;
	}
	
	E_KEY ConvertFromKeyCode(unsigned short keyCode)
	{
		switch (keyCode) {
			case  53: return KEY_ESCAPE;
			case  48: return KEY_TAB;
			case  57: return KEY_CAPSLOCK;
			case  56: return KEY_LSHIFT;
			case  59: return KEY_LCONTROL;
			case  58: return KEY_LMENU;
			case  55: return KEY_LWIN;
			case  49: return KEY_SPACE;
			case  36: return KEY_RETURN;
			case  51: return KEY_BACKSPACE;
			
			case 122: return KEY_F1;
			case 120: return KEY_F2;
			case  99: return KEY_F3;
			case 118: return KEY_F4;
			case  96: return KEY_F5;
			case  97: return KEY_F6;
			case  98: return KEY_F7;
			case 100: return KEY_F8;
			case 101: return KEY_F9;
			case 109: return KEY_F10;
			case 103: return KEY_F11;
			case 111: return KEY_F12;
			case 105: return KEY_F13;
			case 107: return KEY_F14;
			case 113: return KEY_F15;
			
			case  29: return KEY_0;
			case  18: return KEY_1;
			case  19: return KEY_2;
			case  20: return KEY_3;
			case  21: return KEY_4;
			case  23: return KEY_5;
			case  22: return KEY_6;
			case  26: return KEY_7;
			case  28: return KEY_8;
			case  25: return KEY_9;
			
			case  27: return KEY_MINUS;
			case  24: return KEY_PLUS;
			case  43: return KEY_COMMA;
			case  47: return KEY_PERIOD;
			
			case  12: return KEY_Q;
			case  13: return KEY_W;
			case  14: return KEY_E;
			case  15: return KEY_R;
			case  17: return KEY_T;
			case  16: return KEY_Y;
			case  32: return KEY_U;
			case  34: return KEY_I;
			case  31: return KEY_O;
			case  35: return KEY_P;
			
			case   0: return KEY_A;
			case   1: return KEY_S;
			case   2: return KEY_D;
			case   3: return KEY_F;
			case   5: return KEY_G;
			case   4: return KEY_H;
			case  38: return KEY_J;
			case  40: return KEY_K;
			case  37: return KEY_L;
			
			case   6: return KEY_Z;
			case   7: return KEY_X;
			case   8: return KEY_C;
			case   9: return KEY_V;
			case  11: return KEY_B;
			case  45: return KEY_N;
			case  46: return KEY_M;
			
			case 116: return KEY_PAGEUP;
			case 121: return KEY_PAGEDOWN;
			case 115: return KEY_HOME;
			case 119: return KEY_END;
			case 114: return KEY_INSERT;
			case 117: return KEY_DELETE;
			
			case 123: return KEY_LEFT;
			case 126: return KEY_UP;
			case 124: return KEY_RIGHT;
			case 125: return KEY_DOWN;
			
			case  82: return KEY_NUMPAD0;
			case  83: return KEY_NUMPAD1;
			case  84: return KEY_NUMPAD2;
			case  85: return KEY_NUMPAD3;
			case  86: return KEY_NUMPAD4;
			case  87: return KEY_NUMPAD5;
			case  88: return KEY_NUMPAD6;
			case  89: return KEY_NUMPAD7;
			case  91: return KEY_NUMPAD8;
			case  92: return KEY_NUMPAD9;
			
			case  67: return KEY_MULTIPLY;
			case  75: return KEY_DIVIDE;
			case  69: return KEY_ADD;
			case  78: return KEY_SUBTRACT;
			case  65: return KEY_DECIMAL;
		}
		return KEY_UNKNOWN;
	}
}} // namespace bme::cocoa_detail

#endif // BME_SYSTEM_MACOSX
