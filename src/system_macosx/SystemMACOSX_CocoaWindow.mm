/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_macosx/SystemMACOSX_CocoaWindow.mm
 *****************************************************************************/

#import "SystemMACOSX_cocoa.h"

#if BME_SYSTEM == BME_SYSTEM_MACOSX

@implementation BMECocoaWindow

// ----------------------------------------------------------------------------
// 
// Override
// 
// ----------------------------------------------------------------------------

- (BOOL) canBecomeKeyWindow
{
	return YES;
}

- (BOOL) windowShouldClose:(id)sender
{
	using namespace bme;
	Receiver::Event e;
	e.type = Receiver::Event::TYPE_QUIT;
	e.Push();
	return NO;
}

- (void) becomeKeyWindow
{
	_focus = YES;
	if ( _started ) {
		using namespace bme;
		Receiver::Event e;
		e.type = Receiver::Event::TYPE_FOCUS;
		e.focus.gain = [self isFocus];
		e.Push(true);
	}
	[super becomeKeyWindow];
}

- (void) resignKeyWindow
{
	_focus = NO;
	if ( _started ) {
		using namespace bme;
		Receiver::Event e;
		e.type = Receiver::Event::TYPE_FOCUS;
		e.focus.gain = [self isFocus];
		e.Push(true);
	}
	[super resignKeyWindow];
}

- (void)miniaturize:(id)sender
{
	[NSCursor unhide];
	[super miniaturize:sender];
}

// ----------------------------------------------------------------------------
// 
// Impl
// 
// ----------------------------------------------------------------------------

- (void) Start : (NSView*)theView theFullscreen : (BOOL)theFullscreen
{
	[self setAcceptsMouseMovedEvents : YES];
	[self setContentView             : theView];
	[self makeFirstResponder         : theView];
	[self makeKeyAndOrderFront       : nil];
	
	if ( theFullscreen ) {
		[self setLevel : NSMainMenuWindowLevel + 1];
	} else {
		[self setLevel : NSNormalWindowLevel];
		[self center];
	}
	
	_started = YES;
	[self setReleasedWhenClosed:YES];
	[self update];
}

- (void) Stop
{
	[self setDelegate:nil];
	_started = NO;
	[self close];
}

- (BOOL) isFocus
{
	return _focus;
}

@end

#endif // BME_SYSTEM_MACOSX
