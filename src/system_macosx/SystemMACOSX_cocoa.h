/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_macosx/SystemMACOSX_cocoa.h
 *****************************************************************************/

#include <bme/core/base.h>

#if BME_SYSTEM == BME_SYSTEM_MACOSX

#include <sys/time.h>
#import <Cocoa/Cocoa.h>

#include <bme/bme_main.h>
#include <bme/core/System.h>
#include <bme/core/Inputer.h>
#include <bme/core/Receiver.h>
#include <bme/core/EngineImpl.h>
#include <bme/core/RendererImpl.h>

// ----------------------------------------------------------------------------
// 
// BMECocoaView
// 
// ----------------------------------------------------------------------------

@interface BMECocoaView : NSOpenGLView {
@private
	NSTimer*          _timer;
	NSTimer*          _activityTimer;
	NSCursor*         _cursor;
	NSUInteger        _modifierFlags;
	NSTrackingRectTag _tracking;
}
- (void) Start;
- (void) Stop;
- (void) Tick:(NSTimer*)timer;
- (void) ActivityTick:(NSTimer*)timer;
- (void) SetCursor:(NSCursor*)cursor;
@end

// ----------------------------------------------------------------------------
// 
// BMECocoaWindow
// 
// ----------------------------------------------------------------------------

@interface BMECocoaWindow : NSWindow {
@private
	BOOL _started;
	BOOL _focus;
}
- (void) Start : (NSView*)theView theFullscreen : (BOOL)theFullscreen;
- (void) Stop;
- (BOOL) isFocus;
@end

// ----------------------------------------------------------------------------
// 
// BMECocoaDelegate
// 
// ----------------------------------------------------------------------------

@interface BMECocoaDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate> {
@private
	BMECocoaWindow* _window;
	BMECocoaView*   _view;
	BOOL            _active;
	NSString*       _title;
	NSCursor*       _cursor;
}

- (BMECocoaWindow*) window;
- (BMECocoaView*)   view;
- (BOOL)            isActive;
- (BOOL)            CreateWindow:(NSSize)size withFullscreen:(BOOL)fullscreen;
- (void)            DestroyWindow;
- (BOOL)            ToggleFullscreen:(BOOL)fullscreen;
- (void)            SetupMenuBar;
- (void)            SetTitle:(NSString*)title;
- (void)            SetCursor:(NSString*)filename withHotSpot:(NSPoint)hotSpot;
- (void)            SetIcon:(NSString*)filename;
- (void)            Show;
- (void)            Hide;
- (void)            Restore;
- (void)            Minimize;
@end

// ----------------------------------------------------------------------------
// 
// bme::cocoa_detail
// 
// ----------------------------------------------------------------------------

namespace bme { namespace cocoa_detail
{
	BMECocoaView*     GetView                ();
	BMECocoaWindow*   GetWindow              ();
	BMECocoaDelegate* GetDelegate            ();
	NSApplication*    GetApplication         ();
	
	void              DoMouseEvent           (NSView* view, NSEvent* event);
	void              DoKeyEvent             (NSEvent* event, bool down, bool flags);
	E_MOUSE           ConvertFromMouseButton (unsigned short value);
	E_KEY             ConvertFromKeyCode     (unsigned short value);
}} // namespace bme::cocoa_detail

#endif // BME_SYSTEM_MACOSX
