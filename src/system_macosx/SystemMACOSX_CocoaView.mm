/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_macosx/SystemMACOSX_CocoaView.mm
 *****************************************************************************/

#import "SystemMACOSX_cocoa.h"

#if BME_SYSTEM == BME_SYSTEM_MACOSX

@implementation BMECocoaView

// ----------------------------------------------------------------------------
// 
// Override
// 
// ----------------------------------------------------------------------------

- (BOOL) isOpaque              { return YES; }
- (BOOL) canBecomeKeyView      { return YES; }
- (BOOL) acceptsFirstResponder { return YES; }
- (BOOL) becomeFirstResponder  { return YES; }
- (BOOL) resignFirstResponder  { return YES; }

- (void) dealloc
{
	if ( _timer ) {
		[_timer invalidate];
	}
	if ( _activityTimer ) {
		[_activityTimer invalidate];
	}
	if ( _cursor ) {
		[_cursor release];
	}
	[super dealloc];
}

- (id) initWithFrame:(NSRect)frameRect
{
	NSOpenGLPixelFormatAttribute attr[] = {
		NSOpenGLPFADoubleBuffer,
		NSOpenGLPFAAccelerated,
		NSOpenGLPFAColorSize, 32,
		NSOpenGLPFADepthSize, 32,
		0
	};
	
	NSOpenGLPixelFormat* nsglFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:attr];
	if ( !nsglFormat ) {
		return nil;
	}
	
	self = [super initWithFrame:frameRect pixelFormat:nsglFormat];
	[nsglFormat release];
	return self;
}

- (void)prepareOpenGL
{
	if ( bme::theApplication() ) {
		GLint swapInt = bme::theApplication()->getDesc().isVsync ? 1 : 0;
		[[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
	}
	[super prepareOpenGL];
}

- (void)drawRect:(NSRect)dirtyRect
{
	[super drawRect:dirtyRect];
}

- (void)resetCursorRects
{
	[super resetCursorRects];
	[self addCursorRect : [self visibleRect]
				 cursor : _cursor];
}

- (void)mouseEntered:(NSEvent *)theEvent
{
	if ( bme::theSystem()->isCursorVisible() ) {
		[NSCursor unhide];
	} else {
		[NSCursor hide];
	}
}

- (void)mouseExited:(NSEvent *)theEvent
{
	if ( !bme::theSystem()->isCursorVisible() ) {
		[NSCursor unhide];
	}
}

// Left mouse

- (void) mouseDown:(NSEvent *)theEvent {
	bme::cocoa_detail::DoMouseEvent(self, theEvent);
}
- (void) mouseUp:(NSEvent *)theEvent {
	bme::cocoa_detail::DoMouseEvent(self, theEvent);
}
- (void) mouseDragged:(NSEvent *)theEvent {
	bme::cocoa_detail::DoMouseEvent(self, theEvent);
}
- (void) mouseMoved:(NSEvent *)theEvent {
	bme::cocoa_detail::DoMouseEvent(self, theEvent);
}

// Right mouse

- (void) rightMouseDown:(NSEvent *)theEvent {
	bme::cocoa_detail::DoMouseEvent(self, theEvent);
}
- (void) rightMouseUp:(NSEvent *)theEvent {
	bme::cocoa_detail::DoMouseEvent(self, theEvent);
}
- (void) rightMouseDragged:(NSEvent *)theEvent {
	bme::cocoa_detail::DoMouseEvent(self, theEvent);
}
- (void) rightMouseMoved:(NSEvent *)theEvent {
	bme::cocoa_detail::DoMouseEvent(self, theEvent);
}

// Other mouse

- (void) otherMouseDown:(NSEvent *)theEvent {
	bme::cocoa_detail::DoMouseEvent(self, theEvent);
}
- (void) otherMouseUp:(NSEvent *)theEvent {
	bme::cocoa_detail::DoMouseEvent(self, theEvent);
}
- (void) otherMouseDragged:(NSEvent *)theEvent {
	bme::cocoa_detail::DoMouseEvent(self, theEvent);
}
- (void) otherMouseMoved:(NSEvent *)theEvent {
	bme::cocoa_detail::DoMouseEvent(self, theEvent);
}

// Scroll

- (void) scrollWheel:(NSEvent *)theEvent {
	bme::cocoa_detail::DoMouseEvent(self, theEvent);
}

// Keys

- (void) flagsChanged:(NSEvent *)theEvent
{
	NSUInteger flags = [theEvent modifierFlags] | NSDeviceIndependentModifierFlagsMask;
	if ( flags > _modifierFlags ) {
		bme::cocoa_detail::DoKeyEvent(theEvent, true, true);
	} else {
		bme::cocoa_detail::DoKeyEvent(theEvent, false, true);
	}
	_modifierFlags = flags;
}

- (void) keyDown:(NSEvent *)theEvent {
	bme::cocoa_detail::DoKeyEvent(theEvent, true, false);
}
- (void) keyUp:(NSEvent *)theEvent {
	bme::cocoa_detail::DoKeyEvent(theEvent, false, false);
}

// ----------------------------------------------------------------------------
// 
// Impl
// 
// ----------------------------------------------------------------------------

- (void) Start
{
	if ( !_timer ) {
		_timer = [NSTimer scheduledTimerWithTimeInterval : 1.f/1000.f
												  target : self
												selector : @selector(Tick:)
												userInfo : nil
												 repeats : YES];
		[[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSEventTrackingRunLoopMode];
		[[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSModalPanelRunLoopMode];
	}
	
	if ( !_activityTimer ) {
		if ( bme::theApplication() && !bme::theApplication()->getDesc().isAllowScreensaver ) {
			_activityTimer = [NSTimer scheduledTimerWithTimeInterval : 30.f
															  target : self
															selector : @selector(ActivityTick:)
															userInfo : nil
															 repeats : YES];
			[[NSRunLoop currentRunLoop] addTimer:_activityTimer forMode:NSEventTrackingRunLoopMode];
			[[NSRunLoop currentRunLoop] addTimer:_activityTimer forMode:NSModalPanelRunLoopMode];
		}
	}
	
	if ( !_tracking ) {
		_tracking = [self addTrackingRect : [self visibleRect]
									owner : self
								 userData : nil
							 assumeInside : NO];
	}
}

- (void) Stop
{
	if ( _timer ) {
		[_timer invalidate];
		_timer = nil;
	}
	
	if ( _activityTimer ) {
		[_activityTimer invalidate];
		_activityTimer = nil;
	}
	
	if ( _tracking ) {
		[self removeTrackingRect:_tracking];
		_tracking = 0;
	}
}

- (void) Tick:(NSTimer *)timer
{
	if ( bme::theApplication() ) {
		if ( !bme::theApplication()->Step() ) {
			[bme::cocoa_detail::GetDelegate() DestroyWindow];
			return;
		}
	}
}

- (void) ActivityTick:(NSTimer *)timer
{
	UpdateSystemActivity(OverallAct);
}

- (void) SetCursor:(NSCursor*)cursor
{
	if ( cursor ) {
		[cursor retain];
	}
	
	if ( _cursor ) {
		[_cursor release];
	}
	
	_cursor = cursor;
	
	NSWindow* window = [self window];
	if ( window ) {
		[window invalidateCursorRectsForView:self];
	}
}

@end

#endif // BME_SYSTEM_MACOSX
