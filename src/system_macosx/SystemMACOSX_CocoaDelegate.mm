/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_macosx/SystemMACOSX_CocoaDelegate.mm
 *****************************************************************************/

#import "SystemMACOSX_cocoa.h"

#if BME_SYSTEM == BME_SYSTEM_MACOSX

@implementation BMECocoaDelegate

// ----------------------------------------------------------------------------
// 
// Override
// 
// ----------------------------------------------------------------------------

- (void)dealloc
{	
	if ( _title ) {
		[_title release];
	}
	[super dealloc];
}

- (void) applicationDidFinishLaunching:(NSNotification *)notification
{
	// Fix working directory
	NSString* filename = [[NSBundle mainBundle] resourcePath];
	[[NSFileManager defaultManager] changeCurrentDirectoryPath:filename];
	
	BMEMain();
}

- (BOOL) applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
	return YES;
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
	BMEShutdown();
	return NSTerminateNow;
}

- (void)applicationDidBecomeActive:(NSNotification *)notification
{	
	_active = YES;
	
	using namespace bme;
	Receiver::Event e;
	e.type = Receiver::Event::TYPE_ACTIVE;
	e.active.gain = [self isActive];
	e.Push(true);
	
	[self Restore];
}

- (void)applicationDidResignActive:(NSNotification *)notification
{
	_active = NO;
	
	using namespace bme;
	Receiver::Event e;
	e.type = Receiver::Event::TYPE_ACTIVE;
	e.active.gain = [self isActive];
	e.Push(true);
}

// ----------------------------------------------------------------------------
// 
// Impl
// 
// ----------------------------------------------------------------------------

- (BMECocoaWindow*) window
{
	return _window;
}

- (BMECocoaView*) view
{
	return _view;
}

- (BOOL) isActive
{
	return _active;
}

- (BOOL) CreateWindow:(NSSize)size withFullscreen:(BOOL)fullscreen
{
	NSRect     wRect    = NSMakeRect(0, 0, size.width, size.height);
	NSUInteger wBacking = NSBackingStoreBuffered;
	NSUInteger wStyle   = fullscreen ? NSBorderlessWindowMask : NSTitledWindowMask | NSClosableWindowMask | NSMiniaturizableWindowMask;
	
	if ( _window && _view ) {
		// double call.
		// we needed recreate window, but not recreate view
		
		[_view Stop];
		
		[_window Stop];
		_window = nil;
	}
	
	if ( !_window ) {
		_window = [[BMECocoaWindow alloc] initWithContentRect:wRect styleMask:wStyle backing:wBacking defer:NO];
	}
	
	if ( !_view ) {
		_view = [[BMECocoaView alloc] initWithFrame:NSMakeRect(0, 0, wRect.size.width, wRect.size.height)];
	}
	
	if ( _window && _view ) {
		[_window Start:_view theFullscreen:fullscreen];
		[_view   Start];
		
		[_window setTitle:_title ? _title : @""];
		[_view   SetCursor:_cursor];
		
		[self SetupMenuBar];
		return YES;
	}
	
	[self DestroyWindow];
	return NO;
}

- (void) DestroyWindow
{
	if ( _view ) {
		[_view Stop];
		[_view release];
		_view = nil;
	}
	
	if ( _window ) {
		[_window Stop];
		_window = nil;
	}
}

- (BOOL) ToggleFullscreen:(BOOL)fullscreen
{
	using namespace bme;
	if ( !theSystem() ) {
		return NO;
	}
	
	NSSize size = NSMakeSize(
		theSystem()->getWindowResolution().x,
		theSystem()->getWindowResolution().y);
	return [self CreateWindow:size withFullscreen:fullscreen];
}

- (void) SetupMenuBar
{
	// ---------------------------------
	
	NSString* app_name = nil;
	const NSDictionary* bundle_dictionary = (const NSDictionary*)CFBundleGetInfoDictionary(CFBundleGetMainBundle());
	if ( bundle_dictionary ) {
		app_name = [bundle_dictionary objectForKey:@"CFBundleName"];
	}
	
	if ( !app_name || ![app_name length] ) {
		app_name = [[NSProcessInfo processInfo] processName];
	}
	
	//if ( bme::theApplication && !bme::theApplication()->getDesc().gameName.empty() )
	//	app_name = [NSString stringWithUTF8String:bme::theApplication()->getDesc().gameName.c_str()];
	
	// ---------------------------------
	
	NSMenu* menu = [[NSMenu alloc] init];
	
	// ---------------------------------
	
	NSMenuItem* app_menu_item = [menu addItemWithTitle:@"" action:nil keyEquivalent:@""];
	NSMenu*     app_menu      = [[NSMenu alloc] init];
	[app_menu_item setSubmenu:app_menu];
	
	[[app_menu addItemWithTitle:[NSString stringWithFormat:@"About %@", app_name]
						 action:@selector(orderFrontStandardAboutPanel:)
				  keyEquivalent:@""] setTarget:NSApp];
	
	[app_menu addItem:[NSMenuItem separatorItem]];
	
	[[app_menu addItemWithTitle:[NSString stringWithFormat:@"Hide %@", app_name]
						 action:@selector(hide:)
				  keyEquivalent:@"h"] setTarget:NSApp];
	
	NSMenuItem* hide_other_item =
	[app_menu addItemWithTitle:@"Hide Others"
						action:@selector(hideOtherApplications:)
				 keyEquivalent:@"h"];
	[hide_other_item setTarget:NSApp];
	[hide_other_item setKeyEquivalentModifierMask:NSAlternateKeyMask | NSCommandKeyMask];
	
	[[app_menu addItemWithTitle:@"Show All"
						 action:@selector(unhideAllApplications:)
				  keyEquivalent:@""] setTarget:NSApp];
	
	[app_menu addItem:[NSMenuItem separatorItem]];
	
	[[app_menu addItemWithTitle:[NSString stringWithFormat:@"Quit %@", app_name]
						 action:@selector(performClose:)
				  keyEquivalent:@"q"] setTarget:_window];
	
	// --------------------------------------------
	
	NSMenuItem* window_menu_item = [menu addItemWithTitle:@"" action:NULL keyEquivalent:@""];
	NSMenu*     window_menu      = [[NSMenu alloc] initWithTitle:@"Window"];
	[window_menu_item setSubmenu:window_menu];
	
	[[window_menu addItemWithTitle:@"Miniaturize"
							action:@selector(performMiniaturize:)
					 keyEquivalent:@"m"] setTarget:_window];
	
	[[window_menu addItemWithTitle:@"Zoom"
							action:@selector(performZoom:)
					 keyEquivalent:@""] setTarget:_window];
	
	[window_menu addItem:[NSMenuItem separatorItem]];
	
	[[window_menu addItemWithTitle:@"Bring All to Front"
							action:@selector(arrangeInFront:)
					 keyEquivalent:@""] setTarget:NSApp];
	
	// --------------------------------------------
	
	[NSApp setMainMenu:menu];
	[NSApp setWindowsMenu:window_menu];
	
	[window_menu release];
	[app_menu release];
	[menu release];
}

- (void) SetTitle:(NSString*)title
{
	if ( title ) {
		[title retain];
	}
	
	if ( _title ) {
		[_title release];
	}
	
	_title = title;
	
	if ( _window ) {
		[_window setTitle:_title ? _title : @""];
	}
}

- (void) SetCursor:(NSString*)filename withHotSpot:(NSPoint)hotSpot
{	
	if ( _cursor ) {
		[_cursor release];
		_cursor = nil;
	}
	
	NSImage* image = [[NSImage alloc] initWithContentsOfFile:filename];
	if ( image ) {
		_cursor = [[NSCursor alloc] initWithImage:image hotSpot:hotSpot];
		[image release];
	}
	
	if ( _view ) {
		[_view SetCursor:_cursor];
	}
}

- (void) SetIcon:(NSString*)filename
{
	NSImage* image = [[NSImage alloc] initWithContentsOfFile:filename];
	
	[NSApp setApplicationIconImage:image];
	
	if ( image ) {
		[image release];
	}
}

- (void) Show
{
	if ( [NSApp isHidden] ) {
		[NSApp unhide:self];
	}
}

- (void) Hide
{
	if ( ![NSApp isHidden] ) {
		[NSApp hide:self];
	}
}

- (void) Restore
{
	if ( _window && [_window isMiniaturized] ) {
		[_window deminiaturize:self];
	}
}

- (void) Minimize
{
	if ( _window && ![_window isMiniaturized] ) {
		[_window miniaturize:self];
	}
}

@end

#endif // BME_SYSTEM_MACOSX
