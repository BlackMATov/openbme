/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_macosx/SystemMACOSX.h
 *****************************************************************************/

#pragma once

#include <bme/core/SystemImpl.h>

#if BME_SYSTEM == BME_SYSTEM_MACOSX

namespace bme
{

class SystemMACOSX
	: public SystemImpl
	, public Holder<SystemMACOSX>
{
	timeval       _startTimer;
	bool          _cursorVisible;
	
	bool          _initTimers();
	bool          _initWindow();
	void          _deinitWindow();
	
public:
	
	SystemMACOSX();
	virtual ~SystemMACOSX();
	
	bool          Init                  ();
	void          DeInit                ();
	
	// System override --------------------------------------------------------
	void          setWindowCaption      ( const Str&   value );
	void          setWindowIcon         ( const Path&  value );
	
	void          setCursor             ( const Path&  value, const pnt2u& hotSpot );
	void          setCursorPosition     ( const pnt2u& value );
	void          setCursorVisible      ( bool yesno );
	bool          isCursorVisible       () const;
	
	void          ShowWindow            ();
	void          HideWindow            ();
	void          RestoreWindow         ();
	void          MinimizeWindow        ();
	
	bool          isWindowVisible       () const;
	bool          isWindowFocused       () const;
	bool          isWindowActive        () const;
	bool          isWindowMinimized     () const;
	
	void          ShowConsole           () const;
	void          HideConsole           () const;
	
	bool          ShowSoftKeyboard      () const;
	void          HideSoftKeyboard      () const;
	bool          isSoftKeyboardVisible () const;
	E_ORIENTATION getDeviceOrientation  () const;
	// SystemImpl override ----------------------------------------------------
	void          Update                ();
	void          EndFrame              ();
	bool          ToggleFullscreen      ( bool yesno );
	bool          ToggleWidescreen      ( bool yesno );
	void          NativeLog             ( const char* text, Logger::E_LEVEL level ) const;
	size_t        getTicks              () const;
	
	Inputer::Desc getInputerDesc        () const;
	// ------------------------------------------------------------------------
};

} // namespace bme

#endif // BME_SYSTEM_MACOSX
