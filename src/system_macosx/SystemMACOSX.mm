/******************************************************************************
 * This file is part of the "BMEngine"
 * For conditions of distribution and use, see copyright notice in bme.h
 * Copyright (C) 2010-2014 Matvey Cherevko
 * Filename: src/system_macosx/SystemMACOSX.mm
 *****************************************************************************/

#include "SystemMACOSX.h"

#if BME_SYSTEM == BME_SYSTEM_MACOSX

#include "SystemMACOSX_cocoa.h"

namespace bme
{

// ----------------------------------------------------------------------------
// 
// SystemImpl
// 
// ----------------------------------------------------------------------------

bool SystemImpl::Initialize()
{
	SystemMACOSX* self = new SystemMACOSX();
	return self->Init();
}

void SystemImpl::Shutdown()
{
	SystemMACOSX* self = Holder<SystemMACOSX>::getHostage();
	if ( self ) {
		self->DeInit();
		delete self;
	}
}

// ----------------------------------------------------------------------------
// 
// SystemMACOSX
// 
// ----------------------------------------------------------------------------

SystemMACOSX::SystemMACOSX()
: _cursorVisible(true)
{
}

SystemMACOSX::~SystemMACOSX()
{
}

bool SystemMACOSX::_initTimers()
{
	::gettimeofday(&_startTimer, NULL);
	return true;
}

bool SystemMACOSX::_initWindow()
{
	Application* app = theApplication();
	if ( !app ) {
		BME_LOG_ERROR("SystemMACOSX::_initWindow() application not found");
		return false;
	}
	
	// -----------------------------------
	// calculate screen resolutions
	// -----------------------------------
	
	NSRect desktop_frame = [[[NSScreen screens] objectAtIndex:0] frame];
	CalculateResolutions(
		pnt2u(desktop_frame.size.width, desktop_frame.size.height));
	
	// -----------------------------------
	// create window
	// -----------------------------------
	
	BMECocoaDelegate* delegate = cocoa_detail::GetDelegate();
	return YES == [delegate CreateWindow : NSMakeSize(getWindowResolution().x, getWindowResolution().y)
						  withFullscreen : app->getDesc().isFullscreen ? YES : NO];
}

void SystemMACOSX::_deinitWindow()
{
	BMECocoaDelegate* delegate = cocoa_detail::GetDelegate();
	[delegate DestroyWindow];
}

bool SystemMACOSX::Init()
{
	bool success = (_initWindow() && _initTimers());
	if ( !success ) {
		BME_LOG_ERROR("SystemMACOSX::Init() Window init failed!");
		return false;
	}
	
	if ( !RendererImpl::Initialize() ) {
		BME_LOG_ERROR("SystemMACOSX::Init() Renderer init failed!");
		return false;
	}
	
	return true;
}

void SystemMACOSX::DeInit()
{
	RendererImpl::Shutdown();
	_deinitWindow();
}

// ----------------------------------------------------------------------------
// 
// System override
// 
// ----------------------------------------------------------------------------

void SystemMACOSX::setWindowCaption(const Str& value)
{
	BMECocoaDelegate* delegate = cocoa_detail::GetDelegate();
	if ( delegate ) {
		NSString* title = [[NSString alloc] initWithUTF8String:value.c_str()];
		[delegate SetTitle:title];
		[title release];
	}
}

void SystemMACOSX::setWindowIcon(const Path& value)
{
	BMECocoaDelegate* delegate = cocoa_detail::GetDelegate();
	if ( delegate ) {
		NSString* filename = [NSString stringWithUTF8String:value.getNativePath().c_str()];
		[delegate SetIcon:filename];
	}
}

void SystemMACOSX::setCursor(const Path& value, const pnt2u& hotSpot)
{
	BMECocoaDelegate* delegate = cocoa_detail::GetDelegate();
	if ( delegate ) {
		NSString* filename = [NSString stringWithUTF8String:value.getNativePath().c_str()];
		NSPoint   hotpoint = NSMakePoint(hotSpot.x, hotSpot.y);
		[delegate SetCursor:filename withHotSpot:hotpoint];
	}
}

void SystemMACOSX::setCursorPosition(const pnt2u& value)
{
	BMECocoaWindow* window = cocoa_detail::GetWindow();
	BMECocoaView*   view   = cocoa_detail::GetView();
	if ( window && view ) {
		vec2f cp = PointToScreen(vec2f(value));
		
		CGRect screen_rect = CGDisplayBounds(CGMainDisplayID());
		NSPoint p = [window convertBaseToScreen:NSMakePoint(0 ,0)];
		p.y = screen_rect.size.height - p.y - [view frame].size.height;
		
		CGWarpMouseCursorPosition(CGPointMake(
			cp.x + p.x - screen_rect.origin.x,
			cp.y + p.y - screen_rect.origin.y));
		
		Receiver::Event e;
		e.type        = Receiver::Event::TYPE_MOUSE_MOVE;
		e.mouse.key   = MOUSE_UNKNOWN;
		e.mouse.x     = cp.x;
		e.mouse.y     = cp.y;
		e.mouse.which = 0;
		e.Push();
	}
}

void SystemMACOSX::setCursorVisible(bool yesno)
{
	if ( _cursorVisible != yesno ) {
		_cursorVisible = yesno;
		if ( yesno ) {
			[NSCursor unhide];
		} else {
			[NSCursor hide];
		}
	}
}

bool SystemMACOSX::isCursorVisible() const
{
	return _cursorVisible;
}

void SystemMACOSX::ShowWindow()
{
	BMECocoaDelegate* delegate = cocoa_detail::GetDelegate();
	if ( delegate ) {
		[delegate Show];
	}
}

void SystemMACOSX::HideWindow()
{
	BMECocoaDelegate* delegate = cocoa_detail::GetDelegate();
	if ( delegate ) {
		[delegate Hide];
	}
}

void SystemMACOSX::RestoreWindow()
{
	BMECocoaDelegate* delegate = cocoa_detail::GetDelegate();
	if ( delegate ) {
		[delegate Restore];
	}
}

void SystemMACOSX::MinimizeWindow()
{
	BMECocoaDelegate* delegate = cocoa_detail::GetDelegate();
	if ( delegate ) {
		[delegate Minimize];
	}
}

bool SystemMACOSX::isWindowVisible() const
{
	BMECocoaWindow* window = cocoa_detail::GetWindow();
	return (window && YES == [window isVisible]);
}

bool SystemMACOSX::isWindowFocused() const
{
	BMECocoaWindow* window = cocoa_detail::GetWindow();
	return (window && YES == [window isFocus]);
}

bool SystemMACOSX::isWindowActive() const
{
	BMECocoaDelegate* delegate = cocoa_detail::GetDelegate();
	return (delegate && YES == [delegate isActive]);
}

bool SystemMACOSX::isWindowMinimized() const
{
	BMECocoaWindow* window = cocoa_detail::GetWindow();
	return (window && YES == [window isMiniaturized]);
}

void SystemMACOSX::ShowConsole() const
{
	/// \todo notimpl
}

void SystemMACOSX::HideConsole() const
{
	/// \todo notimpl
}

bool SystemMACOSX::ShowSoftKeyboard() const
{
	return false;
}

void SystemMACOSX::HideSoftKeyboard() const
{
}

bool SystemMACOSX::isSoftKeyboardVisible() const
{
	return false;
}

System::E_ORIENTATION SystemMACOSX::getDeviceOrientation() const
{
	if ( getDesktopResolution().x >= getDesktopResolution().y ) {
		return ORIENTATION_LANDSCAPE;
	} else {
		return ORIENTATION_PORTRAIT;
	}
}

// ----------------------------------------------------------------------------
// 
// SystemImpl override
// 
// ----------------------------------------------------------------------------

void SystemMACOSX::Update()
{
}

void SystemMACOSX::EndFrame()
{
	BMECocoaView* view = cocoa_detail::GetView();
	if ( view ) {
		[[view openGLContext] flushBuffer];
	}
	
	RendererImpl* renderer = Holder<RendererImpl>::getHostage();
	if ( renderer ) {
		renderer->EndFrame();
	}
}

bool SystemMACOSX::ToggleFullscreen(bool yesno)
{
	NSRect desktop_frame = [[[NSScreen screens] objectAtIndex:0] frame];
	CalculateResolutions(
		pnt2u(desktop_frame.size.width, desktop_frame.size.height));
	
	BMECocoaDelegate* delegate = cocoa_detail::GetDelegate();
	if ( !delegate || ![delegate ToggleFullscreen:yesno ? YES : NO] ) {
		return false;
	}
		
	RendererImpl* renderer = Holder<RendererImpl>::getHostage();
	if ( renderer ) {
		renderer->Resize();
	}
	
	return true;
}

bool SystemMACOSX::ToggleWidescreen(bool yesno)
{
	return true;
}

void SystemMACOSX::NativeLog(const char* text, Logger::E_LEVEL /*level*/) const
{
	if ( !text || !text[0] ) {
		return;
	}
	::printf("%s", text);
}

size_t SystemMACOSX::getTicks() const
{
	timeval now;
	gettimeofday(&now, NULL);
	size_t ticks =
		(now.tv_sec  - _startTimer.tv_sec ) * 1000 +
		(now.tv_usec - _startTimer.tv_usec) / 1000;
	return ticks;
}

Inputer::Desc SystemMACOSX::getInputerDesc() const
{
	Inputer::Desc desc;
	desc.mouses    = 1;
	desc.keyboards = 1;
	desc.joysticks = 0;
	return desc;
}

} // namespace bme

#endif // BME_SYSTEM_MACOSX
